  //
  //  AppDelegate.swift
  //  Health4Life
  
  
  import UIKit
  import CoreData
  import SlideMenuControllerSwift
  import IQKeyboardManagerSwift
  import GooglePlaces
  import GoogleMaps
  import TSMessages
  import Fabric
  import Crashlytics
  import RealmSwift
  import AudioToolbox
  
  @UIApplicationMain
  class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInDelegate{
    var window: UIWindow?
    var notificationFlag = Bool()
    var openMsgFlag = Bool()
    var pushCountFlag = Bool()
    var player: AVAudioPlayer?
    var serverInt = NSInteger()
    //MARK :- Realm Migration Functions
    
    func testRealmDB(){
        do {
            try Realm().objects(User.self)
            try Realm().objects(Group.self)
        } catch {
            print("can't access realm, migration needed")
            deleteRealmFile()
        }
    }
    func deleteRealmFile(){
        if let path = Realm.Configuration.defaultConfiguration.fileURL{
            do{
                try NSFileManager.defaultManager().removeItemAtURL(path)
                print("realm file deleted")
            } catch {
                print("no realm file to delete")
            }
        }
    }
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        self.testRealmDB()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        //Fabric.with([Crashlytics.self])
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        GMSServices.provideAPIKey("AIzaSyBxo-6T_iMrlnq22I-cgHJe5H38obDIRrg")
        GMSPlacesClient.provideAPIKey("AIzaSyBxo-6T_iMrlnq22I-cgHJe5H38obDIRrg")
        GIDSignIn.sharedInstance().delegate = self
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        //NSThread.sleepForTimeInterval(1.0)
        IQKeyboardManager.sharedManager().enable = true
               registerForPushNotifications(application)
        self.window?.backgroundColor = UIColor.whiteColor()
        
        let settings = UIUserNotificationSettings(forTypes: [.Badge, .Sound, .Alert], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        print("\(defaults)");
        
        if let usertype = defaults.valueForKey("usertype") {
            if(usertype as! NSObject == 0){
                
                var storyboard = UIStoryboard()
                switch UIDevice.currentDevice().userInterfaceIdiom {
                case .Phone:
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                case .Pad:
                    storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                default:
                    break
                }
                
                let searchDoc = defaults.boolForKey("searchDoc")
                if(searchDoc == true){
                    let selectPharma = defaults.boolForKey("selectPharma")
                    if(selectPharma == true){
                       
                        let vc = storyboard.instantiateViewControllerWithIdentifier("PatientHomeViewController") as! PatientHomeViewController
                        let nav = UINavigationController(rootViewController: vc)
                        self.window?.rootViewController = nav
                        self.window?.makeKeyAndVisible()
                        
                    }else{
                        let vc = storyboard.instantiateViewControllerWithIdentifier("SelectPharmacyViewController") as! SelectPharmacyViewController
                        vc.nextBool = true
                        let nav = UINavigationController(rootViewController: vc)
                        self.window?.rootViewController = nav
                        self.window?.makeKeyAndVisible()
                    }
                }else{
                    
                    let vc = storyboard.instantiateViewControllerWithIdentifier("SearchDoctorViewController") as! SearchDoctorViewController
                    vc.searchDoc   = true
                    vc.firstSearch = true
                    let nav = UINavigationController(rootViewController: vc)
                    self.window?.rootViewController = nav
                    self.window?.makeKeyAndVisible()
                }
            }else if(usertype as! NSObject == 1){
                var storyboard = UIStoryboard()
                let drType = defaults.valueForKey("drType") as! String
                switch UIDevice.currentDevice().userInterfaceIdiom {
                case .Phone:
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorHomeViewController") as! DoctorHomeViewController
                    if(drType == "MedicinePrescribers"){
                        vc.tableVwRow = 4
                    }else{
                        vc.tableVwRow = 3
                    }
                    let nav = UINavigationController(rootViewController: vc)
                    self.window?.rootViewController = nav
                    self.window?.makeKeyAndVisible()
                case .Pad:
                    storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorHomeViewController") as! DoctorHomeViewController
                    if(drType == "MedicinePrescribers"){
                        vc.tableVwRow = 4
                    }else{
                        vc.tableVwRow = 3
                    }
                    let nav = UINavigationController(rootViewController: vc)
                    self.window?.rootViewController = nav
                    self.window?.makeKeyAndVisible()
                default:
                    break
                }
            }else if(usertype as! NSObject == 3){
                var storyboard = UIStoryboard()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                }else{
                    storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                }
                let mainViewController = storyboard.instantiateViewControllerWithIdentifier("StaffHomeViewController") as! StaffHomeViewController
                let leftViewController = storyboard.instantiateViewControllerWithIdentifier("StaffSelectDoctorViewController") as! StaffSelectDoctorViewController
                let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                leftViewController.mainViewController = nvc
                let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController:leftViewController)
                slideMenuController.automaticallyAdjustsScrollViewInsets = true
                slideMenuController.delegate = mainViewController
                let nav = UINavigationController(rootViewController: slideMenuController)
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
            }
            
        }
        
        if launchOptions != nil {
            if let info = (launchOptions![UIApplicationLaunchOptionsRemoteNotificationKey] as? NSDictionary)
            {
                notificationFlag = true
                var storyboard = UIStoryboard()
                switch UIDevice.currentDevice().userInterfaceIdiom {
                case .Phone:
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                case .Pad:
                    storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                default:
                    break
                }
                let status = info.valueForKey("Status") as! String
                if(status == "video"){
                    NSNotificationCenter.defaultCenter().postNotificationName( "timerInvalidate",  object: nil)
                    let drdetail = info.valueForKey("Dr") as! NSDictionary
                    let schData = info.valueForKey("drschedulesetsId") as! NSString
                    let bookD = info.valueForKey("BookingId") as! NSString
                    self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
                    let vc = storyboard.instantiateViewControllerWithIdentifier("ConferenceVideoViewController") as! ConferenceVideoViewController
                    let nav = UINavigationController(rootViewController: vc)
                    vc.drDetail = drdetail
                    vc.bookId = bookD
                    vc.ptSchId = schData
                    vc.kToken = info.valueForKey("TokenData") as! NSString
                    vc.kSessionId = info.valueForKey("sessionData") as! NSString
                    vc.kApiKey = info.valueForKey("apiKey") as! NSString
                    vc.groupId = info.valueForKey("GroupId") as! String
                    self.window?.rootViewController = nav
                    self.window?.makeKeyAndVisible()
                }
                else if(status == "Dr notification status"){
                    self.playSound()
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(7.0 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        AppManager.sharedManager.hideActivityIndicatorInView(self.window!)
                        let vc = storyboard.instantiateViewControllerWithIdentifier("DocNotificationViewController") as! DocNotificationViewController
                        vc.docNotiBool = true
                        self.window?.rootViewController = vc
                        self.window?.makeKeyAndVisible()
                    }
                }else if(status == "chat"){
                    openMsgFlag = true
                    self.playSound()
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
                    
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        UIApplication.sharedApplication().applicationIconBadgeNumber = 1
                    }
                }
                else if(status == "Staff notification status"){
                    self.playSound()
                    let defaults = NSUserDefaults.standardUserDefaults()
                    if let name = info.valueForKeyPath("Drname"){
                        print(name)
                        defaults.setObject(info.valueForKeyPath("Drid.ProfilePic"), forKey: "selectDrImg")
                        defaults.setObject(info.valueForKeyPath("Drname"), forKey: "selectDrName")
                        defaults.setObject(info.valueForKeyPath("Drid.Speciality"), forKey: "selectDrSpeciality")
                        defaults.setObject(info.valueForKeyPath("Drid._id"), forKey: "id")
                        defaults.setValue(0, forKey: "indexPath")
                        var storyboard = UIStoryboard()
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            storyboard = UIStoryboard(name: "Main", bundle: nil)
                        }else{
                            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                        }
                        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("StaffHomeViewController") as! StaffHomeViewController
                        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("StaffSelectDoctorViewController") as! StaffSelectDoctorViewController
                        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                        leftViewController.mainViewController = nvc
                        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController:leftViewController)
                        slideMenuController.automaticallyAdjustsScrollViewInsets = true
                        slideMenuController.delegate = mainViewController
                        let nav = UINavigationController(rootViewController: slideMenuController)
                        self.window?.rootViewController = nav
                        self.window?.makeKeyAndVisible()
                    }
                    else{
                        NSNotificationCenter.defaultCenter().postNotificationName("Staff notification status", object: nil, userInfo: nil)
                    }
                    
                }
                else{
                    self.playSound()
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(7.0 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        AppManager.sharedManager.hideActivityIndicatorInView(self.window!)
                        let vc = storyboard.instantiateViewControllerWithIdentifier("PatientNotificationViewController") as! PatientNotificationViewController
                        vc.notiBool = true
                        self.window?.rootViewController = vc
                        self.window?.makeKeyAndVisible()
                    }
                }
            }
            else{
                let defaults = NSUserDefaults.standardUserDefaults()
                print("\(defaults)");
                if let usertype = defaults.valueForKey("usertype") {
                    if(usertype as! NSObject == 0){
                        var storyboard = UIStoryboard()
                        switch UIDevice.currentDevice().userInterfaceIdiom {
                        case .Phone:
                            storyboard = UIStoryboard(name: "Main", bundle: nil)
                        case .Pad:
                            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                        default:
                            break
                        }
                        let searchDoc = defaults.boolForKey("searchDoc")
                        if(searchDoc == true){
                            let selectPharma = defaults.boolForKey("selectPharma")
                            if(selectPharma == true){
                                self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
                                let vc = storyboard.instantiateViewControllerWithIdentifier("PatientHomeViewController") as! PatientHomeViewController
                                let nav = UINavigationController(rootViewController: vc)
                                self.window?.rootViewController = nav
                                self.window?.makeKeyAndVisible()
                                
                            }else{
                                self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
                                let vc = storyboard.instantiateViewControllerWithIdentifier("SelectPharmacyViewController") as! SelectPharmacyViewController
                                vc.nextBool = true
                                let nav = UINavigationController(rootViewController: vc)
                                self.window?.rootViewController = nav
                                self.window?.makeKeyAndVisible()
                            }
                        }
                        else{
                            self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
                            let vc = storyboard.instantiateViewControllerWithIdentifier("SearchDoctorViewController") as! SearchDoctorViewController
                            vc.searchDoc   = true
                            vc.firstSearch = true
                            let nav = UINavigationController(rootViewController: vc)
                            self.window?.rootViewController = nav
                            self.window?.makeKeyAndVisible()
                        }
                    }else if(usertype as! NSObject == 1){
                        var storyboard = UIStoryboard()
                        let drType = defaults.valueForKey("drType") as! String
                        switch UIDevice.currentDevice().userInterfaceIdiom {
                        case .Phone:
                            storyboard = UIStoryboard(name: "Main", bundle: nil)
                            self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
                            let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorHomeViewController") as! DoctorHomeViewController
                            if(drType == "MedicinePrescribers"){
                                vc.tableVwRow = 4
                            }else{
                                vc.tableVwRow = 3
                            }
                            let nav = UINavigationController(rootViewController: vc)
                            self.window?.rootViewController = nav
                            self.window?.makeKeyAndVisible()
                        case .Pad:
                            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                            self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
                            let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorHomeViewController") as! DoctorHomeViewController
                            if(drType == "MedicinePrescribers"){
                                vc.tableVwRow = 4
                            }else{
                                vc.tableVwRow = 3
                            }
                            let nav = UINavigationController(rootViewController: vc)
                            self.window?.rootViewController = nav
                            self.window?.makeKeyAndVisible()
                        default:
                            break
                        }
                    }
                    else if(usertype as! NSObject == 3){
                        var storyboard = UIStoryboard()
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            storyboard = UIStoryboard(name: "Main", bundle: nil)
                        }else{
                            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                        }
                        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("StaffHomeViewController") as! StaffHomeViewController
                        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("StaffSelectDoctorViewController") as! StaffSelectDoctorViewController
                        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                        leftViewController.mainViewController = nvc
                        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController:leftViewController)
                        slideMenuController.automaticallyAdjustsScrollViewInsets = true
                        slideMenuController.delegate = mainViewController
                        let nav = UINavigationController(rootViewController: slideMenuController)
                        self.window?.rootViewController = nav
                        self.window?.makeKeyAndVisible()
                    }
                }
            }
        }
       
        return true
    }

    func application(application: UIApplication,
                     openURL url: NSURL, options: [String: AnyObject]) -> Bool {
        print(url)
        if #available(iOS 9.0, *) {
            
            
            if(url.absoluteString?.containsString("healthlife") == true){
                let destUrl = url.absoluteString?.stringByReplacingOccurrencesOfString("healthlife", withString: "http")
                AppManager.sharedManager.showActivityIndicatorInView(self.window!, withLabel: "")
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5.0 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    AppManager.sharedManager.hideActivityIndicatorInView(self.window!)
                    self.navToVideoLink(destUrl!)
                }
                
                return true
            }
            else
            {
                return GIDSignIn.sharedInstance().handleURL(url,sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as? String,annotation: options[UIApplicationOpenURLOptionsAnnotationKey]) || FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as? String, annotation: options[UIApplicationOpenURLOptionsAnnotationKey])
            }
            
            
        } else {
            return false
            // Fallback on earlier versions
        }
        
    }
    func navToVideoLink(destUrl:String){
        var storyboard = UIStoryboard()
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        }else{
            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
        }
        if let navC = self.window?.rootViewController as? UINavigationController{
            let vc = storyboard.instantiateViewControllerWithIdentifier("VideoCallLinkViewController") as! VideoCallLinkViewController
            vc.autoFill = "\(destUrl)"
            vc.exit = true
            let vw = navC.topViewController! as UIViewController
            vw.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func application(application: UIApplication,
                     openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        if #available(iOS 9.0, *) {
            var options: [String: AnyObject] = [UIApplicationOpenURLOptionsSourceApplicationKey: sourceApplication!,
                                                UIApplicationOpenURLOptionsAnnotationKey: annotation]
        } else {
            // Fallback on earlier versions
        }
        return GIDSignIn.sharedInstance().handleURL(url,sourceApplication: sourceApplication,
         annotation: annotation) || FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
                withError error: NSError!) {
        if (error == nil) {
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            let imageUrl = user.profile.imageURLWithDimension(100)
            
            NSUserDefaults.standardUserDefaults().setValue(givenName, forKey: "firstName")
             NSUserDefaults.standardUserDefaults().setValue(familyName, forKey: "lastName")
            NSUserDefaults.standardUserDefaults().setValue(email, forKey: "email")
            NSUserDefaults.standardUserDefaults().setValue(userId, forKey: "googleId")
             NSUserDefaults.standardUserDefaults().setValue("", forKey: "fbId")
             NSUserDefaults.standardUserDefaults().setURL(imageUrl, forKey: "imageUrl")
            GIDSignIn.sharedInstance().signOut()
            NSNotificationCenter.defaultCenter().postNotificationName("signUpStatus", object: nil, userInfo: nil)
            // ...
        } else {
            print("\(error.localizedDescription)")
        }
    }
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
                withError error: NSError!) {
    }

    func chatNotify(){
        NSNotificationCenter.defaultCenter().postNotificationName("chatRecieveNotification", object: nil, userInfo: nil)
    }
    func doRequestPost(url:String,data:[String: NSObject]){
        
        
        
        let requestDic = data
        print(requestDic)
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        
        print("Request Object:\(data)")
        print("Request string = \(jsonString!)")
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                print("Succes:")
                let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                print(responceString)
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                dispatch_async(dispatch_get_main_queue()) {
                    if(self.serverInt == 1){
                        let defaults = NSUserDefaults.standardUserDefaults()
                        if let usertype = defaults.valueForKey("usertype") {
                            var userId = NSString()
                            if(usertype as! NSObject == 1 || usertype as! NSObject == 0){
                                userId = defaults.valueForKey("id") as! String
                            }else if(usertype as! NSObject == 3){
                                userId = defaults.valueForKey("staffId") as! String
                            }
                            self.doRequestPost("\(Header.BASE_URL)messageChat/sessionChange", data: ["UserId":"\(userId)"])
                            self.serverInt = 2
                        }
                        
                        let version = responseDict.valueForKey("version") as! String
                        let forceUpdate = responseDict.valueForKey("FourceUpdate") as! String
                        let versionNumber = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as! String
                        if(version != versionNumber){
                            let alert = UIAlertController(title: "Health4life", message: "Please Update Your App.", preferredStyle: UIAlertControllerStyle.Alert)
                            let okAction = UIAlertAction(title: "Upgrade", style: UIAlertActionStyle.Default) {
                                UIAlertAction in
                                let url = NSURL(string: "https://itunes.apple.com/us/app/health4life-concierge-telemedicine/id1269065511?l=es&mt=8")!
                                UIApplication.sharedApplication().openURL(url)
                                
                            }
                            let cancelAction = UIAlertAction(title: "Not Now", style: UIAlertActionStyle.Cancel) {
                                UIAlertAction in
                            }
                            alert.addAction(okAction)
                            if(forceUpdate == "0"){
                                alert.addAction(cancelAction)
                            }
                            self.window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
                        }
                    }else if(self.serverInt == 2){
                        
                    }
                }
                    
                }
            }
        
        dataTask.resume()
    }
    func applicationWillResignActive(application: UIApplication) {
       
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        let defaults = NSUserDefaults.standardUserDefaults()
        if(defaults.valueForKey("deviceToken") != nil){
            let token = defaults.valueForKey("deviceToken") as! NSString
            self.doRequestPost("\(Header.BASE_URL)users/TokenCheck", data: ["DeviceToken":"\(token))"])
            serverInt = 1
        }
        else{
            let defaults = NSUserDefaults.standardUserDefaults()
            if let usertype = defaults.valueForKey("usertype") {
                var userId = NSString()
                if(usertype as! NSObject == 1 || usertype as! NSObject == 0){
                    userId = defaults.valueForKey("id") as! String
                }else if(usertype as! NSObject == 3){
                    userId = defaults.valueForKey("staffId") as! String
                }
                self.doRequestPost("\(Header.BASE_URL)messageChat/sessionChange", data: ["UserId":"\(userId)"])
                self.serverInt = 2
            }
        }
        
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        if(notificationFlag == false){
            let defaults = NSUserDefaults.standardUserDefaults()
            if let usertype = defaults.valueForKey("usertype") {
                if(usertype as! NSObject == 0){
                    if let patientHomeViewController =  (self.window?.rootViewController as! UINavigationController).viewControllers[0] as? PatientHomeViewController{
                        patientHomeViewController.tokenGetHit()
                    }
                }
                else if(usertype as! NSObject == 1){
                    if let doctorHomeViewController =  (self.window?.rootViewController as! UINavigationController).viewControllers[0] as? DoctorHomeViewController{
                        doctorHomeViewController.tokenGetHit()
                    }
                }
                else if(usertype as! NSObject == 3){
                    if let staffHomeViewController =  (self.window?.rootViewController as! UINavigationController).viewControllers[0] as? StaffHomeViewController{
                        staffHomeViewController.tokenGetHit()
                    }
                }
            }
        }
        else{
            notificationFlag = false
        }
    }
    
    func applicationWillTerminate(application: UIApplication) {
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = NSBundle.mainBundle().URLForResource("TicTokDoc", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            dict[NSUnderlyingErrorKey] = error as! NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        notification.timeZone = NSTimeZone(name: "GMT-8")
        let info = notification.userInfo! as NSDictionary
        let status = info.valueForKey("Status") as! String
        if(application.applicationState == .Active){
            if(status == "PatientLocal"){
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Health4life", description: "You have a scheduled video visit with your Doctor in 15 minutes. Please log into Health4life App", type: TWMessageBarMessageType.Success)
            }
            else{
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Health4life", description: "You have a scheduled video visit with your Patient in 5 minutes. Please log into Health4life App", type: TWMessageBarMessageType.Success)
            }
        }
        application.cancelLocalNotification(notification)
    }
    
    func registerForPushNotifications(application: UIApplication) {
        let notificationSettings = UIUserNotificationSettings(
            forTypes: [.Badge, .Sound, .Alert], categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
        application.registerForRemoteNotifications()
    }
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings)
    {
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let dataString = deviceToken.hexString()
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setValue(dataString, forKey: "deviceToken")
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error.localizedDescription)
    }
    func playSound() {
        
        guard let url = NSBundle.mainBundle().URLForResource("iphonenoti_cRjTITC7", withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player              = try AVAudioPlayer(contentsOfURL: url)
            guard let player    = player else { return }
            
            player.play()
        } catch let error {
            
        }
    }
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject])
    {
        var storyBoard = UIStoryboard()
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Phone:
            storyBoard = UIStoryboard(name: "Main", bundle: nil)
        case .Pad:
            storyBoard = UIStoryboard(name: "Storyboard", bundle: nil)
        default:
            break
        }
        if(application.applicationState == .Active){
            let info = userInfo as NSDictionary
            print(info)
            
            if let discription = info.valueForKeyPath("aps.alert"){
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Health4Life", description: discription as? String, type: TWMessageBarMessageType.Success)
            }
            
            let status = info.valueForKey("Status") as! String
            if(status == "video"){
                UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                NSNotificationCenter.defaultCenter().postNotificationName("timerInvalidate", object: nil)
                let drdetail = info.valueForKey("Dr") as! NSDictionary
                let schData = info.valueForKey("drschedulesetsId") as! NSString
                let bookD = info.valueForKey("BookingId") as! NSString
                let vc = storyBoard.instantiateViewControllerWithIdentifier("ConferenceVideoViewController") as! ConferenceVideoViewController
                let nav = UINavigationController(rootViewController: vc)
                vc.drDetail = drdetail
                vc.bookId = bookD
                vc.ptSchId = schData
                vc.kToken = info.valueForKey("TokenData") as! NSString
                vc.kSessionId = info.valueForKey("sessionData") as! NSString
                vc.kApiKey = info.valueForKey("apiKey") as! NSString
                vc.groupId = info.valueForKey("GroupId") as! String
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
            }
            else if(status == "Dr notification status"){
                self.playSound()
                UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                NSNotificationCenter.defaultCenter().postNotificationName("Dr notification status", object: nil, userInfo: nil)
            }
            else if(status == "Staff notification status"){
                self.playSound()
                let defaults = NSUserDefaults.standardUserDefaults()
                if let name = info.valueForKeyPath("Drname"){
                    print(name)
                    defaults.setObject(info.valueForKeyPath("Drid.ProfilePic"), forKey: "selectDrImg")
                    defaults.setObject(info.valueForKeyPath("Drname"), forKey: "selectDrName")
                    defaults.setObject(info.valueForKeyPath("Drid.Speciality"), forKey: "selectDrSpeciality")
                    defaults.setObject(info.valueForKeyPath("Drid._id"), forKey: "id")
                    defaults.setValue(0, forKey: "indexPath")
                    var storyboard = UIStoryboard()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                    }else{
                        storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    }
                    let mainViewController = storyboard.instantiateViewControllerWithIdentifier("StaffHomeViewController") as! StaffHomeViewController
                    let leftViewController = storyboard.instantiateViewControllerWithIdentifier("StaffSelectDoctorViewController") as! StaffSelectDoctorViewController
                    let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                    leftViewController.mainViewController = nvc
                    let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController:leftViewController)
                    slideMenuController.automaticallyAdjustsScrollViewInsets = true
                    slideMenuController.delegate = mainViewController
                    let nav = UINavigationController(rootViewController: slideMenuController)
                    self.window?.rootViewController = nav
                    self.window?.makeKeyAndVisible()
                }
                else{
                    self.playSound()
                    NSNotificationCenter.defaultCenter().postNotificationName("Staff notification status", object: nil, userInfo: nil)
                }
                
            }
            else if(status == "chat"){
                self.playSound()
                UIApplication.sharedApplication().applicationIconBadgeNumber = 1
                NSNotificationCenter.defaultCenter().postNotificationName("ChatBadgeNotif", object: nil, userInfo: nil)
                NSNotificationCenter.defaultCenter().postNotificationName("ChatListNotif", object: nil, userInfo: nil)
                
            }
            else{
                self.playSound()
                UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                
                NSNotificationCenter.defaultCenter().postNotificationName("patient notification status", object: nil, userInfo: nil)
            }
            
        }else if(application.applicationState == .Inactive){
            let defaults = NSUserDefaults.standardUserDefaults()
            if let usertype = defaults.valueForKey("usertype"){
                notificationFlag = true
                let info = userInfo as NSDictionary
                print(info)
                let status = info.valueForKey("Status") as! String
                if(status == "video"){
                    NSNotificationCenter.defaultCenter().postNotificationName("timerInvalidate", object: nil)
                    let drdetail = info.valueForKey("Dr") as! NSDictionary
                    let schData = info.valueForKey("drschedulesetsId") as! NSString
                    let bookD = info.valueForKey("BookingId") as! NSString
                    let vc = storyBoard.instantiateViewControllerWithIdentifier("ConferenceVideoViewController") as! ConferenceVideoViewController
                    let nav = UINavigationController(rootViewController: vc)
                    vc.drDetail = drdetail
                    vc.bookId = bookD
                    vc.ptSchId = schData
                    vc.kToken = info.valueForKey("TokenData") as! NSString
                    vc.kSessionId = info.valueForKey("sessionData") as! NSString
                    vc.kApiKey = info.valueForKey("apiKey") as! NSString
                    vc.groupId = info.valueForKey("GroupId") as! String
                    self.window?.rootViewController = nav
                    self.window?.makeKeyAndVisible()
                    
                    UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                    
                }
                else if(status == "Dr notification status"){
                    let vc = storyBoard.instantiateViewControllerWithIdentifier("DocNotificationViewController") as! DocNotificationViewController
                    vc.docNotiBool = true
                    self.window?.rootViewController = vc
                    self.window?.makeKeyAndVisible()
                    
                    UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                }
                else if(status == "chat"){
                    var storyboard = UIStoryboard()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                    }else{
                        storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    }
                    let mainViewController = storyboard.instantiateViewControllerWithIdentifier("MessageListingViewController") as! MessageListingViewController
                    mainViewController.chatPush = true
                    let userTypeInt = defaults.valueForKey("usertype") as! Int
                    if(userTypeInt == 0){
                        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("PatientSideBarViewController") as! PatientSideBarViewController
                        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                        leftViewController.mainViewController = nvc
                        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController:leftViewController)
                        slideMenuController.automaticallyAdjustsScrollViewInsets = true
                        slideMenuController.delegate = mainViewController
                        let nav = UINavigationController(rootViewController: slideMenuController)
                        self.window?.rootViewController = nav
                        self.window?.makeKeyAndVisible()
                    }
                    else if(userTypeInt == 1){
                        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("DoctorSideBarViewController") as! DoctorSideBarViewController
                        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                        leftViewController.mainViewController = nvc
                        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController:leftViewController)
                        slideMenuController.automaticallyAdjustsScrollViewInsets = true
                        slideMenuController.delegate = mainViewController
                        let nav = UINavigationController(rootViewController: slideMenuController)
                        self.window?.rootViewController = nav
                        self.window?.makeKeyAndVisible()
                        
                    }
                    else if(userTypeInt == 3){
                        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("StaffSlideViewController") as! StaffSlideViewController
                        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                        leftViewController.mainViewController = nvc
                        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController:leftViewController)
                        slideMenuController.automaticallyAdjustsScrollViewInsets = true
                        slideMenuController.delegate = mainViewController
                        let nav = UINavigationController(rootViewController: slideMenuController)
                        self.window?.rootViewController = nav
                        self.window?.makeKeyAndVisible()
                    }
                }
                else if(status == "logout"){
                    let defaults = NSUserDefaults.standardUserDefaults()
                    defaults.removeObjectForKey("id")
                    defaults.removeObjectForKey("usertype")
                    let vc = storyBoard.instantiateViewControllerWithIdentifier("mainViewController") as! ViewController
                    let nav = UINavigationController(rootViewController: vc)
                    self.window?.rootViewController = nav
                    self.window?.makeKeyAndVisible()
                    
                    UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                }
                    
                else if(status == "Staff notification status"){
                    let defaults = NSUserDefaults.standardUserDefaults()
                    if let name = info.valueForKeyPath("Drname"){
                        print(name)
                        defaults.setObject(info.valueForKeyPath("Drid.ProfilePic"), forKey: "selectDrImg")
                        defaults.setObject(info.valueForKeyPath("Drname"), forKey: "selectDrName")
                        defaults.setObject(info.valueForKeyPath("Drid.Speciality"), forKey: "selectDrSpeciality")
                        defaults.setObject(info.valueForKeyPath("Drid._id"), forKey: "id")
                        defaults.setValue(0, forKey: "indexPath")
                        var storyboard = UIStoryboard()
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            storyboard = UIStoryboard(name: "Main", bundle: nil)
                        }else{
                            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                        }
                        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("StaffHomeViewController") as! StaffHomeViewController
                        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("StaffSelectDoctorViewController") as! StaffSelectDoctorViewController
                        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                        leftViewController.mainViewController = nvc
                        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController:leftViewController)
                        slideMenuController.automaticallyAdjustsScrollViewInsets = true
                        slideMenuController.delegate = mainViewController
                        let nav = UINavigationController(rootViewController: slideMenuController)
                        self.window?.rootViewController = nav
                        self.window?.makeKeyAndVisible()
                    }
                    else{
                        NSNotificationCenter.defaultCenter().postNotificationName("Staff notification status", object: nil, userInfo: nil)
                    }
                    
                }
                else if(status == "patient notification status"){
                    let vc = storyBoard.instantiateViewControllerWithIdentifier("PatientNotificationViewController") as! PatientNotificationViewController
                    vc.notiBool = true
                    self.window?.rootViewController = vc
                    self.window?.makeKeyAndVisible()
                    UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                }
            }else{
                let vc = storyBoard.instantiateViewControllerWithIdentifier("mainViewController") as! ViewController
                let nav = UINavigationController(rootViewController: vc)
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
                
                UIApplication.sharedApplication().applicationIconBadgeNumber = 0
            }
        }
    }
  }
  
  
  extension NSData {
    func hexString() -> NSString {
        let str = NSMutableString()
        let bytes = UnsafeBufferPointer<UInt8>(start: UnsafePointer(self.bytes), count:self.length)
        for byte in bytes {
            str.appendFormat("%02hhx", byte)
        }
        return str
    }
  }
