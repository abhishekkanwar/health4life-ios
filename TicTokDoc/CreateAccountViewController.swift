

import UIKit
import IQKeyboardManagerSwift
import KMPlaceholderTextView
import CoreLocation
import GooglePlaces
class CreateAccountViewController: UIViewController,SSRadioButtonControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate,WebServiceDelegate,UISearchBarDelegate,CLLocationManagerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIActionSheetDelegate{
    @IBOutlet weak var scrollVwContentVw: UIView!
    @IBOutlet weak var scrollVwSignUp: UIScrollView!
    @IBOutlet weak var btnDoneSave: UIButton!
    @IBOutlet weak var maleBtn: SSRadioButton!
    @IBOutlet weak var femaleBtn: SSRadioButton!
    @IBOutlet weak var createAcLbl: UILabel!
    @IBOutlet weak var createAcProfilePic: UIImageView!
   
    @IBOutlet weak var codeVw: UIView!
    @IBOutlet weak var firstnameVw: UIView!
    @IBOutlet weak var lastnameVw: UIView!
    @IBOutlet weak var emailVw: UIView!
    @IBOutlet weak var paswrdVw: UIView!
    @IBOutlet weak var confrmPswrdVw: UIView!
    @IBOutlet weak var specialityVw: UIView!
    @IBOutlet weak var stateVw: UIView!
    @IBOutlet weak var languageVw: UIView!
    @IBOutlet weak var bioVw: UIView!
    @IBOutlet weak var zipcodeVw: UIView!
    @IBOutlet weak var addressVw: UIView!
    @IBOutlet weak var cityVw: UIView!
    @IBOutlet weak var qualificationVw: UIView!
    @IBOutlet weak var phnVw: UIView!
    
    

    @IBOutlet weak var arCodeFld: UITextField!
    @IBOutlet weak var firstnameFld: UITextField!
    @IBOutlet weak var lastnameFld: UITextField!
    @IBOutlet weak var pswrdFld: UITextField!
    @IBOutlet weak var emailaddressFld: UITextField!
    @IBOutlet weak var cnfrmpswrdFld: UITextField!
    @IBOutlet weak var phnnoFld: UITextField!
    @IBOutlet weak var adresFld: UITextField!
    @IBOutlet weak var zipcodeFld: UITextField!
    @IBOutlet weak var cityFld: UITextField!
    @IBOutlet weak var stateFld: UITextField!
    @IBOutlet weak var qualificationFld: UITextField!
    @IBOutlet weak var specialityFld: UITextField!
    @IBOutlet weak var lngFld: UITextField!
    @IBOutlet weak var bioFld: KMPlaceholderTextView!
    
    
    @IBOutlet weak var introVw: UIView!
    @IBOutlet weak var userAddVw: UIView!
    @IBOutlet weak var userEduVw: UIView!
    
    
    var searchResultController: SearchResultsController!
    var resultsArray = [String]()
    var lat = String()
    var long = String()
     var gender = String()
    var createAc = Bool()
    var selectGender = Bool()
    var usState = []
    var arCode = []
    var fieldName = NSString()
    
    var firstName = NSString()
    var lastName = NSString()
    var emailId = NSString()
    var urlImage = NSURL()
    var fbId = NSString()
    var googleId = NSString()
    var socialMedia = Bool()
    var radioButtonController: SSRadioButtonsController?
    override func viewDidLoad() {
        super.viewDidLoad()
        usState = ["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Carolina","North Dakota","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]
        arCode = ["201", "202", "203", "204", "205", "206", "207", "208", "209", "210", "212", "213", "214", "215", "216", "217", "218", "219", "224", "225", "226", "228", "229", "231", "234", "239", "240", "242", "246", "248", "250", "251", "252", "253", "254", "256", "260", "262", "264", "267", "268", "269", "270", "276", "281", "284", "289", "301", "302", "303", "304", "305", "306", "307", "308", "309", "310", "312", "313", "314", "315", "316", "317", "318", "319", "320", "321", "323", "325", "330", "331", "334", "336", "337", "339", "340", "343", "345", "347", "351", "352", "360", "361", "385", "386", "401", "402", "403", "404", "405", "406", "407", "408", "409", "410", "412", "413", "414", "415", "416", "417", "418", "419", "423", "424", "425", "430", "432", "434", "435", "438", "440", "441", "442", "443", "450", "456", "458", "469", "470", "473", "475", "478", "479", "480", "484", "500", "501", "502", "503", "504", "505", "506", "507", "508", "509", "510", "512", "513", "514", "515", "516", "517", "518", "519", "520", "530", "533", "534", "539", "540", "541", "551", "559", "561", "562", "563", "567", "570", "571", "573", "574", "575", "579", "580", "581", "585", "586", "587", "600", "601", "602", "603", "604", "605", "606", "607", "608", "609", "610", "612", "613", "614", "615", "616", "617", "618", "619", "620", "623", "626", "630", "631", "636", "641", "646", "647", "649", "650", "651", "657", "660", "661", "662", "664", "670", "671", "678", "681", "682", "684", "669", "700", "701", "702", "703", "704", "705", "706", "707", "708", "709", "710", "712", "713", "714", "715", "716", "717", "718", "719", "720", "724", "727", "731", "732", "734", "740", "747", "754", "757", "758", "760", "762", "763", "765", "767", "769", "770", "772", "773", "774", "775", "778", "779", "780", "781", "784", "785", "786", "787", "800", "801", "802", "803", "804", "805", "806", "807", "808", "809", "810", "812", "813", "814", "815", "816", "817", "818", "819", "828", "829", "830", "831", "832", "843", "845", "847", "848", "849", "850", "855", "856", "857", "858", "859", "860", "862", "863", "864", "865", "866", "867", "868", "869", "870", "872", "876", "877", "878", "888", "900", "901", "902", "903", "904", "905", "906", "907", "908", "909", "910", "912", "913", "914", "915", "916", "917", "918", "919", "920", "925", "928", "931", "936", "937", "938", "939", "940", "941", "947", "949", "951", "952", "954", "956", "970", "971", "972", "973", "978", "979", "980", "985", "989"]
        bioFld.delegate = self
        createAcProfilePic.layer.cornerRadius = createAcProfilePic.frame.size.height/2
        createAcProfilePic.layer.borderWidth = createAcProfilePic.frame.size.height*0.07
        createAcProfilePic.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
          createAcProfilePic.clipsToBounds = true
        
        firstnameFld.text = firstName as String
        lastnameFld.text = lastName as String
        emailaddressFld.text = emailId as String
        
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().enable = true
        radioButtonController = SSRadioButtonsController(buttons: maleBtn, femaleBtn)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(CreateAccountViewController.imageTapped(_:)))
        createAcProfilePic.userInteractionEnabled = true
        createAcProfilePic.addGestureRecognizer(tapGestureRecognizer)
        
        firstnameFld.tintColor     = UIColor.blackColor()
        lastnameFld.tintColor = UIColor.blackColor()
        emailaddressFld.tintColor   = UIColor.blackColor()
        pswrdFld.tintColor   = UIColor.blackColor()
        cnfrmpswrdFld.tintColor   = UIColor.blackColor()
        phnnoFld.tintColor     = UIColor.blackColor()
        adresFld.tintColor = UIColor.blackColor()
        cityFld.tintColor   = UIColor.blackColor()
        arCodeFld.tintColor   = UIColor.blackColor()
        qualificationFld.tintColor   = UIColor.blackColor()
        specialityFld.tintColor     = UIColor.blackColor()
        lngFld.tintColor = UIColor.blackColor()
        zipcodeFld.tintColor   = UIColor.blackColor()
        bioFld.tintColor = UIColor.blackColor()
        
        firstnameFld.autocapitalizationType = .Words
        lastnameFld.autocapitalizationType = .Words
        cityFld.autocapitalizationType = .Words
        qualificationFld.autocapitalizationType = .Words
        specialityFld.autocapitalizationType = .Words
        lngFld.autocapitalizationType = .Words
        bioFld.autocapitalizationType = .Words
        adresFld.autocapitalizationType = .Words
        
        firstnameFld.userInteractionEnabled = true
        lastnameFld.userInteractionEnabled = true
        cityFld.userInteractionEnabled = true
        qualificationFld.userInteractionEnabled = true
        specialityFld.userInteractionEnabled = true
        lngFld.userInteractionEnabled = true
        bioFld.userInteractionEnabled = true
        adresFld.userInteractionEnabled = true
        cnfrmpswrdFld.userInteractionEnabled = true
        arCodeFld.userInteractionEnabled = true
         phnnoFld.userInteractionEnabled = true
        zipcodeFld.userInteractionEnabled = true
        
        firstnameFld.delegate = self
        lastnameFld.delegate = self
        emailaddressFld.delegate = self
        pswrdFld.delegate = self
        cnfrmpswrdFld.delegate = self
        phnnoFld.delegate = self
        adresFld.delegate = self
        cityFld.delegate = self
        stateFld.delegate = self
        qualificationFld.delegate = self
        specialityFld.delegate = self
        lngFld.delegate = self
        arCodeFld.delegate = self
        zipcodeFld.delegate = self
        
        selectGender = false
        stateFld.tintColor     = UIColor.clearColor()
        if(firstnameFld.text == "" && lastnameFld.text == "" && emailaddressFld.text == "" && pswrdFld.text == "" && cnfrmpswrdFld.text == "" && phnnoFld.text == "" && adresFld.text == "" && cityFld.text == "" && stateFld.text == "" && qualificationFld.text == "" && specialityFld.text == "" && lngFld.text == "" && bioFld.text == "" && zipcodeFld.text == "" && selectGender == false && arCodeFld.text == ""){
            btnDoneSave.enabled = false
        }
        self.boxRound()
        if(firstName != "" || lastName != "" || emailId != ""){
            firstnameFld.userInteractionEnabled = false
            lastnameFld.userInteractionEnabled = false
            emailaddressFld.userInteractionEnabled = false
        }
        
        
        if(socialMedia == true){
            createAcProfilePic.userInteractionEnabled = false
            if let url = NSURL(string: "\(urlImage)") {
                self.createAcProfilePic.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
            }
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                paswrdVw.hidden = true
                confrmPswrdVw.hidden = true
                introVw.frame.size.height = introVw.frame.size.height-lastnameVw.frame.size.height-55
            }else{
                paswrdVw.removeFromSuperview()
                confrmPswrdVw.removeFromSuperview()
                let topConstraint1 = NSLayoutConstraint(item:codeVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: emailVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 40)
                self.view.addConstraints([topConstraint1])
                
                let topConstraint2 = NSLayoutConstraint(item:phnVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: emailVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 40)
                self.view.addConstraints([topConstraint2])
            }
        }
        
        
        
//        if(socialMedia == true){
//             createAcProfilePic.userInteractionEnabled = false
//            if let url = NSURL(string: "\(urlImage)") {
//                self.createAcProfilePic.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
//            }
//        paswrdVw.removeFromSuperview()
//        confrmPswrdVw.removeFromSuperview()
//        let topConstraint1 = NSLayoutConstraint(item:codeVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: emailVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 40)
//        self.view.addConstraints([topConstraint1])
//        
//        let topConstraint2 = NSLayoutConstraint(item:phnVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: emailVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 40)
//        self.view.addConstraints([topConstraint2])
//        }
        btnDoneSave.enabled = false
    }
    func boxRound(){
        codeVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        codeVw.layer.borderColor   = UIColor.darkGrayColor().CGColor
        codeVw.layer.masksToBounds = false;
        codeVw.layer.borderWidth   = 0.8
        
        emailVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        emailVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        emailVw.layer.masksToBounds = false;
        emailVw.layer.borderWidth = 0.8
        
        firstnameVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        firstnameVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        firstnameVw.layer.masksToBounds = false;
        firstnameVw.layer.borderWidth = 0.8
        
        lastnameVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        lastnameVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        lastnameVw.layer.masksToBounds = false;
        lastnameVw.layer.borderWidth = 0.8
        
        paswrdVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        paswrdVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        paswrdVw.layer.masksToBounds = false;
        paswrdVw.layer.borderWidth = 0.8

        
        confrmPswrdVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        confrmPswrdVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        confrmPswrdVw.layer.masksToBounds = false;
        confrmPswrdVw.layer.borderWidth = 0.8
        
        specialityVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        specialityVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        specialityVw.layer.masksToBounds = false;
        specialityVw.layer.borderWidth = 0.8
        
        languageVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        languageVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        languageVw.layer.masksToBounds = false;
        languageVw.layer.borderWidth = 0.8
        
        bioVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        bioVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        bioVw.layer.masksToBounds = false;
        bioVw.layer.borderWidth = 0.8
        
        qualificationVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        qualificationVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        qualificationVw.layer.masksToBounds = false;
        qualificationVw.layer.borderWidth = 0.8
        
        cityVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        cityVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        cityVw.layer.masksToBounds = false;
        cityVw.layer.borderWidth = 0.8

        
        stateVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        stateVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        stateVw.layer.masksToBounds = false;
        stateVw.layer.borderWidth = 0.8
        
        zipcodeVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        zipcodeVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        zipcodeVw.layer.masksToBounds = false;
        zipcodeVw.layer.borderWidth = 0.8
        
        phnVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        phnVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        phnVw.layer.masksToBounds = false;
        phnVw.layer.borderWidth = 0.8
        
        addressVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        addressVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        addressVw.layer.masksToBounds = false;
        addressVw.layer.borderWidth = 0.8
    
     
    
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        searchResultController = SearchResultsController()
    }
    
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        let addr = searchResultController.address
        adresFld.text = addr
        if(addr != ""){
        self.getLocationFromAddressString(adresFld.text!)
        }
        return true;
    }
    func getLocationFromAddressString(loadAddressStr: String)  {
        var latitude :Double = 0
        var longitude : Double = 0
        let esc_addr = loadAddressStr.stringByAppendingFormat("%@","")
        let req = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=\(esc_addr)"
        let escapedAddress = req.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        let url: NSURL = NSURL(string: escapedAddress!)!
        let result:String? = try? String(contentsOfURL:url)
        if result != nil {
            let scanner: NSScanner = NSScanner(string: result!)
            if(scanner.scanUpToString("\"lat\" :", intoString: nil) && scanner.scanString("\"lat\" :", intoString: nil)){
                scanner.scanDouble(&latitude)
                if(scanner.scanUpToString("\"lng\" :", intoString: nil) && scanner.scanString("\"lng\" :", intoString: nil)){
                    scanner.scanDouble(&longitude)
                }
            }
        }
        var center :CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0);
        center.latitude = latitude;
        center.longitude = longitude;
        
        lat = String(format: "%.20f", center.latitude)
        long = String(format: "%.20f", center.longitude)
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        if(textField == adresFld){
            fieldName = "Address"
            
        }
        else if(textField == firstnameFld){
            fieldName = "First Name"
            
        }
        else if(textField == lastnameFld){
            fieldName = "Last Name"
        }
        else if(textField == cityFld){
            fieldName = "City"
        }
        else if(textField == bioFld){
            fieldName = "Bio"
        }
        else if(textField == qualificationFld){
            fieldName = "Qualification"
        }
        else if(textField == lngFld){
            fieldName = "Language"
        }
        else if(textField == specialityFld){
            fieldName = "Speciality"
        }
        else if(textField == stateFld){
            let InputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0.4))
            let PickerVw  : UIPickerView = UIPickerView(frame: CGRectMake(0, 40,UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height*0.32))
            PickerVw.delegate            = self
            PickerVw.dataSource          = self
            InputView.addSubview(PickerVw)
            textField.inputView = InputView
        }
    }
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        if(stateFld.isFirstResponder()){
          return usState.count
        }
        return 0
        
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        return usState[row] as? String
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        stateFld.text = usState[row] as? String
    }
       func textViewDidEndEditing(textView: UITextView) {
        let trimNameString1 = firstnameFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        let trimNameString2 = lastnameFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString3 = cityFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString4 = pswrdFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString5 = zipcodeFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString6 = emailaddressFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString7 = adresFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString8 = phnnoFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString9 = lngFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString10 = bioFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString11 = qualificationFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString12 = specialityFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString13 = arCodeFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if(socialMedia == false){
        if(trimNameString1 != "" && trimNameString2 != "" && trimNameString3 != "" && trimNameString4 != "" && cnfrmpswrdFld.text != "" && trimNameString5 != "" && trimNameString6 != "" && trimNameString7 != "" && stateFld.text != "" && trimNameString8 != "" && trimNameString9 != "" && trimNameString10 != "" && trimNameString11 != "" && trimNameString12 != "" && selectGender == true && trimNameString13 != ""){
            btnDoneSave.enabled = true
        }
        else if(trimNameString1.characters.count == 0 || trimNameString2.characters.count == 0 || trimNameString3.characters.count == 0 || trimNameString4.characters.count == 0 || trimNameString11.characters.count == 0 || trimNameString5.characters.count == 0 || trimNameString6.characters.count == 0 || trimNameString7.characters.count == 0 || trimNameString12.characters.count == 0 || trimNameString8.characters.count == 0 || trimNameString9.characters.count == 0 || selectGender == false || trimNameString10.characters.count == 0 || cnfrmpswrdFld.text == "" || stateFld.text == "" || trimNameString13.characters.count == 0){
            btnDoneSave.enabled = false
           }
        }
    else{
            if( trimNameString3 != "" &&  trimNameString5 != ""  && trimNameString7 != "" && stateFld.text != "" && trimNameString8 != "" && trimNameString9 != "" && trimNameString10 != "" && trimNameString11 != "" && trimNameString12 != "" && selectGender == true && trimNameString13 != ""){
                btnDoneSave.enabled = true
            }
            else if(trimNameString3.characters.count == 0 || trimNameString11.characters.count == 0 || trimNameString5.characters.count == 0 || trimNameString7.characters.count == 0 || trimNameString12.characters.count == 0 || trimNameString8.characters.count == 0 || trimNameString9.characters.count == 0 || selectGender == false || trimNameString10.characters.count == 0 || stateFld.text == "" || trimNameString13.characters.count == 0){
                btnDoneSave.enabled = false
            }
        }
    }
    func textFieldDidEndEditing(textField: UITextField) {
        let trimNameString1 = firstnameFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        let trimNameString2 = lastnameFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString3 = cityFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString4 = pswrdFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString5 = zipcodeFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString6 = emailaddressFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString7 = adresFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString8 = phnnoFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString9 = lngFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString10 = bioFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString11 = qualificationFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString12 = specialityFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString13 = arCodeFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if(socialMedia == false){
        if(trimNameString1 != "" && trimNameString2 != "" && trimNameString3 != "" && trimNameString4 != "" && cnfrmpswrdFld.text != "" && trimNameString5 != "" && trimNameString6 != "" && trimNameString7 != "" && stateFld.text != "" && trimNameString8 != "" && trimNameString9 != "" && trimNameString10 != "" && trimNameString11 != "" && trimNameString12 != "" && selectGender == true && trimNameString13 != ""){
            btnDoneSave.enabled = true
        }
        else if(trimNameString1.characters.count == 0 || trimNameString2.characters.count == 0 || trimNameString3.characters.count == 0 || trimNameString4.characters.count == 0 || trimNameString11.characters.count == 0 || trimNameString5.characters.count == 0 || trimNameString6.characters.count == 0 || trimNameString7.characters.count == 0 || trimNameString12.characters.count == 0 || trimNameString8.characters.count == 0 || trimNameString9.characters.count == 0 || selectGender == false || trimNameString10.characters.count == 0 || cnfrmpswrdFld.text == "" || stateFld.text == "" || trimNameString13.characters.count == 0){
            btnDoneSave.enabled = false
        }
        }else{
            if(trimNameString3 != "" && trimNameString5 != ""  && trimNameString7 != "" && stateFld.text != "" && trimNameString8 != "" && trimNameString9 != "" && trimNameString10 != "" && trimNameString11 != "" && trimNameString12 != "" && selectGender == true && trimNameString13 != ""){
                btnDoneSave.enabled = true
            }
            else if(trimNameString3.characters.count == 0 || trimNameString11.characters.count == 0 || trimNameString5.characters.count == 0 || trimNameString7.characters.count == 0 || trimNameString12.characters.count == 0 || trimNameString8.characters.count == 0 || trimNameString9.characters.count == 0 || selectGender == false || trimNameString10.characters.count == 0 || stateFld.text == "" || trimNameString13.characters.count == 0){
                btnDoneSave.enabled = false
            }
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if( textField == firstnameFld || textField == cityFld || textField == lastnameFld ){
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            if(newLength == 36){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "\(fieldName) does not contain more than 35 characters.")
            }
            return newLength <= 35
        }
        if(textField == adresFld){
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            if(newLength == 65){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "\(fieldName) does not contain more than 65 characters.")
            }
            return newLength <= 64
        }
        if(textField == phnnoFld){
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            return newLength <= 7
        }
        if(textField == arCodeFld){
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            if(newLength > 3){
                phnnoFld.becomeFirstResponder()
            }
            return newLength <= 3
        }
        if(textField == zipcodeFld){
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            return newLength <= 9
        }
        return true
    }
    
    func imageTapped(img: AnyObject)
    {
        self.view.endEditing(true)
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        actionSheet.showInView(self.view)
    }
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex){
        case 0:
            print("Cancel")
        case 1:
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.allowsEditing = false
                        imagePicker.sourceType = .Camera
                        self.presentViewController(imagePicker, animated: true, completion:nil)
                    }
                })
            }else{
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .Camera
                    presentViewController(imagePicker, animated: true, completion:nil)
                }
            }
        case 2:
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.allowsEditing = false
                        imagePicker.sourceType = .PhotoLibrary
                        self.presentViewController(imagePicker, animated: true, completion:nil)
                    }
                })
            }else{
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .PhotoLibrary
                    self.presentViewController(imagePicker, animated: true, completion:nil)
                }
            }
        default:
            print("Default")
        }
    }
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        let selectedImage : UIImage = image
        createAcProfilePic.image = self.ResizeImage(selectedImage, targetSize: CGSizeMake(200.0, 200.0))
        createAcProfilePic.clipsToBounds = true
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    override func viewDidLayoutSubviews() {
        if((self.storyboard?.valueForKey("name"))! as! String == "Main"){
       scrollVwSignUp.scrollEnabled = true
            if(self.view.frame.size.height < 600){
             if(socialMedia == false){
              scrollVwSignUp.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1.92)
             }else{
               scrollVwSignUp.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1.65)
              }
            }
            else if(self.view.frame.size.height > 600 && self.view.frame.size.height < 700){
                if(socialMedia == false){
              scrollVwSignUp.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1.76)
                }
                else{
                    scrollVwSignUp.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1.45)
                }
            }
            else{
                if(socialMedia == false){
              scrollVwSignUp.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1.69)
                }else{
                    scrollVwSignUp.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1.38)
                }
            }
            }
        }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func doneBtnAc(sender: AnyObject) {
        if(socialMedia == true){
            self.socialMediaTrue()
        }else{
        self.socialMediaFalse()
        }
    }
    
    
    
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString)
    {
        if((responseDict.valueForKey("error")) != nil){
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            dispatch_async(dispatch_get_main_queue()) {
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                backVw.tag = 2369
                self.view.addSubview(backVw)
                let alerVw = UIView()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
                }else{
                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
                }
                
                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                alerVw.backgroundColor = UIColor.whiteColor()
                backVw.addSubview(alerVw)
                
                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                rightImg.image = UIImage(named: "verifyImg")
                alerVw.addSubview(rightImg)
                
                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                verificationLbl.text = " Alert"
                verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(verificationLbl)
                
                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                lineVw.backgroundColor = UIColor.grayColor()
                alerVw.addSubview(lineVw)
                
              
                let contentLbl = UILabel()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    contentLbl.font  = UIFont(name: "Helvetica", size:14)
                    contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
                }else{
                    contentLbl.font  = UIFont(name: "Helvetica", size:19)
                    contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
                }
                contentLbl.textAlignment = .Center
                contentLbl.textColor = UIColor.grayColor()
                contentLbl.text = "This Email id is already taken."
                contentLbl.numberOfLines = 2
                alerVw.addSubview(contentLbl)
                
                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
                okLbl.setTitle("OK", forState: .Normal)
                if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                }else{
                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                }
                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                okLbl.titleLabel?.textAlignment = .Center
                okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(okLbl)
            }
        }else{
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
             dispatch_async(dispatch_get_main_queue()) {
                if(self.view.frame.size.height < 600){
                    let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                    backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                    backVw.tag = 2369
                    self.view.addSubview(backVw)
                    
                    let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.5)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.5))
                    alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                    alerVw.backgroundColor = UIColor.whiteColor()
                    backVw.addSubview(alerVw)
                    
                    let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                    rightImg.image = UIImage(named: "confirm")
                    alerVw.addSubview(rightImg)
                    
                    let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                    verificationLbl.text = " Account Created"
                    verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                    verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(verificationLbl)
                    
                    let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                    lineVw.backgroundColor = UIColor.grayColor()
                    alerVw.addSubview(lineVw)
                    
                    let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.7))
                    contentLbl.font = UIFont(name: "Helvetica", size:14)
                    contentLbl.textAlignment = .Center
                    contentLbl.textColor = UIColor.grayColor()
                    contentLbl.text = "You will now be logged out of Health4life. Once you verify your email address and Health4life Admin verifies your account, you will be notified via email. At that time, you will be able to log into the App and create your Video Visit schedule for your patients.\n\nThank you for joining Health4life!"
                    contentLbl.numberOfLines = 13
                    alerVw.addSubview(contentLbl)
                    
                    let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.14),alerVw.frame.size.width, alerVw.frame.size.height*0.14))
                    okLbl.setTitle("OK", forState: .Normal)
                    okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                    okLbl.titleLabel?.textAlignment = .Center
                    okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                    okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(okLbl)
                }
                else if(self.view.frame.size.height > 600 && self.view.frame.size.height < 700){
                    let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                    backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                    backVw.tag = 2369
                    self.view.addSubview(backVw)
                    
                    let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.5)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.38))
                    alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                    alerVw.backgroundColor = UIColor.whiteColor()
                    backVw.addSubview(alerVw)
                    
                    let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                    rightImg.image = UIImage(named: "confirm")
                    alerVw.addSubview(rightImg)
                    
                    let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                    verificationLbl.text = " Account Created"
                    verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                    verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(verificationLbl)
                    
                    let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                    lineVw.backgroundColor = UIColor.grayColor()
                    alerVw.addSubview(lineVw)
                    
                    let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+15, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.7))
                    contentLbl.font = UIFont(name: "Helvetica", size:14)
                    contentLbl.textAlignment = .Center
                    contentLbl.textColor = UIColor.grayColor()
                    contentLbl.text = "You will now be logged out of Health4life. Once you verify your email address and Health4life Admin verifies your account, you will be notified via email. At that time, you will be able to log into the App and create your Video Visit schedule for your patients.\n\nThank you for joining Health4life!!"
                    contentLbl.numberOfLines = 13
                    alerVw.addSubview(contentLbl)
                    
                    let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.16),alerVw.frame.size.width, alerVw.frame.size.height*0.16))
                    okLbl.setTitle("OK", forState: .Normal)
                    okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                    okLbl.titleLabel?.textAlignment = .Center
                    okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                    okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(okLbl)
                }
                else{
                    let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                    backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                    backVw.tag = 2369
                    self.view.addSubview(backVw)
                    let alerVw = UIView()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.5)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.3)
                    }else{
                        alerVw.frame = CGRectMake(self.view.frame.size.width*0.22,self.view.frame.size.height/2-((self.view.frame.size.height*0.5)/2), self.view.frame.size.width-(self.view.frame.size.width*0.44), self.view.frame.size.height*0.3)
                    }
                    alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                    alerVw.backgroundColor = UIColor.whiteColor()
                    backVw.addSubview(alerVw)
                    
                    let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                    rightImg.image = UIImage(named: "confirm")
                    alerVw.addSubview(rightImg)
                    
                    let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                    verificationLbl.text = " Account Created"
                    verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                    verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(verificationLbl)
                    
                    let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                    lineVw.backgroundColor = UIColor.grayColor()
                    alerVw.addSubview(lineVw)
                    
                    let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+8, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.7))
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        contentLbl.font  = UIFont(name: "Helvetica", size:14)
                    }else{
                        contentLbl.font  = UIFont(name: "Helvetica", size:19)
                    }
                    contentLbl.textAlignment = .Center
                    contentLbl.textColor = UIColor.grayColor()
                    contentLbl.text = "You will now be logged out of Health4life. Once you verify your email address and Health4life Admin verifies your account, you will be notified via email. At that time, you will be able to log into the App and create your Video Visit schedule for your patients.\n\nThank you for joining Health4life!!"
                    contentLbl.numberOfLines = 13
                    alerVw.addSubview(contentLbl)
                    
                    let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.14),alerVw.frame.size.width, alerVw.frame.size.height*0.14))
                    okLbl.setTitle("OK", forState: .Normal)
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                        okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                    }else{
                        okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                    }
                    okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                    okLbl.titleLabel?.textAlignment = .Center
                    okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                    okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(okLbl)
                }
            
               
            }
        }
    }
    func socialMediaTrue(){
          let compPh = "\(arCodeFld.text! as String)\(phnnoFld.text! as String)"
        if(!(arCode.containsObject(arCodeFld.text!))){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
          
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter a Valid Area Code."
            contentLbl.numberOfLines = 3
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(zipcodeFld.text?.characters.count != 5 && zipcodeFld.text?.characters.count < 5){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter valid zipcode"
            contentLbl.numberOfLines = 1
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(zipcodeFld.text?.characters.count != 9 && zipcodeFld.text?.characters.count > 5){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter valid zipcode"
            contentLbl.numberOfLines = 1
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(compPh.characters.count != 10){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please enter 10 digit phone number."
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(!(AppManager.sharedManager.isValidEmail(emailaddressFld.text!))){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please enter valid email address."
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
//            SignUpImg
        else if(createAcProfilePic.image == UIImage.init(named: "SignUpImg")){
            AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select Your Profile Picture")
            return
        }
        let num = Int(phnnoFld.text!)
        if num == nil {
            AppManager.sharedManager.Showalert("Alert", alertmessage: "Please enter numeric value in phone number Field.")
            return
        }
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let emailLowerCaseString:String = (emailaddressFld.text?.lowercaseString)!
        let firstNameLowerCaseString:String = (firstnameFld.text?.lowercaseString)!
        let lastNameLowerCaseString:String = (lastnameFld.text?.lowercaseString)!
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Signing Up")
        
        let testImage = NSData(contentsOfURL: urlImage)
        let base64String = testImage!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.EncodingEndLineWithLineFeed)
        let media = NSUserDefaults.standardUserDefaults().valueForKey("mediaType") as! String
        if(media == "facebook"){
            
            let params = ["FirstName":"\(firstNameLowerCaseString)" ,"LastName":"\(lastNameLowerCaseString)", "Gender":"\(gender)" , "Mobile":"\(phnnoFld.text!)", "Code":"\(arCodeFld.text!)", "Address":"\(adresFld.text!)" , "State":"\(stateFld.text!)", "City":"\(cityFld.text!)", "Zipcode":"\(zipcodeFld.text!)", "Lat":"\(lat)", "Long":"\(long)", "Qualification":"\(qualificationFld.text!)", "ProfilePic":"\(base64String)", "UserType":"1", "Email":"\(emailLowerCaseString)", "Speciality":"\(specialityFld.text!)", "Languages":"\(lngFld.text!)", "Bio":"\(bioFld.text!)", "DeviceToken":"123456", "DeviceType":"ios", "MediaType":"F","MediaID":"\(fbId)"]
            
            self.doRequestPost("\(Header.BASE_URL)users/sign", data: params)
        }else if(media == "google"){
            
            
            let params = ["FirstName":"\(firstNameLowerCaseString)" ,"LastName":"\(lastNameLowerCaseString)", "Gender":"\(gender)" , "Mobile":"\(phnnoFld.text!)", "Code":"\(arCodeFld.text!)", "Address":"\(adresFld.text!)" , "State":"\(stateFld.text!)", "City":"\(cityFld.text!)", "Zipcode":"\(zipcodeFld.text!)", "Lat":"\(lat)", "Long":"\(long)", "Qualification":"\(qualificationFld.text!)", "ProfilePic":"\(base64String)", "UserType":"1", "Email":"\(emailLowerCaseString)", "Speciality":"\(specialityFld.text!)", "Languages":"\(lngFld.text!)", "Bio":"\(bioFld.text!)", "DeviceToken":"123456", "DeviceType":"ios", "MediaType":"G","MediaID":"\(googleId)"]
            
            self.doRequestPost("\(Header.BASE_URL)users/sign", data: params)
        }
    }
    func socialMediaFalse(){
          let compPh = "\(arCodeFld.text! as String)\(phnnoFld.text! as String)"
        if(!(arCode.containsObject(arCodeFld.text!))){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter a Valid Area Code."
            contentLbl.numberOfLines = 3
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(cnfrmpswrdFld.text != pswrdFld.text){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.15)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.3)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Password and Confirm Password does not match."
            contentLbl.numberOfLines = 3
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(zipcodeFld.text?.characters.count != 5 && zipcodeFld.text?.characters.count < 5){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter valid zipcode"
            contentLbl.numberOfLines = 1
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(zipcodeFld.text?.characters.count != 9 && zipcodeFld.text?.characters.count > 5){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter valid zipcode"
            contentLbl.numberOfLines = 1
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
            
        else if(!(pswrdFld.text?.characters.count >= 6 && pswrdFld.text?.characters.count <= 15)){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.15)
            }
            
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.3)
            }
            
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "please enter password in between 6 to 15 digits"
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(compPh.characters.count != 10){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please enter 10 digit phone number."
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(!(AppManager.sharedManager.isValidEmail(emailaddressFld.text!))){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please enter valid email address."
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(createAcProfilePic.image == UIImage.init(named: "SignUpImg")){
            AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select Your Profile Picture")
            return
        }
        let num = Int(phnnoFld.text!)
        if num == nil {
            AppManager.sharedManager.Showalert("Alert", alertmessage: "Please enter numeric value in phone number Field.")
            return
        }
        let defaults = NSUserDefaults.standardUserDefaults()
        let emailLowerCaseString:String = (emailaddressFld.text?.lowercaseString)!
        let firstNameLowerCaseString:String = (firstnameFld.text?.lowercaseString)!
        let lastNameLowerCaseString:String = (lastnameFld.text?.lowercaseString)!
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Signing Up")
        
        let image : UIImage = createAcProfilePic.image!
        let  mImageData              = UIImagePNGRepresentation(image)! as NSData
        let base64String = mImageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.EncodingEndLineWithLineFeed)
        NSUserDefaults.standardUserDefaults().setValue("manual", forKey: "mediaType")
        
        let params = ["FirstName":"\(firstNameLowerCaseString)" ,"LastName":"\(lastNameLowerCaseString)", "Gender":"\(gender)" , "Mobile":"\(phnnoFld.text!)", "Code":"\(arCodeFld.text!)", "Address":"\(adresFld.text!)" , "State":"\(stateFld.text!)", "City":"\(cityFld.text!)", "Zipcode":"\(zipcodeFld.text!)", "Lat":"\(lat)", "Long":"\(long)", "Qualification":"\(qualificationFld.text!)", "ProfilePic":"\(base64String)", "UserType":"1", "Email":"\(emailLowerCaseString)", "Password":"\(pswrdFld.text!)", "Speciality":"\(specialityFld.text!)", "Languages":"\(lngFld.text!)", "Bio":"\(bioFld.text!)", "DeviceToken":"123456", "DeviceType":"ios", "MediaType":"M"]
        
        self.doRequestPost("\(Header.BASE_URL)users/sign", data: params)
        
    }
    
    
    func doRequestPost(url:String,data:[String: NSObject]){
        let requestDic = data
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSUTF8StringEncoding)
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                dispatch_async(dispatch_get_main_queue()) {
                    if((responseDict.valueForKey("error")) != nil){
                        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                        dispatch_async(dispatch_get_main_queue()) {
                            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                            backVw.tag = 2369
                            self.view.addSubview(backVw)
                            let alerVw = UIView()
                            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
                            }else{
                                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
                            }
                            
                            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                            alerVw.backgroundColor = UIColor.whiteColor()
                            backVw.addSubview(alerVw)
                            
                            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                            rightImg.image = UIImage(named: "verifyImg")
                            alerVw.addSubview(rightImg)
                            
                            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                            verificationLbl.text = " Alert"
                            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                            alerVw.addSubview(verificationLbl)
                            
                            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                            lineVw.backgroundColor = UIColor.grayColor()
                            alerVw.addSubview(lineVw)
                            
                            
                            let contentLbl = UILabel()
                            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
                            }else{
                                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
                            }
                            contentLbl.textAlignment = .Center
                            contentLbl.textColor = UIColor.grayColor()
                            contentLbl.text = "This Email id is already taken."
                            contentLbl.numberOfLines = 2
                            alerVw.addSubview(contentLbl)
                            
                            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
                            okLbl.setTitle("OK", forState: .Normal)
                            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                            }else{
                                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                            }
                            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                            okLbl.titleLabel?.textAlignment = .Center
                            okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
                            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                            alerVw.addSubview(okLbl)
                        }
                    }else{
                        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                        dispatch_async(dispatch_get_main_queue()) {
                            if(self.view.frame.size.height < 600){
                                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                                backVw.tag = 2369
                                self.view.addSubview(backVw)
                                
                                let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.5)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.5))
                                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                                alerVw.backgroundColor = UIColor.whiteColor()
                                backVw.addSubview(alerVw)
                                
                                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                                rightImg.image = UIImage(named: "confirm")
                                alerVw.addSubview(rightImg)
                                
                                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                                verificationLbl.text = " Account Created"
                                verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                                alerVw.addSubview(verificationLbl)
                                
                                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                                lineVw.backgroundColor = UIColor.grayColor()
                                alerVw.addSubview(lineVw)
                                
                                let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.7))
                                contentLbl.font = UIFont(name: "Helvetica", size:14)
                                contentLbl.textAlignment = .Center
                                contentLbl.textColor = UIColor.grayColor()
                                contentLbl.text = "You will now be logged out of Health4life. Once you verify your email address and Health4life Admin verifies your account, you will be notified via email. At that time, you will be able to log into the App and create your Video Visit schedule for your patients.\n\nThank you for joining Health4life!"
                                contentLbl.numberOfLines = 13
                                alerVw.addSubview(contentLbl)
                                
                                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.14),alerVw.frame.size.width, alerVw.frame.size.height*0.14))
                                okLbl.setTitle("OK", forState: .Normal)
                                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                                okLbl.titleLabel?.textAlignment = .Center
                                okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                                alerVw.addSubview(okLbl)
                            }
                            else if(self.view.frame.size.height > 600 && self.view.frame.size.height < 700){
                                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                                backVw.tag = 2369
                                self.view.addSubview(backVw)
                                
                                let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.5)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.38))
                                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                                alerVw.backgroundColor = UIColor.whiteColor()
                                backVw.addSubview(alerVw)
                                
                                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                                rightImg.image = UIImage(named: "confirm")
                                alerVw.addSubview(rightImg)
                                
                                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                                verificationLbl.text = " Account Created"
                                verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                                alerVw.addSubview(verificationLbl)
                                
                                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                                lineVw.backgroundColor = UIColor.grayColor()
                                alerVw.addSubview(lineVw)
                                
                                let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+15, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.7))
                                contentLbl.font = UIFont(name: "Helvetica", size:14)
                                contentLbl.textAlignment = .Center
                                contentLbl.textColor = UIColor.grayColor()
                                contentLbl.text = "You will now be logged out of Health4life. Once you verify your email address and Health4life Admin verifies your account, you will be notified via email. At that time, you will be able to log into the App and create your Video Visit schedule for your patients.\n\nThank you for joining Health4life!!"
                                contentLbl.numberOfLines = 13
                                alerVw.addSubview(contentLbl)
                                
                                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.16),alerVw.frame.size.width, alerVw.frame.size.height*0.16))
                                okLbl.setTitle("OK", forState: .Normal)
                                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                                okLbl.titleLabel?.textAlignment = .Center
                                okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                                alerVw.addSubview(okLbl)
                            }
                            else{
                                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                                backVw.tag = 2369
                                self.view.addSubview(backVw)
                                let alerVw = UIView()
                                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.5)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.3)
                                }else{
                                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.22,self.view.frame.size.height/2-((self.view.frame.size.height*0.5)/2), self.view.frame.size.width-(self.view.frame.size.width*0.44), self.view.frame.size.height*0.3)
                                }
                                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                                alerVw.backgroundColor = UIColor.whiteColor()
                                backVw.addSubview(alerVw)
                                
                                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                                rightImg.image = UIImage(named: "confirm")
                                alerVw.addSubview(rightImg)
                                
                                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                                verificationLbl.text = " Account Created"
                                verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                                alerVw.addSubview(verificationLbl)
                                
                                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                                lineVw.backgroundColor = UIColor.grayColor()
                                alerVw.addSubview(lineVw)
                                
                                let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+8, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.7))
                                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                                    contentLbl.font  = UIFont(name: "Helvetica", size:14)
                                }else{
                                    contentLbl.font  = UIFont(name: "Helvetica", size:19)
                                }
                                contentLbl.textAlignment = .Center
                                contentLbl.textColor = UIColor.grayColor()
                                contentLbl.text = "You will now be logged out of Health4life. Once you verify your email address and Health4life Admin verifies your account, you will be notified via email. At that time, you will be able to log into the App and create your Video Visit schedule for your patients.\n\nThank you for joining Health4life!!"
                                contentLbl.numberOfLines = 13
                                alerVw.addSubview(contentLbl)
                                
                                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.14),alerVw.frame.size.width, alerVw.frame.size.height*0.14))
                                okLbl.setTitle("OK", forState: .Normal)
                                if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                                }else{
                                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                                }
                                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                                okLbl.titleLabel?.textAlignment = .Center
                                okLbl.addTarget(self, action: #selector(CreateAccountViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                                alerVw.addSubview(okLbl)
                            }
                            
                            
                        }
                    }
                }
            }
        }
        dataTask.resume()
    }
    
    
    
    
    func okBtnAc2(){
        let view = self.view.viewWithTag(2369)! as UIView
        view.removeFromSuperview()
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if(aViewController is ViewController){
                self.navigationController!.popToViewController(aViewController, animated: true);
            }
        }
    }
    
    
    
    
    
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }

    func okBtnAc(){
        let view = self.view.viewWithTag(2369)! as UIView
        view.removeFromSuperview()
    }
    func didSelectButton(aButton: UIButton?) {
        selectGender = false
        if(aButton == maleBtn){
            gender = "Male"
            selectGender = true
        }else if(aButton == femaleBtn){
            gender = "Female"
            selectGender = true
        }
        let trimNameString1 = firstnameFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        let trimNameString2 = lastnameFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString3 = cityFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString4 = pswrdFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString5 = zipcodeFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString6 = emailaddressFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString7 = adresFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString8 = phnnoFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString9 = lngFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString10 = bioFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString11 = qualificationFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString12 = specialityFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString13 = arCodeFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if(socialMedia == false){
        if(trimNameString1 != "" && trimNameString2 != "" && trimNameString3 != "" && trimNameString4 != "" && cnfrmpswrdFld.text != "" && trimNameString5 != "" && trimNameString6 != "" && trimNameString7 != "" && stateFld.text != "" && trimNameString8 != "" && trimNameString9 != "" && trimNameString10 != "" && trimNameString11 != "" && trimNameString12 != "" && selectGender == true && trimNameString13 != ""){
            btnDoneSave.enabled = true
        }
        else if(trimNameString1.characters.count == 0 || trimNameString2.characters.count == 0 || trimNameString3.characters.count == 0 || trimNameString4.characters.count == 0 || trimNameString11.characters.count == 0 || trimNameString5.characters.count == 0 || trimNameString6.characters.count == 0 || trimNameString7.characters.count == 0 || trimNameString12.characters.count == 0 || trimNameString8.characters.count == 0 || trimNameString9.characters.count == 0 || selectGender == false || trimNameString10.characters.count == 0 || cnfrmpswrdFld.text == "" || stateFld.text == "" || trimNameString13.characters.count == 0){
            btnDoneSave.enabled = false
        }
        }else{
            if(trimNameString3 != "" && trimNameString5 != ""  && trimNameString7 != "" && stateFld.text != "" && trimNameString8 != "" && trimNameString9 != "" && trimNameString10 != "" && trimNameString11 != "" && trimNameString12 != "" && selectGender == true && trimNameString13 != ""){
                btnDoneSave.enabled = true
            }
            else if(trimNameString3.characters.count == 0 || trimNameString11.characters.count == 0 || trimNameString5.characters.count == 0 || trimNameString7.characters.count == 0 || trimNameString12.characters.count == 0 || trimNameString8.characters.count == 0 || trimNameString9.characters.count == 0 || selectGender == false || trimNameString10.characters.count == 0 || stateFld.text == "" || trimNameString13.characters.count == 0){
                btnDoneSave.enabled = false
            }
        }
        
    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        let placeClient = GMSPlacesClient()
        
        placeClient.autocompleteQuery(searchText, bounds: nil, filter: nil) { (data, error: NSError?) -> Void in
            self.resultsArray.removeAll()
        }
        placeClient.autocompleteQuery(searchText, bounds: nil, filter: nil) { (results, error: NSError?) -> Void in
            
            self.resultsArray.removeAll()
            if results == nil {
                return
            }
            
            for result in results! {
                if let result = result as? GMSAutocompletePrediction {
                    self.resultsArray.append(result.attributedFullText.string)
                }
            }
            
            self.searchResultController.reloadDataWithArray(self.resultsArray)
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
