

import UIKit
import SlideMenuControllerSwift
import EventKit
import RealmSwift
class DoctorHomeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,WebServiceDelegate{

    @IBOutlet weak var profilePicImgVw: UIImageView!
    @IBOutlet weak var drName: UILabel!
    @IBOutlet weak var notifiLbl: UILabel!
    @IBOutlet weak var speciality: UILabel!
   
    @IBOutlet weak var chatNotifBadge : UILabel!
    
    @IBOutlet weak var searchDoc: UIButton!
    @IBOutlet weak var joinVideoBtn: UIButton!
    var arMenu = ["My Appointments", "My Schedules","My Patients","Prescriptions"]
    var arMenuImages = ["HomeClock", "HomeCalender","HomePateints","HomeMedicine"]
    var serverInt = NSInteger()
    var count = NSInteger()
    var pndingPatCount = NSInteger()
    var schCount = NSInteger()
    var timer = NSTimer()
    var logoutCon = Bool()
    let realm = try! Realm()
    var tableVwRow = NSInteger()
    var serverWho = NSInteger()
    var logoutDrtype = Bool()
    let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    @IBOutlet weak var homeTableVw: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
         AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        let drType = NSUserDefaults.standardUserDefaults().valueForKey("drType") as! String
        if(drType == "MedicinePrescribers"){
            tableVwRow = 4
        }else{
           tableVwRow = 3
        }
        if let msgCount =  NSUserDefaults.standardUserDefaults().integerForKey("msgCount") as? Int{

        profilePicImgVw.layer.cornerRadius = self.profilePicImgVw.frame.size.height/2
        profilePicImgVw.layer.borderWidth = self.view.frame.size.height*0.012
        profilePicImgVw.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        profilePicImgVw.clipsToBounds = true
        profilePicImgVw.setShowActivityIndicatorView(true)
        profilePicImgVw.setIndicatorStyle(.Gray)
        let defaults = NSUserDefaults.standardUserDefaults()
        let imgUrl:String = defaults.valueForKey("profilePic") as! String
        if let url = NSURL(string: "http://\(imgUrl)") {
            profilePicImgVw.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        profilePicImgVw.clipsToBounds = true
        
        notifiLbl.layer.cornerRadius = self.notifiLbl.frame.size.height/2
        notifiLbl.clipsToBounds = true
        self.notifiLbl.hidden = true
        
        chatNotifBadge.layer.cornerRadius = self.chatNotifBadge.frame.size.height/2
        chatNotifBadge.clipsToBounds = true
        
        
        
        homeTableVw.dataSource = self
        homeTableVw.delegate = self
        self.navigationController?.navigationBarHidden = true
         NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(chatRecieveNotification), name: "chatRecieveNotification", object: nil)
    }
    }
//    MARK: - post service
    func tokenGetHit(){
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            var token =  NSString()
            if(UIDevice.isSimulator == true){
                token = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
            }else{
                token = defaults.valueForKey("deviceToken") as! NSString
            }
            serverWho = 1
            self.doRequestPost("\(Header.BASE_URL)users/LoginLogout", data: ["DeviceToken":"\(token)","DeviceType":"ios","UserId":"\(userId)"])
        }
    }
    
    func updateChatBadge() -> Void {
        
        if(UIApplication.sharedApplication().applicationIconBadgeNumber > 0)
        {
            self.chatNotifBadge.hidden = false
        }
        else
        {
            self.chatNotifBadge.hidden = true
        }
    }
    
    func chatRecieveNotification(){
        if(appdelegate.openMsgFlag == false){
            if(self.navigationController?.viewControllers.count > 2){
                self.navigationController?.popToViewController(self.navigationController?.viewControllers[2] as! ViewController, animated:false)
            }
        }
        else{
           appdelegate.openMsgFlag = false
        }
      
        self.messageListAc(self)
    }
    func doRequestPost(url:String,data:[String: NSObject]){
        print("URL FOR DEVICETOKEN ==> \(url)")
        let requestDic = data
        print(requestDic)
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        
        print("Request Object:\(data)")
        print("Request string = \(jsonString!)")
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                print("Succes:")
                let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                print(responceString)
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
                dispatch_async(dispatch_get_main_queue()) {
                   
                    if(self.serverWho == 1){
                     let drType = responseDict.valueForKey("DrType") as! NSString
                         //let drType:String = "MedicinePrescribers"
                    let drTypeDefault = NSUserDefaults.standardUserDefaults().valueForKey("drType") as! String
                     let status = responseDict.valueForKey("userStatus") as! NSInteger
                    if(status == 0){
                        let defaults = NSUserDefaults.standardUserDefaults()
                        if let userId = defaults.valueForKey("id") {
                            let token = defaults.valueForKey("deviceToken") as! NSString
                            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                            self.serverWho = 2
                            self.logoutCon = true
                            self.doRequestPost("\(Header.BASE_URL)users/logout", data: ["UserId":"\(userId)","DeviceToken":"\(token)"])
                            return
                        }
                    }
                    if(drType != drTypeDefault){
                        let defaults = NSUserDefaults.standardUserDefaults()
                        if let userId = defaults.valueForKey("id") {
                            let token = defaults.valueForKey("deviceToken") as! NSString
                            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                            self.serverWho = 2
                            self.logoutDrtype = true
                            self.doRequestPost("\(Header.BASE_URL)users/logout", data: ["UserId":"\(userId)","DeviceToken":"\(token)"])
                            return
                        }
                     }
                    }else{
                        let defaults = NSUserDefaults.standardUserDefaults()
                        defaults.removeObjectForKey("id")
                        defaults.removeObjectForKey("usertype")
                        defaults.removeObjectForKey("firstName")
                        defaults.removeObjectForKey("drType")
                        var storyboard = UIStoryboard()
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            storyboard = UIStoryboard(name: "Main", bundle: nil)
                        }else{
                            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                        }
                        try! self.realm.write({
                            self.realm.deleteAll()
                        })
                        let vc = storyboard.instantiateViewControllerWithIdentifier("mainViewController") as! ViewController
                        if(self.logoutCon == true){
                            let alert = UIAlertController(title: "Tik Tok Doc", message: "Your account has been deleted or deactivated, please contact our support for more information.", preferredStyle: UIAlertControllerStyle.Alert)
                            let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                                UIAlertAction in
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            alert.addAction(cancelAction)
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                        if(self.logoutDrtype == true){
                                let alert = UIAlertController(title: "Tik Tok Doc", message: "You do not have privileges to prescribe medications. If this is a mistake, please contact Tik Tok Doc at 702.626.8200.", preferredStyle: UIAlertControllerStyle.Alert)
                                let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                                    UIAlertAction in
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                                alert.addAction(cancelAction)
                                self.presentViewController(alert, animated: true, completion: nil)
                        }
                        
                    }
                     AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                }
                
              }
            }
        
        dataTask.resume()
    }
    
//    MARK: Functionality
    override func viewWillAppear(animated: Bool) {
        self.tokenGetHit()
         AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        let defaults = NSUserDefaults.standardUserDefaults()
        let imgUrl:String = defaults.valueForKey("profilePic") as! String
        if let url = NSURL(string: "http://\(imgUrl)") {
            profilePicImgVw.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        let firstname:String = defaults.valueForKey("firstName") as! String
        let lastname:String = defaults.valueForKey("lastName") as! String
        let special:String = defaults.valueForKey("Speciality") as! String
        let qual:String = defaults.valueForKey("qualification") as! String
        drName.text = "Dr. \(firstname) \(lastname),\(qual)"
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: "HomeStethoscope")
        let offsetY: CGFloat = -8.0
        attachment.bounds = CGRectMake(0, offsetY, attachment.image!.size.width, attachment.image!.size.height)
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string:"")
        myString.appendAttributedString(attachmentString)
        let myString1 = NSMutableAttributedString(string:" \(special)")
        myString.appendAttributedString(myString1)
        speciality.textAlignment = .Center
        speciality.attributedText = myString
       
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let params = "UserId=\(userId)&Type=1"
            self.serverInt = 1
            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/notificationCount")
        }
        
        
        timer = NSTimer.scheduledTimerWithTimeInterval(90.0, target: self, selector: #selector(PatientHomeViewController().apiHit), userInfo: nil, repeats: true)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DoctorHomeViewController.drStatusFunc), name: "Dr notification status", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updateChatBadge), name: "ChatBadgeNotif", object: nil)
        if let msgCount =  NSUserDefaults.standardUserDefaults().integerForKey("msgCount") as? Int{
        if(msgCount == 1){
           self.chatNotifBadge.hidden = false
        }else{
          self.chatNotifBadge.hidden = true
        }
        }
        
    }
    func drStatusFunc(){
        self.apiHit()
    }
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        self.navigationController?.navigationBarHidden = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(chatRecieveNotification), name: "chatRecieveNotification", object: nil)
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        dispatch_async(dispatch_get_main_queue()){
            if(self.serverInt == 2){
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("id")
                defaults.removeObjectForKey("usertype")
                defaults.removeObjectForKey("firstName")
                 defaults.removeObjectForKey("drType")
                 defaults.removeObjectForKey("msgCount")
                var storyboard = UIStoryboard()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                 storyboard = UIStoryboard(name: "Main", bundle: nil)
                }else{
                 storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                }
                try! self.realm.write({
                  self.realm.deleteAll()
                })
                let vc = storyboard.instantiateViewControllerWithIdentifier("mainViewController") as! ViewController
                if(self.logoutCon == true){
                    let alert = UIAlertController(title: "Tik Tok Doc", message: "Your account has been deleted or deactivated, please contact our support for more information.", preferredStyle: UIAlertControllerStyle.Alert)
                    let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                      self.navigationController?.pushViewController(vc, animated: true)
                    }
                    alert.addAction(cancelAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                else{
                 self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else if( self.serverInt == 3){
                UIApplication.sharedApplication().cancelAllLocalNotifications()
                if(responseDict.valueForKey("data")!.count != 0){
                    let data = responseDict.valueForKey("data") as! NSArray
                    for i in 0...data.count-1{
                        let schSeq = data[i] as! NSDictionary
                        let schF = schSeq.valueForKey("From") as! NSString
                        let schD = schSeq.valueForKey("Schedule") as! NSString
                        let schT = schSeq.valueForKey("To") as! NSString
                        var minutes = Double()
                        let dateFormatter = NSDateFormatter()
                         dateFormatter.dateFormat = "dd-MMMM-yyyy ,HH:mm"
                        let timeZone = NSTimeZone(name: "GMT-8")
                        dateFormatter.timeZone=timeZone
                        let schDate = dateFormatter.dateFromString("\(schD) ,\(schF)")!
                        let notification = UILocalNotification()
                        notification.fireDate = schDate
                        notification.alertBody = "You have a scheduled video visit with your Patient in 5 minutes. Please log into Tik Tok Doc App"
                        notification.timeZone = timeZone
                        notification.alertAction = "be awesome!"
                        notification.soundName = "iphonenoti_cRjTITC7.wav"
                        notification.userInfo = ["Status":"DoctorLocal"]
                        UIApplication.sharedApplication().scheduleLocalNotification(notification)
                        
                        dateFormatter.dateFormat = "HH:mm" 
                        let timeTo = dateFormatter.dateFromString("\(schT)")!
                        let timeFrom = dateFormatter.dateFromString("\(schF)")!
                        let plusTimeFrom = timeFrom.dateByAddingTimeInterval(60*5)
                         let plusString = dateFormatter.stringFromDate(plusTimeFrom)
                        dateFormatter.dateFormat = "hh:mm a"
                       let plusFromString = dateFormatter.stringFromDate(plusTimeFrom)
                        let plustoStr = dateFormatter.stringFromDate(timeTo)
                        let finalTo = dateFormatter.dateFromString(plustoStr)
                        let finalfrom = dateFormatter.dateFromString(plusFromString)
                        let timeI =  (finalTo!.timeIntervalSinceDate(finalfrom!))
                        let i = Int(timeI)
                        let minutesSlot = i/60
                        minutes = Double(minutesSlot)

                        dateFormatter.dateFormat = "dd-MMMM-yyyy ,HH:mm"
                        let schDate2 = dateFormatter.dateFromString("\(schD) ,\(plusString)")!
                        let store = EKEventStore()
                        let event = EKEvent(eventStore: store)
                        let startDate=schDate2
                        let endDate=startDate.dateByAddingTimeInterval(minutes*60)
                        let predicate2 = store.predicateForEventsWithStartDate(startDate, endDate: endDate, calendars: nil)
                        
                        let eV = store.eventsMatchingPredicate(predicate2) as [EKEvent]!
                        
                        if eV != nil {
                            for i in eV {
                                print("Title  \(i.title)" )
                                print("stareDate: \(i.startDate)" )
                                print("endDate: \(i.endDate)" )
                                do{
                                    (try store.removeEvent(i, span: EKSpan.ThisEvent, commit: true))
                                }
                                catch let error {
                                }
                                
                            }
                        }
                        store.requestAccessToEntityType(.Event) {(granted, error) in
                            if !granted {
                                return
                            }
                            event.title = "Tik Tok Doc Video Visit with your Doctor."
                            event.startDate = schDate2 //today
                            event.endDate = event.startDate.dateByAddingTimeInterval(minutes*60) //1 hour long meeting
                            event.calendar = store.defaultCalendarForNewEvents
                            do {
                                try store.saveEvent(event, span: .ThisEvent, commit: true)
                                let savedEventId = event.eventIdentifier //save event id to access this particular event later
                                print(savedEventId)
                            } catch {
                                // Display error to user
                            }
                        }
                    }
                }
            }
            else if(self.serverInt == 4){
                let status = responseDict.valueForKey("Status") as! NSInteger
                if(status == 200){
                var storyboard = UIStoryboard()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                 storyboard = UIStoryboard(name: "Main", bundle: nil)
                }
                else{
                  storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                }
                let mainViewController = storyboard.instantiateViewControllerWithIdentifier("PrescriptionViewController") as! PrescriptionViewController
                mainViewController.pendingPrescriptionCount = responseDict.valueForKey("RefillRequestsCount") as! NSString
                mainViewController.transmissionErrCount = responseDict.valueForKey("TransactionErrorsCount") as! NSString
                mainViewController.nsurlData = responseDict.valueForKey("url") as! NSString
                let leftViewController = storyboard.instantiateViewControllerWithIdentifier("DoctorSideBarViewController") as! DoctorSideBarViewController
                let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                leftViewController.mainViewController = nvc
                let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
                slideMenuController.automaticallyAdjustsScrollViewInsets = true
                slideMenuController.delegate = mainViewController
                self.navigationController?.pushViewController(slideMenuController, animated: true)
                }
                else if(status == 201){
                   AppManager.sharedManager.Showalert("Alert", alertmessage: "value not found")
                }else if(status == 301){
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "user id wrong")
  
                }else{
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "dr id empty")
                }
            }
            else{
                self.count = responseDict.valueForKey("count") as! NSInteger
                self.pndingPatCount = responseDict.valueForKey("requestcount") as! NSInteger
                self.schCount = responseDict.valueForKey("scheduleCountAppointment") as! NSInteger
                let msgCount = responseDict.valueForKey("Messagedotshow") as! NSInteger
                if(msgCount == 1){
                    self.chatNotifBadge.hidden = false
                }else{
                    self.chatNotifBadge.hidden = true
                }
                
                if(self.count == 0){
                    self.notifiLbl.hidden = true
                }else{
                    self.notifiLbl.hidden = false
                    self.notifiLbl.text = "\(self.count)"
                }
                dispatch_async(dispatch_get_main_queue()){
                    self.homeTableVw.reloadData()
                    let defaults = NSUserDefaults.standardUserDefaults()
                    if let userId = defaults.valueForKey("id") {
                        AppManager.sharedManager.delegate=self
                        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                        let params = "Dr_Id=\(userId)"
                        self.serverInt = 3
                        AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrAppointmentDate")
                    }
                }
            }
             AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        }
       
    }
    func apiHit(){
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let params = "UserId=\(userId)&Type=1"
            serverInt = 1
            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/notificationCount")
        }
    }
   
    @IBAction func logoutAction(sender: AnyObject) {
        timer.invalidate()
        let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To Logout?", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            NSLog("OK Pressed")
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("id") {
                var token =  NSString()
                if(UIDevice.isSimulator == true){
                 token = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
                }else{
                   token = defaults.valueForKey("deviceToken") as! NSString
                }
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Log Out")
                let params = "UserId=\(userId)&DeviceToken=\(token)"
                self.serverInt = 2
                self.logoutCon = false
                AppManager.sharedManager.postDataOnserver(params, postUrl: "users/logout")
            }
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
  
    }
    @IBAction func myProfileAc(sender: AnyObject) {
        timer.invalidate()
       // let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("MyProfilePhViewController") as! MyProfilePhViewController
            let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("DoctorSideBarViewController") as! DoctorSideBarViewController
            let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
            leftViewController.mainViewController = nvc
            let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            slideMenuController.delegate = mainViewController
            self.navigationController?.pushViewController(slideMenuController, animated: true)
        }else{
      let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("DocMyProfileViewController") as! DocMyProfileViewController
      let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("DoctorSideBarViewController") as! DoctorSideBarViewController
      let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
      leftViewController.mainViewController = nvc
      let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
      slideMenuController.automaticallyAdjustsScrollViewInsets = true
      slideMenuController.delegate = mainViewController
      self.navigationController?.pushViewController(slideMenuController, animated: true)
        }
    }
    @IBAction func contactAc(sender: AnyObject){
        timer.invalidate()
        let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("ContactUsViewController") as! ContactUsViewController
        mainViewController.contactSwich = true
        let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("DoctorSideBarViewController") as! DoctorSideBarViewController
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        leftViewController.mainViewController = nvc
        let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        self.navigationController?.pushViewController(slideMenuController, animated: true)
    }
     @IBAction func myStaffAc(sender: AnyObject){
        timer.invalidate()
        var storyboard = UIStoryboard()
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        }else{
            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
        }
        let vc = storyboard.instantiateViewControllerWithIdentifier("StaffListingViewController") as! StaffListingViewController
        vc.stffListWho = false
        vc.doctorItself = true
        let defaults = NSUserDefaults.standardUserDefaults()
        let firstname:String = defaults.valueForKey("firstName") as! String
        let lastname:String = defaults.valueForKey("lastName") as! String
        vc.drName = "Dr. \(firstname)"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func notificationAc(sender: AnyObject){
        timer.invalidate()
        let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("DocNotificationViewController") as! DocNotificationViewController
        mainViewController.docNotiBool = false
        let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("DoctorSideBarViewController") as! DoctorSideBarViewController
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        leftViewController.mainViewController = nvc
        let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        self.navigationController?.pushViewController(slideMenuController, animated: true)
    }
    @IBAction func messageListAc(sender: AnyObject){
        timer.invalidate()
        let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("MessageListingViewController") as! MessageListingViewController
        let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("DoctorSideBarViewController") as! DoctorSideBarViewController
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        leftViewController.mainViewController = nvc
        let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        self.navigationController?.pushViewController(slideMenuController, animated: true)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(tableVwRow)
        return tableVwRow
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
         let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        let imageView: UIView = (cell?.viewWithTag(9111))!
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Phone:
            imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
            imageView.layer.shadowOffset = CGSizeMake(10, 5);
            imageView.layer.shadowRadius = 0.5;
            imageView.layer.shadowOpacity = 0.06;
            imageView.layer.masksToBounds = false;
            imageView.layer.borderWidth = 0.5
        case .Pad:
            imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
            imageView.layer.shadowOffset = CGSizeMake(7, 3);
            imageView.layer.shadowRadius = 0.5;
            imageView.layer.shadowOpacity = 0.02;
            imageView.layer.masksToBounds = false;
            imageView.layer.borderWidth = 0.5
        default:
            break
        }

        if(indexPath.row == 0){
            imageView.backgroundColor = UIColor.init(red: 10/255, green: 187/255, blue: 167/255, alpha: 1)
        }
        else if(indexPath.row == 1){
            imageView.backgroundColor = UIColor.init(red: 0/255, green: 174/255, blue: 153/255, alpha: 1)
        }
        else if(indexPath.row == 2){
            imageView.backgroundColor = UIColor.init(red: 55/255, green: 132/255, blue: 143/255, alpha: 1)
        }
        else{
          imageView.backgroundColor = UIColor.init(red: 76/255, green: 109/255, blue: 132/255, alpha: 1)
        }
        let img: UIImageView = (cell!.viewWithTag(9112) as! UIImageView)
        img.image = UIImage(named:arMenuImages[indexPath.row])
        let valueLbl: UILabel = (cell!.viewWithTag(9113) as! UILabel)
        valueLbl.text = arMenu[indexPath.row]
        
        if(tableVwRow == 3){
            if(indexPath.row == 3 || indexPath.row == 2){
                let lineLbl1: UILabel = (cell!.viewWithTag(1920) as! UILabel)
                lineLbl1.backgroundColor = UIColor.clearColor()
                
                let lineLbl2: UILabel = (cell!.viewWithTag(1921) as! UILabel)
                lineLbl2.backgroundColor = UIColor.clearColor()
            }
            else{
                let lineLbl1: UILabel = (cell!.viewWithTag(1920) as! UILabel)
                lineLbl1.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
                
                let lineLbl2: UILabel = (cell!.viewWithTag(1921) as! UILabel)
                lineLbl2.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
            }
            
            if(indexPath.row == 0){
                let lineLbl1: UILabel = (cell!.viewWithTag(1922) as! UILabel)
                lineLbl1.backgroundColor = UIColor.clearColor()
                
                let lineLbl2: UILabel = (cell!.viewWithTag(1923) as! UILabel)
                lineLbl2.backgroundColor = UIColor.clearColor()
            }
            else if(indexPath.row == 1){
                let lineLbl1: UILabel = (cell!.viewWithTag(1922) as! UILabel)
                lineLbl1.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
                
                let lineLbl2: UILabel = (cell!.viewWithTag(1923) as! UILabel)
                lineLbl2.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
            }
        }
        else{
            if(indexPath.row == 3){
            let lineLbl1: UILabel = (cell!.viewWithTag(1920) as! UILabel)
            lineLbl1.backgroundColor = UIColor.clearColor()
            
            let lineLbl2: UILabel = (cell!.viewWithTag(1921) as! UILabel)
            lineLbl2.backgroundColor = UIColor.clearColor()
          }
          else{
            let lineLbl1: UILabel = (cell!.viewWithTag(1920) as! UILabel)
            lineLbl1.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
            
            let lineLbl2: UILabel = (cell!.viewWithTag(1921) as! UILabel)
            lineLbl2.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
          }
        
           if(indexPath.row == 0){
            let lineLbl1: UILabel = (cell!.viewWithTag(1922) as! UILabel)
            lineLbl1.backgroundColor = UIColor.clearColor()
            
            let lineLbl2: UILabel = (cell!.viewWithTag(1923) as! UILabel)
            lineLbl2.backgroundColor = UIColor.clearColor()
           }
           else{
            let lineLbl1: UILabel = (cell!.viewWithTag(1922) as! UILabel)
            lineLbl1.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
            
            let lineLbl2: UILabel = (cell!.viewWithTag(1923) as! UILabel)
            lineLbl2.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
          }
         }
        let countLabel: UILabel = (cell!.viewWithTag(9114) as! UILabel)
        countLabel.layer.cornerRadius = countLabel.frame.size.height/2
        countLabel.clipsToBounds = true
        if(indexPath.row == 2){
            if(pndingPatCount != 0){
                    countLabel.text = "\(pndingPatCount)"
                    countLabel.backgroundColor = UIColor.redColor()
                   countLabel.textColor = UIColor.whiteColor()
            }
            else{
                    countLabel.text = ""
                    countLabel.backgroundColor = UIColor.clearColor()
                    countLabel.textColor = UIColor.clearColor()
                }
                
            }
        if(indexPath.row == 0){
            if(schCount != 0){
                countLabel.text = "\(schCount)"
                countLabel.backgroundColor = UIColor.redColor()
                countLabel.textColor = UIColor.whiteColor()
            }
            else{
                countLabel.text = ""
                countLabel.backgroundColor = UIColor.clearColor()
                countLabel.textColor = UIColor.clearColor()
            }
        }
        return cell! as UITableViewCell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Phone:
            if(self.view.frame.size.height < 600){
                return homeTableVw.frame.size.height*0.25
            }else{
                return homeTableVw.frame.size.height * 0.23
            }
        case .Pad:
            return homeTableVw.frame.size.height * 0.22
        default:
            break
        }
        return 0
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        if(indexPath.row == 0){
            timer.invalidate()
                let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("DoctorMyAppointmentsViewController") as! DoctorMyAppointmentsViewController
                let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("DoctorSideBarViewController") as! DoctorSideBarViewController
                let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                leftViewController.mainViewController = nvc
                let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
                slideMenuController.automaticallyAdjustsScrollViewInsets = true
                slideMenuController.delegate = mainViewController
                self.navigationController?.pushViewController(slideMenuController, animated: true)
        }else if(indexPath.row == 1){
            timer.invalidate()
                let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("TabBar") as! TabBarViewController
                let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("DoctorSideBarViewController") as! DoctorSideBarViewController
                let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                 nvc.navigationBarHidden = true
                leftViewController.mainViewController = nvc
                let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
                slideMenuController.automaticallyAdjustsScrollViewInsets = true
                slideMenuController.delegate = mainViewController
                self.navigationController?.pushViewController(slideMenuController, animated: true)
        }else if(indexPath.row == 2){
            timer.invalidate()
                let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("DocMyPateintViewController") as! DocMyPateintViewController
                let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("DoctorSideBarViewController") as! DoctorSideBarViewController
                let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                leftViewController.mainViewController = nvc
                let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
                slideMenuController.automaticallyAdjustsScrollViewInsets = true
                slideMenuController.delegate = mainViewController
                self.navigationController?.pushViewController(slideMenuController, animated: true)
        }else if(indexPath.row == 3){
            timer.invalidate()
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("id") {
                let params = "DrId=\(userId)"
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                serverInt = 4
                AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/soap")
            }
        }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    
    @IBAction func searchDocAc(sender: AnyObject){
        timer.invalidate()
        let vc = storyboard!.instantiateViewControllerWithIdentifier("SearchDoctorViewController") as! SearchDoctorViewController
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "searchDoc")
        vc.sndRequ = false
        vc.searchWho = "doctor"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func joinVideoAc(sender: AnyObject) {
        timer.invalidate()
        let vc = storyboard!.instantiateViewControllerWithIdentifier("VideoCallLinkViewController") as! VideoCallLinkViewController
        vc.user = "Doctor"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}
extension UIDevice {
    static var isSimulator: Bool {
        return NSProcessInfo.processInfo().environment["SIMULATOR_DEVICE_NAME"] != nil
    }
}
