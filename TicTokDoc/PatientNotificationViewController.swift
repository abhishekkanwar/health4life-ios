

import UIKit
import SlideMenuControllerSwift
class PatientNotificationViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,WebServiceDelegate {
    var imgAr = ["DocImgLogo","DocImgLogo","DocImgLogo"]
    var notificationCount = NSInteger()
    var nameAr = NSMutableArray()
    var valueAr = NSMutableArray()
    var timeAr = NSMutableArray()
    var profilrpicAr = NSMutableArray()
    var serverInt = NSInteger()
    var notiBool = Bool()
    @IBOutlet weak var patientNotification: UITableView!
    @IBOutlet weak var patientNotificationImgVw: UIImageView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var sideBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = NSUserDefaults.standardUserDefaults()
         let userId = defaults.valueForKey("id") as! NSString
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        let params = "PatientId=\(userId)"
        serverInt = 1
        AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/Notification")
        patientNotification.delegate = self
        patientNotification.dataSource = self
     self.navigationController?.navigationBarHidden = true
        if(notiBool == true){
          sideBtn.hidden = true
          backBtn.setBackgroundImage(UIImage(named: "NotiHomeBtn"), forState: .Normal)
        }else{
          sideBtn.hidden = false
          backBtn.setBackgroundImage(UIImage(named: "DocloginBackBtn"), forState: .Normal)
        }
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        if(serverInt == 1){
         if(responseDict.objectForKey("data")?.count != 0){
        let data = responseDict.objectForKey("data") as! NSArray
        notificationCount = data.count
        dispatch_async(dispatch_get_main_queue()) {
            for i in 0...data.count-1 {
                let d = data[i]
                let firstName = d.valueForKeyPath("DrDeatils.FirstName")! as! NSString
                let lastName = d.valueForKeyPath("DrDeatils.LastName")! as! NSString
                let fullName = "\(firstName) \(lastName)" as NSString
                let time = d.valueForKey("DaysLeft") as! NSString
                let trimNameString = fullName.stringByTrimmingCharactersInSet(
                    NSCharacterSet.whitespaceAndNewlineCharacterSet());
                self.nameAr.addObject(trimNameString)
                self.timeAr.addObject(time)
                self.profilrpicAr.addObject(d.valueForKey("profile")! as! NSString)
                self.valueAr.addObject(d.valueForKey("Message")! as! NSString)
            }
            self.patientNotification.reloadData()
            let defaults = NSUserDefaults.standardUserDefaults()
            let userId = defaults.valueForKey("id") as! NSString
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
            let params1 = "UserId=\(userId)&Type=0"
            self.serverInt = 2
            AppManager.sharedManager.postDataOnserver(params1, postUrl: "users/notificationread")
        }
         }else{
            dispatch_async(dispatch_get_main_queue()) {
                self.patientNotificationImgVw.image = UIImage(named:"PndingNot")
                self.patientNotification.hidden = true
            }
        }
        }else{
            
        }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationCount
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        cell!.layer.cornerRadius = 7.0
        cell!.layer.masksToBounds = true
        
        let imageView: UIView = (cell?.viewWithTag(720))!
        imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
        imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
        imageView.layer.shadowOffset = CGSizeMake(10, 5);
        imageView.layer.shadowRadius = 1;
        imageView.layer.shadowOpacity = 0.06;
        imageView.layer.masksToBounds = false;
        imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        imageView.layer.borderWidth = 0.5
        if(indexPath.row%2 == 0){
            imageView.backgroundColor = UIColor.whiteColor()
        }else{
            imageView.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
        }
        let itemImg: UIImageView = (cell!.viewWithTag(714) as! UIImageView)
        itemImg.layer.cornerRadius = itemImg.frame.size.height/2
        itemImg.layer.borderWidth = self.view.frame.size.height*0.008
        itemImg.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        if let url = NSURL(string: "http://\(profilrpicAr[indexPath.row])") {
            itemImg.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        let nameLbl: UILabel = (cell!.viewWithTag(715) as! UILabel)
        nameLbl.text = nameAr[indexPath.row] as? String
        let timeLbl: UILabel = (cell!.viewWithTag(717) as! UILabel)
        timeLbl.text = timeAr[indexPath.row] as? String
        let notiLbl: UILabel = (cell!.viewWithTag(716) as! UILabel)
        notiLbl.text = valueAr[indexPath.row] as? String
        return cell!
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return self.view.frame.size.width*0.1
    }

    @IBAction func backBtnAc(sender: AnyObject){
        if(notiBool == false){
           self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
        }else{
            //let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window = UIWindow(frame: UIScreen.mainScreen().bounds)
            let vc = storyboard!.instantiateViewControllerWithIdentifier("PatientHomeViewController") as! PatientHomeViewController
            let nav = UINavigationController(rootViewController: vc)
            appDelegate.window?.rootViewController = nav
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension PatientNotificationViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
