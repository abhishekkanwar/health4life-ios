

import UIKit
import SlideMenuControllerSwift
import RealmSwift
class PatientHomeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,WebServiceDelegate{
    @IBOutlet weak var patName: UILabel!
    @IBOutlet weak var profilePicImgVw: UIImageView!
    @IBOutlet weak var patAddress: UILabel!
    @IBOutlet weak var homeTableVw: UITableView!
    @IBOutlet weak var notiLbl: UILabel!
    
    @IBOutlet weak var chatNotifBadge : UILabel!
    
    var serverInt = NSInteger()
    var count = NSInteger()
    var schCount = NSInteger()
    var timer = NSTimer()
    var logoutCon = Bool()
    var arMenu = ["My Appointments","Self Schedule","My Doctors","Doctor's Staff"]
    var arMenuImages = ["HomeClock", "HomeCalender","HomeDoc","HomeStaffIcon"]
    let realm = try! Realm()
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()

        profilePicImgVw.layer.cornerRadius = self.profilePicImgVw.frame.size.height/2
        profilePicImgVw.layer.borderWidth = self.view.frame.size.height*0.012
        profilePicImgVw.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        
        profilePicImgVw.clipsToBounds = true
        profilePicImgVw.setShowActivityIndicatorView(true)
        profilePicImgVw.setIndicatorStyle(.Gray)
        let defaults = NSUserDefaults.standardUserDefaults()
        let imgUrl:String = defaults.valueForKey("profilePic") as! String
        if let url1 = NSURL(string: "http://\(imgUrl)"),
            let withoutExt = url1.URLByDeletingPathExtension,
            let name = withoutExt.lastPathComponent {
            let result = name.substringFromIndex(name.startIndex.advancedBy(5))
            print(result)
        }
        if let url = NSURL(string: "http://\(imgUrl)") {
            profilePicImgVw.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
            // AppManager.sharedManager.setProfileImage(profilePicImgVw.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png")), onImageView: profilePicImgVw.image)
        }
        
        notiLbl.layer.cornerRadius = self.notiLbl.frame.size.height/2
        notiLbl.clipsToBounds = true
        self.notiLbl.hidden = true
        
        chatNotifBadge.layer.cornerRadius = self.chatNotifBadge.frame.size.height/2
        chatNotifBadge.clipsToBounds = true
        
        homeTableVw.delegate = self
        homeTableVw.dataSource = self
        self.navigationController?.navigationBarHidden = true
//        if(self.view.frame.size.height > 600){
//            let footerView = UIView()
//            footerView.frame = CGRectMake(10, 0, self.view.frame.size.width, self.view.frame.size.height*0.14)
//            let iconImg = UIImageView()
//            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
//                iconImg.frame = CGRectMake(self.view.frame.size.width/2-self.view.frame.size.width*0.1,footerView.frame.size.height*0.06, self.view.frame.size.width*0.2,self.view.frame.size.width*0.2)
//            }else{
//                iconImg.frame = CGRectMake(self.view.frame.size.width/2-footerView.frame.size.width*0.06,footerView.frame.size.height*0.08, footerView.frame.size.width*0.12,footerView.frame.size.width*0.12)
//            }
//            iconImg.image = UIImage(named: "HomeIcon")
//            iconImg.contentMode = .ScaleAspectFit
//            footerView.addSubview(iconImg)
//            homeTableVw.tableFooterView = footerView
//        }
        self.navigationController?.navigationBarHidden = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(chatRecieveNotification), name: "chatRecieveNotification", object: nil)
        self.tokenGetHit()
    }
    
    
    func updateChatBadge() -> Void {
        
        if(UIApplication.sharedApplication().applicationIconBadgeNumber > 0)
        {
            self.chatNotifBadge.hidden = false
        }
        else
        {
            self.chatNotifBadge.hidden = true
        }
    }
    
    
    func chatRecieveNotification(){
        
        if(self.navigationController?.viewControllers.count > 2)
        {
            self.navigationController?.popToViewController(self.navigationController?.viewControllers[1] as! ViewController, animated:false)
        }
        
        self.msgListingAc(self)
    }
    func tokenGetHit(){
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            var token =  NSString()
            if(UIDevice.isSimulator == true){
                token = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
            }else{
                token = defaults.valueForKey("deviceToken") as! NSString
            }
            self.doRequestPost("\(Header.BASE_URL)users/LoginLogout", data: ["DeviceToken":"\(token)","DeviceType":"ios","UserId":"\(userId)"])
        }
    }
    func doRequestPost(url:String,data:[String: NSObject]){
        print("URL FOR DEVICETOKEN ==> \(url)")
        let requestDic = data
        print(requestDic)
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        
        print("Request Object:\(data)")
        print("Request string = \(jsonString!)")
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                print("Succes:")
                let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                print(responceString)
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                dispatch_async(dispatch_get_main_queue()) {
                    let status = responseDict.valueForKey("userStatus") as! NSInteger
                    if(status == 0){
                        let defaults = NSUserDefaults.standardUserDefaults()
                        if let userId = defaults.valueForKey("id") {
                            let token = defaults.valueForKey("deviceToken") as! NSString
                            AppManager.sharedManager.delegate=self
                            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "")
                            let params = "UserId=\(userId)&DeviceToken=\(token)"
                            self.logoutCon = true
                            self.serverInt = 1
                            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/logout")
                        }
                    }
                }
            }
        }
        
        dataTask.resume()
    }
    //    patient notification status
    override func viewWillAppear(animated: Bool) {
        self.tokenGetHit()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PatientHomeViewController.timerInvalidate), name: "timerInvalidate", object: nil)
        let defaults = NSUserDefaults.standardUserDefaults()
        let firstname:String = defaults.valueForKey("firstName") as! String
        let lastname:String = defaults.valueForKey("lastName") as! String
        let add:String = defaults.valueForKey("address") as! String
        let imgUrl:String = defaults.valueForKey("profilePic") as! String
        //        if let url1 = NSURL(string: "http://\(imgUrl)"),
        //            let withoutExt = url1.URLByDeletingPathExtension,
        //            let name = withoutExt.lastPathComponent {
        //            let result = name.substringFromIndex(name.startIndex.advancedBy(5))
        //            print(result)
        //             AppManager.sharedManager.setProfileImage(UIImage(named:"\(result).png")!, onImageView: profilePicImgVw)
        //        }
        if let url = NSURL(string: "http://\(imgUrl)") {
            profilePicImgVw.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
            
        }
        
        
        
        patName.text = "\(firstname) \(lastname)"
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: "cityState")
        let offsetY: CGFloat = -8.0
        attachment.bounds = CGRectMake(-1, offsetY, attachment.image!.size.width, attachment.image!.size.height)
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string:"")
        myString.appendAttributedString(attachmentString)
        let myString1 = NSMutableAttributedString(string:add)
        myString.appendAttributedString(myString1)
        patAddress.textAlignment = .Center
        patAddress.attributedText = myString
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let params = "UserId=\(userId)&Type=0"
            serverInt = 3
            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/notificationCount")
        }
        timer = NSTimer.scheduledTimerWithTimeInterval(90.0, target: self, selector: #selector(PatientHomeViewController().apiHit), userInfo: nil, repeats: true)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PatientHomeViewController.ptStatusFunc), name: "patient notification status", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updateChatBadge), name: "ChatBadgeNotif", object: nil)
        if let msgCount =  NSUserDefaults.standardUserDefaults().integerForKey("msgCount") as? Int{
            if(msgCount == 1){
                self.chatNotifBadge.hidden = false
            }else{
                self.chatNotifBadge.hidden = true
            }
        }
        //self.updateChatBadge()
        
    }
    func ptStatusFunc(){
        self.apiHit()
    }
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        self.navigationController?.navigationBarHidden = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(chatRecieveNotification), name: "chatRecieveNotification", object: nil)
    }
    func timerInvalidate(){
        timer.invalidate()
    }
    func apiHit(){
        if(appDel.pushCountFlag == false){
        appDel.pushCountFlag = true
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            let params = "UserId=\(userId)&Type=0"
            print(params)
            serverInt = 3
            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/notificationCount")
        }
     }
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        if(serverInt == 1){
            dispatch_async(dispatch_get_main_queue()){
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("id")
                defaults.removeObjectForKey("searchDoc")
                defaults.removeObjectForKey("selectPharma")
                defaults.removeObjectForKey("usertype")
                defaults.removeObjectForKey("msgCount")
                try! self.realm.write({
                    self.realm.deleteAll()
                })
                var storyboard = UIStoryboard()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                }else{
                    storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                }
                let vc = storyboard.instantiateViewControllerWithIdentifier("mainViewController") as! ViewController
                if(self.logoutCon == true){
                    let alert = UIAlertController(title: "Tik Tok Doc", message: "Your account has been deleted or deactivated, please contact our support for more information.", preferredStyle: UIAlertControllerStyle.Alert)
                    let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    alert.addAction(cancelAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }else{
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }else if(serverInt == 3){
            dispatch_async(dispatch_get_main_queue()){
                self.count = responseDict.valueForKey("count") as! NSInteger
                self.schCount = responseDict.valueForKey("scheduleCountAppointment") as! NSInteger
                let msgCount = responseDict.valueForKey("Messagedotshow") as! NSInteger
                if(msgCount == 1){
                    self.chatNotifBadge.hidden = false
                }else{
                    self.chatNotifBadge.hidden = true
                }
            
                if(self.count == 0){
                    self.notiLbl.hidden = true
                }else{
                    self.notiLbl.hidden = false
                    self.notiLbl.text = "\(self.count)"
                }
                self.homeTableVw.reloadData()
                let defaults = NSUserDefaults.standardUserDefaults()
                if let userId = defaults.valueForKey("id") {
                    AppManager.sharedManager.delegate=self
                    AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                    let params = "PatientId=\(userId)"
                    self.serverInt = 4
                    AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/patientpushlist")
                }
            }
        }
        else if(serverInt == 4){
            dispatch_async(dispatch_get_main_queue()){
                self.appDel.pushCountFlag = false
                UIApplication.sharedApplication().cancelAllLocalNotifications()
                if(responseDict.valueForKey("records")!.count != 0){
                    let records = responseDict.valueForKey("records") as! NSArray
                    for record in 0...records.count-1 {
                        let schSeq = records[record] as! NSDictionary
                        let schT = schSeq.valueForKey("From") as! NSString
                        let schD = schSeq.valueForKey("Schedule") as! NSString
                        
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "dd-MMMM-yyyy ,HH:mm"
                        let timeZone = NSTimeZone(name: "GMT-8")
                        dateFormatter.timeZone=timeZone
                        let schDate = dateFormatter.dateFromString("\(schD) ,\(schT)")!
                        let notification = UILocalNotification()
                        notification.fireDate = schDate
                        notification.alertBody = "You have a scheduled video visit with your Doctor in 15 minutes. Please log into Tik Tok Doc App"
                        notification.timeZone = timeZone
                        notification.alertAction = "be awesome!"
                        notification.soundName = "iphonenoti_cRjTITC7.wav"
                        notification.userInfo = ["Status":"PatientLocal"]
                        UIApplication.sharedApplication().scheduleLocalNotification(notification)
                    }
                }
                
            }
        }else if(serverInt == 6){
            dispatch_async(dispatch_get_main_queue()){
                if( responseDict.objectForKey("data") != nil){
                    let docMutableAr = NSMutableArray()
                    let docidAr = NSMutableArray()
                    let dataDict = responseDict.objectForKey("data") as! NSArray
                    for i in 0...dataDict.count-1 {
                        let d = dataDict[i]
                        let status = d.valueForKey("status") as! NSInteger
                        if(status == 1){
                            docMutableAr.addObject("Dr. \(d.valueForKey("Name")!)")
                            docidAr.addObject(d.valueForKey("Dr_id")!)
                        }
                    }
                    if(docMutableAr.count != 0){
                        var storyboard = UIStoryboard()
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            storyboard = UIStoryboard(name: "Main", bundle: nil)
                        }else{
                            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                        }
                        let vc = storyboard.instantiateViewControllerWithIdentifier("SearchStaffViewController") as! SearchStaffViewController
                        vc.drNameAr = docMutableAr
                        vc.drIdAr = docidAr
                        self.navigationController?.pushViewController(vc, animated: true)
  
                    }else{
                        AppManager.sharedManager.Showalert("Alert", alertmessage: "Your doctor has not verified your account yet. Please contact your doctor’s office to expedite this process or send a new request.")
                    }
                    
                }
                else{
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Your doctor has not verified your account yet. Please contact your doctor’s office to expedite this process or send a new request.")
                }
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue()){
                if( responseDict.objectForKey("data") != nil){
                    let docMutableAr = NSMutableArray()
                    let docidAr = NSMutableArray()
                    let dataDict = responseDict.objectForKey("data") as! NSArray
                    for i in 0...dataDict.count-1 {
                        let d = dataDict[i]
                        let status = d.valueForKey("status") as! NSInteger
                        if(status == 1){
                            docMutableAr.addObject(d.valueForKey("Name")!)
                            docidAr.addObject(d.valueForKey("Dr_id")!)
                        }
                    }
                    if(docMutableAr.count != 0){
                        //let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        var storyboard = UIStoryboard()
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            storyboard = UIStoryboard(name: "Main", bundle: nil)
                        }else{
                            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                        }
                        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("PatientSetScheduleViewController") as! PatientSetScheduleViewController
                        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("PatientSideBarViewController") as! PatientSideBarViewController
                        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                        leftViewController.mainViewController = nvc
                        let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
                        slideMenuController.automaticallyAdjustsScrollViewInsets = true
                        slideMenuController.delegate = mainViewController
                        self.navigationController?.pushViewController(slideMenuController, animated: true)
                        
                    }else{
                        AppManager.sharedManager.Showalert("Alert", alertmessage: "Your doctor has not verified your account yet. Please contact your doctor’s office to expedite this process or send a new request.")
                    }
                    
                }
                else{
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Your doctor has not verified your account yet. Please contact your doctor’s office to expedite this process or send a new request.")
                }
            }
        }
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        
        let imageView: UIView = (cell?.viewWithTag(9120))!
        imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
        imageView.layer.shadowOffset = CGSizeMake(10, 5);
        imageView.layer.shadowRadius = 1;
        imageView.layer.shadowOpacity = 0.06;
        imageView.layer.masksToBounds = false;
        imageView.layer.borderWidth = 0.5
        if(indexPath.row == 0){
            imageView.backgroundColor = UIColor.init(red: 10/255, green: 187/255, blue: 167/255, alpha: 1)
        }
        else if(indexPath.row == 1){
            imageView.backgroundColor = UIColor.init(red: 55/255, green: 154/255, blue: 153/255, alpha: 1)
        }
        else if(indexPath.row == 2){
           imageView.backgroundColor = UIColor.init(red: 55/255, green: 132/255, blue: 143/255, alpha: 1) 
        }
        else{
            imageView.backgroundColor = UIColor.init(red: 76/255, green: 109/255, blue: 132/255, alpha: 1)
        }
        let img: UIImageView = (cell!.viewWithTag(9121) as! UIImageView)
        img.image = UIImage(named:arMenuImages[indexPath.row])
        let valueLbl: UILabel = (cell!.viewWithTag(9122) as! UILabel)
        valueLbl.text = arMenu[indexPath.row]
        
        
        if(indexPath.row == 3){
            let lineLbl1: UILabel = (cell!.viewWithTag(9124) as! UILabel)
            lineLbl1.backgroundColor = UIColor.clearColor()
            
            let lineLbl2: UILabel = (cell!.viewWithTag(9125) as! UILabel)
            lineLbl2.backgroundColor = UIColor.clearColor()
        }
        else{
            let lineLbl1: UILabel = (cell!.viewWithTag(9124) as! UILabel)
            lineLbl1.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
            
            let lineLbl2: UILabel = (cell!.viewWithTag(9125) as! UILabel)
            lineLbl2.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
        }
        
        if(indexPath.row == 0){
            let lineLbl1: UILabel = (cell!.viewWithTag(9126) as! UILabel)
            lineLbl1.backgroundColor = UIColor.clearColor()
            
            let lineLbl2: UILabel = (cell!.viewWithTag(9127) as! UILabel)
            lineLbl2.backgroundColor = UIColor.clearColor()
        }
        else{
            let lineLbl1: UILabel = (cell!.viewWithTag(9126) as! UILabel)
            lineLbl1.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
            
            let lineLbl2: UILabel = (cell!.viewWithTag(9127) as! UILabel)
            lineLbl2.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
        }
        
        let countLabel: UILabel = (cell!.viewWithTag(9114) as! UILabel)
        countLabel.layer.cornerRadius = countLabel.frame.size.height/2
        countLabel.clipsToBounds = true
        if(indexPath.row == 0){
            if(schCount != 0){
                countLabel.text = "\(schCount)"
                countLabel.backgroundColor = UIColor.redColor()
                countLabel.textColor = UIColor.whiteColor()
            }
            else{
                countLabel.text = ""
                countLabel.backgroundColor = UIColor.clearColor()
                countLabel.textColor = UIColor.clearColor()
            }
            
        }
        return cell! as UITableViewCell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        if(indexPath.row == 0){
            timer.invalidate()
            let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("PatientAppointmentViewController") as! PatientAppointmentViewController
            let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("PatientSideBarViewController") as! PatientSideBarViewController
            let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
            leftViewController.mainViewController = nvc
            let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            slideMenuController.delegate = mainViewController
            self.navigationController?.pushViewController(slideMenuController, animated: true)
        }else if(indexPath.row == 1){
            timer.invalidate()
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("id") {
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                let params = "Patient_id=\(userId)"
                serverInt = 2
                AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/PatientDrList")
            }
        }else if(indexPath.row == 2){
            timer.invalidate()
            let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("MyDoctorsViewController") as! MyDoctorsViewController
            let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("PatientSideBarViewController") as! PatientSideBarViewController
            let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
            leftViewController.mainViewController = nvc
            let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            slideMenuController.delegate = mainViewController
            self.navigationController?.pushViewController(slideMenuController, animated: true)
        }else if(indexPath.row == 3){
            timer.invalidate()
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("id") {
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                let params = "Patient_id=\(userId)"
                serverInt = 6
                AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/PatientDrList")
            }
//            timer.invalidate()
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Phone:
            if(self.view.frame.size.height < 600){
                return homeTableVw.frame.size.height*0.25
            }else{
                return homeTableVw.frame.size.height * 0.23
            }
        case .Pad:
            return homeTableVw.frame.size.height * 0.22
        default:
            break
        }
        return 0
    }
    @IBAction func contactUsAc(sender: AnyObject) {
        timer.invalidate()
        //  let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("ContactUsViewController") as! ContactUsViewController
        mainViewController.contactSwich = false
        let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("PatientSideBarViewController") as! PatientSideBarViewController
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        leftViewController.mainViewController = nvc
        let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        self.navigationController?.pushViewController(slideMenuController, animated: true)
    }
    @IBAction func logoutAc(sender: AnyObject) {
        timer.invalidate()
        let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To LogOut?", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            NSLog("OK Pressed")
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("id") {
                var token =  NSString()
                if(UIDevice.isSimulator == true){
                    token = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
                }else{
                    token = defaults.valueForKey("deviceToken") as! NSString
                }
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "LogOut")
                let params = "UserId=\(userId)&DeviceToken=\(token)"
                self.logoutCon = false
                self.serverInt = 1
                AppManager.sharedManager.postDataOnserver(params, postUrl: "users/logout")
            }
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    @IBAction func myInfoAc(sender: AnyObject) {
        timer.invalidate()
        //  let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("MyInformationViewController") as! MyInformationViewController
        let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("PatientSideBarViewController") as! PatientSideBarViewController
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        leftViewController.mainViewController = nvc
        let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        self.navigationController?.pushViewController(slideMenuController, animated: true)
    }
    @IBAction func notificationAc(sender: AnyObject) {
        timer.invalidate()
        // let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("PatientNotificationViewController") as! PatientNotificationViewController
        mainViewController.notiBool = false
        let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("PatientSideBarViewController") as! PatientSideBarViewController
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        leftViewController.mainViewController = nvc
        let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        self.navigationController?.pushViewController(slideMenuController, animated: true)
    }
    @IBAction func msgListingAc(sender: AnyObject) {
        timer.invalidate()
        // let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("MessageListingViewController") as! MessageListingViewController
        let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("PatientSideBarViewController") as! PatientSideBarViewController
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        leftViewController.mainViewController = nvc
        let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        self.navigationController?.pushViewController(slideMenuController, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
