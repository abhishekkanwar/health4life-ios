

import UIKit
import IQKeyboardManagerSwift
import RealmSwift

class SearchStaffViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var selectDocVw: UIView!
    @IBOutlet weak var selectDocFld: UITextField!
    
    @IBOutlet weak var nostaffImg: UIImageView!
    @IBOutlet weak var staffListTableVw: UITableView!
    
    var apiInt = NSInteger()
    var drIdAr = NSMutableArray()
    var drNameAr = NSMutableArray()
    var drId = NSString()
    var staffNameAr = NSMutableArray()
    var staffDesignationAr = NSMutableArray()
    var staffGenderAr = NSMutableArray()
    var staffPicAr = NSMutableArray()
    var staffIdAr = NSMutableArray()
    var staffPhAr = NSMutableArray()
    var staffStatusAr = NSMutableArray()
    var staffEmailAr = NSMutableArray()
    var nameStaff = NSString()
    var realm = try! Realm()
    var token = NSString()
    var apiKey = NSString()
    var session = NSString()
    var groupId = NSString()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        selectDocVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        selectDocVw.layer.borderColor   = UIColor.whiteColor().CGColor
        selectDocVw.layer.shadowOffset  = CGSizeMake(10, 5);
        selectDocVw.layer.shadowRadius  = 1;
        selectDocVw.layer.shadowOpacity = 0.06;
        selectDocVw.backgroundColor     = UIColor.clearColor()
        selectDocVw.layer.masksToBounds = false;
        selectDocVw.layer.shadowColor   = UIColor.lightGrayColor().CGColor
        selectDocVw.layer.borderWidth   = 0.5
        
        staffListTableVw.delegate = self
        staffListTableVw.dataSource = self
        
        selectDocFld.delegate = self
    }
    
    //MARK:- TextFieldMethod
    func textFieldDidBeginEditing(textField: UITextField) {
        drId = ""
        self.pickerShow()
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- ServerResponce
    func doRequestPost(url:String,data:[String: NSObject]){
        let requestDic = data
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                print("Succes:")
                let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                dispatch_async(dispatch_get_main_queue()) {
                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                    if(self.apiInt == 2){
                        if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 200){
                            self.staffListTableVw.hidden = false
                            let data = responseDict.valueForKey("data") as! NSArray
                            self.staffNameAr.removeAllObjects()
                            self.staffGenderAr.removeAllObjects()
                            self.staffPicAr.removeAllObjects()
                            self.staffDesignationAr.removeAllObjects()
                            self.staffStatusAr.removeAllObjects()
                            self.staffPhAr.removeAllObjects()
                            self.staffEmailAr.removeAllObjects()
                            if(data.count != 0){
                                for i in 0...data.count-1{
                                    let dict = data[i]
                                    if(dict.valueForKey("StaffStatus") as! Int == 1){
                                        self.staffNameAr.addObject(dict.valueForKey("Name")!)
                                        self.staffIdAr.addObject(dict.valueForKey("StaffId")!)
                                        self.staffGenderAr.addObject(dict.valueForKey("Gender")!)
                                        self.staffPicAr.addObject(dict.valueForKey("ProfilePic")!)
                                        self.staffStatusAr.addObject(dict.valueForKey("StaffStatus")!)
                                        let phoneNumber = "(\(dict.valueForKey("Code")!))\(dict.valueForKey("Mobile")!)"
                                        self.staffPhAr.addObject(phoneNumber)
                                        self.staffEmailAr.addObject(dict.valueForKey("Email")!)
                                        self.staffDesignationAr.addObject(dict.valueForKey("Designation")!)
                                    }
                                }
                                self.staffListTableVw.reloadData()
                            }else{
                                self.staffListTableVw.hidden = true
                            }
                        }
                        else{
                            self.staffListTableVw.hidden = true
                        }
                    }else{
                        self.token = responseDict.valueForKeyPath("data.TokenData")! as! NSString
                        self.session = responseDict.valueForKeyPath("data.sessionData")! as! NSString
                        self.apiKey = responseDict.valueForKeyPath("data.apiKey")! as! NSString
                        self.groupId = responseDict.valueForKeyPath("data.groupId")! as! NSString
                        
                        let user = NSMutableDictionary()
                        user.setValue(self.token, forKey: "TokenData")
                        user.setValue(self.apiKey, forKey: "ApiKey")
                        user.setValue(self.groupId, forKey: "GroupId")
                        user.setValue(self.session, forKey: "sessionData")
                        user.setValue(self.nameStaff, forKey: "Name")
                        let groupId = self.groupId
                        self.msgCountList(groupId as String) { (result) in
                            let chatCount = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", groupId ))
                            if(chatCount.count < result.valueForKey("count") as! Int){
                                var userId = NSString()
                                let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
                                if(userType == 3){
                                    userId = NSUserDefaults.standardUserDefaults().valueForKey("staffId") as! String
                                }else{
                                    userId = NSUserDefaults.standardUserDefaults().valueForKey("id") as! String
                                }
                                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                                RealmInteractor.shared.getMsgsFromServer(userId as String, groupId:groupId as String) { (result) in
                                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                                    
                                    if(result.valueForKey("status") as! Int == 200)
                                    {
                                        let chatResults = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", groupId))
                                        try! self.realm.write({
                                            self.realm.delete(chatResults)
                                        })
                                        
                                        let chatArray = result.objectForKey("data") as! NSArray
                                        
                                        for dict in chatArray
                                        {
                                            try! self.realm.write({
                                                
                                                let group = Group()
                                                group.displayName = dict.valueForKey("DisplayName") as! String
                                                group.message = dict.valueForKey("message") as! String
                                                group.messageType = dict.valueForKey("messageType") as! String
                                                group.fromId = dict.valueForKey("From") as! String
                                                group.groupId = dict.valueForKey("GroupId") as! String
                                                self.realm.add(group)
                                                
                                            })
                                            
                                        }
                                        self.navigateToChatView(user)
                                        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                                    }
                                }
                            }
                            else
                            {
                                self.navigateToChatView(user)
                                AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                            }
                        }
                    }
                }
            }
        }
        dataTask.resume()
    }
    
    //MARK:- ChatMethod
    func navigateToChatView(user:NSDictionary) -> Void {
        
        let chatResults = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", user.valueForKey("GroupId") as! String))
        let msgArray = NSMutableArray()
        
        for group in chatResults
        {
            var mySelf = false
            let msgDict = group
            let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
            if(userType == 1 || userType == 0 || userType == 2){
                if(msgDict.fromId == (NSUserDefaults.standardUserDefaults().valueForKey("id") as! String))
                {
                    mySelf = true
                }
            }else if(userType == 3){
                if(msgDict.fromId == (NSUserDefaults.standardUserDefaults().valueForKey("staffId") as! String))
                {
                    mySelf = true
                }
            }
            let mdict = ["msg":msgDict.message,"bool":mySelf, "msgType":msgDict.messageType,"name":msgDict.displayName]
            msgArray.addObject(mdict)
        }
        
        let firstname:String = NSUserDefaults.standardUserDefaults().valueForKey("firstName") as! String
        
        let vc = storyboard!.instantiateViewControllerWithIdentifier("ChatViewController") as! ChatViewController
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let usertype = defaults.valueForKey("usertype") as! NSInteger
        if(usertype == 3){
            vc.userId = defaults.valueForKey("staffId") as! String
        }else{
            vc.userId = defaults.valueForKey("id") as! String
        }
        
        vc.chatUserId = user.valueForKey("GroupId") as! String
        
        vc.userName = user.valueForKey("Name") as! String
        vc.Token = user.valueForKey("TokenData") as! String
        vc.SessionID = user.valueForKey("sessionData") as! String
        vc.apiKey = user.valueForKey("ApiKey") as! String
        vc.msgAr = msgArray
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func msgCountList(groupId:String,completion: (result: NSDictionary)->()) -> Void {
        let paramString = "GroupId=\(groupId)"
        AppManager.sharedManager.generateGroupIdForUser("messageChat/listChat", params: paramString) { (result) in
            completion(result: result)
        }
    }
    
    
    //MARK:- PickerViewMethod
    func pickerShow(){
        let InputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0.4))
        
        let PickerVw  : UIPickerView = UIPickerView(frame: CGRectMake(0, 40,UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height*0.32))
        PickerVw.delegate            = self
        PickerVw.dataSource          = self
        InputView.addSubview(PickerVw)
        
        let doneButton = UIButton(frame: CGRectMake((self.view.frame.size.width-100), 0, 100, 50))
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        doneButton.setTitle("Done", forState: UIControlState.Highlighted)
        doneButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        doneButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        InputView.addSubview(doneButton)
        doneButton.addTarget(self, action: #selector(SearchStaffViewController.doneButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let cancelBtn = UIButton(frame: CGRectMake(0, 0, 100, 50))
        cancelBtn.setTitle("Cancel", forState: UIControlState.Normal)
        cancelBtn.setTitle("Cancel", forState: UIControlState.Highlighted)
        cancelBtn.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        cancelBtn.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        InputView.addSubview(cancelBtn)
        cancelBtn.addTarget(self, action: #selector(SearchStaffViewController.cancelBtn(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        selectDocFld.inputView = InputView
    }
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return drNameAr.count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return drNameAr[row] as? String
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectDocFld.text = drNameAr[row] as? String
        drId = (drIdAr[row] as? String)!
    }
    
    
    //MARK:- ButtonAction
    func doneButton(sender:UIButton){
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
        if(drId == ""){
            drId = drIdAr[0] as! NSString
            selectDocFld.text = drNameAr[0] as? String
        }
        apiInt = 2
        self.doRequestPost("\(Header.BASE_URL)docter/DRStaffList", data: ["DrId":"\(drId)","StaffId":"abc"])
        selectDocFld.resignFirstResponder()
    }
    func cancelBtn(sender:UIButton){
        selectDocFld.resignFirstResponder()
    }
    @IBAction func chatBtnAc(sender: AnyObject) {
        let btnPos = sender.convertPoint(CGPointZero, toView: self.staffListTableVw)
        let indexPath:NSIndexPath = self.staffListTableVw.indexPathForRowAtPoint(btnPos)!
        nameStaff = staffNameAr[indexPath.row] as! NSString
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            apiInt = 3
            self.doRequestPost("\(Header.BASE_URL)docter/DRdrcreatetoken", data: ["From":"\(userId)","To":"\(staffIdAr[indexPath.row])"])
        }
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    //MARK:- TableViewMethod
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return staffNameAr.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        
        let staffImg: UIImageView = (cell!.viewWithTag(3) as! UIImageView)
        staffImg.layer.cornerRadius = staffImg.frame.size.height/2
        staffImg.layer.borderWidth = self.view.frame.size.height*0.012
        staffImg.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        if let url = NSURL(string: "http://\(staffPicAr[indexPath.row])") {
            staffImg.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        let imageView: UIView = (cell?.viewWithTag(2))!
        imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
        imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
        imageView.layer.shadowOffset = CGSizeMake(10, 5);
        imageView.layer.shadowRadius = 1;
        imageView.layer.shadowOpacity = 0.06;
        imageView.backgroundColor = UIColor.whiteColor()
        imageView.layer.masksToBounds = false;
        imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        imageView.layer.borderWidth = 0.5
        if(indexPath.row%2 == 0){
            imageView.backgroundColor = UIColor.whiteColor()
            staffImg.backgroundColor = UIColor.lightGrayColor()
        }else{
            imageView.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            staffImg.backgroundColor = UIColor.whiteColor()
        }
        let name = cell?.viewWithTag(4) as! UILabel
        name.text = "\(staffNameAr[indexPath.row])"
        let gender = cell?.viewWithTag(5) as! UILabel
        gender.text = "\(staffGenderAr[indexPath.row])"
        let designation = cell?.viewWithTag(6) as! UILabel
        designation.text = "\(staffDesignationAr[indexPath.row])"
        
        return cell!
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        var storyboard = UIStoryboard()
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        }else{
            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
        }
        let vc = storyboard.instantiateViewControllerWithIdentifier("DocStaffDetailViewController") as! DocStaffDetailViewController
        var compPh = "+1\(staffPhAr[indexPath.row])"
        compPh = compPh.insert("-", ind: 10)
        vc.valueAr.addObject(staffNameAr[indexPath.row])
        vc.valueAr.addObject(staffGenderAr[indexPath.row])
        vc.valueAr.addObject(compPh)
        vc.valueAr.addObject(staffEmailAr[indexPath.row])
        vc.valueAr.addObject(staffDesignationAr[indexPath.row])
        vc.staffStatus = Int(staffStatusAr[indexPath.row] as! NSNumber)
        vc.apiKey = apiKey
        vc.Token = token
        vc.sessionId = session
        vc.groupId = groupId
        vc.staffIdAr = staffIdAr[indexPath.row] as! NSString
        vc.imgUrl = staffPicAr[indexPath.row] as! NSString
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return self.view.frame.size.width*0.1
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
