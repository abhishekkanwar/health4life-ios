
import UIKit
import SlideMenuControllerSwift
class PrescriptionViewController: UIViewController,WebServiceDelegate {

    @IBOutlet weak var pendingPrescriptionVw: UIView!
    @IBOutlet weak var transmissionErrorVw: UIView!
    @IBOutlet weak var transmissionErrorLbl: UILabel!
    @IBOutlet weak var pendingPrescriptionLbl: UILabel!
    var nsurlData = NSString()
    var pendingPrescriptionCount = NSString()
    var transmissionErrCount = NSString()
    var timer = NSTimer()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiHit()
        pendingPrescriptionVw.hidden = true
        self.navigationController?.navigationBarHidden = true
        transmissionErrorLbl.backgroundColor = UIColor.clearColor()
        pendingPrescriptionLbl.backgroundColor = UIColor.clearColor()
        pendingPrescriptionVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        pendingPrescriptionVw.layer.borderColor = UIColor.lightGrayColor().CGColor
        pendingPrescriptionVw.layer.shadowOffset = CGSizeMake(20, 15);
        pendingPrescriptionVw.layer.shadowRadius = 2;
        pendingPrescriptionVw.layer.shadowOpacity = 0.06;
        pendingPrescriptionVw.layer.masksToBounds = false;
        pendingPrescriptionVw.layer.shadowColor = UIColor.lightGrayColor().CGColor
        pendingPrescriptionVw.layer.borderWidth = 0.5
        
        transmissionErrorVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        transmissionErrorVw.layer.borderColor = UIColor.lightGrayColor().CGColor
        transmissionErrorVw.layer.shadowOffset = CGSizeMake(7, 5);
        transmissionErrorVw.layer.shadowRadius = 1;
        transmissionErrorVw.layer.shadowOpacity = 0.05;
        transmissionErrorVw.layer.masksToBounds = false;
        transmissionErrorVw.layer.shadowColor = UIColor.lightGrayColor().CGColor
        transmissionErrorVw.layer.borderWidth = 0.5
        
        pendingPrescriptionLbl.text = "\(pendingPrescriptionCount)"
        transmissionErrorLbl.text = "\(transmissionErrCount)"
       
      }
    func apiHit(){
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
        let params = "DrId=\(userId)"
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/soap")
        }
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        dispatch_async(dispatch_get_main_queue()) {
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            self.pendingPrescriptionLbl.text = responseDict.valueForKey("RefillRequestsCount") as? String
            self.transmissionErrorLbl.text = responseDict.valueForKey("TransactionErrorsCount") as? String
        }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }

    @IBAction func backBtnAc(sender: AnyObject) {
        self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func resolvedBtnAc(sender: AnyObject) {
        timer.invalidate()
       // let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard!.instantiateViewControllerWithIdentifier("TransmissionErrorViewController") as! TransmissionErrorViewController
        vc.webUrl = nsurlData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension PrescriptionViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
