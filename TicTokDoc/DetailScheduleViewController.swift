

import UIKit

class DetailScheduleViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,WebServiceDelegate{
    @IBOutlet weak var schDetailTableVw: UITableView!
    @IBOutlet weak var detailImgVw: UIImageView!
    
    var drId = NSString()
    var drSchId = NSString()
    var fromT = NSString()
    var toT = NSString()
    var ptNameAr = NSMutableArray()
    var ptFrom = NSMutableArray()
    var durationAr = NSMutableArray()
    var profilepicAr = NSMutableArray()
    var sh_Id = NSString()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.hidden = true
       schDetailTableVw.delegate = self
       schDetailTableVw.dataSource = self
       schDetailTableVw.separatorStyle = .None
        schDetailTableVw.allowsSelection = false
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        let params = "Dr_id=\(drId)&DrScheduleId=\(drSchId)&From=\(fromT)&To=\(toT)&ScheduleId=\(sh_Id)"
        print(params)
        AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrSchedulePatientList")
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        dispatch_async(dispatch_get_main_queue()) {
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        if(responseDict.valueForKey("patient")?.count != 0){
            self.schDetailTableVw.hidden = false
          let data = responseDict.valueForKey("patient") as! NSArray
            for schT in 0...data.count-1 {
                let ptName = data[schT].valueForKeyPath("data.PatientName") as! NSString
                self.ptNameAr.addObject(ptName)
                let fromTime = data[schT].valueForKeyPath("data.From") as! NSString
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "HH:mm"
                let timeZone = NSTimeZone(name: "GMT-8")
                dateFormatter.timeZone=timeZone
                let schF:NSDate = dateFormatter.dateFromString("\(fromTime)")!
                dateFormatter.dateFormat = "hh:mm a"
                let from12hrStr = dateFormatter.stringFromDate(schF) as NSString
                self.ptFrom.addObject(from12hrStr)
                let toTime = data[schT].valueForKeyPath("data.To") as! NSString
                let dateFormatter2 = NSDateFormatter()
                dateFormatter2.timeStyle = .ShortStyle
                dateFormatter2.dateFormat = "HH:mm" // k = Hour in 1~24, mm = Minute
                let timeZone2 = NSTimeZone(name: "UTC")
                dateFormatter2.timeZone=timeZone2
                let timeTo = dateFormatter2.dateFromString("\(toTime)")!
                let timeFrom = dateFormatter2.dateFromString("\(fromTime)")!
                let timeI =  (timeTo.timeIntervalSinceDate(timeFrom))
                let i = Int(timeI)
                let minutesSlot = i/60
                self.durationAr.addObject(minutesSlot)
                self.profilepicAr.addObject(data[schT].valueForKey("profile") as! NSString)
            }
          self.schDetailTableVw.reloadData()
         }
        else{
            self.schDetailTableVw.reloadData()
            self.schDetailTableVw.hidden = true
          self.detailImgVw.image = UIImage(named: "detailImg")
            
        }
      }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ptNameAr.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        cell!.layer.cornerRadius = 7.0
        cell!.layer.masksToBounds = true
        
        let imageView: UIView = (cell?.viewWithTag(3900))!
        imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
        imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
        imageView.layer.shadowOffset = CGSizeMake(10, 5);
        imageView.layer.shadowRadius = 1;
        imageView.layer.shadowOpacity = 0.06;
        imageView.layer.masksToBounds = false;
        imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        imageView.layer.borderWidth = 0.5
        if(indexPath.row%2 == 0){
            imageView.backgroundColor = UIColor.whiteColor()
        }else{
            imageView.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
        }

       let itemImg: UIImageView = (cell!.viewWithTag(3901) as! UIImageView)
        itemImg.layer.cornerRadius = itemImg.frame.size.height/2
        itemImg.layer.borderWidth = self.view.frame.size.height*0.012
        itemImg.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        if let url = NSURL(string: "http://\(profilepicAr[indexPath.row])") {
            itemImg.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        let ptLbl: UILabel = (cell!.viewWithTag(3902) as! UILabel)
        ptLbl.text = ptNameAr[indexPath.row] as? String
       let durationLbl: UILabel = (cell!.viewWithTag(3903) as! UILabel)
      durationLbl.text = "\(durationAr[indexPath.row]) Min"
       let timeLbl: UILabel = (cell!.viewWithTag(3904) as! UILabel)
        timeLbl.text = ptFrom[indexPath.row] as? String
        return cell!
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Phone:
             return self.view.frame.size.width*0.2
        case .Pad:
            return self.view.frame.size.width*0.12
        default:
            break
        }
     return self.view.frame.size.width*0.0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    


}
