

import UIKit
import IQKeyboardManagerSwift
class StaffIdViewController: UIViewController,WebServiceDelegate, UITextFieldDelegate{
    
    @IBOutlet weak var enterStaffVw: UIView!
    @IBOutlet weak var enterStaffIdTxtFld: UITextField!
    var staffUserId = NSString()
    override func viewDidLoad() {
        super.viewDidLoad()
        enterStaffIdTxtFld.delegate = self
        IQKeyboardManager.sharedManager().enable = true
        enterStaffIdTxtFld.tintColor = UIColor.blackColor()
        enterStaffVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        enterStaffVw.layer.borderColor   = UIColor.darkGrayColor().CGColor
        enterStaffVw.layer.masksToBounds = false;
        enterStaffVw.layer.borderWidth   = 0.8
        enterStaffIdTxtFld.placeholder = "Enter staff id"
        // Do any additional setup after loading the view.
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func doneBtnAc(sender: AnyObject) {
        let trimNameString = enterStaffIdTxtFld.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
        if(trimNameString != ""){
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("id") {
                let firstName = defaults.valueForKey("firstName") as! String
                let lastName = defaults.valueForKey("lastName") as! String
                let params = "DrId=\(userId)&StaffId=\(trimNameString)&StaffUserId=\(staffUserId)&DrName=\(firstName) \(lastName)"
                AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/drcheckstaff")
            }
        }
        else{
            AppManager.sharedManager.Showalert("Alert", alertmessage: "Please enter a valid staff Id.")
        }
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        dispatch_async(dispatch_get_main_queue()) {
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 401){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Enter a Valid StaffId.")
            }else if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 400){
                let alert = UIAlertController(title: "Alert", message: "staff member already exist with this email id.", preferredStyle: UIAlertControllerStyle.Alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) {
                    UIAlertAction in
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                    for aViewController in viewControllers {
                        if(aViewController is StaffListingViewController){
                            self.navigationController!.popToViewController(aViewController, animated: true);
                        }
                    }
                }
                alert.addAction(okAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }else if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 200){
                let alert = UIAlertController(title: "Confirm", message: "Staff Member added successfully.", preferredStyle: UIAlertControllerStyle.Alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) {
                    UIAlertAction in
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                    for aViewController in viewControllers {
                        if(aViewController is StaffListingViewController){
                            self.navigationController!.popToViewController(aViewController, animated: true);
                        }
                    }
                }
                alert.addAction(okAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
