

import UIKit
import SlideMenuControllerSwift
class PatientAppointmentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,WebServiceDelegate {
    @IBOutlet weak var appointmntSgmnt: UISegmentedControl!
    @IBOutlet weak var backImgVw: UIImageView!
    @IBOutlet weak var appointmentTableVw: UITableView!
    @IBOutlet weak var schBtn: UIButton!
    @IBOutlet weak var btmlbl: UILabel!
    
    var previouslySelectedHeaderIndex: Int?
    var selectedHeaderIndex: Int?
    var selectedItemIndex: Int?
     var headerVw = UIView()
    var dateSch = NSString()
    let cells = SwiftyAccordionCells()
     var arrayAllData    = NSArray()
    var imageDrop =  UIImageView()
    var serverInt = NSInteger()
    var countAr = NSMutableDictionary()
    var botmConst = NSLayoutConstraint()
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let params = "PatientId=\(userId)"
            serverInt = 1
            AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/PatientAppointmentList")
        }
        appointmentTableVw.delegate = self
        appointmentTableVw.dataSource = self
        self.navigationController?.navigationBarHidden = true
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            let font = UIFont.systemFontOfSize(20)
            appointmntSgmnt.setTitleTextAttributes([NSFontAttributeName: font],
                                                   forState: UIControlState.Normal)
        }
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Selected)
        btmlbl.hidden = true
        
//        if(appointmntSgmnt.selectedSegmentIndex == 0){
//            self.setup()
//        }else{
//            self.pastSetUp()
//        }
    }
    override func viewDidAppear(animated: Bool) {
//        let defaults = NSUserDefaults.standardUserDefaults()
//        if let userId = defaults.valueForKey("id") {
//            AppManager.sharedManager.delegate=self
//            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
//            let params = "PatientId=\(userId)"
//            AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/PatientAppointmentList")
//        }
//        appointmentTableVw.delegate = self
//        appointmentTableVw.dataSource = self
//        self.navigationController?.navigationBarHidden = true
//        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Selected)
//        if(appointmntSgmnt.selectedSegmentIndex == 0){
//            self.setup()
//        }else{
//            self.pastSetUp()
//        }
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
         dispatch_async(dispatch_get_main_queue()) {
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        if(self.serverInt == 1){
        if(responseDict.objectForKey("data")?.count != 0){
            let data      = responseDict.objectForKey("data")
            self.arrayAllData  = data?.mutableCopy() as! NSArray
            let bookingData = self.arrayAllData.valueForKey("Booking") as! NSArray
                 self.appointmentTableVw.hidden = false
            self.btmlbl.hidden = false
                if(((bookingData.valueForKey("past")).count) == 0 && self.appointmntSgmnt.selectedSegmentIndex == 1 && ((bookingData.valueForKey("past")) as! NSArray).objectAtIndex(0).count != 0 ){
                    self.appointmentTableVw.hidden = true
                    self.backImgVw.image = UIImage(named: "pastApp")
                }
                if(((bookingData.valueForKey("upcoming")).count) != 0 && self.appointmntSgmnt.selectedSegmentIndex == 0 && ((bookingData.valueForKey("upcoming")) as! NSArray).objectAtIndex(0).count == 0 ){
                    self.backImgVw.image = UIImage(named: "upcomingApp")
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                     self.botmConst = NSLayoutConstraint(item:self.view,attribute: NSLayoutAttribute.BottomMargin,relatedBy: NSLayoutRelation.Equal,toItem: self.schBtn,attribute: NSLayoutAttribute.TopMargin,multiplier: 1,constant: 40)
                    }else{
                       self.botmConst = NSLayoutConstraint(item:self.view,attribute: NSLayoutAttribute.BottomMargin,relatedBy: NSLayoutRelation.Equal,toItem: self.schBtn,attribute: NSLayoutAttribute.TopMargin,multiplier: 1,constant: 80)
                    }
                    self.view.addConstraints([self.botmConst])
                    self.btmlbl.hidden = true
                    self.appointmentTableVw.hidden = true
                }else{
                self.setup()
            }
            
            let defaults = NSUserDefaults.standardUserDefaults()
            let userId = defaults.valueForKey("id") as! NSString
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
            let params1 = "UserId=\(userId)&Type=0"
            self.serverInt = 3
            AppManager.sharedManager.postDataOnserver(params1, postUrl: "users/scheduleread")
            
        }else if(responseDict.objectForKey("data")?.count == 0){
            dispatch_async(dispatch_get_main_queue()){
                if(self.appointmntSgmnt.selectedSegmentIndex == 0){
                    self.backImgVw.image = UIImage(named: "upcomingApp")
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        self.botmConst = NSLayoutConstraint(item:self.view,attribute: NSLayoutAttribute.BottomMargin,relatedBy: NSLayoutRelation.Equal,toItem: self.schBtn,attribute: NSLayoutAttribute.TopMargin,multiplier: 1,constant: 40)
                    }else{
                        self.botmConst = NSLayoutConstraint(item:self.view,attribute: NSLayoutAttribute.BottomMargin,relatedBy: NSLayoutRelation.Equal,toItem: self.schBtn,attribute: NSLayoutAttribute.TopMargin,multiplier: 1,constant: 80)
                    }
                    self.view.addConstraints([self.botmConst])
                    self.btmlbl.hidden = true
                    self.appointmentTableVw.hidden = true
                }
                else{
                    self.appointmentTableVw.hidden = true
                    self.backImgVw.image = UIImage(named: "pastApp")
                }
            }
        }
        else{
            let dic = responseDict.valueForKey("error") as! String
            if(dic == "No Data"){
                dispatch_async(dispatch_get_main_queue()){
                    if(self.appointmntSgmnt.selectedSegmentIndex == 0){
                        self.backImgVw.image = UIImage(named: "upcomingApp")
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            self.botmConst = NSLayoutConstraint(item:self.view,attribute: NSLayoutAttribute.BottomMargin,relatedBy: NSLayoutRelation.Equal,toItem: self.schBtn,attribute: NSLayoutAttribute.TopMargin,multiplier: 1,constant: 40)
                        }else{
                            self.botmConst = NSLayoutConstraint(item:self.view,attribute: NSLayoutAttribute.BottomMargin,relatedBy: NSLayoutRelation.Equal,toItem: self.schBtn,attribute: NSLayoutAttribute.TopMargin,multiplier: 1,constant: 80)
                        }
                        self.view.addConstraints([self.botmConst])
                        self.btmlbl.hidden = true
                        self.appointmentTableVw.hidden = true
                    }
                    else{
                        self.appointmentTableVw.hidden = true
                        self.backImgVw.image = UIImage(named: "pastApp")
                    }
                }
            }
        }
        }else if(self.serverInt == 3){
            
        }
        else{
           if( responseDict.objectForKey("data") != nil){
            let docMutableAr = NSMutableArray()
            let docidAr = NSMutableArray()
            let dataDict = responseDict.objectForKey("data") as! NSArray
            for i in 0...dataDict.count-1 {
                let d = dataDict[i]
                let status = d.valueForKey("status") as! NSInteger
                if(status == 1){
                    docMutableAr.addObject(d.valueForKey("Name")!)
                    docidAr.addObject(d.valueForKey("Dr_id")!)
                }
            }
            if(docMutableAr.count != 0){
                //let storyboard = UIStoryboard(name: "Main", bundle: nil)
                var storyboard = UIStoryboard()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                }else{
                    storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                }
                let vc = storyboard.instantiateViewControllerWithIdentifier("PatientSetScheduleViewController") as! PatientSetScheduleViewController
                self.navigationController!.pushViewController(vc, animated: true)
            
            }else{
              AppManager.sharedManager.Showalert("Alert", alertmessage: "Your doctor has not verified your account yet. Please contact your doctor’s office to expedite this process or send a new request.")
            }
        }
            else{
              AppManager.sharedManager.Showalert("Alert", alertmessage: "Your doctor has not verified your account yet. Please contact your doctor’s office to expedite this process or send a new request.")
            }
        }
        }
    }
    
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }

    @IBAction func schAction(sender: AnyObject) {
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let params = "Patient_id=\(userId)"
            serverInt = 2
            AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/PatientDrList")
        }
    }
    func setup() {
        if(arrayAllData.count != 0){
            let headerView: UIView = UIView(frame: CGRectMake(0, 0, appointmentTableVw.frame.size.width, self.view.frame.size.height*0.09))
            let contentLbl = UILabel.init(frame: CGRectMake(headerView.frame.size.width*0.0,headerView.frame.size.height*0.12, headerView.frame.size.width-(headerView.frame.size.width*0.0),headerView.frame.size.height*1.0))
            contentLbl.font = UIFont(name: "Helvetica", size:14)
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Tap on your scheduled appointment to enter into the Waiting Room and your Doctor will Video Call you!"
            contentLbl.numberOfLines = 3
            headerView.addSubview(contentLbl)
            appointmentTableVw.tableHeaderView = headerView
            
            let footerVw: UIView = UIView(frame: CGRectMake(0, appointmentTableVw.frame.size.height-(self.view.frame.size.height*0.1), appointmentTableVw.frame.size.width, self.view.frame.size.height*0.09))
            let contentLbl1 = UILabel.init(frame: CGRectMake(footerVw.frame.size.width*0.0,footerVw.frame.size.height*0.12, footerVw.frame.size.width-(footerVw.frame.size.width*0.0),footerVw.frame.size.height*1.0))
            contentLbl1.font = UIFont(name: "Helvetica", size:14)
            contentLbl1.textAlignment = .Center
            contentLbl1.textColor = UIColor.grayColor()
            contentLbl1.text = "If you need to cancel your appointment, please call your doctor’s office directly."
            contentLbl1.numberOfLines = 2
            footerVw.addSubview(contentLbl1)
            //appointmentTableVw.tableFooterView = footerVw
            
            let arrayTempPast = arrayAllData.valueForKey("Booking") as! NSArray
            let upcomingData = arrayTempPast.valueForKey("upcoming") as! NSArray
            let arraTemp = upcomingData[0]
            let ar = arraTemp.valueForKey("ScheduleDate")! as! NSArray
            var duplicateAr = NSArray()
            duplicateAr = uniq(ar  as! [String])
            if(arraTemp.count != 0){
                duplicateAr.enumerateObjectsUsingBlock ({ (obj, index, value) in
                    self.cells.append(SwiftyAccordionCells.HeaderItem(value:obj))
                    arraTemp.enumerateObjectsUsingBlock ({ (obj1, index1, value1) in
                        let date = obj1.valueForKey("ScheduleDate") as! String
                        if date == String(obj){
                            //let tempDict = NSMutableDictionary()
                            let tempDict = (obj1 as! NSDictionary).mutableCopy()
                            self.countAr = tempDict as! NSMutableDictionary 
                            let strFirstName        = obj1.valueForKeyPath("DrId.FirstName") as! String
                            let strSpeciality       = obj1.valueForKeyPath("DrId.Speciality") as! String
                            let strQualification    = obj1.valueForKeyPath("DrId.Qualification") as! String
                            let strLang             = obj1.valueForKeyPath("DrId.Languages") as! String
                            let strmobile           = obj1.valueForKeyPath("DrId.Mobile") as! NSString
                            let strCode           = obj1.valueForKeyPath("DrId.Code") as! NSString
                            let strAdd              = obj1.valueForKeyPath("DrId.Address") as! String
                            let strid   = obj1.valueForKeyPath("DrId._id") as! String
                            tempDict.setValue(strFirstName, forKey: "FirstName")
                            tempDict.setValue(strSpeciality, forKey: "Speciality")
                            tempDict.setValue(strQualification, forKey: "Qualification")
                            tempDict.setValue(strLang, forKey: "Languages")
                            tempDict.setValue(strmobile, forKey: "Mobile")
                            tempDict.setValue(strAdd, forKey: "Address")
                            tempDict.setValue(strid, forKey: "Dr_id")
                            tempDict.setValue(strCode, forKey: "Code")
                            let defaults = NSUserDefaults.standardUserDefaults()
                            defaults.setObject(strid, forKey: "drId")
                            self.cells.append(SwiftyAccordionCells.Item(value: tempDict))
                        }
                    })
                })
                
                appointmentTableVw.reloadData()
            }
        }
        
        
    }
    
    func uniq<S: SequenceType, E: Hashable where E==S.Generator.Element>(source: S) -> [E] {
        var seen: [E:Bool] = [:]
        return source.filter { seen.updateValue(true, forKey: $0) == nil }
    }
    func pastSetUp(){
        if(arrayAllData.count != 0){
            appointmentTableVw.tableHeaderView = nil
            let arrayTempPast = arrayAllData.valueForKey("Booking") as! NSArray
            let upcomingData = arrayTempPast.valueForKey("past") as! NSArray
            let arraTemp = upcomingData[0]
            let ar = arraTemp.valueForKey("ScheduleDate")! as! NSArray
            var duplicateAr = NSArray()
            duplicateAr = uniq(ar  as! [String])
            if(arraTemp.count != 0){
                duplicateAr.enumerateObjectsUsingBlock ({ (obj, index, value) in
                    self.cells.append(SwiftyAccordionCells.HeaderItem(value:obj))
                    arraTemp.enumerateObjectsUsingBlock ({ (obj1, index1, value1) in
                        let date = obj1.valueForKey("ScheduleDate") as! String
                        if date == String(obj){
                            let tempDict = (obj1 as! NSDictionary).mutableCopy()
                            let strFirstName       = obj1.valueForKeyPath("DrId.FirstName") as! String
                            let strSpeciality       = obj1.valueForKeyPath("DrId.Speciality") as! String
                            let strQualification    = obj1.valueForKeyPath("DrId.Qualification") as! String
                            tempDict.setValue(strFirstName, forKey: "FirstName")
                            tempDict.setValue(strSpeciality, forKey: "Speciality")
                            tempDict.setValue(strQualification, forKey: "Qualification")
                            self.cells.append(SwiftyAccordionCells.Item(value: tempDict))
                        }
                    })
                })
                appointmentTableVw.reloadData()
            }
        }
        
    }
    @IBAction func appointmntSegmntAc(sender: AnyObject) {
        if(appointmntSgmnt.selectedSegmentIndex == 0){
            imageDrop.image = UIImage(named: "dropDown")
            self.cells.removeAll()
            self.selectedHeaderIndex = nil
            self.setup()
            self.appointmentTableVw.hidden = false
            self.btmlbl.hidden = false
            if(self.cells.items.count == 0){
                self.backImgVw.image = UIImage(named: "upcomingApp")
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    self.botmConst = NSLayoutConstraint(item:self.view,attribute: NSLayoutAttribute.BottomMargin,relatedBy: NSLayoutRelation.Equal,toItem: self.schBtn,attribute: NSLayoutAttribute.TopMargin,multiplier: 1,constant: 40)
                }else{
                    self.botmConst = NSLayoutConstraint(item:self.view,attribute: NSLayoutAttribute.BottomMargin,relatedBy: NSLayoutRelation.Equal,toItem: self.schBtn,attribute: NSLayoutAttribute.TopMargin,multiplier: 1,constant: 80)
                }
                self.view.addConstraints([self.botmConst])
                self.btmlbl.hidden = true
                self.appointmentTableVw.hidden = true
            }
        }
       else{
            imageDrop.image = UIImage(named: "dropDown")
            self.cells.removeAll()
            self.selectedHeaderIndex = nil
            self.pastSetUp()
            self.appointmentTableVw.hidden = false
            if(self.cells.items.count == 0){
                self.appointmentTableVw.hidden = true
                self.backImgVw.image = UIImage(named: "pastApp")
            }
        }
        appointmentTableVw.reloadData()
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(appointmntSgmnt.selectedSegmentIndex == 0){
            return self.cells.items.count
        }else{
            return self.cells.items.count
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
      
        let item = self.cells.items[indexPath.row]
        let value = item.value as? NSMutableDictionary
        if(appointmntSgmnt.selectedSegmentIndex == 0){
            if(self.cells.items[indexPath.row] is SwiftyAccordionCells.HeaderItem){
                let cell = tableView.dequeueReusableCellWithIdentifier("cell")
                let dateLbl: UILabel = (cell!.viewWithTag(301) as! UILabel)
                cell?.userInteractionEnabled = true
                dateLbl.text = item.value as? String
                if(dateLbl.text != nil){
                dateSch = dateLbl.text!
                }
                let vw: UIView = (cell!.viewWithTag(300))!
                vw.layer.cornerRadius = 7
                cell!.layer.masksToBounds = true
                headerVw = vw
                return cell!
            }else{
                let cell = tableView.dequeueReusableCellWithIdentifier("detailPast")
                let drLbl: UILabel = (cell!.viewWithTag(303) as! UILabel)
                if(indexPath.row == 3){
                    let imageView = UIImageView(frame: CGRectMake(0, 0, cell!.frame.width, cell!.frame.height))
                    let image = UIImage(named: "dropBox2")
                    imageView.image = image
                    cell!.backgroundView = imageView
                }
                let imageView = UIImageView(frame: CGRectMake(0, 0, cell!.frame.width, cell!.frame.height))
                let image = UIImage(named: "dropBox1")
                imageView.image = image
                cell!.backgroundView = imageView
                let firstname = value?.valueForKey("FirstName") as! String
                let lastname = value?.valueForKeyPath("DrId.LastName") as! String
                 drLbl.text = "Dr. \(firstname) \(lastname)"
                let drSpeciality: UILabel = (cell!.viewWithTag(305) as! UILabel)
                drSpeciality.text = value?.valueForKey("Speciality")  as? String
                let drquali: UILabel = (cell!.viewWithTag(306) as! UILabel)
                drquali.text = value?.valueForKey("Qualification")  as? String
                
                let fromStr = value?.valueForKey("From")  as? String
                let dateFormatter = NSDateFormatter()
                let timeZone = NSTimeZone(name: "GMT-8")
                dateFormatter.timeZone=timeZone
                dateFormatter.dateFormat = "HH:mm"
                let date24Hr = dateFormatter.dateFromString(fromStr! as String)!
                dateFormatter.dateFormat = "hh:mm a"
                let date12Hr = dateFormatter.stringFromDate(date24Hr)
                
                let from: UILabel = (cell!.viewWithTag(307) as! UILabel)
                from.text = date12Hr
                let drImg: UIImageView = (cell!.viewWithTag(304) as! UIImageView)
                let profilePic = value?.valueForKey("profile") as! NSString
                drImg.layer.cornerRadius = drImg.frame.size.height/2
                drImg.layer.borderWidth = self.view.frame.size.height*0.008
                drImg.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
                if let url = NSURL(string: "http://\(profilePic)") {
                    drImg.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
                }

                return cell!
            }
        }else{
            if(self.cells.items[indexPath.row] is SwiftyAccordionCells.HeaderItem){
                let cell = tableView.dequeueReusableCellWithIdentifier("cell")
                let dateLbl: UILabel = (cell!.viewWithTag(301) as! UILabel)
                cell?.userInteractionEnabled = true
                dateLbl.text = item.value as? String
                let vw: UIView = (cell!.viewWithTag(300))!
                vw.layer.cornerRadius = 7
                cell!.layer.masksToBounds = true
                headerVw = vw
                return cell!
            }else{
                let cell = tableView.dequeueReusableCellWithIdentifier("detailPast")
                let drLbl: UILabel = (cell!.viewWithTag(303) as! UILabel)
                if(indexPath.row == 3){
                    let imageView = UIImageView(frame: CGRectMake(0, 0, cell!.frame.width, cell!.frame.height))
                    let image = UIImage(named: "dropBox2")
                    imageView.image = image
                    cell!.backgroundView = imageView
                }
                let imageView = UIImageView(frame: CGRectMake(0, 0, cell!.frame.width, cell!.frame.height))
                let image = UIImage(named: "dropBox1")
                imageView.image = image
                cell!.backgroundView = imageView
                let firstname = value?.valueForKey("FirstName") as! String
                let lastname = value?.valueForKeyPath("DrId.LastName") as! String
                drLbl.text = "Dr. \(firstname) \(lastname)"
                
                let drSpeciality: UILabel = (cell!.viewWithTag(305) as! UILabel)
                drSpeciality.text = value?.valueForKey("Speciality")  as? String
                let drquali: UILabel = (cell!.viewWithTag(306) as! UILabel)
                drquali.text = value?.valueForKey("Qualification")  as? String
                
                let fromStr = value?.valueForKey("From")  as? String
                let dateFormatter = NSDateFormatter()
                let timeZone = NSTimeZone(name: "GMT-8")
                dateFormatter.timeZone=timeZone
                dateFormatter.dateFormat = "HH:mm"
                let date24Hr = dateFormatter.dateFromString(fromStr! as String)!
                dateFormatter.dateFormat = "hh:mm a"
                let date12Hr = dateFormatter.stringFromDate(date24Hr)
                
                let from: UILabel = (cell!.viewWithTag(307) as! UILabel)
                from.text = date12Hr
                let drImg: UIImageView = (cell!.viewWithTag(304) as! UIImageView)
                let profilePic = value?.valueForKey("profile") as! NSString
                drImg.layer.cornerRadius = drImg.frame.size.height/2
                drImg.layer.borderWidth = self.view.frame.size.height*0.008
                drImg.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
                if let url = NSURL(string: "http://\(profilePic)") {
                    drImg.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
                }
                return cell!
            }
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let item = self.cells.items[indexPath.row]
         let value = item.value as? NSMutableDictionary
        if item is SwiftyAccordionCells.HeaderItem {
            let indexPath = appointmentTableVw.indexPathForSelectedRow
            let currentCell = appointmentTableVw.cellForRowAtIndexPath(indexPath!)! as UITableViewCell
            headerVw.layer.cornerRadius = 7;
            imageDrop.image = UIImage(named: "dropDown")
            let vw: UIView = (currentCell.viewWithTag(300))!
            vw.layer.cornerRadius = 0;
            let path = UIBezierPath(roundedRect:vw.bounds, byRoundingCorners:[.TopLeft, .TopRight], cornerRadii: CGSize(width: 7, height:  7))
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.CGPath
            vw.layer.mask = maskLayer
            headerVw = vw

            let dropDown: UIImageView = (currentCell.viewWithTag(56) as! UIImageView)
            dropDown.image = UIImage(named: "dropUp")
            imageDrop = dropDown
            
            if self.selectedHeaderIndex == nil {
                self.selectedHeaderIndex = indexPath!.row
            } else {
                self.previouslySelectedHeaderIndex = self.selectedHeaderIndex
                self.selectedHeaderIndex = indexPath!.row
            }
            if let previouslySelectedHeaderIndex = self.previouslySelectedHeaderIndex {
                self.cells.collapse(previouslySelectedHeaderIndex)
            }
            if self.previouslySelectedHeaderIndex != self.selectedHeaderIndex {
                self.cells.expand(self.selectedHeaderIndex!)
            } else {
                self.selectedHeaderIndex = nil
                self.previouslySelectedHeaderIndex = nil
                headerVw.layer.cornerRadius = 0
                headerVw.layer.cornerRadius = 7;
                dropDown.image = UIImage(named: "dropDown")
            }
            self.appointmentTableVw.beginUpdates()
            self.appointmentTableVw.endUpdates()
        } else {
            if(appointmntSgmnt.selectedSegmentIndex == 0){
                if indexPath.row != self.selectedItemIndex {
                    //let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard!.instantiateViewControllerWithIdentifier("PatientWaitingRoomViewController") as! PatientWaitingRoomViewController
                    let schFrom = value?.valueForKey("From") as! String
                    let schTo = value?.valueForKey("To") as! String
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.timeStyle = .ShortStyle
                    dateFormatter.dateFormat = "k:mm" // k = Hour in 1~24, mm = Minute
                    let timeZone = NSTimeZone(name: "UTC")
                    dateFormatter.timeZone=timeZone
                   let timeTo = dateFormatter.dateFromString("\(schTo)")!
                   let timeFrom = dateFormatter.dateFromString("\(schFrom)")!
                    switch timeTo.compare(timeFrom) {
                    case .OrderedAscending     :
                        print("TimeTo is small than TimeFrom")
                    case .OrderedDescending
                        :   print("timeto is greater than date timefrom")
                        let calendar = NSCalendar.currentCalendar()
                        let datecomponenets = calendar.components(NSCalendarUnit.Minute, fromDate: timeFrom, toDate: timeTo, options: NSCalendarOptions.MatchLast)
                        let min = datecomponenets.minute
                        vc.scheduleMin = "\(min)"
                    case .OrderedSame          :
                        print("Same")
                    }
                    vc.drSpecial = value?.valueForKey("Speciality") as! String
                    let firstname = value?.valueForKey("FirstName") as! String
                   // let lastname = value?.valueForKey("LastName") as! String
                    let lastname = value?.valueForKeyPath("DrId.LastName") as! String
                    vc.drname = "\(firstname) \(lastname)"
                    vc.drAddres = value?.valueForKey("Address") as! String
                    vc.drLang = value?.valueForKey("Languages") as! String
                    vc.drQual = value?.valueForKey("Qualification") as! String
                    let fromStr = value?.valueForKey("From")  as? String
                    let dateFormatter1 = NSDateFormatter()
                    let timeZone1 = NSTimeZone(name: "GMT-8")
                    dateFormatter1.timeZone=timeZone1
                    dateFormatter1.dateFormat = "HH:mm"
                    let date24Hr = dateFormatter.dateFromString(fromStr! as String)!
                    dateFormatter.dateFormat = "hh:mm a"
                    let date12Hr = dateFormatter.stringFromDate(date24Hr)
                    vc.drscheduleTime = date12Hr
                    vc.drId = value?.valueForKey("Dr_id") as! String
                    vc.profilepicurl = value?.valueForKey("profile") as! NSString
                    let mob = value?.valueForKey("Mobile") as! NSString
                    let code = value?.valueForKey("Code") as! NSString
                    var compPh = "+1(\(code))\(mob)"
                    compPh = compPh.insert("-", ind: 10)
                    vc.drMobil = compPh
                    vc.drScheduleDate = value?.valueForKey("ScheduleDate") as! NSString
                    self.navigationController?.pushViewController(vc, animated: true)
                }
             }
           }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let item = self.cells.items[indexPath.row]
       // if(appointmntSgmnt.selectedSegmentIndex == 0){
            if item is SwiftyAccordionCells.HeaderItem {
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    
                    return self.view.frame.size.width*0.16
                }else{
                    if(self.view.frame.size.height > 1365){
                        return self.view.frame.size.width*0.1
                    }else{
                        return self.view.frame.size.width*0.11
                    }
                }
            } else if (item.isHidden) {
                return 0
            } else {
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    return self.view.frame.size.width*0.23
                }else{
                    if(self.view.frame.size.height > 1365){
                        return self.view.frame.size.width*0.13
                    }else{
                        return self.view.frame.size.width*0.15
                    }
                }
                
                //return self.view.frame.size.width*0.2
        }
//        }else{
//            if item is SwiftyAccordionCells.HeaderItem {
//                return self.view.frame.size.width*0.16
//            } else if (item.isHidden) {
//                return 0
//            } else {
//                return self.view.frame.size.width*0.23
//            }
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   

  }
extension PatientAppointmentViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
