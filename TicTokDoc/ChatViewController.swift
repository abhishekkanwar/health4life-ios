

import UIKit
import OpenTok
import RealmSwift
import SafariServices
import AVFoundation
import IQKeyboardManagerSwift
import KMPlaceholderTextView
class ChatViewController: UIViewController,UITextViewDelegate,OTPublisherDelegate,UIScrollViewDelegate,OTSessionDelegate,OTSubscriberKitDelegate,UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    @IBOutlet weak var userNameText: UILabel!
    @IBOutlet weak var textChatTableVw: UITableView!
    @IBOutlet weak var connectingLbl: UILabel!
    @IBOutlet weak var sndingVw: UIView!
    @IBOutlet weak var textmsgTxtFld: KMPlaceholderTextView!
    @IBOutlet weak var chatTxtVw: UIView!
    var session: OTSession!
    var publisher: OTPublisher!
    var subscriber: OTSubscriber!
    var msgAr = NSMutableArray()
    var tempMsgAr = NSMutableArray()
    var msgClr = Bool()
    var msgLbl = UILabel()
    var labelBounds = CGRect()
    var rowHeight = CGFloat()
    var textUser = NSString()
    var dbChatMsgs = NSMutableArray()
    
    var chatUserId = ""
    var userId = ""
    var anotherUserId = NSString()
    var realmArray = NSMutableArray()
    var userName = NSString()
    var realm = try! Realm()
    var tapGesture = UITapGestureRecognizer()
    
    
    var apiKey = ""
    var SessionID = ""
    var Token = ""
    var push = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        textmsgTxtFld.delegate = self
        chatTxtVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        chatTxtVw.layer.borderColor   = UIColor.darkGrayColor().CGColor
        chatTxtVw.layer.masksToBounds = false;
        chatTxtVw.layer.borderWidth   = 0.8
        //IQKeyboardManager.sharedManager().enableAutoToolbar = false
        title = "\(textUser)"
        userNameText.text = userName as String
        textmsgTxtFld.tintColor = UIColor.blackColor()
        textChatTableVw.separatorStyle = .None
        session = OTSession(apiKey: apiKey, sessionId: SessionID as String, delegate: self)
        textChatTableVw.delegate = self
        textChatTableVw.dataSource = self
        doConnect()
        connectingLbl.text = "Connecting..."
        connectingLbl.textColor = UIColor.redColor()
        sndingVw.userInteractionEnabled = false
        
        self.doRequestPost("\(Header.BASE_URL)messageChat/messageRead", data: ["UserId":"\(userId)","GroupId":"\(chatUserId)"])
    }
    
    func doRequestPost(url:String,data:[String: NSObject]){
        print("URL FOR DEVICETOKEN ==> \(url)")
        let requestDic = data
        print(requestDic)
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        
        print("Request Object:\(data)")
        print("Request string = \(jsonString!)")
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                print("Succes:")
                let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                dispatch_async(dispatch_get_main_queue()) {
                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                }
            }
        }
        dataTask.resume()
    }
    func hideKeyboard() {
        for tapGesture in textChatTableVw.gestureRecognizers! {
            textChatTableVw.removeGestureRecognizer(tapGesture)
        }
        textChatTableVw.removeGestureRecognizer(tapGesture)
        textChatTableVw.userInteractionEnabled = true
        textmsgTxtFld.resignFirstResponder()
    }
    //    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
    //        if touch.view?.isDescendantOfView(self.textChatTableVw) == true {
    //            return false
    //        }
    //        return true
    //    }
    //    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    //        self.view.endEditing(true)
    //    }
    
    func textViewShouldReturn(textView: UITextView) -> Bool {
        for tapGesture in textChatTableVw.gestureRecognizers! {
            textChatTableVw.removeGestureRecognizer(tapGesture)
        }
        textChatTableVw.removeGestureRecognizer(tapGesture)
        textChatTableVw.userInteractionEnabled = true
        textView.resignFirstResponder()
        return true
    }
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = true
        textChatTableVw.addGestureRecognizer(tapGesture)
        return true
    }
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        for tapGesture in textChatTableVw.gestureRecognizers! {
            textChatTableVw.removeGestureRecognizer(tapGesture)
        }
        textChatTableVw.removeGestureRecognizer(tapGesture)
        textChatTableVw.userInteractionEnabled = true
        return true
    }
    override func viewDidAppear(animated: Bool) {
        
    }
    
    override func viewWillAppear(animated: Bool){
        if(msgAr.count != 0)
        {
            dispatch_async(dispatch_get_main_queue(),{
                let indexPath = NSIndexPath.init(forRow: self.msgAr.count-1, inSection: 0)
                self.textChatTableVw.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom
                    , animated: false)
            })
        }
        else{
            for group in tempMsgAr
            {
                
                msgAr.addObject(group)
                
                self.textChatTableVw.reloadData()
                self.textChatTableVw.layoutIfNeeded()
                
                let indexPath = NSIndexPath.init(forRow: self.msgAr.count-1, inSection: 0)
                self.textChatTableVw.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: false)
            }
        }
        //        msgAr.removeAllObjects()
        //        realmArray.removeAllObjects()
        //
        ////        let user = RealmInteractor.shared.getUserBy(self.chatUserId)
        ////
        ////        for i in 0..<user.groupMsgs.count
        ////        {
        ////            let group = user.groupMsgs[i] as Group
        ////            let chatMsg = NSMutableDictionary()
        ////            chatMsg.setValue(group.msgId, forKey: "msgId")
        ////            chatMsg.setValue(group.groupId, forKey: "groupId")
        ////            chatMsg.setValue(group.fromId, forKey: "fromId")
        ////            chatMsg.setValue(group.message, forKey: "message")
        ////            chatMsg.setValue(group.messageType, forKey: "messageType")
        ////            chatMsg.setValue(group.readStatus, forKey: "readStatus")
        ////            realmArray.addObject(chatMsg)
        ////        }
        //
        //
        //        for dict in realmArray
        //        {
        //            var mySelf = false
        //            let msgDict = dict as! NSMutableDictionary
        //
        //            print("fromId => \(msgDict.valueForKey("fromId") as! String)")
//            print("userId => \(self.userId)")
//
//
//            if(msgDict.valueForKey("fromId") as! String == userId)
//            {
//                mySelf = true
//            }
//
//            let mdict = ["msg":msgDict.valueForKey("message") as! String,"bool":mySelf, "msgType":msgDict.valueForKey("messageType") as! String]
//            msgAr.addObject(mdict)
//
//        }
//        textChatTableVw.reloadData()
//
//        //doConnect()
        self.navigationController?.navigationBarHidden=true
    }
    func getMsgsFromServer() -> Void {
        
        RealmInteractor.shared.getMsgsFromServer(self.userId, groupId: self.chatUserId) { (result) in
            
            if(result.valueForKey("status") as! Int == 200)
            {
                self.msgAr.removeAllObjects()
                
                let chatArray = result.objectForKey("data") as! NSArray
                
                for dict in chatArray
                {
                    var mySelf = false
                    let msgDict = dict as! NSDictionary
                    
                    if(msgDict.valueForKey("From") as! String == self.userId)
                    {
                        mySelf = true
                    }
                    
                    let mdict = ["msg":msgDict.valueForKey("message") as! String,"bool":mySelf, "msgType":msgDict.valueForKey("messageType") as! String]
                    self.msgAr.addObject(mdict)
                    
                }
                
                self.textChatTableVw.reloadData()
                
            }
            
        }
    }
    @IBAction func sendBtnAction(sender: AnyObject) {
        if(textmsgTxtFld.text == ""){
          textmsgTxtFld.resignFirstResponder()
        }
        else{
           self.sendChatMessage()
        }
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func attachmentAction(sender: AnyObject) {
        self.view.endEditing(true)
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        actionSheet.showInView(self.view)
    }
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex){
        case 0:
            print("Cancel")
        case 1:
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.allowsEditing = false
                        imagePicker.sourceType = .Camera
                        self.presentViewController(imagePicker, animated: true, completion:nil)
                    }
                })
            }else{
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .Camera
                    presentViewController(imagePicker, animated: true, completion:nil)
                }
            }
        case 2:
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.allowsEditing = false
                        imagePicker.sourceType = .PhotoLibrary
                        imagePicker.mediaTypes = ["public.image", "public.movie"]
                        self.presentViewController(imagePicker, animated: true, completion:nil)
                    }
                })
            }else{
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .PhotoLibrary
                    imagePicker.mediaTypes = ["public.image", "public.movie"]
                    //imagePicker.view.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.mainScreen().bounds.size.width, height: UIScreen.mainScreen().bounds.size.height)
                    
                    self.presentViewController(imagePicker, animated: true, completion:nil)
                    
                    //self.view.addSubview(imagePicker.view)
                }
            }
        default:
            print("Default")
            //Some code here..
        }
    }
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        let selectedImage : UIImage = image
        //let resizeImage = selectedImage.resizeWithPercentage(0.1)
        let imageData = UIImageJPEGRepresentation(selectedImage, 0.4)
        self.uploadFile(imageData!,ext: "jpg")
        //        createAcProfilePic.image = selectedImage
        //        createAcProfilePic.clipsToBounds = true
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
         let mediaType = info[UIImagePickerControllerMediaType] as! NSString
       if(mediaType == "public.image"){
        let imgUrl =  info["UIImagePickerControllerOriginalImage"] as! UIImage
        do {
             let imageData = UIImageJPEGRepresentation(imgUrl, 0.4)
            self.uploadFile(imageData!,ext: "jpg")
        } catch {
            print(error)
            return
        }
        
        }else{
            let videoURL = info["UIImagePickerControllerMediaURL"] as? NSURL
            
            let fileExt  = videoURL!.lastPathComponent
            
            
            var movieData: NSData?
            do {
                let video = try NSData(contentsOfURL: videoURL!, options: .DataReadingMappedIfSafe)
                self.uploadFile(video,ext: "mp4")
            } catch {
                print(error)
                return
            }
        }
    
        
        
        //let videoData = NSData(con
        
        //self.uploadFile(videoData!)
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func uploadFile(docData: NSData, ext:String) {
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "uploading...")
        var string: String = ""
        
        //        var request = NSMutableURLRequest(url: NSURL(string: "https://staging.tiktokdok.com:10001/videoChat/uploadApi")!, cachePolicy: NSURLRequest.CachePolicy(rawValue: 0)!, timeoutInterval: 10.0)
        
        let request = NSMutableURLRequest(URL: NSURL(string: "\(Header.BASE_URL)videoChat/uploadApi")!, cachePolicy: NSURLRequestCachePolicy(rawValue: UInt(0))!, timeoutInterval: 10.0)
        
        request.HTTPMethod = "POST"
        request.addValue("0", forHTTPHeaderField: "devicetype")
        
        let body = NSMutableData()
        let my_time: Double = NSDate().timeIntervalSince1970
        
        let imageName: String = "\(1)\(Int(my_time))"
        let imagetag: String = "Content-Disposition: form-data; name=\"File\"; filename=\""
        string = "\(imagetag)\(imageName)\(".\(ext)\"\r\n\"")"
        let boundary: String = "---------------------------14737809831466499882746641449"
        let contentType: String = "multipart/form-data; boundary=\(boundary)"
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        
        body.appendData("\r\n--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData(string.dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData("Content-Type: application/octet-stream\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        body.appendData(docData)
        body.appendData("\r\n--\(boundary)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        request.HTTPBody = body
        
        NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
           dispatch_async(dispatch_get_main_queue(),{
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            
            if(data?.length != 0 && error == nil)
            {
                let str = String(data: data!, encoding: NSUTF8StringEncoding)
                let result = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: UInt(0)))
                
                if let resultDict = result as? NSDictionary{
                    
                    let error: OTError? = nil
                    do {
                        
                        let docLink = resultDict.valueForKey("link")
                        
                        let defaults = NSUserDefaults.standardUserDefaults()
                        let usertype = defaults.valueForKey("usertype")
                        let firstname:String = defaults.valueForKey("firstName") as! String
                        let msgId = "\(usertype!)_"+"\(NSDate())"
                        let chatMsg = NSMutableDictionary()
                        chatMsg.setValue(msgId, forKey: "msgId")
                        chatMsg.setValue(self.chatUserId, forKey: "groupId")
                        chatMsg.setValue(self.userId, forKey: "fromId")
                        chatMsg.setValue(docLink, forKey: "message")
                        chatMsg.setValue("link", forKey: "messageType")
                        if(usertype as! NSObject == 1){
                            chatMsg.setValue("Dr. \(defaults.valueForKey("lastName") as! String)", forKey: "displayName")
                        }else{
                            chatMsg.setValue(firstname, forKey: "displayName")
                        }
                        //chatMsg.setValue(firstname, forKey: "displayName")
                        chatMsg.setValue(false, forKey: "readStatus")
                        
                        let strinData = try! NSJSONSerialization.dataWithJSONObject(chatMsg, options: NSJSONWritingOptions.PrettyPrinted)
                        let str = NSString(data: strinData, encoding: 0)
                        
                        self.session.signalWithType("chat", string:  str as? String, connection: nil ,error: nil)
                    }
                    if error != nil {
                        print("Signal error: \(error!)")
                    }
                    else {
                        print("Signal sent: ")
                    }
                    
                    
                }
                
            }else{
                let alert = UIAlertController(title: "Alert", message: "Sending Failed.", preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "Retry", style: UIAlertActionStyle.Cancel) {
                    UIAlertAction in
                   self.uploadFile(docData,ext: ext)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        })
            }.resume()
        
        
        //        body.append("\r\n--\(boundary)\r\n".data(using: String.Encoding.utf8))
        //        body.append(string.data(using: String.Encoding.utf8))
        //        body.append("Content-Type: application/octet-stream\r\n\r\n".data(using: String.Encoding.utf8))
        
    }
    
    
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    @IBAction func sendBtnAc(sender: AnyObject) {
         textmsgTxtFld.resignFirstResponder()
        if(textmsgTxtFld.text == ""){
        textmsgTxtFld.resignFirstResponder()
        }else{
         textmsgTxtFld.resignFirstResponder()
        self.sendChatMessage()
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.textmsgTxtFld.endEditing(true)
    }
    
    
    
    // MARK: - OpenTok Methods
    func doConnect() {
        if let session = self.session {
            var maybeError : OTError?
            session.connectWithToken(Token as String, error: &maybeError)
            if let error = maybeError {
                let alert = UIAlertController(title: "Alert", message: "Connection Time Out", preferredStyle: UIAlertControllerStyle.Alert)
                let cancelAction = UIAlertAction(title: "Try Later!", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.navigationController?.popViewControllerAnimated(true)
                }
                alert.addAction(cancelAction)
                self.presentViewController(alert, animated: true, completion: nil)
                //showAlert(error.localizedDescription)
            }
        }
    }
    func doPublish() {
        publisher = OTPublisher(delegate: self)
        
        var maybeError : OTError?
        session?.publish(publisher, error: &maybeError)
        
        if let error = maybeError {
            //print(error.localizedDescription)
            let alert = UIAlertController(title: "Alert", message: "Connection Time Out", preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: "Try Later!", style: UIAlertActionStyle.Default) {
                UIAlertAction in
                self.navigationController?.popViewControllerAnimated(true)
            }
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        publisher!.view!.frame = CGRect(x: 20.0, y: self.view.frame.size.height-280, width: 200, height: 200)
    }
    func doSubscribe(stream : OTStream) {
        if let session = self.session {
            subscriber = OTSubscriber(stream: stream, delegate: self)
            
            var maybeError : OTError?
            session.subscribe(subscriber, error: &maybeError)
            if let error = maybeError {
                //showAlert(error.localizedDescription)
                let alert = UIAlertController(title: "Alert", message: "Connection Time Out", preferredStyle: UIAlertControllerStyle.Alert)
                let cancelAction = UIAlertAction(title: "Try Later!", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.navigationController?.popViewControllerAnimated(true)
        }
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    func sendChatMessage() {
         let defaults = NSUserDefaults.standardUserDefaults()
        let firstname:String = defaults.valueForKey("firstName") as! String
        let error: OTError? = nil
        do {
            let defaults = NSUserDefaults.standardUserDefaults()
            let usertype = defaults.valueForKey("usertype")
            let msgId = "\(usertype!)_"+"\(NSDate())"
            let chatMsg = NSMutableDictionary()
            chatMsg.setValue(msgId, forKey: "msgId")
            chatMsg.setValue(chatUserId, forKey: "groupId")
            chatMsg.setValue(userId, forKey: "fromId")
            chatMsg.setValue("\(firstname):-\n\(textmsgTxtFld.text!)", forKey: "message")
            chatMsg.setValue("text", forKey: "messageType")
            if(usertype as! NSObject == 1){
                chatMsg.setValue("Dr. \(defaults.valueForKey("lastName") as! String):-\n\(textmsgTxtFld.text!)", forKey: "message")
                chatMsg.setValue("Dr. \(firstname)", forKey: "displayName")
            }else{
                chatMsg.setValue("\(firstname):-\n\(textmsgTxtFld.text!)", forKey: "message")
                chatMsg.setValue(firstname, forKey: "displayName")
            }
//            if(usertype as! NSObject == 1){
//             chatMsg.setValue("Dr. \(firstname)", forKey: "displayName")
//            }else{
//             chatMsg.setValue(firstname, forKey: "displayName")
//            }
            chatMsg.setValue(false, forKey: "readStatus")
            
            let strinData = try! NSJSONSerialization.dataWithJSONObject(chatMsg, options: NSJSONWritingOptions.PrettyPrinted)
            let str = NSString(data: strinData, encoding: NSUTF8StringEncoding)
            
            session.signalWithType("chat", string:  str as? String, connection: nil ,error: nil)
        }
        if error != nil {
            print("Signal error: \(error!)")
        }
        else {
            print("Signal sent: \(textmsgTxtFld.text)")
        }
        self.textmsgTxtFld.text = ""
//        let sections = 1
//        let rows = textChatTableVw.numberOfRowsInSection(sections - 1)
//        textChatTableVw.scrollToRowAtIndexPath(NSIndexPath(forRow: rows - 1, inSection: sections - 1), atScrollPosition: .Bottom, animated: true)
    }
    
    func logSignalString(string: String, fromSelf: Bool, msgType:String, name:String) {
        let dict = ["msg":string,"bool":fromSelf, "msgType":msgType, "name":name]
        msgAr.addObject(dict)
        textChatTableVw.reloadData()
        
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            let indexPath = NSIndexPath.init(forRow: self.msgAr.count-1, inSection: 0)
            self.textChatTableVw.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
        }
        
        
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return msgAr.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MessageTableViewCell", forIndexPath: indexPath) as! LGChatMessageCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        
        let dic = self.msgAr[indexPath.row]
        print(dic)
        let message = dic.valueForKey("msg") as! String
        if((dic.valueForKey("msgType") as! String) == "link")
        {
            msgClr = dic["bool"] as! Bool
            cell.msgcolor = msgClr
            cell.setupThumbnail(message,name: (dic["name"] as! String))
        }
        else
        {
            let padding: CGFloat = 10.0
            // print(dic)
            
            let height = cell.setupWithMessage(message).height
            rowHeight = height + padding
            
            msgClr = dic["bool"] as! Bool
            cell.msgcolor = msgClr
            //cell.backgroundColor = UIColor.blueColor()
            cell.setupWithMessage(message)
            cell.selectionStyle = .None
        }
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let dic = self.msgAr[indexPath.row]
        if((dic["msgType"] as! String) == "link")
        {
            return 110
        }
        return rowHeight
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.textmsgTxtFld.endEditing(true)
        
        let dic = self.msgAr[indexPath.row]
        
      //  print("\(dic)")
        
        if((dic["msgType"] as! String) == "link")
        {
            if #available(iOS 9.0, *) {
                let controller = SFSafariViewController(URL: NSURL.init(string: dic["msg"] as! String)!)
                self.presentViewController(controller, animated: true, completion: nil)
            } else {
                // Fallback on earlier versions
            }
            
        }
    }
    func session(session: OTSession, receivedSignalType type: String?, fromConnection connection: OTConnection?, withString string: String?) {
        print("Received signal \(string!)")
        
        let recData = string?.dataUsingEncoding(NSUTF8StringEncoding)
        let recDictionary = try! NSJSONSerialization.JSONObjectWithData(recData!, options: NSJSONReadingOptions(rawValue: UInt(0)))
        
        
        try! realm.write({
            let group = Group()
            group.displayName = recDictionary.valueForKey("displayName") as! String
            group.message = recDictionary.valueForKey("message") as! String
            group.messageType = recDictionary.valueForKey("messageType") as! String
            group.msgId = recDictionary.valueForKey("msgId") as! String
            group.fromId = recDictionary.valueForKey("fromId") as! String
            group.groupId = chatUserId
           realm.add(group)
        })
        
        
        var fromSelf = false
        if (connection!.connectionId == session.connection!.connectionId) {
            fromSelf = true
        }
        
        let groupDict = NSMutableDictionary()
        groupDict.setValue(chatUserId, forKey:"GroupId")
        groupDict.setValue((recDictionary.valueForKey("fromId") as! String), forKey:"From")
        groupDict.setValue((recDictionary.valueForKey("message") as! String), forKey:"message")
        groupDict.setValue((recDictionary.valueForKey("msgId") as! String), forKey:"messageId")
        groupDict.setValue((recDictionary.valueForKey("messageType") as! String), forKey:"messageType")
        groupDict.setValue((recDictionary.valueForKey("displayName") as! String), forKey:"DisplayName")
        
        let groupArray = NSMutableArray()
        groupArray.addObject(groupDict)
        if(self.subscriber == nil){
         self.push = 1
        }else{
            self.push = 0
        }
       let chatDict = NSDictionary(objects: [groupArray,self.push,self.userId,self.chatUserId], forKeys: ["Data","BatchPush","PushUserId","GroupId"])
        if(fromSelf)
        {
            RealmInteractor.shared.syncMsgsWithMsg(chatDict, push: false)
        }
        //let params = ["Data" : groupArray,"BatchPush": Int(0),"PushUserId":self.chatUserId]
//        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
//        RealmInteractor.shared.syncMsgsWithMsg(chatDict, push: false)
//        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        self.logSignalString(recDictionary.valueForKey("message") as! String, fromSelf: fromSelf, msgType: recDictionary.valueForKey("messageType") as! String, name:recDictionary.valueForKey("displayName") as! String)
        
    }
    
    /**
     * Cleans the subscriber from the view hierarchy, if any.
     */
    func doUnsubscribe() {
        if let subscriber = self.subscriber {
            var maybeError : OTError?
            session?.unsubscribe(subscriber, error: &maybeError)
            if let error = maybeError {
                //showAlert(error.localizedDescription)
                let alert = UIAlertController(title: "Alert", message: "Connection Time Out", preferredStyle: UIAlertControllerStyle.Alert)
                let cancelAction = UIAlertAction(title: "Try Later!", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.navigationController?.popViewControllerAnimated(true)
                }
                alert.addAction(cancelAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            subscriber.view!.removeFromSuperview()
            self.subscriber = nil
        }
    }
    
    // MARK: - OTSession delegate callbacks
    
    func sessionDidConnect(session: OTSession) {
        connectingLbl.text = "Connected"
        connectingLbl.textColor = UIColor.whiteColor()
        sndingVw.userInteractionEnabled = true
        NSLog("sessionDidConnect (\(session.sessionId))")
    }
    
    func sessionDidDisconnect(session : OTSession) {
        connectingLbl.text = "Connecting..."
        connectingLbl.textColor = UIColor.redColor()
        sndingVw.userInteractionEnabled = false
         self.doConnect()
        
        NSLog("Session disconnected (\( session.sessionId))")
    }
    
    func session(session: OTSession, streamCreated stream: OTStream) {
        NSLog("session streamCreated (\(stream.streamId))")
        
        
        //        if subscriber == nil && !SubscribeToSelf {
        //        }
    }
    
    func session(session: OTSession, streamDestroyed stream: OTStream) {
        NSLog("session streamCreated (\(stream.streamId))")
        
        if (subscriber?.stream!.streamId)! == stream.streamId {
            doUnsubscribe()
        }
    }
    
    func session(session: OTSession, connectionCreated connection : OTConnection) {
        NSLog("session connectionCreated (\(connection.connectionId))")
    }
    
    func session(session: OTSession, connectionDestroyed connection : OTConnection) {
        NSLog("session connectionDestroyed (\(connection.connectionId))")
    }
    
    func session(session: OTSession, didFailWithError error: OTError) {
        NSLog("session didFailWithError (%@)", error)
        let alert = UIAlertController(title: "Alert", message: "Connection Time Out", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "Try Later!", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.navigationController?.popViewControllerAnimated(true)
        }
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    // MARK: - OTSubscriber delegate callbacks
    
    func subscriberDidConnectToStream(subscriberKit: OTSubscriberKit) {
        NSLog("subscriberDidConnectToStream (\(subscriberKit))")
        if let view = subscriber?.view {
            view.frame =  CGRect(x: 0.0, y: 60, width: self.view.frame.size.width, height: self.view.frame.size.height-115)
        }
    }
    
    func subscriber(subscriber: OTSubscriberKit, didFailWithError error : OTError) {
        NSLog("subscriber %@ didFailWithError %@", subscriber.stream!.streamId, error)
    }
    
    // MARK: - OTPublisher delegate callbacks
    
    func publisher(publisher: OTPublisherKit, streamCreated stream: OTStream) {
        NSLog("publisher streamCreated %@", stream)
        
        // Step 3b: (if YES == subscribeToSelf): Our own publisher is now visible to
        // all participants in the OpenTok session. We will attempt to subscribe to
        // our own stream. Expect to see a slight delay in the subscriber video and
        // an echo of the audio coming from the device microphone.
        //        if subscriber == nil && SubscribeToSelf {
        //           // doSubscribe(stream)
        //        }
    }
    
    func publisher(publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
        NSLog("publisher streamDestroyed %@", stream)
        
        if (subscriber?.stream!.streamId)! == stream.streamId {
            doUnsubscribe()
        }
    }
    
    func publisher(publisher: OTPublisherKit, didFailWithError error: OTError) {
        NSLog("publisher didFailWithError %@", error)
    }
    
    
    func showAlert(message: String) {
        dispatch_async(dispatch_get_main_queue()) {
            
            let alertController = UIAlertController(title: "Oops",
                                                    message:message, preferredStyle: UIAlertControllerStyle.Alert)
            
            alertController.addAction(UIAlertAction(title: "Ok",
                                                    style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func serverReponse(responseDict: NSDictionary,serviceurl:NSString)
    {
        //        print("responceDictionary=\(responseDict)")
        //        print(responseDict.valueForKey("status"))
        //
        //        let stringNumber=responseDict.valueForKey("status")as! String
        //        let success = Int(stringNumber)
        
    }
    
    func failureRsponseError(failureError:NSError)
    {
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isLogIn")
        
    }
    // MARK:textfield Delegate:
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
//MARK: Message Cell

class LGChatMessageCell : UITableViewCell {
    
    
    private lazy var thumbnailView : UIImageView = {
        
        let thumbImage = UIImageView()
        thumbImage.frame = CGRectMake(10.0, 10.0, CGRectGetWidth(self.bounds) * 0.75, 100.0)
        
        thumbImage.layer.cornerRadius = 6
        //thumbImage.layer.borderWidth = 2.0
        thumbImage.contentMode = UIViewContentMode.ScaleAspectFill
        thumbImage.clipsToBounds = true
        thumbImage.backgroundColor = UIColor.redColor()
        thumbImage.hidden = true
        self.contentView.addSubview(thumbImage)
        
        return thumbImage
    }()
    
    
    
    // MARK: Global MessageCell Appearance Modifier
    var msgcolor = Bool()
    struct Appearance {
        //static var opponentColor = UIColor(red: 0.142954, green: 0.60323, blue: 0.862548, alpha: 0.88)
        static var opponentColor = UIColor.lightGrayColor()
        static var userColor = UIColor.redColor()
        //static var userColor = UIColor(red: 0.14726, green: 0.838161, blue: 0.533935, alpha: 1)
        static var font: UIFont = UIFont.systemFontOfSize(17.0)
    }
    
    /*
     These methods are included for ObjC compatibility.  If using Swift, you can set the Appearance variables directly.
     */
    
    class func setAppearanceOpponentColor(opponentColor: UIColor) {
        Appearance.opponentColor = opponentColor
    }
    
    class func setAppearanceUserColor(userColor: UIColor) {
        Appearance.userColor = userColor
    }
    
    class  func setAppearanceFont(font: UIFont) {
        Appearance.font = font
    }
    
    // MARK: Message Bubble TextView
    
    private lazy var textView: MessageBubbleTextView = {
        let textView = MessageBubbleTextView(frame: CGRectZero, textContainer: nil)
        textView.backgroundColor = UIColor.blueColor()

       // textView.textColor = UIColor.whiteColor()
        self.contentView.addSubview(textView)
        return textView
    }()
    
    
    
    
    
    private class MessageBubbleTextView : UITextView {
        
        override init(frame: CGRect = CGRectZero, textContainer: NSTextContainer? = nil) {
            super.init(frame: frame, textContainer: textContainer)
            self.font = Appearance.font
            self.scrollEnabled = false
            self.editable = false
            self.textContainerInset = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
            self.layer.cornerRadius = 6
            self.layer.borderWidth = 2.0
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    // MARK: Sizing
    private let padding: CGFloat = 5.0
    
    private let minimumHeight: CGFloat = 30.0 // arbitrary minimum height
    
    private var size = CGSizeZero
    
    private var maxSize: CGSize {
        get {
            let maxWidth = CGRectGetWidth(self.bounds) * 0.75 // Cells can take up to 3/4 of screen
            let maxHeight = CGFloat.max
            return CGSize(width: maxWidth, height: maxHeight)
        }
    }
    
    // MARK: Setup Call
    /*!
     Use this in cellForRowAtIndexPath to setup the cell.
     */
    
    func setupThumbnail(message: NSString, name:String) -> Void {
        
        if let btn = thumbnailView.viewWithTag(10001) as? UIButton
        {
            btn.removeFromSuperview()
        }
        
        if let nameLbl = thumbnailView.viewWithTag(10002) as? UILabel
        {
            nameLbl.removeFromSuperview()
        }
        
        let nameLabel = UILabel()
        nameLabel.text = name
        nameLabel.tag = 10002
        nameLabel.frame = CGRectMake(5, 5, thumbnailView.frame.size.width-10, 12)
        nameLabel.font = UIFont.systemFontOfSize(12)
        
        thumbnailView.addSubview(nameLabel)
        
        
        if(msgcolor == true){
            let x = UIScreen.mainScreen().bounds.width
            textView.frame = CGRectMake(x-(thumbnailView.frame.size.width)-20, 10, thumbnailView.frame.size.width, thumbnailView.frame.size.height)
            thumbnailView.frame = CGRectMake(x-(thumbnailView.frame.size.width)-20, 10, thumbnailView.frame.size.width, thumbnailView.frame.size.height)
            nameLabel.textColor = UIColor.blackColor()
            //textView.backgroundColor = UIColor.lightGrayColor()
        }
        else
        {
            nameLabel.textColor = UIColor.grayColor()
            textView.frame = CGRectMake(10,10, thumbnailView.frame.size.width, thumbnailView.frame.size.height)
        }
        
        //textView.frame = CGRectMake(10,10, thumbnailView.frame.size.width, thumbnailView.frame.size.height)
        textView.text = ""
        textView.hidden = true
        
        thumbnailView.hidden = false
        thumbnailView.backgroundColor = UIColor.redColor()
        

        let urlArray = message.componentsSeparatedByString(".")
        
        
        print("\(self.getMIMEType(urlArray.last!.uppercaseString))")
        
        
        
        
        if(self.getMIMEType(urlArray.last!.uppercaseString) == "image")
        {
            //thumbnailView.sd_setImageWithURL(NSURL(string: message as String), completed:nil)
            thumbnailView.setShowActivityIndicatorView(true)
            thumbnailView.setIndicatorStyle(.Gray)
            thumbnailView.sd_setImageWithURL(NSURL(string: message as String), placeholderImage: UIImage(named: "image_placeholder"), completed: nil)
        }
        else if(self.getMIMEType(urlArray.last!.uppercaseString) == "video")
        {
            
             thumbnailView.sd_setImageWithURL(NSURL(string: message as String), placeholderImage: UIImage(named: "video_placeholder"), completed: nil)
            
            let playBtnImg = UIImage(named: "play-icon")
            
            let playBtn = UIButton(type: .Custom)
            playBtn.setImage(playBtnImg, forState: .Normal)
            playBtn.tag = 10001
            playBtn.frame = CGRectMake((thumbnailView.frame.size.width-(playBtnImg?.size.width)!)/2, (thumbnailView.frame.size.height-(playBtnImg?.size.height)!)/2, playBtnImg!.size.width, playBtnImg!.size.height)
            thumbnailView.addSubview(playBtn)
            
            //thumbnailView.sd_setImageWithURL(NSURL(string: message as String), completed:nil)
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0)) {
                // ...Run some task in the background here...
            if let thumbnailImage = self.getThumbnailImage(forUrl: NSURL(string: message as String)!) {
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.thumbnailView.image = thumbnailImage
                }
                
                }
                
            }
        }
        else if(self.getMIMEType(urlArray.last!.uppercaseString) == "doc")
        {
//            thumbnailView.frame = CGRectMake(10,10, 50, thumbnailView.frame.size.height)
//            textView.frame = CGRectMake(thumbnailView.frame.size.width,10, thumbnailView.frame.size.width, thumbnailView.frame.size.height)
//            textView.hidden = false
            
            thumbnailView.sd_setImageWithURL(NSURL(string: message as String), placeholderImage: UIImage(named: "document_placeholder"), completed: nil)
            
        }
    }
    
    
    func getThumbnailImage(forUrl url: NSURL) -> UIImage? {

            let asset: AVAsset = AVAsset(URL: url)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            
            do {
                let thumbnailImage = try imageGenerator.copyCGImageAtTime(CMTimeMake(1, 60) , actualTime: nil)
                return UIImage(CGImage: thumbnailImage)
            } catch let error {
                print(error)
            }
            
            return nil
        
    }
    
    
    func setupWithMessage(message: NSString) -> CGSize {
        
        thumbnailView.hidden = true
        textView.hidden = false
        
        var yourAttributes = [NSForegroundColorAttributeName: UIColor.blackColor(), NSFontAttributeName: UIFont.systemFontOfSize(12)]
        
        if(msgcolor == true)
        {
            yourAttributes = [NSForegroundColorAttributeName: UIColor.blackColor(), NSFontAttributeName: UIFont.systemFontOfSize(12)]
        }
        
        let yourOtherAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont.systemFontOfSize(20)]
        let first = message.componentsSeparatedByString("\n").first!
        print(first)
        let partOne = NSMutableAttributedString(string: "\(first)", attributes: yourAttributes)
    
        let msg = message.stringByReplacingOccurrencesOfString(first, withString: "")
    
        let partTwo = NSMutableAttributedString(string: "\(msg)", attributes: yourOtherAttributes)
        let combination = NSMutableAttributedString()
        
   
        combination.appendAttributedString(partOne)
        combination.appendAttributedString(partTwo)


        
        
        textView.attributedText = combination
        //textView.text = message as String
        textView.userInteractionEnabled = false
        size = textView.sizeThatFits(maxSize)
        if size.height < minimumHeight {
            size.height = minimumHeight
        }
        textView.bounds.size = size
         self.textView.layer.borderColor = UIColor.clearColor().CGColor
        print(size)
        if(msgcolor == true){
            let x = UIScreen.mainScreen().bounds.width
            textView.frame = CGRectMake(x-(size.width)-20, 10, size.width, size.height)
            textView.backgroundColor = UIColor.lightGrayColor()
        }else if(msgcolor == false){
            // let x = UIScreen.mainScreen().bounds.width
            textView.frame = CGRectMake(10,10, size.width, size.height)
            textView.backgroundColor = UIColor.init(red: 54/255, green: 203/255, blue: 179/255, alpha: 1)
        }
        
        
        return size
    }
    
    
    func getMIMEType(ext:String) -> String {
        
        switch ext {
        case "JPG":
            return "image"
            
        case "PNG":
            return "image"
            
        case "JPEG":
            return "image"
            
        case "BMP":
            return "image"
            
        case "MP4":
            return "video"
            
        case "M4A":
            return "video"
            
        case "MPEG":
            return "video"
            
        case "MOV":
            return "video"
            
        case "PDF":
            return "doc"
            
        case "DOC":
            return "doc"
            
        case "DOCX":
            return "doc"
            
        default:
            return "image"
        }
        
        
    }
    
    
}



