
import Foundation
import RealmSwift

class User: Object {
    dynamic var userId = ""
    dynamic var name = ""
    dynamic var age = ""
    dynamic var gender = ""
    dynamic var speciality = ""
    dynamic var profilePic = ""
    dynamic var groupId = ""
    dynamic var openTokSession = ""
    dynamic var openTokToken = ""
    dynamic var activeStatus = false
    dynamic var openTokAPIKey = ""
    dynamic var userType = 0
    dynamic var chatCount = 0
    var groupMsgs = List<Group>()
    override static func primaryKey() -> String? {
        return "userId"
    }
}

class Group: Object {
    dynamic var msgId = ""
    dynamic var groupId = ""
    dynamic var fromId = ""
    dynamic var message = ""
    dynamic var displayName = ""
    dynamic var messageType = ""
    dynamic var readStatus = false
    dynamic var timeStamp = ""
    dynamic var synced = false
}

