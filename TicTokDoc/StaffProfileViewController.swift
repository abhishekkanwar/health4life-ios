

import UIKit
import SlideMenuControllerSwift
import Toast_Swift
class StaffProfileViewController: UIViewController,WebServiceDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate {
    @IBOutlet weak var firstNameVw: UIView!
    @IBOutlet weak var lastNameVw: UIView!
    @IBOutlet weak var emailAddVw: UIView!
    @IBOutlet weak var codeVw: UIView!
    @IBOutlet weak var phonenumberVw: UIView!
    @IBOutlet weak var designationVw: UIView!
    
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var changePswrdBtn: UIButton!
    
    @IBOutlet weak var myProfileScrollVw: UIScrollView!
    @IBOutlet weak var scrollContentVw: UIView!
    
    @IBOutlet weak var staffCode: UILabel!
    @IBOutlet weak var imgVwProfile: UIImageView!
    
    @IBOutlet weak var phCodeText: UITextView!
    @IBOutlet weak var nameText: UITextView!
    @IBOutlet weak var emailAddText: UITextView!
    @IBOutlet weak var phnoText: UITextView!
    @IBOutlet weak var designationText: UITextView!
    @IBOutlet weak var lastNameTextVw: UITextView!
    
    @IBOutlet weak var myProfileTittleLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    var imageSet = Bool()
    var serverInt = NSInteger()
    var firstNm = NSString()
    var lastNm = NSString()
    var editable = Bool()
    var topConstraint = NSLayoutConstraint()
    var arCode = []
    var staff_Code = NSString()
    override func viewDidLoad() {
        super.viewDidLoad()
        arCode = ["201", "202", "203", "204", "205", "206", "207", "208", "209", "210", "212", "213", "214", "215", "216", "217", "218", "219", "224", "225", "226", "228", "229", "231", "234", "239", "240", "242", "246", "248", "250", "251", "252", "253", "254", "256", "260", "262", "264", "267", "268", "269", "270", "276", "281", "284", "289", "301", "302", "303", "304", "305", "306", "307", "308", "309", "310", "312", "313", "314", "315", "316", "317", "318", "319", "320", "321", "323", "325", "330", "331", "334", "336", "337", "339", "340", "343", "345", "347", "351", "352", "360", "361", "385", "386", "401", "402", "403", "404", "405", "406", "407", "408", "409", "410", "412", "413", "414", "415", "416", "417", "418", "419", "423", "424", "425", "430", "432", "434", "435", "438", "440", "441", "442", "443", "450", "456", "458", "469", "470", "473", "475", "478", "479", "480", "484", "500", "501", "502", "503", "504", "505", "506", "507", "508", "509", "510", "512", "513", "514", "515", "516", "517", "518", "519", "520", "530", "533", "534", "539", "540", "541", "551", "559", "561", "562", "563", "567", "570", "571", "573", "574", "575", "579", "580", "581", "585", "586", "587", "600", "601", "602", "603", "604", "605", "606", "607", "608", "609", "610", "612", "613", "614", "615", "616", "617", "618", "619", "620", "623", "626", "630", "631", "636", "641", "646", "647", "649", "650", "651", "657", "660", "661", "662", "664", "670", "671", "678", "681", "682", "684", "669", "700", "701", "702", "703", "704", "705", "706", "707", "708", "709", "710", "712", "713", "714", "715", "716", "717", "718", "719", "720", "724", "727", "731", "732", "734", "740", "747", "754", "757", "758", "760", "762", "763", "765", "767", "769", "770", "772", "773", "774", "775", "778", "779", "780", "781", "784", "785", "786", "787", "800", "801", "802", "803", "804", "805", "806", "807", "808", "809", "810", "812", "813", "814", "815", "816", "817", "818", "819", "828", "829", "830", "831", "832", "843", "845", "847", "848", "849", "850", "855", "856", "857", "858", "859", "860", "862", "863", "864", "865", "866", "867", "868", "869", "870", "872", "876", "877", "878", "888", "900", "901", "902", "903", "904", "905", "906", "907", "908", "909", "910", "912", "913", "914", "915", "916", "917", "918", "919", "920", "925", "928", "931", "936", "937", "938", "939", "940", "941", "947", "949", "951", "952", "954", "956", "970", "971", "972", "973", "978", "979", "980", "985", "989"]
        print(imgVwProfile?.image)
        self.navigationController?.navigationBarHidden = true
        self.viewSet()
        self.userInfo()
        self.editAndNonEdit()
        let tapGestureRecognizer2 = UITapGestureRecognizer(target:self, action:#selector(StaffProfileViewController.codeCopy(_:)))
        staffCode.addGestureRecognizer(tapGestureRecognizer2)
    }
    func textViewDidEndEditing(textView: UITextView) {
        if(imageSet != true){
            if(textView == nameText || textView == lastNameTextVw){
                if(nameText.text != "" && lastNameTextVw.text != ""){
                    let lblNameInitialize = UILabel()
                    lblNameInitialize.frame.size = CGSize(width: 100.0, height: 100.0)
                    lblNameInitialize.textColor = UIColor.whiteColor()
                    lblNameInitialize.font = UIFont(name: "Helvetica Bold", size: 30)
                    lblNameInitialize.text = String(nameText.text!.characters.first!) + String(lastNameTextVw.text!.characters.first!)
                    lblNameInitialize.textAlignment = NSTextAlignment.Center
                    lblNameInitialize.backgroundColor = UIColor.init(red: 45/255, green: 78/255, blue: 107/255, alpha: 1)
                    lblNameInitialize.layer.cornerRadius = 50.0
                    
                    UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
                    lblNameInitialize.layer.renderInContext(UIGraphicsGetCurrentContext()!)
                    imgVwProfile.image = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                }
                else{
                    let lblNameInitialize = UILabel()
                    lblNameInitialize.frame.size = CGSize(width: 100.0, height: 100.0)
                    lblNameInitialize.textColor = UIColor.whiteColor()
                    lblNameInitialize.font = UIFont(name: "Helvetica Bold", size: 30)
                    lblNameInitialize.text = String("") + String("")
                    lblNameInitialize.textAlignment = NSTextAlignment.Center
                    lblNameInitialize.backgroundColor = UIColor.init(red: 45/255, green: 78/255, blue: 107/255, alpha: 1)
                    lblNameInitialize.layer.cornerRadius = 50.0
                    
                    UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
                    lblNameInitialize.layer.renderInContext(UIGraphicsGetCurrentContext()!)
                    imgVwProfile.image = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                }
            }
        }
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        if(textView == nameText || textView == lastNameTextVw || textView == designationText){
            let currentCharacterCount = textView.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + text.characters.count - range.length
            if(newLength == 36){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "This field does not contain more than 35 characters.")
            }
            return newLength <= 35
        }
        if(textView == phnoText){
            let currentCharacterCount = textView.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + text.characters.count - range.length
            return newLength <= 7
        }
        if(textView == phCodeText){
            let currentCharacterCount = textView.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + text.characters.count - range.length
            return newLength <= 3
        }
        
        return true
    }
    
    func editAndNonEdit(){
        if(editable == true){
            editBtn.hidden = true
            lastNameVw.hidden = false
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                let topConstraint1 = NSLayoutConstraint(
                    item:lastNameVw,
                    attribute: NSLayoutAttribute.TopMargin,
                    relatedBy: NSLayoutRelation.Equal,
                    toItem: firstNameVw,
                    attribute: NSLayoutAttribute.BottomMargin,
                    multiplier: 1,
                    constant: 30)
                self.view.addConstraints([topConstraint1])
                self.view.removeConstraint(topConstraint)
            }
            self.nameText.editable = true
            self.phnoText.editable = true
            self.phCodeText.editable = true
            self.lastNameTextVw.editable = true
            
            nameLbl.text = "First Name"
            nameText.text = firstNm as String
            myProfileTittleLbl.text = "Edit Profile"
            changePswrdBtn.hidden = true
            saveBtn.hidden = false
            nameText.becomeFirstResponder()
        }
        else{
            saveBtn.hidden = true
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                topConstraint = NSLayoutConstraint(item:emailAddVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: firstNameVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 30)
                lastNameVw.hidden = true
                self.view.addConstraints([topConstraint])
            }
            self.nameText.editable = false
            self.emailAddText.editable = false
            self.phnoText.editable = false
            self.phCodeText.editable = false
            self.designationText.editable = false
            self.lastNameTextVw.editable = false
        }
    }
    override func viewDidLayoutSubviews() {
        if((self.storyboard?.valueForKey("name"))! as! String == "Main"){
            if(editable == true){
                myProfileScrollVw.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*0.7)
            }else{
                myProfileScrollVw.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*0.6)
            }
        }
    }
    
    func userInfo(){
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("staffId"){
            AppManager.sharedManager.delegate=self
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.85)
            backVw.tag = 10
            self.view.addSubview(backVw)
            AppManager.sharedManager.showActivityIndicatorInView(backVw, withLabel: "Loading..")
            let params = "id=\(userId)"
            serverInt = 1
            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/details")
        }
    }
    
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        dispatch_async(dispatch_get_main_queue()) {
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            if(self.serverInt == 1){
                if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 200){
                    let view = self.view.viewWithTag(10)! as UIView
                    AppManager.sharedManager.hideActivityIndicatorInView(view)
                    view.removeFromSuperview()
                    let data = responseDict.valueForKey("data") as! NSDictionary
                    let firstName = data.valueForKey("FirstName") as! String
                    self.firstNm = firstName
                    let lastName = data.valueForKey("LastName") as! String
                    let code = data.valueForKey("Code") as! String
                    let phNo = data.valueForKey("Mobile") as! String
                    let designation = data.valueForKey("Designation") as! String
                    let email = data.valueForKey("Email") as! String
                    let profilePic = data.valueForKey("ProfilePic") as! String
                    let stff_code = data.valueForKey("StaffCode") as! String
                    self.staff_Code = stff_code
                    self.staffCode.text = "Staff Member Code:\(stff_code)"
                    self.lastNameTextVw.text = "\(lastName)"
                    if(self.editable == true){
                        self.nameText.text = "\(firstName)"
                    }else{
                        self.nameText.text = "\(firstName) \(lastName)"
                    }
                    self.phCodeText.text = "\(code)"
                    self.phnoText.text = "\(phNo)"
                    self.designationText.text = "\(designation)"
                    self.emailAddText.text = "\(email)"
                    if let url = NSURL(string: "http://\(profilePic)") {
                        self.imgVwProfile.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
                    }
                }
            }
            else{
                self.saveBtn.hidden = true
                self.editBtn.hidden = false
                self.changePswrdBtn.hidden = false
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    self.topConstraint = NSLayoutConstraint(item:self.emailAddVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: self.firstNameVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 30)
                    self.lastNameVw.hidden = true
                    self.view.addConstraints([self.topConstraint])
                }
                self.nameText.editable = false
                self.emailAddText.editable = false
                self.phnoText.editable = false
                self.phCodeText.editable = false
                self.designationText.editable = false
                self.lastNameTextVw.editable = false
                self.nameLbl.text = "Name"
                self.myProfileTittleLbl.text = "Edit Profile"
                self.nameText.text = "\(self.firstNm) \(self.lastNm)"
                
            }
        }
    }
    func failureRsponseError(failureError: NSError) {
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    
    func viewSet(){
        imgVwProfile.layer.cornerRadius = imgVwProfile.frame.size.height/2
        imgVwProfile.layer.borderWidth = imgVwProfile.frame.size.height*0.07
        imgVwProfile.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        imgVwProfile.clipsToBounds = true
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(StaffProfileViewController.profileImageTapped(_:)))
        imgVwProfile.userInteractionEnabled = true
        imgVwProfile.addGestureRecognizer(tapGestureRecognizer1)
        
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone)
        {
            firstNameVw.backgroundColor = UIColor.whiteColor()
            lastNameVw.backgroundColor = UIColor.whiteColor()
            designationVw.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            codeVw.backgroundColor = UIColor.whiteColor()
            phonenumberVw.backgroundColor = UIColor.whiteColor()
            emailAddVw.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
        }else{
            firstNameVw.backgroundColor = UIColor.whiteColor()
            lastNameVw.backgroundColor = UIColor.whiteColor()
            designationVw.backgroundColor = UIColor.whiteColor()
            codeVw.backgroundColor = UIColor.whiteColor()
            phonenumberVw.backgroundColor = UIColor.whiteColor()
            emailAddVw.backgroundColor = UIColor.whiteColor()
        }
        firstNameVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        firstNameVw.layer.shadowOffset = CGSizeMake(7, 5);
        firstNameVw.layer.shadowRadius = 0.5;
        firstNameVw.layer.shadowOpacity = 0.06;
        firstNameVw.layer.masksToBounds = false;
        firstNameVw.layer.borderWidth = 0.5
        
        lastNameVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        lastNameVw.layer.shadowOffset = CGSizeMake(7, 5);
        lastNameVw.layer.shadowRadius = 0.5;
        lastNameVw.layer.shadowOpacity = 0.06;
        lastNameVw.layer.masksToBounds = false;
        lastNameVw.layer.borderWidth = 0.5
        
        designationVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        designationVw.layer.shadowOffset = CGSizeMake(7, 5);
        designationVw.layer.shadowRadius = 0.5;
        designationVw.layer.shadowOpacity = 0.06;
        designationVw.layer.masksToBounds = false;
        designationVw.layer.borderWidth = 0.5
        
        codeVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        codeVw.layer.shadowOffset = CGSizeMake(7, 5);
        codeVw.layer.shadowRadius = 0.5;
        codeVw.layer.shadowOpacity = 0.06;
        codeVw.layer.masksToBounds = false;
        codeVw.layer.borderWidth = 0.5
        
        phonenumberVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        phonenumberVw.layer.shadowOffset = CGSizeMake(7, 5);
        phonenumberVw.layer.shadowRadius = 0.5;
        phonenumberVw.layer.shadowOpacity = 0.06;
        phonenumberVw.layer.masksToBounds = false;
        phonenumberVw.layer.borderWidth = 0.5
        
        emailAddVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        emailAddVw.layer.shadowOffset = CGSizeMake(7, 5);
        emailAddVw.layer.shadowRadius = 0.5;
        emailAddVw.layer.shadowOpacity = 0.06;
        emailAddVw.layer.masksToBounds = false;
        emailAddVw.layer.borderWidth = 0.5
        
        lastNameTextVw.delegate = self
        nameText.delegate = self
        phnoText.delegate = self
        emailAddText.delegate = self
        phCodeText.delegate = self
        designationText.delegate = self
        
        lastNameTextVw.tintColor = UIColor.blackColor()
        emailAddText.tintColor = UIColor.blackColor()
        nameText.tintColor = UIColor.blackColor()
        phnoText.tintColor = UIColor.blackColor()
        phCodeText.tintColor = UIColor.blackColor()
        designationText.tintColor = UIColor.blackColor()
        
        lastNameTextVw.autocapitalizationType = .Words
        emailAddText.autocapitalizationType = .Words
        nameText.autocapitalizationType = .Words
        designationText.autocapitalizationType = .Words
    }
    func profileImageTapped(img: AnyObject){
        if(editable == true){
            self.view.endEditing(true)
            let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
            actionSheet.showInView(self.view)
        }
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex){
        case 0:
            print("Cancel")
        case 1:
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.allowsEditing = false
                        imagePicker.sourceType = .Camera
                        self.presentViewController(imagePicker, animated: true, completion:nil)
                    }
                })
            }else{
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .Camera
                    presentViewController(imagePicker, animated: true, completion:nil)
                }
            }
        case 2:
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.allowsEditing = false
                        imagePicker.sourceType = .PhotoLibrary
                        self.presentViewController(imagePicker, animated: true, completion:nil)
                    }
                })
            }else{
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .PhotoLibrary
                    presentViewController(imagePicker, animated: true, completion:nil)
                }
            }
        default:
            print("Default")
        }
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        let selectedImage : UIImage = image
        imgVwProfile.image=self.ResizeImage(selectedImage, targetSize: CGSizeMake(200.0, 200.0))
        imageSet = true
        imgVwProfile.clipsToBounds = true
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func codeCopy(img: AnyObject){
        UIPasteboard.generalPasteboard().string = "\(staff_Code)"
        self.view.makeToast("Staff Member Code Copied")
    }
    @IBAction func saveBtnAc(sender: AnyObject) {
        let firstName = nameText.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let lastName = lastNameTextVw.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString1 = phnoText.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let trimNameString2 = designationText.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString3 = phCodeText.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if(!(arCode.containsObject(trimNameString3))){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            
            let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21))
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.39))
            contentLbl.font = UIFont(name: "Helvetica", size:14)
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter a Valid Area Code."
            contentLbl.numberOfLines = 3
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(StaffProfileViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(trimNameString3.characters.count != 3 || trimNameString1.characters.count != 7){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            
            let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21))
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
            contentLbl.font = UIFont(name: "Helvetica", size:14)
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please enter 10 digit phone number."
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(StaffProfileViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("staffId") {
            let image : UIImage = imgVwProfile.image!
            let  mImageData    = UIImagePNGRepresentation(image)! as NSData
            let base64String = mImageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.EncodingEndLineWithLineFeed)
            AppManager.sharedManager.delegate = self
            if(firstName == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the FirstName")
                return
            }else if(lastName == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the LastName")
                return
            }else if(trimNameString3 == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the area code")
                return
            }else if(trimNameString1 == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the phone")
                return
            }else if(trimNameString2 == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the designation")
                return
            }
            if(firstName == "" || lastName == "" || trimNameString1 == "" || trimNameString2 == "" || trimNameString3 == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill All The Fields")
                return
            }
            let params = "FirstName=\(firstName)&LastName=\(lastName)&Mobile=\(trimNameString1)&Code=\(trimNameString3)&Designation=\(trimNameString2)&id=\(userId)&ProfilePic=\(base64String)"
            serverInt = 2
            firstNm = firstName
            lastNm = lastName
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/useredit")
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                myProfileScrollVw.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
            }
        }
    }
    func okBtnAc(){
        let view = self.view.viewWithTag(2369)! as UIView
        view.removeFromSuperview()
    }
    @IBAction func editBtnAc(sender: AnyObject) {
        editable = true
        editBtn.hidden = true
        lastNameVw.hidden = false
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            let topConstraint1 = NSLayoutConstraint(
                item:lastNameVw,
                attribute: NSLayoutAttribute.TopMargin,
                relatedBy: NSLayoutRelation.Equal,
                toItem: firstNameVw,
                attribute: NSLayoutAttribute.BottomMargin,
                multiplier: 1,
                constant: 30)
            self.view.addConstraints([topConstraint1])
            self.view.removeConstraint(topConstraint)
        }
        self.nameText.editable = true
        self.phnoText.editable = true
        self.phCodeText.editable = true
        self.lastNameTextVw.editable = true
        
        nameLbl.text = "First Name"
        nameText.text = firstNm as String
        myProfileTittleLbl.text = "Edit Profile"
        changePswrdBtn.hidden = true
        saveBtn.hidden = false
        nameText.becomeFirstResponder()
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.slideMenuController()!.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension StaffProfileViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
