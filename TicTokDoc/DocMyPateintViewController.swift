
import UIKit
import SlideMenuControllerSwift
import RealmSwift
class DocMyPateintViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,WebServiceDelegate, PateintProfileViewControllerDelegate,UITextFieldDelegate{
    @IBOutlet weak var tableVwPateint: UITableView!
    @IBOutlet weak var pateintSegment: UISegmentedControl!
    @IBOutlet weak var segmentDataImgVw: UIImageView!
    var staff = Bool()
    var doReq = NSInteger()
    var groupIdAr = NSString()
    var namePat = NSString()
    var tokenAr = NSString()
    var apiKeyAr = NSString()
    var sessionAr = NSString()
    var nameAr1 = NSMutableArray()
    var AgeAr1 = NSMutableArray()
    var genderAr1 = NSMutableArray()
    var phAr1 = NSMutableArray()
    var locAr1 = NSMutableArray()
    var imgAr1 = ["malePateint","malePateint","femalePateint"]
    var imgAr2 = ["malePateint","malePateint","femalePateint"]
    var nameAr2 = NSMutableArray()
    var ageAr2 = NSMutableArray()
    var gnderAr2 = NSMutableArray()
    var phArCode = NSMutableArray()
    var pharaCode2 = NSMutableArray()
    var phAr2 = NSMutableArray()
    let emailAr = NSMutableArray()
    var locAr2 = NSMutableArray()
    var subscriberAr = NSMutableArray()
    var pendingBool = Bool()
    var ageGenderAr2 = NSMutableArray()
    var selectSegment = NSInteger()
    var pendingdataCount = NSInteger()
    var activeDataCount = NSInteger()
    var switchserver = Bool()
    var acceptRejctId = NSMutableArray()
    var profilepicAr = NSMutableArray()
    var profilepicAr2 = NSMutableArray()
    var urlAr = NSMutableArray()
    var removeIndex = NSIndexPath()
    var totalData = NSArray()
    var ptIdAr = NSMutableArray()
    var pendingPatDeviceTkn = NSMutableArray()
    var realm = try! Realm()
    func refreshMyPatientsList( patientId : String ){
        acceptRejctId.removeObject(patientId)
        self.tableVwPateint.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableVwPateint.delegate = self
        tableVwPateint.dataSource = self
        tableVwPateint.showsVerticalScrollIndicator = false
        self.tableVwPateint.separatorStyle = UITableViewCellSeparatorStyle.None
        segmentDataImgVw.hidden = true
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            let font = UIFont.systemFontOfSize(20)
            pateintSegment.setTitleTextAttributes([NSFontAttributeName: font],
                                                  forState: UIControlState.Normal)
        }
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Selected)
        self.navigationController?.navigationBarHidden = true
        
        switchserver = false
        selectSegment = 0
        pateintSegment.selectedSegmentIndex = 0
    }
    override func viewDidAppear(animated: Bool) {
        if let headVw = tableVwPateint.viewWithTag(100){
            let searchFld = headVw.viewWithTag(5000) as! UITextField
            searchFld.text = ""
            searchFld.resignFirstResponder()
        }
        pateintSegment.selectedSegmentIndex = selectSegment
        var statusInt = NSInteger()
        if(selectSegment == 0){
            statusInt = 1
            nameAr1.removeAllObjects()
            AgeAr1.removeAllObjects()
            genderAr1.removeAllObjects()
            locAr1.removeAllObjects()
            phAr1.removeAllObjects()
            profilepicAr.removeAllObjects()
        }else if(selectSegment == 1){
            tableVwPateint.tableHeaderView = nil
            statusInt = 0
        }
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let params = "Dr_id=\(userId)&Status=\(statusInt)"
            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRPatientList")
        }
    }
    @IBAction func confirmBtnAc(sender: AnyObject) {
        let btnPos = sender.convertPoint(CGPointZero, toView: self.tableVwPateint)
        switchserver = true
        let indexPath:NSIndexPath = self.tableVwPateint.indexPathForRowAtPoint(btnPos)!
        removeIndex = indexPath
        AppManager.sharedManager.delegate=self
        let defaults = NSUserDefaults.standardUserDefaults()
        let userType = defaults.valueForKey("usertype") as! NSObject
        let firstName = defaults.valueForKey("firstName") as! NSString
        let lastName = defaults.valueForKey("lastName") as! NSString
        let drId = defaults.valueForKey("id") as! String
        var createdId = NSString()
        if(userType == 1){
            let userId = defaults.valueForKey("id") as! String
            createdId = userId
        }else{
            let userId = defaults.valueForKey("staffId") as! String
            createdId = userId
        }
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
        let params = "AcceptReject_Id=\(acceptRejctId[indexPath.row])&Status=1&DeviceToken=\(pendingPatDeviceTkn[indexPath.row])&Email=\(emailAr[indexPath.row])&CreateId=\(createdId)&CreateName=\(firstName) \(lastName)&CreateUserType=\(userType)&Dr_id=\(drId)&PatientName=\(self.nameAr2[indexPath.row])"
        acceptRejctId.removeObjectAtIndex(indexPath.row)
        self.pendingPatDeviceTkn.removeObjectAtIndex(indexPath.row)
        self.nameAr2.removeObjectAtIndex(indexPath.row)
        self.emailAr.removeObjectAtIndex(indexPath.row)
        AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRAcceptReject")
    }
    @IBAction func deleteBtnAc(sender: AnyObject) {
        let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To Decline The Request??", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default) {
            UIAlertAction in
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            let btnPos = sender.convertPoint(CGPointZero, toView: self.tableVwPateint)
            self.switchserver = true
            let indexPath:NSIndexPath = self.tableVwPateint.indexPathForRowAtPoint(btnPos)!
            self.removeIndex = indexPath
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let defaults = NSUserDefaults.standardUserDefaults()
            let userType = defaults.valueForKey("usertype") as! NSObject
            let firstName = defaults.valueForKey("firstName") as! NSString
            let lastName = defaults.valueForKey("lastName") as! NSString
            let drId = defaults.valueForKey("id") as! String
            var createdId = NSString()
            if(userType == 1){
                let userId = defaults.valueForKey("id") as! String
                createdId = userId
            }else{
                let userId = defaults.valueForKey("staffId") as! String
                createdId = userId
            }
            let params = "AcceptReject_Id=\(self.acceptRejctId[indexPath.row])&Status=2&DeviceToken=\(self.pendingPatDeviceTkn[indexPath.row])&Email=\(self.emailAr[indexPath.row])&CreateId=\(createdId)&CreateName=\(firstName) \(lastName)&CreateUserType=\(userType)&Dr_id=\(drId)&PatientName=\(self.nameAr2[indexPath.row])"
            self.acceptRejctId.removeObjectAtIndex(indexPath.row)
            self.pendingPatDeviceTkn.removeObjectAtIndex(indexPath.row)
            self.nameAr2.removeObjectAtIndex(indexPath.row)
            self.emailAr.removeObjectAtIndex(indexPath.row)
            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRAcceptReject")
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
        
    }
    
    func doRequestPost(url:String,data:[String: NSObject]){
        let requestDic = data
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                print("Succes:")
                
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                dispatch_async(dispatch_get_main_queue())
                {
                    if(self.doReq == 1){
                        
                    }
                    else{
                        
                        self.tokenAr = responseDict.valueForKeyPath("data.TokenData")! as! NSString
                        self.sessionAr = responseDict.valueForKeyPath("data.sessionData")! as! NSString
                        self.apiKeyAr = responseDict.valueForKeyPath("data.apiKey")! as! NSString
                        self.groupIdAr = responseDict.valueForKeyPath("data.groupId")! as! NSString
                        
                        let user = NSMutableDictionary()
                        user.setValue(self.tokenAr, forKey: "TokenData")
                        user.setValue(self.apiKeyAr, forKey: "ApiKey")
                        user.setValue(self.groupIdAr, forKey: "GroupId")
                        user.setValue(self.sessionAr, forKey: "sessionData")
                        user.setValue(self.namePat, forKey: "Name")
                        let groupId = self.groupIdAr
                        self.msgCountList(groupId as String) { (result) in
                            let chatCount = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", groupId ))
                            if(chatCount.count < result.valueForKey("count") as! Int){
                                var userId = NSString()
                                let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
                                if(userType == 3){
                                    userId = NSUserDefaults.standardUserDefaults().valueForKey("staffId") as! String
                                }else{
                                    userId = NSUserDefaults.standardUserDefaults().valueForKey("id") as! String
                                }
                                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                                RealmInteractor.shared.getMsgsFromServer(userId as String, groupId:groupId as String) { (result) in
                                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                                    
                                    if(result.valueForKey("status") as! Int == 200)
                                    {
                                        let chatResults = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", groupId))
                                        try! self.realm.write({
                                            self.realm.delete(chatResults)
                                        })
                                        
                                        let chatArray = result.objectForKey("data") as! NSArray
                                        
                                        for dict in chatArray
                                        {
                                            try! self.realm.write({
                                                
                                                let group = Group()
                                                group.displayName = dict.valueForKey("DisplayName") as! String
                                                group.message = dict.valueForKey("message") as! String
                                                group.messageType = dict.valueForKey("messageType") as! String
                                                group.fromId = dict.valueForKey("From") as! String
                                                group.groupId = dict.valueForKey("GroupId") as! String
                                                self.realm.add(group)
                                                
                                            })
                                            
                                        }
                                        self.navigateToChatView(user)
                                        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                                    }
                                }
                            }
                            else
                            {
                                self.navigateToChatView(user)
                                AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                            }
                        }
                    }
                }
            }
        }
        dataTask.resume()
    }
    @IBAction func pateintSegmentAc(sender: AnyObject){
        if(pateintSegment.selectedSegmentIndex == 0){
            self.segmentDataImgVw.hidden = true
            tableVwPateint.tableFooterView = UIView()
            nameAr1.removeAllObjects()
            AgeAr1.removeAllObjects()
            genderAr1.removeAllObjects()
            locAr1.removeAllObjects()
            phAr1.removeAllObjects()
            profilepicAr.removeAllObjects()
            switchserver = false
            selectSegment = 0
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("id") {
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                let params = "Dr_id=\(userId)&Status=1"
                AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRPatientList")
            }
        }else{
            self.segmentDataImgVw.hidden = true
            tableVwPateint.tableFooterView = UIView()
            acceptRejctId.removeAllObjects()
            nameAr2.removeAllObjects()
            ageGenderAr2.removeAllObjects()
            ageAr2.removeAllObjects()
            phAr2.removeAllObjects()
            locAr2.removeAllObjects()
            profilepicAr2.removeAllObjects()
            gnderAr2.removeAllObjects()
            selectSegment = 1
            switchserver = false
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("id") {
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                let params = "Dr_id=\(userId)&Status=0"
                AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRPatientList")
            }
        }
    }
    
    
    func textFieldDidChange(textField: UITextField) {
        print(textField.text)
        let exactText =  textField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let predicate = NSPredicate(format:"Name CONTAINS[cd] %@","\(exactText)")
        let names = totalData.filteredArrayUsingPredicate(predicate)
        print(names)
        
        if(exactText != ""){
            if(names.count != 0){
                self.nameAr1.removeAllObjects()
                self.genderAr1.removeAllObjects()
                self.AgeAr1.removeAllObjects()
                self.locAr1.removeAllObjects()
                self.subscriberAr.removeAllObjects()
                self.phAr1.removeAllObjects()
                self.profilepicAr.removeAllObjects()
                self.phArCode.removeAllObjects()
                self.ptIdAr.removeAllObjects()
                self.urlAr.removeAllObjects()
                activeDataCount = names.count
                for i in 0...self.activeDataCount-1{
                    let d = names[i]
                    self.nameAr1.addObject(d.valueForKey("Name")!)
                    let gnder:String = d.valueForKey("Gender") as! String
                    let age = d.valueForKey("DOB") as! NSString
                    self.genderAr1.addObject("\(gnder)")
                    self.AgeAr1.addObject("\(age)")
                    self.locAr1.addObject(d.valueForKey("Address")!)
                    self.subscriberAr.addObject(d.valueForKey("Subscription")!)
                    self.phAr1.addObject(d.valueForKey("Mobile")!)
                    self.profilepicAr.addObject(d.valueForKey("ProfilePic")!)
                    self.phArCode.addObject(d.valueForKey("Code")!)
                    self.ptIdAr.addObject(d.valueForKey("Patient_id")!)
                    self.urlAr.addObject(d.valueForKey("Url")!)
                }
                self.tableVwPateint.reloadData()
            }
            else{
                self.nameAr1.removeAllObjects()
                self.genderAr1.removeAllObjects()
                self.AgeAr1.removeAllObjects()
                self.locAr1.removeAllObjects()
                self.subscriberAr.removeAllObjects()
                self.phAr1.removeAllObjects()
                self.profilepicAr.removeAllObjects()
                self.phArCode.removeAllObjects()
                self.ptIdAr.removeAllObjects()
                self.urlAr.removeAllObjects()
                activeDataCount = names.count
                AppManager.sharedManager.Showalert("Alert", alertmessage: "No Search Found.")
                
                self.tableVwPateint.reloadData()
                
            }
        }
        else{
            self.nameAr1.removeAllObjects()
            self.genderAr1.removeAllObjects()
            self.AgeAr1.removeAllObjects()
            self.locAr1.removeAllObjects()
            self.subscriberAr.removeAllObjects()
            self.phAr1.removeAllObjects()
            self.profilepicAr.removeAllObjects()
            self.phArCode.removeAllObjects()
            self.ptIdAr.removeAllObjects()
            self.urlAr.removeAllObjects()
            activeDataCount = totalData.count
            for i in 0...self.activeDataCount-1{
                let d = totalData[i]
                self.nameAr1.addObject(d.valueForKey("Name")!)
                let gnder:String = d.valueForKey("Gender") as! String
                let age = d.valueForKey("DOB") as! NSString
                self.genderAr1.addObject("\(gnder)")
                self.AgeAr1.addObject("\(age)")
                self.locAr1.addObject(d.valueForKey("Address")!)
                self.subscriberAr.addObject(d.valueForKey("Subscription")!)
                self.phAr1.addObject(d.valueForKey("Mobile")!)
                self.profilepicAr.addObject(d.valueForKey("ProfilePic")!)
                self.phArCode.addObject(d.valueForKey("Code")!)
                self.ptIdAr.addObject(d.valueForKey("Patient_id")!)
                self.urlAr.addObject(d.valueForKey("Url")!)
            }
            self.tableVwPateint.reloadData()
        }
        
        
    }
    
    
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        dispatch_async(dispatch_get_main_queue()){
            if(self.switchserver == false){
                if(self.selectSegment == 0){
                    if(((responseDict.objectForKey("data")) != nil)){
                        self.headerVw()
                        self.segmentDataImgVw.hidden = true
                        self.totalData = []
                        self.totalData = responseDict.objectForKey("data")as! NSArray
                        self.activeDataCount = (responseDict.objectForKey("data")?.count)!
                        let dataDict = responseDict.objectForKey("data") as! NSArray
                        for i in 0...self.activeDataCount-1 {
                            let d = dataDict[i]
                            let groupId = d.valueForKey("GroupId") as! NSString
                            let patId = d.valueForKey("Patient_id") as! NSString
                            if(groupId == ""){
                                let defaults = NSUserDefaults.standardUserDefaults()
                                if let userId = defaults.valueForKey("id") {
                                    self.doReq = 1
                                    self.doRequestPost("\(Header.BASE_URL)messageChat/createsession", data: ["DrId":"\(userId)","PatientId":"\(patId)","Group":"0"])
                                }
                            }
                            self.nameAr1.addObject(d.valueForKey("Name")!)
                            let gnder:String = d.valueForKey("Gender") as! String
                            let age = d.valueForKey("DOB") as! NSString
                            self.genderAr1.addObject("\(gnder)")
                            self.AgeAr1.addObject("\(age)")
                            self.locAr1.addObject(d.valueForKey("Address")!)
                            self.subscriberAr.addObject(d.valueForKey("Subscription")!)
                            self.phAr1.addObject(d.valueForKey("Mobile")!)
                            self.profilepicAr.addObject(d.valueForKey("ProfilePic")!)
                            self.phArCode.addObject(d.valueForKey("Code")!)
                            self.ptIdAr.addObject(d.valueForKey("Patient_id")!)
                            self.urlAr.addObject(d.valueForKey("Url")!)
                            
                        }
                    }
                    else{
                        self.tableVwPateint.tableHeaderView = nil
                        self.segmentDataImgVw.hidden = false
                        self.segmentDataImgVw.image = UIImage(named:"activepatient")
                        
                    }
                    dispatch_async(dispatch_get_main_queue()){
                        self.tableVwPateint.reloadData()
                    }
                }else if(self.selectSegment == 1){
                    self.tableVwPateint.tableHeaderView = nil
                    if(((responseDict.objectForKey("data")) != nil)){
                        self.pendingdataCount = (responseDict.objectForKey("data")?.count)!
                        let dataDict = responseDict.objectForKey("data") as! NSArray
                        for i in 0...self.pendingdataCount-1 {
                            let d = dataDict[i]
                            self.nameAr2.addObject(d.valueForKey("Name")!)
                            let gnder:String = d.valueForKey("Gender") as! String
                            let age = d.valueForKey("DOB") as! NSString
                            self.ageAr2.addObject(age)
                            self.gnderAr2.addObject(gnder)
                            self.emailAr.addObject(d.valueForKey("Email")!)
                            self.locAr2.addObject(d.valueForKey("Address")!)
                            self.phAr2.addObject(d.valueForKey("Mobile")!)
                            self.pharaCode2.addObject(d.valueForKey("Code")!)
                            self.profilepicAr2.addObject(d.valueForKey("ProfilePic")!)
                            self.acceptRejctId.addObject(d.valueForKey("AcceptReject_Id")!)
                            self.ageGenderAr2.addObject("\(gnder)-\(age)")
                            self.pendingPatDeviceTkn.addObject(d.valueForKey("DeviceToken")!)
                            
                        }
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue()){
                            self.pendingdataCount = 0
                            self.segmentDataImgVw.hidden = false
                            self.segmentDataImgVw.image = UIImage(named:"pndingpatient")
                        }
                    }
                    dispatch_async(dispatch_get_main_queue()){
                        self.tableVwPateint.reloadData()
                    }
                }
            }
            else if(self.switchserver == true){
                if(self.selectSegment == 0){
                    
                }else if(self.selectSegment == 1){
                    dispatch_async(dispatch_get_main_queue()){
                        self.pendingdataCount = self.pendingdataCount-1
                        let status = responseDict.valueForKey("data") as! NSString
                        if(status == "Accepted"){
                            if(self.view.frame.size.height < 600){
                                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                                backVw.tag = 2356
                                self.view.addSubview(backVw)
                                
                                let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.34)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.34))
                                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                                alerVw.backgroundColor = UIColor.whiteColor()
                                backVw.addSubview(alerVw)
                                
                                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                                rightImg.image = UIImage(named: "confirm")
                                alerVw.addSubview(rightImg)
                                
                                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                                verificationLbl.text = " Patient Accepted"
                                verificationLbl.font = UIFont.init(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                                alerVw.addSubview(verificationLbl)
                                
                                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                                lineVw.backgroundColor = UIColor.grayColor()
                                alerVw.addSubview(lineVw)
                                
                                let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+10, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.7))
                                contentLbl.font = UIFont.init(name: "Helvetica", size:14)
                                contentLbl.textAlignment = .Center
                                contentLbl.textColor = UIColor.grayColor()
                                contentLbl.text = "You have successfully accepted your patient’s request. They will be notified that their account has been verified and they will now be able to self schedule video visits with you!"
                                contentLbl.numberOfLines = 6
                                alerVw.addSubview(contentLbl)
                                
                                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.14),alerVw.frame.size.width, alerVw.frame.size.height*0.14))
                                okLbl.setTitle("OK", forState: .Normal)
                                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                                okLbl.titleLabel?.textAlignment = .Center
                                okLbl.addTarget(self, action: #selector(DocMyPateintViewController.okBtnAc), forControlEvents: .TouchUpInside)
                                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                                alerVw.addSubview(okLbl)
                            }
                            else if(self.view.frame.size.height > 600 && self.view.frame.size.height < 700){
                                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                                backVw.tag = 2356
                                self.view.addSubview(backVw)
                                
                                let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.34)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.28))
                                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                                alerVw.backgroundColor = UIColor.whiteColor()
                                backVw.addSubview(alerVw)
                                
                                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                                rightImg.image = UIImage(named: "confirm")
                                alerVw.addSubview(rightImg)
                                
                                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                                verificationLbl.text = " Patient Accepted"
                                verificationLbl.font = UIFont.init(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                                alerVw.addSubview(verificationLbl)
                                
                                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                                lineVw.backgroundColor = UIColor.grayColor()
                                alerVw.addSubview(lineVw)
                                
                                let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+8, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.7))
                                contentLbl.font = UIFont.init(name: "Helvetica", size:14)
                                contentLbl.textAlignment = .Center
                                contentLbl.textColor = UIColor.grayColor()
                                contentLbl.text = "You have successfully accepted your patient’s request. They will be notified that their account has been verified and they will now be able to self schedule video visits with you!"
                                contentLbl.numberOfLines = 6
                                alerVw.addSubview(contentLbl)
                                
                                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.18),alerVw.frame.size.width, alerVw.frame.size.height*0.18))
                                okLbl.setTitle("OK", forState: .Normal)
                                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                                okLbl.titleLabel?.textAlignment = .Center
                                okLbl.addTarget(self, action: #selector(DocMyPateintViewController.okBtnAc), forControlEvents: .TouchUpInside)
                                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                                alerVw.addSubview(okLbl)
                            }else{
                                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                                backVw.tag = 2356
                                self.view.addSubview(backVw)
                                let alerVw = UIView()
                                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.34)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.34)
                                }else{
                                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.2)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.2)
                                }
                                
                                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                                alerVw.backgroundColor = UIColor.whiteColor()
                                backVw.addSubview(alerVw)
                                
                                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                                rightImg.image = UIImage(named: "confirm")
                                alerVw.addSubview(rightImg)
                                
                                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                                verificationLbl.text = " Patient Accepted"
                                verificationLbl.font = UIFont.init(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                                alerVw.addSubview(verificationLbl)
                                
                                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                                lineVw.backgroundColor = UIColor.grayColor()
                                alerVw.addSubview(lineVw)
                                
                                let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+10, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.7))
                                contentLbl.textAlignment = .Center
                                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                                    contentLbl.font          = UIFont.init(name: "Helvetica", size:14)
                                }else{
                                    contentLbl.font          = UIFont.init(name: "Helvetica", size:19)
                                }
                                contentLbl.textColor = UIColor.grayColor()
                                contentLbl.text = "You have successfully accepted your patient’s request. They will be notified that their account has been verified and they will now be able to self schedule video visits with you!"
                                contentLbl.numberOfLines = 6
                                alerVw.addSubview(contentLbl)
                                
                                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.14),alerVw.frame.size.width, alerVw.frame.size.height*0.14))
                                okLbl.setTitle("OK", forState: .Normal)
                                if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                                }else{
                                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                                }
                                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                                okLbl.titleLabel?.textAlignment = .Center
                                okLbl.addTarget(self, action: #selector(DocMyPateintViewController.okBtnAc), forControlEvents: .TouchUpInside)
                                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                                alerVw.addSubview(okLbl)
                            }
                        }else if(status == "Rejected"){
                            AppManager.sharedManager.Showalert("Alert", alertmessage: "Patient Declined Successfully.")
                        }
                        self.tableVwPateint.deleteRowsAtIndexPaths([self.removeIndex], withRowAnimation: .Right)
                        if(self.pendingdataCount == 0){
                            self.segmentDataImgVw.hidden = false
                            self.segmentDataImgVw.image = UIImage(named:"pndingpatient")
                        }
                    }
                }
            }
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        }
    }
    func headerVw(){
        let headerView: UIView = UIView(frame: CGRectMake(0, 0, tableVwPateint.frame.size.width, self.view.frame.size.height*0.14))
        headerView.tag = 100
        let imageView: UIImageView = UIImageView(frame: CGRectMake(tableVwPateint.frame.size.width*0.085, tableVwPateint.frame.size.height*0.06, tableVwPateint.frame.size.width-(2*(tableVwPateint.frame.size.width*0.085)), self.view.frame.size.height*0.06))
        imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
        imageView.userInteractionEnabled = true
        imageView.image = UIImage(named: "box2")
        headerView.addSubview(imageView)
        
        let stethoscopImageView: UIImageView = UIImageView(frame: CGRectMake(10,self.view.frame.size.height*0.01, self.view.frame.size.height*0.04, self.view.frame.size.height*0.04))
        stethoscopImageView.image = UIImage(named: "searchPat")
        imageView.addSubview(stethoscopImageView)
        let searchVw : UIView = UIView(frame: CGRectMake(18+self.view.frame.size.height*0.04,self.view.frame.size.height*0.002,1.3, self.view.frame.size.height*0.056))
        searchVw.backgroundColor = UIColor.lightGrayColor()
        imageView.addSubview(searchVw)
        
        let searchField : UITextField = UITextField(frame: CGRectMake(20+self.view.frame.size.height*0.04,self.view.frame.size.height*0.005,imageView.frame.size.width-(25+self.view.frame.size.height*0.04), self.view.frame.size.height*0.05))
        searchField.tag = 5000
        searchField.delegate = self
        searchField.autocorrectionType = .No
        searchField.tintColor     = UIColor.blackColor()
        searchField.userInteractionEnabled = true
        searchField.addTarget(self, action: #selector(DocMyPateintViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        searchField.font = UIFont.systemFontOfSize(self.view.frame.size.width*0.04)
        searchField.placeholder = " Enter Patient Name"
        imageView.addSubview(searchField)
        
        
        tableVwPateint.tableHeaderView = headerView
    }
    func okBtnAc(){
        let view = self.view.viewWithTag(2356)! as UIView
        view.removeFromSuperview()
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("Alert", alertmessage: failureError.localizedDescription)
    }
    
    @IBAction func backBtnAc(sender: AnyObject) {
        self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func chatBtnAction(sender: AnyObject) {
        
        let btnPos = sender.convertPoint(CGPointZero, toView: self.tableVwPateint)
        let indexPath:NSIndexPath = self.tableVwPateint.indexPathForRowAtPoint(btnPos)!
        namePat = nameAr1[indexPath.row] as! NSString
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        let defaults = NSUserDefaults.standardUserDefaults()
        let usertype = defaults.valueForKey("usertype") as! Int
        if(usertype == 1){
            if let userId = defaults.valueForKey("id") {
                doReq = 2
                self.doRequestPost("\(Header.BASE_URL)docter/DRdrcreatetoken", data: ["From":"\(userId)","To":"\(ptIdAr[indexPath.row])"])
            }
        }else{
            if let userId = defaults.valueForKey("staffId") {
                doReq = 2
                self.doRequestPost("\(Header.BASE_URL)docter/DRdrcreatetoken", data: ["From":"\(userId)","To":"\(ptIdAr[indexPath.row])"])
            }
        }
    }
    
    func navigateToChatView(user:NSDictionary) -> Void {
        
        let chatResults = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", user.valueForKey("GroupId") as! String))
        print(chatResults.count)
        
        let msgArray = NSMutableArray()
        
        for group in chatResults
        {
            var mySelf = false
            let msgDict = group
            
            let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
            if(userType == 1 || userType == 0 || userType == 2){
                if(msgDict.fromId == (NSUserDefaults.standardUserDefaults().valueForKey("id") as! String))
                {
                    mySelf = true
                }
            }else if(userType == 3){
                if(msgDict.fromId == (NSUserDefaults.standardUserDefaults().valueForKey("staffId") as! String))
                {
                    mySelf = true
                }
            }
            
            let mdict = ["msg":msgDict.message,"bool":mySelf, "msgType":msgDict.messageType,"name":msgDict.displayName]
            msgArray.addObject(mdict)
        }
        
        let firstname:String = NSUserDefaults.standardUserDefaults().valueForKey("firstName") as! String
        
        let vc = storyboard!.instantiateViewControllerWithIdentifier("ChatViewController") as! ChatViewController
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let usertype = defaults.valueForKey("usertype") as! NSInteger
        if(usertype == 3){
            vc.userId = defaults.valueForKey("staffId") as! String
        }else{
            vc.userId = defaults.valueForKey("id") as! String
        }
        
        vc.chatUserId = user.valueForKey("GroupId") as! String
        
        vc.userName = user.valueForKey("Name") as! String
        vc.Token = user.valueForKey("TokenData") as! String
        vc.SessionID = user.valueForKey("sessionData") as! String
        vc.apiKey = user.valueForKey("ApiKey") as! String
        vc.msgAr = msgArray
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func msgCountList(groupId:String,completion: (result: NSDictionary)->()) -> Void {
        let paramString = "GroupId=\(groupId)"
        AppManager.sharedManager.generateGroupIdForUser("messageChat/listChat", params: paramString) { (result) in
            print("\(result)")
            
            completion(result: result)
        }
    }
    
    //tableview delegate & data source method:
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(pateintSegment.selectedSegmentIndex == 0){
            return activeDataCount
        }else if(pateintSegment.selectedSegmentIndex == 1){
            return pendingdataCount
        }
        return 0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(pateintSegment.selectedSegmentIndex == 0){
            let cell = tableView.dequeueReusableCellWithIdentifier("cell")
            cell!.layer.cornerRadius = 7.0
            cell!.layer.masksToBounds = true
            let defaults = NSUserDefaults.standardUserDefaults()
            
            let imageView: UIView = (cell?.viewWithTag(2001))!
            imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
            imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
            imageView.layer.shadowOffset = CGSizeMake(10, 5);
            imageView.layer.shadowRadius = 1;
            imageView.layer.shadowOpacity = 0.06;
            imageView.backgroundColor = UIColor.whiteColor()
            imageView.layer.masksToBounds = false;
            imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
            imageView.layer.borderWidth = 0.5
            let ptImg: UIImageView = (cell!.viewWithTag(2002) as! UIImageView)
            ptImg.layer.cornerRadius = ptImg.frame.size.height/2
            ptImg.layer.borderWidth = self.view.frame.size.height*0.008
            ptImg.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
            if let url = NSURL(string: "http://\(profilepicAr[indexPath.row])") {
                ptImg.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
            }
            let nameLbl: UILabel = (cell!.viewWithTag(2003) as! UILabel)
            nameLbl.text = nameAr1[indexPath.row] as? String
            let yearLbl: UILabel = (cell!.viewWithTag(2004) as! UILabel)
            yearLbl.text = AgeAr1[indexPath.row] as? String
            let gnderLbl: UILabel = (cell!.viewWithTag(2005) as! UILabel)
            gnderLbl.text = genderAr1[indexPath.row] as? String
            let locationLbl: UILabel = (cell!.viewWithTag(2006) as! UILabel)
            locationLbl.text = locAr1[indexPath.row] as? String
            return cell!
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("pendingCell")
            let imageView: UIView = (cell?.viewWithTag(2012))!
            imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
            imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
            imageView.layer.shadowOffset = CGSizeMake(10, 5);
            imageView.layer.shadowRadius = 1;
            imageView.layer.shadowOpacity = 0.06;
            imageView.layer.masksToBounds = false;
            imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
            imageView.layer.borderWidth = 0.5
            if(indexPath.row%2 == 0){
                imageView.backgroundColor = UIColor.whiteColor()
            }else{
                imageView.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            }
            let ptImg: UIImageView = (cell!.viewWithTag(2007) as! UIImageView)
            ptImg.layer.cornerRadius = ptImg.frame.size.height/2
            ptImg.layer.borderWidth = self.view.frame.size.height*0.008
            ptImg.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
            if let url = NSURL(string: "http://\(profilepicAr2[indexPath.row])") {
                ptImg.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
            }
            let nameLbl: UILabel = (cell!.viewWithTag(2008) as! UILabel)
            nameLbl.text = nameAr2[indexPath.row] as? String
            let ageGenderLbl: UILabel = (cell!.viewWithTag(2009) as! UILabel)
            ageGenderLbl.text = ageGenderAr2[indexPath.row] as? String
            return cell!
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(pateintSegment.selectedSegmentIndex == 0){
            return UITableViewAutomaticDimension
        }
        else{
            return UITableViewAutomaticDimension
        }
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return self.view.frame.size.width*0.1
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(pateintSegment.selectedSegmentIndex == 0){
            pendingBool = true
            let vc = storyboard!.instantiateViewControllerWithIdentifier("PateintProfileViewController") as! PateintProfileViewController
            vc.staffSelect = staff
            vc.pending = pendingBool
            vc.btnInt = 1
            vc.address = locAr1[indexPath.row] as! NSString
            vc.age = AgeAr1[indexPath.row] as! NSString
            vc.gnder = genderAr1[indexPath.row] as! NSString
            vc.name = nameAr1[indexPath.row] as! NSString
            vc.ptId = ptIdAr[indexPath.row] as! NSString
            vc.profilepicUrl = profilepicAr[indexPath.row] as! NSString
            vc.urlPrescribe = urlAr[indexPath.row] as! NSString
            let code = phArCode[indexPath.row] as! NSString
            let ph = phAr1[indexPath.row] as! NSString
            var compPh = "+1(\(code))\(ph)"
            compPh = compPh.insert("-", ind: 10)
            vc.phone = compPh
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            pendingBool = false
            let vc = storyboard!.instantiateViewControllerWithIdentifier("PateintProfileViewController") as! PateintProfileViewController
            vc.acceptRejId = acceptRejctId[indexPath.row] as! NSString
            vc.address = locAr2[indexPath.row] as! NSString
            vc.age = ageAr2[indexPath.row] as! NSString
            vc.gnder = gnderAr2[indexPath.row] as! NSString
            vc.name = nameAr2[indexPath.row] as! NSString
            vc.email = emailAr[indexPath.row] as! NSString
            let code = pharaCode2[indexPath.row] as! NSString
            let ph = phAr2[indexPath.row] as! NSString
            var compPh = "+1(\(code))\(ph)"
            compPh = compPh.insert("-", ind: 10)
            vc.phone = compPh
            vc.pending = pendingBool
            vc.indexPath = indexPath.row
            vc.acceptRejectIdAr = acceptRejctId
            vc.delegate = self
            vc.profilepicUrl = profilepicAr2[indexPath.row] as! NSString
            vc.deviceTkn = pendingPatDeviceTkn[indexPath.row] as! NSString
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension String {
    func insert(string:String,ind:Int) -> String {
        return  String(self.characters.prefix(ind)) + string + String(self.characters.suffix(self.characters.count-ind))
    }
}
extension DocMyPateintViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
