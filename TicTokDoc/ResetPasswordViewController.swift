
import UIKit

class ResetPasswordViewController: UIViewController,WebServiceDelegate,UITextFieldDelegate {

    @IBOutlet weak var btnCofirmPswrd: UIButton!
    @IBOutlet weak var btnNewPswrd: UIButton!
    @IBOutlet weak var btnOldPswrd: UIButton!
    @IBOutlet weak var textFieldConfirmPswrd: UITextField!
    @IBOutlet weak var textFieldNewPswrd: UITextField!
    @IBOutlet weak var changePswrdBtn: UIButton!
    @IBOutlet weak var textFieldOldPswrd: UITextField!
    var textFieldSecureEntry = Bool()
    var logBool = Bool()
    var logoutC : UIViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldSecureEntry = true
        textFieldNewPswrd.delegate = self
        textFieldConfirmPswrd.delegate = self
        textFieldOldPswrd.delegate = self
        changePswrdBtn.enabled = false
        
        let logoutVw = storyboard!.instantiateViewControllerWithIdentifier("mainViewController") as! ViewController
        self.logoutC = UINavigationController(rootViewController: logoutVw)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if(textFieldConfirmPswrd.text != "" && textFieldNewPswrd.text != "" && textFieldOldPswrd.text != ""){
           changePswrdBtn.enabled = true
        }
        else if(textFieldConfirmPswrd.text == "" ||  textFieldNewPswrd.text == "" || textFieldOldPswrd.text == ""){
            changePswrdBtn.enabled = false
        }
    }
    @IBAction func eyeOpenCloseAc(sender:AnyObject ){
        if(textFieldSecureEntry == true){
            textFieldSecureEntry = false
        if(sender.tag == 11){
            btnOldPswrd.setImage(UIImage(named: "ResetNotEye"), forState: UIControlState.Normal)
            textFieldOldPswrd.secureTextEntry = false
        }else if(sender.tag == 12){
            btnNewPswrd.setImage(UIImage(named: "ResetNotEye"), forState: UIControlState.Normal)
            textFieldNewPswrd.secureTextEntry = false
        }else if(sender.tag == 13){
            btnCofirmPswrd.setImage(UIImage(named: "ResetNotEye"), forState: UIControlState.Normal)
            textFieldConfirmPswrd.secureTextEntry = false
        }
        }else if(textFieldSecureEntry == false){
            textFieldSecureEntry = true
            if(sender.tag == 11){
                 btnOldPswrd.setImage(UIImage(named: "ResetEye"), forState: UIControlState.Normal)
                textFieldOldPswrd.secureTextEntry = true
            }else if(sender.tag == 12){
                 btnNewPswrd.setImage(UIImage(named: "ResetEye"), forState: UIControlState.Normal)
                textFieldNewPswrd.secureTextEntry = true
            }else if(sender.tag == 13){
                 btnCofirmPswrd.setImage(UIImage(named: "ResetEye"), forState: UIControlState.Normal)
                textFieldConfirmPswrd.secureTextEntry = true
            }
        }
    }
    @IBAction func changePswrdAc(sender: AnyObject) {
        if(textFieldConfirmPswrd.text != textFieldNewPswrd.text){
           AppManager.sharedManager.Showalert("Alert", alertmessage: "New Password and Confirm Password does not match. Please re-enter the password.")
            return
        }else if(!(textFieldNewPswrd.text!.characters.count >= 6 && textFieldNewPswrd.text!.characters.count <= 15)){
            AppManager.sharedManager.Showalert("Alert", alertmessage: "please enter password in between 6 to 10 digits")
            return
        }
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userTypeInt = defaults.valueForKey("usertype"){
            var userId = NSString()
            if(userTypeInt as! NSObject == 0 || userTypeInt as! NSObject == 1){
               userId = defaults.valueForKey("id") as! String
            }else{
               userId = defaults.valueForKey("staffId") as! String
            }
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let params = "id=\(userId)&OldPassword=\(textFieldOldPswrd.text!)&NewPassword=\(textFieldNewPswrd.text!)"
            logBool = true
            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/ChangePassword")
        }
       // if let userId = defaults.valueForKey("id") {
        
       // }
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString)
    {
        if(logBool == true){
        if let dic:String = responseDict.valueForKey("error") as? String {
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            if dic == "Old Password Not Match"{
              AppManager.sharedManager.Showalert("Alert", alertmessage: "Please enter correct Old password")
            }
        }
        else{
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            dispatch_async(dispatch_get_main_queue()) {
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
                let alerVw = UIView()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.24)
                }else{
                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.15)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.19)
                }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "confirm")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.55, self.view.frame.size.width*0.05))
            verificationLbl.text = " Password Reset"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+23, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.35))
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    contentLbl.font          = UIFont(name: "Helvetica", size:14)
                }else{
                    contentLbl.font          = UIFont(name: "Helvetica", size:19)
                }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = " You have successfully reset your password! Please proceed to login with your new password."
            contentLbl.numberOfLines = 4
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
                if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                }else{
                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
        }
      }
    }else{
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            dispatch_async(dispatch_get_main_queue()){
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("id")
                defaults.removeObjectForKey("usertype")
                self.slideMenuController()?.changeMainViewController(self.logoutC, close: true)
            }
        }
    }
    func okBtnAc(){
        let view = self.view.viewWithTag(2369)! as UIView
        view.removeFromSuperview()
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Log Out")
            let params = "UserId=\(userId)"
            logBool = false
            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/logout")
        }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("Oops", alertmessage: failureError.localizedDescription)
    }

    @IBAction func backBtnac(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
