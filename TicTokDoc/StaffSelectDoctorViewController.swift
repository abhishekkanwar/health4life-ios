

import UIKit
import SlideMenuControllerSwift
import RealmSwift

class StaffSelectDoctorViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,WebServiceDelegate {
    var mainViewController: UIViewController!
    
    @IBOutlet weak var docListLbl: UILabel!
    @IBOutlet weak var docListTableVw: UITableView!
    var logoutC : UIViewController!
    var nameAr = NSMutableArray()
    var specialityAr = NSMutableArray()
    var imgAr = NSMutableArray()
    var drIdAr = NSMutableArray()
    var indexPath = NSIndexPath()
    var showDotAr = NSMutableArray()
    var serverInt = NSInteger()
    let realm = try! Realm()
    override func viewDidLoad() {
        self.navigationController?.navigationBarHidden = true
        super.viewDidLoad()
       let attachment = NSTextAttachment()
        attachment.image = UIImage(named: "whiteSte")
        let offsetY: CGFloat = -6.0
        attachment.bounds = CGRectMake(-10, offsetY, attachment.image!.size.width, attachment.image!.size.height)
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string:"")
        myString.appendAttributedString(attachmentString)
        let myString1 = NSMutableAttributedString(string:"  My Doctors")
        myString.appendAttributedString(myString1)
        docListLbl.textAlignment = .Center
        docListLbl.attributedText = myString
        
        docListTableVw.delegate = self
        docListTableVw.dataSource = self
        
    }
    func doRequestPost(url:String,data:[String: NSObject]){
        print("URL FOR DEVICETOKEN ==> \(url)")
        let requestDic = data
        print(requestDic)
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        
        print("Request Object:\(data)")
        print("Request string = \(jsonString!)")
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil)
            {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
                print("Succes:")
                let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                print(responceString)
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                dispatch_async(dispatch_get_main_queue())
                {
                    if(self.serverInt == 1){
                    self.nameAr.removeAllObjects()
                    self.specialityAr.removeAllObjects()
                    self.imgAr.removeAllObjects()
                    self.drIdAr.removeAllObjects()
                    self.showDotAr.removeAllObjects()
                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                    if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 200)
                    {
                      let data = responseDict.valueForKey("data") as! NSMutableArray
                        for i in 0...data.count-1{
                            let dict = data.objectAtIndex(i) as! NSDictionary
                            if(dict.valueForKey("StaffStatus") as! Int == 1){
                            self.nameAr.addObject(dict.valueForKey("Name")!)
                            self.specialityAr.addObject(dict.valueForKey("Speciality")!)
                            self.imgAr.addObject(dict.valueForKey("ProfilePic")!)
                            self.drIdAr.addObject(dict.valueForKey("DrId")!)
                            self.showDotAr.addObject(dict.valueForKey("showDot")!)
                         }
                       }
                    }
                    else{
                        let defaults = NSUserDefaults.standardUserDefaults()
                        if let userId = defaults.valueForKey("staffId") {
                            var token =  NSString()
                            if(UIDevice.isSimulator == true){
                                token = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
                            }else{
                                token = defaults.valueForKey("deviceToken") as! NSString
                            }
                            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                            self.serverInt = 2
                            self.doRequestPost("\(Header.BASE_URL)users/logout", data: ["UserId":"\(userId)","DeviceToken":"\(token)"])
//                            AppManager.sharedManager.delegate=self
//                            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Log Out")
//                            let params = "UserId=\(userId)&DeviceToken=\(token)"
//                            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/logout")
                        }
                      return
                    }
                    
                    if(self.nameAr.count == 0){
                        let defaults = NSUserDefaults.standardUserDefaults()
                        if let userId = defaults.valueForKey("staffId") {
                            var token =  NSString()
                            if(UIDevice.isSimulator == true){
                                token = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
                            }else{
                                token = defaults.valueForKey("deviceToken") as! NSString
                            }
                            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                            self.serverInt = 2
                            self.doRequestPost("\(Header.BASE_URL)users/logout", data: ["UserId":"\(userId)","DeviceToken":"\(token)"])
//                            AppManager.sharedManager.delegate=self
//                            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Log Out")
//                            let params = "UserId=\(userId)&DeviceToken=\(token)"
//                            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/logout")
                        }
                        return
                    }else{
                    let defaults = NSUserDefaults.standardUserDefaults()
                    if let idGet = defaults.valueForKey("id"){
                        let idDrGet = defaults.valueForKey("id") as! String
                        if(self.drIdAr.containsObject(idDrGet)){
                            let drIdIndex = self.drIdAr.indexOfObject(idDrGet)
                            let row = Int(drIdIndex)
                            let indexPath = NSIndexPath(forRow: row, inSection: 0)
                            let  vw = self.view.viewWithTag(30+indexPath.row)
                            vw?.backgroundColor = UIColor.init(red: 10/255, green: 4/255, blue: 88/255, alpha: 1)
                            NSUserDefaults.standardUserDefaults().setValue(indexPath.row, forKey: "indexPath")
                        }
                    }
                    self.docListTableVw.reloadData()
                    //let defaults = NSUserDefaults.standardUserDefaults()
                    if let indexPathRow = defaults.valueForKey("indexPath"){
                        print(indexPathRow)
                        let row = Int(indexPathRow as! NSInteger)
                        let indexPath = NSIndexPath(forRow: row, inSection: 0)
                        let  vw = self.view.viewWithTag(30+indexPath.row)
                        vw?.backgroundColor = UIColor.init(red: 10/255, green: 4/255, blue: 88/255, alpha: 1)
                        self.docListTableVw.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.None)
                        NSUserDefaults.standardUserDefaults().setValue(self.nameAr[indexPath.row], forKey: "selectDrName")
                        NSUserDefaults.standardUserDefaults().setValue(self.specialityAr[indexPath.row], forKey: "selectDrSpeciality")
                        NSUserDefaults.standardUserDefaults().setValue(self.imgAr[indexPath.row], forKey: "selectDrImg")
                        NSUserDefaults.standardUserDefaults().setValue(self.drIdAr[indexPath.row], forKey: "id")
                    }
                }
            }
                    else{
                        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                        dispatch_async(dispatch_get_main_queue()){
                            let defaults = NSUserDefaults.standardUserDefaults()
                            defaults.removeObjectForKey("id")
                            defaults.removeObjectForKey("usertype")
                            defaults.removeObjectForKey("firstName")
                            defaults.removeObjectForKey("drType")
                            defaults.removeObjectForKey("staffId")
                            defaults.removeObjectForKey("selectDrName")
                            defaults.removeObjectForKey("selectDrImg")
                            defaults.removeObjectForKey("selectDrSpeciality")
                            defaults.removeObjectForKey("selectDrId")
                            // defaults.removeObjectForKey("deviceToken")
                            try! self.realm.write({
                                self.realm.deleteAll()
                            })
                            UIApplication.sharedApplication().cancelAllLocalNotifications()
                            self.slideMenuController()?.changeMainViewController(self.logoutC, close: true)
                        }
                    }
            }
            }
        }
        dataTask.resume()
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            dispatch_async(dispatch_get_main_queue()){
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("id")
                defaults.removeObjectForKey("usertype")
                defaults.removeObjectForKey("firstName")
                defaults.removeObjectForKey("drType")
                defaults.removeObjectForKey("staffId")
                try! self.realm.write({
                    self.realm.deleteAll()
                })
                UIApplication.sharedApplication().cancelAllLocalNotifications()
                self.slideMenuController()?.changeMainViewController(self.logoutC, close: true)
            }
        }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    override func viewWillAppear(animated: Bool){
      self.docListTableVw.reloadData()
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("staffId")
        {
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
            serverInt = 1
            self.doRequestPost("\(Header.BASE_URL)staffapi/staffDrList", data: ["StaffId":"\(userId)"])
        }
        let logoutVw = storyboard!.instantiateViewControllerWithIdentifier("mainViewController") as! ViewController
        self.logoutC = UINavigationController(rootViewController: logoutVw)
        if let indexPathRow = defaults.valueForKey("indexPath"){
            print(indexPathRow)
            let row = Int(indexPathRow as! NSInteger)
        let indexPath = NSIndexPath(forRow: row, inSection: 0)
        let  vw = self.view.viewWithTag(30+indexPath.row)
        vw?.backgroundColor = UIColor.init(red: 10/255, green: 4/255, blue: 88/255, alpha: 1)
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameAr.count
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        for i in 0...drIdAr.count-1{
            let defaults = NSUserDefaults.standardUserDefaults()
            if let idGet = defaults.valueForKey("id"){
                let idDrGet = defaults.valueForKey("id") as! String
                let idDr = drIdAr[indexPath.row] as! String
                if(idDr == idGet as! String){
                    let row = Int(indexPath.row)
                    let indexPath = NSIndexPath(forRow: row, inSection: 0)
                    let  vw = self.view.viewWithTag(30+indexPath.row)
                    vw?.backgroundColor = UIColor.init(red: 10/255, green: 4/255, blue: 88/255, alpha: 1)
                    NSUserDefaults.standardUserDefaults().setValue(indexPath.row, forKey: "indexPath")
                }
            }
        }
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        cell!.selectionStyle = .None

        let nameLbl: UILabel = (cell!.viewWithTag(13) as! UILabel)
        nameLbl.text = "Dr. \(nameAr[indexPath.row])" as? String
        
        let specialityLbl: UILabel = (cell!.viewWithTag(14) as! UILabel)
        specialityLbl.text = specialityAr[indexPath.row] as? String
        
        let countLbl: UILabel = (cell!.viewWithTag(17) as! UILabel)
        countLbl.layer.cornerRadius = countLbl.frame.size.height/2
        countLbl.clipsToBounds = true
        if(showDotAr[indexPath.row] as! NSObject  == 0){
          countLbl.backgroundColor = UIColor.clearColor()
        }
        else{
         countLbl.backgroundColor = UIColor.redColor()
        }
       
        
        
        let vwSelect = UIView(frame: CGRectMake(tableView.frame.size.width-8,0,8, cell!.frame.size.height))
        vwSelect.backgroundColor = UIColor.clearColor()
        vwSelect.tag = 30+indexPath.row
        cell?.contentView.addSubview(vwSelect)
        return cell!
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let  vw = self.view.viewWithTag(30+indexPath.row)
        vw?.backgroundColor = UIColor.init(red: 10/255, green: 4/255, blue: 88/255, alpha: 1)
        
       NSUserDefaults.standardUserDefaults().setValue(indexPath.row, forKey: "indexPath")
       NSUserDefaults.standardUserDefaults().setValue(nameAr[indexPath.row], forKey: "selectDrName")
       NSUserDefaults.standardUserDefaults().setValue(specialityAr[indexPath.row], forKey: "selectDrSpeciality")
       NSUserDefaults.standardUserDefaults().setValue(imgAr[indexPath.row], forKey: "selectDrImg")
       NSUserDefaults.standardUserDefaults().setValue(self.drIdAr[indexPath.row], forKey: "id")
        var storyboard = UIStoryboard()
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        }else{
            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
        }
        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("StaffHomeViewController") as! StaffHomeViewController
        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("StaffSelectDoctorViewController") as! StaffSelectDoctorViewController
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        leftViewController.mainViewController = nvc
        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController:leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        self.navigationController?.pushViewController(slideMenuController, animated: true)
        //UIView.transitionWithView(self.navigationController!.view, duration: 1, options: .TransitionFlipFromRight, animations: nil, completion: nil)
      //  UIView.transitionWithView((self.navigationController?.view)!, duration: 1, options: .TransitionFlipFromRight, animations: nil, completion: nil)
        slideMenuController.closeLeft()
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.view.frame.size.height*0.12
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
