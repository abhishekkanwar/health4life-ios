

import UIKit
import IQKeyboardManagerSwift
class VideoCallLinkViewController: UIViewController {
    
    @IBOutlet weak var linkVw: UIView!
    @IBOutlet weak var enterUrlTxtFld: UITextField!
    var user = NSString()
    var autoFill = NSString()
    var exit = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.sharedManager().enable = true
        if(autoFill != ""){
            enterUrlTxtFld.text = autoFill as String
        }
        else{
            enterUrlTxtFld.text = ""
        }
        
        enterUrlTxtFld.tintColor = UIColor.blackColor()
        linkVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        linkVw.layer.borderColor   = UIColor.darkGrayColor().CGColor
        linkVw.layer.masksToBounds = false;
        linkVw.layer.borderWidth   = 0.8
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func connectNow(sender: AnyObject){
        
        enterUrlTxtFld.resignFirstResponder()
        
        if(enterUrlTxtFld.text != ""){
            let params = ["":""]
            let urlString: String = enterUrlTxtFld.text!
            print(urlString)
            let exactText =  urlString.stringByReplacingOccurrencesOfString(" ", withString: "")
            let fileUrl = NSURL(string: exactText)
            if(UIApplication.sharedApplication().canOpenURL(fileUrl!)){
                self.sendRequest(exactText, parameters: params) { (data, response, error) in
                    if(data != nil){
                        let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                        let responseDict = self.convertStringToDictionary(responseString as! String)
                        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                        if(responseDict != nil){
                            
                            if( ((responseDict as! AnyObject).valueForKey("status"))  as! Int == 401)
                            {
                                dispatch_async(dispatch_get_main_queue()) {
                                    let alert = UIAlertController(title: "Alert", message: "Link has been expired.", preferredStyle: UIAlertControllerStyle.Alert)
                                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel) {
                                        UIAlertAction in
                                        var storyboard = UIStoryboard()
                                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                                            storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        }else{
                                            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                                        }
                                        
                                        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "loading...")
                                        if(self.user == "Doctor"){
                                          self.navigationController?.popViewControllerAnimated(true)
                                        }else{
                                        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                                        let vc = storyboard.instantiateViewControllerWithIdentifier("mainViewController") as! ViewController
                                        let nav = UINavigationController(rootViewController: vc)
                                        appDelegate.window?.rootViewController = nav
                                        appDelegate.window?.makeKeyAndVisible()
                                     }
                                    }
                                 alert.addAction(okAction)
                                 self.presentViewController(alert, animated: true, completion: nil)
                                  return
                                }
                            }
                            else if(((responseDict as! AnyObject).valueForKey("status"))  as! Int == 201){
                                dispatch_async(dispatch_get_main_queue()) {
                                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Enter Correct url.")
                                    return
                                }
                            }
                            if(((responseDict as! AnyObject).valueForKey("data")) != nil){
                                let datadict = ((responseDict as! AnyObject).valueForKey("data"))  as! NSDictionary
                                var storyboard = UIStoryboard()
                                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                                }else{
                                    storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                                }
                                dispatch_async(dispatch_get_main_queue()) {
                                    let vc = storyboard.instantiateViewControllerWithIdentifier("ConferenceVideoViewController") as! ConferenceVideoViewController
                                    let defaults = NSUserDefaults.standardUserDefaults()
                                    defaults.setValue(2, forKey: "usertype")
                                    print(datadict.valueForKey("apiKey") as! String)
                                    print(datadict.valueForKey("sessionData") as! String)
                                    print(datadict.valueForKey("TokenData") as! String)
                                    vc.kApiKey = datadict.valueForKey("apiKey") as! String
                                    vc.kSessionId = datadict.valueForKey("sessionData") as! String
                                    vc.kToken = datadict.valueForKey("TokenData") as! String
                                    vc.connectingPerson = self.user
                                    vc.groupId = datadict.valueForKey("GroupId") as! String
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        }else{
                             dispatch_async(dispatch_get_main_queue()) {
                            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                            AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Enter Correct url.")
                            }
                        }
                    }else{
                         dispatch_async(dispatch_get_main_queue()) {
                        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                        AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Enter Correct url.")
                        }
                    }
                    
                }
            }else{
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please enter valid Url.")
            }
        }else{
            AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Enter url.")
        }
        
    }
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sendRequest(url: String, parameters: [String: AnyObject], completionHandler: (NSData?, NSURLResponse?, NSError?) -> Void) -> NSURLSessionTask {
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        let parameterString = parameters.stringFromHttpParameters()
        print("\(url)")
        let requestURL = NSURL(string:"\(url)")!
        
        let request = NSMutableURLRequest(URL: requestURL)
        request.setValue("0", forHTTPHeaderField: "devicetype")
        request.HTTPMethod = "GET"
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request, completionHandler:completionHandler)
        task.resume()
        
        return task
    }
}
extension String {
    func stringByAddingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
        
        return self.stringByAddingPercentEncodingWithAllowedCharacters(allowedCharacters)
    }
    
}

extension Dictionary {
    
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String).stringByAddingPercentEncodingForURLQueryValue()!
            let percentEscapedValue = (value as! String).stringByAddingPercentEncodingForURLQueryValue()!
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joinWithSeparator("&")
    }
    
}
