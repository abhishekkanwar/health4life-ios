

import UIKit
import IQKeyboardManagerSwift
import CoreLocation
import GooglePlaces
class PateintCreateAccountViewController: UIViewController,SSRadioButtonControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate ,UITextFieldDelegate,WebServiceDelegate,UISearchBarDelegate,CLLocationManagerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIActionSheetDelegate {
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var firstNameFld: UITextField!
    @IBOutlet weak var lastNameFld: UITextField!
    @IBOutlet weak var emailadressFld: UITextField!
    @IBOutlet weak var pswrdFld: UITextField!
    @IBOutlet weak var confrmPaswrdFld: UITextField!
    @IBOutlet weak var phnNumberFld: UITextField!
    @IBOutlet weak var addressFld: UITextField!
    @IBOutlet weak var arCode: UITextField!
    @IBOutlet weak var zipcodeFld: UITextField!
    @IBOutlet weak var cityFld: UITextField!
    @IBOutlet weak var stateFld: UITextField!
    @IBOutlet weak var scrollVwContentVw: UIView!
    @IBOutlet weak var scrollVwSignUp: UIScrollView!
    @IBOutlet weak var patientProfile: UIImageView!
    @IBOutlet weak var maleBtn: SSRadioButton!
    @IBOutlet weak var femaleBtn: SSRadioButton!
    @IBOutlet weak var patientCreatAcLbl: UILabel!
    @IBOutlet weak var birthTextField: UITextField!
    @IBOutlet weak var codeVw: UIView!
    @IBOutlet weak var firstNamevw: UIView!
    @IBOutlet weak var lastNameVw: UIView!
    @IBOutlet weak var emailVw: UIView!
    @IBOutlet weak var pswrdVw: UIView!
    @IBOutlet weak var confirmPswrdVw: UIView!
    @IBOutlet weak var dobVw: UIView!
    @IBOutlet weak var addressVw: UIView!
    @IBOutlet weak var zipcodeVw: UIView!
    @IBOutlet weak var cityVw: UIView!
    @IBOutlet weak var stateVw: UIView!
    @IBOutlet weak var phoneVw: UIView!
    @IBOutlet weak var agreemntVw: UIView!
    
    @IBOutlet weak var intoVw: UIView!
    @IBOutlet weak var userAddVw: UIView!

    
    var searchResultController: SearchResultsController!
    var usState = []
    var arCod = []
    var resultsArray = [String]()
    var gender = String()
    var createAc = Bool()
    var selectGender = Bool()
    var lat = String()
    var long = String()
    var dateS = NSString()
    let date1 = NSDate()
    var date2 = NSDate()
    var fieldName = NSString()
    var radioButtonController: SSRadioButtonsController?
    
    var firstName = NSString()
    var lastName = NSString()
    var emailId = NSString()
    var socialMedia = Bool()
    
    var fbId = NSString()
    var googleId = NSString()
    var urlImage = NSURL()
    override func viewDidLoad() {
        super.viewDidLoad()
         self.boxRound()
        agreemntVw.hidden = true
        agreemntVw.layer.cornerRadius = self.agreemntVw.frame.size.height*0.04
        agreemntVw.layer.borderWidth = self.view.frame.size.height*0.0012
        agreemntVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        agreemntVw.clipsToBounds = true
        usState = ["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Carolina","North Dakota","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]
          arCod = ["201", "202", "203", "204", "205", "206", "207", "208", "209", "210", "212", "213", "214", "215", "216", "217", "218", "219", "224", "225", "226", "228", "229", "231", "234", "239", "240", "242", "246", "248", "250", "251", "252", "253", "254", "256", "260", "262", "264", "267", "268", "269", "270", "276", "281", "284", "289", "301", "302", "303", "304", "305", "306", "307", "308", "309", "310", "312", "313", "314", "315", "316", "317", "318", "319", "320", "321", "323", "325", "330", "331", "334", "336", "337", "339", "340", "343", "345", "347", "351", "352", "360", "361", "385", "386", "401", "402", "403", "404", "405", "406", "407", "408", "409", "410", "412", "413", "414", "415", "416", "417", "418", "419", "423", "424", "425", "430", "432", "434", "435", "438", "440", "441", "442", "443", "450", "456", "458", "469", "470", "473", "475", "478", "479", "480", "484", "500", "501", "502", "503", "504", "505", "506", "507", "508", "509", "510", "512", "513", "514", "515", "516", "517", "518", "519", "520", "530", "533", "534", "539", "540", "541", "551", "559", "561", "562", "563", "567", "570", "571", "573", "574", "575", "579", "580", "581", "585", "586", "587", "600", "601", "602", "603", "604", "605", "606", "607", "608", "609", "610", "612", "613", "614", "615", "616", "617", "618", "619", "620", "623", "626", "630", "631", "636", "641", "646", "647", "649", "650", "651", "657", "660", "661", "662", "664", "670", "671", "678", "681", "682", "684", "669", "700", "701", "702", "703", "704", "705", "706", "707", "708", "709", "710", "712", "713", "714", "715", "716", "717", "718", "719", "720", "724", "727", "731", "732", "734", "740", "747", "754", "757", "758", "760", "762", "763", "765", "767", "769", "770", "772", "773", "774", "775", "778", "779", "780", "781", "784", "785", "786", "787", "800", "801", "802", "803", "804", "805", "806", "807", "808", "809", "810", "812", "813", "814", "815", "816", "817", "818", "819", "828", "829", "830", "831", "832", "843", "845", "847", "848", "849", "850", "855", "856", "857", "858", "859", "860", "862", "863", "864", "865", "866", "867", "868", "869", "870", "872", "876", "877", "878", "888", "900", "901", "902", "903", "904", "905", "906", "907", "908", "909", "910", "912", "913", "914", "915", "916", "917", "918", "919", "920", "925", "928", "931", "936", "937", "938", "939", "940", "941", "947", "949", "951", "952", "954", "956", "970", "971", "972", "973", "978", "979", "980", "985", "989"]
        radioButtonController = SSRadioButtonsController(buttons: maleBtn, femaleBtn)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        
        if(firstName != "" || lastName != "" || emailId != ""){
            firstNameFld.userInteractionEnabled = false
            lastNameFld.userInteractionEnabled = false
            emailadressFld.userInteractionEnabled = false
            
        }
        firstNameFld.text = firstName as String
        lastNameFld.text = lastName as String
        emailadressFld.text = emailId as String
       
        
        
        patientProfile.layer.cornerRadius = patientProfile.frame.size.height/2
        patientProfile.layer.borderWidth = patientProfile.frame.size.height*0.07
        patientProfile.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        patientProfile.clipsToBounds = true
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(PateintCreateAccountViewController.imageTapped(_:)))
        patientProfile.userInteractionEnabled = true
        patientProfile.addGestureRecognizer(tapGestureRecognizer)
        doneBtn.setBackgroundImage(UIImage(named: "SignUpDoneBtn"), forState: .Normal)
        firstNameFld.delegate    = self
        lastNameFld.delegate     = self
        cityFld.delegate         = self
        pswrdFld.delegate        = self
        stateFld.delegate        = self
        zipcodeFld.delegate      = self
        emailadressFld.delegate  = self
        addressFld.delegate      = self
        confrmPaswrdFld.delegate = self
        phnNumberFld.delegate    = self
        birthTextField.delegate  = self
        arCode.delegate = self
        
        firstNameFld.autocapitalizationType = .Words
        lastNameFld.autocapitalizationType = .Words
        cityFld.autocapitalizationType = .Words
        addressFld.autocapitalizationType = .Words
        
        
        firstNameFld.tintColor     = UIColor.blackColor()
        lastNameFld.tintColor = UIColor.blackColor()
        emailadressFld.tintColor   = UIColor.blackColor()
        pswrdFld.tintColor   = UIColor.blackColor()
        confrmPaswrdFld.tintColor   = UIColor.blackColor()
        phnNumberFld.tintColor     = UIColor.blackColor()
        cityFld.tintColor   = UIColor.blackColor()
        arCode.tintColor   = UIColor.blackColor()
        zipcodeFld.tintColor   = UIColor.blackColor()
        addressFld.tintColor = UIColor.blackColor()
        stateFld.tintColor     = UIColor.clearColor()
        let dateInputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0.4))
        let datePickerView  : UIDatePicker = UIDatePicker(frame: CGRectMake(0, 40,UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height*0.36))
        datePickerView.datePickerMode = UIDatePickerMode.Date
        dateInputView.addSubview(datePickerView)
        birthTextField.inputView = dateInputView
//        let yesterday = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -1, toDate: NSDate(), options: [])!
//        datePickerView.maximumDate = yesterday
        datePickerView.addTarget(self, action: #selector(MyInformationViewController.handlePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        handlePicker(datePickerView)
        if(firstNameFld.text == "" || lastNameFld.text == "" || cityFld.text  == "" || pswrdFld.text    == "" || stateFld.text == "" || zipcodeFld.text  == "" || emailadressFld.text == "" || addressFld.text == "" || confrmPaswrdFld.text == "" || phnNumberFld.text == "" || birthTextField.text == "" || selectGender == false || arCode.text == ""){
            doneBtn.enabled = false
        }
        
        if(socialMedia == true){
            if let url = NSURL(string: "\(urlImage)") {
                patientProfile.userInteractionEnabled = false
                self.patientProfile.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
            }
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                pswrdVw.hidden = true
                confirmPswrdVw.hidden = true
                let topConstraint1 = NSLayoutConstraint(item:dobVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: emailVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 55)
                self.view.addConstraints([topConstraint1])
                intoVw.frame.size.height = intoVw.frame.size.height-dobVw.frame.size.height-55
                
            }else{
                pswrdVw.removeFromSuperview()
                confirmPswrdVw.removeFromSuperview()
                let topConstraint1 = NSLayoutConstraint(item:codeVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: emailVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 40)
                self.view.addConstraints([topConstraint1])
                
                let topConstraint2 = NSLayoutConstraint(item:phoneVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: emailVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 40)
                self.view.addConstraints([topConstraint2])
            }
        }
        
        
        
        
        
        
//         if(socialMedia == true){
//            if let url = NSURL(string: "\(urlImage)") {
//                patientProfile.userInteractionEnabled = false
//                self.patientProfile.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
//            }
//        pswrdVw.removeFromSuperview()
//        confirmPswrdVw.removeFromSuperview()
//        let topConstraint1 = NSLayoutConstraint(item:codeVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: emailVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 40)
//        self.view.addConstraints([topConstraint1])
//        
//        let topConstraint2 = NSLayoutConstraint(item:phoneVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: emailVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 40)
//        self.view.addConstraints([topConstraint2])
//        }
    }
    func boxRound(){
        codeVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        codeVw.layer.borderColor   = UIColor.darkGrayColor().CGColor
        codeVw.layer.masksToBounds = false;
        codeVw.layer.borderWidth   = 0.8
        
        dobVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        dobVw.layer.borderColor   = UIColor.darkGrayColor().CGColor
        dobVw.layer.masksToBounds = false;
        dobVw.layer.borderWidth   = 0.8
        
        emailVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        emailVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        emailVw.layer.masksToBounds = false;
        emailVw.layer.borderWidth = 0.8
        
        firstNamevw.layer.cornerRadius =  self.view.frame.size.height*0.009
        firstNamevw.layer.borderColor = UIColor.darkGrayColor().CGColor
        firstNamevw.layer.masksToBounds = false;
        firstNamevw.layer.borderWidth = 0.8
        
        lastNameVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        lastNameVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        lastNameVw.layer.masksToBounds = false;
        lastNameVw.layer.borderWidth = 0.8
        
        pswrdVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        pswrdVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        pswrdVw.layer.masksToBounds = false;
        pswrdVw.layer.borderWidth = 0.8
        
        
        confirmPswrdVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        confirmPswrdVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        confirmPswrdVw.layer.masksToBounds = false;
        confirmPswrdVw.layer.borderWidth = 0.8
        
       
        
        
        cityVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        cityVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        cityVw.layer.masksToBounds = false;
        cityVw.layer.borderWidth = 0.8
        
        
        stateVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        stateVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        stateVw.layer.masksToBounds = false;
        stateVw.layer.borderWidth = 0.8
        
        zipcodeVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        zipcodeVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        zipcodeVw.layer.masksToBounds = false;
        zipcodeVw.layer.borderWidth = 0.8
        
        phoneVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        phoneVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        phoneVw.layer.masksToBounds = false;
        phoneVw.layer.borderWidth = 0.8
        
        addressVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        addressVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        addressVw.layer.masksToBounds = false;
        addressVw.layer.borderWidth = 0.8
    }
    
    @IBAction func agreeBtnAc(sender: AnyObject) {
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
        let userId = NSUserDefaults.standardUserDefaults().valueForKey("id") as! String
        self.doRequestPost("\(Header.BASE_URL)users/agreement", data: ["UserId":"\(userId)","agreement":"1"])
        
    }
    func doRequestPost(url:String,data:[String: NSObject]){
        print("URL FOR DEVICETOKEN ==> \(url)")
        let requestDic = data
        print(requestDic)
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        
        print("Request Object:\(data)")
        print("Request string = \(jsonString!)")
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                print("Succes:")
                let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                print(responceString)
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                dispatch_async(dispatch_get_main_queue()){
                    self.agreemntVw.hidden = true
                    var storyboard = UIStoryboard()
                    switch UIDevice.currentDevice().userInterfaceIdiom {
                    case .Phone:
                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                    case .Pad:
                        storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    default:
                        break
                    }
                    let vc     = storyboard.instantiateViewControllerWithIdentifier("SearchDoctorViewController") as! SearchDoctorViewController
                    vc.searchDoc   = true
                    vc.firstSearch = true
                    self.navigationController!.pushViewController(vc, animated: true)
                }
            }
        }
        
        dataTask.resume()
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        searchResultController = SearchResultsController()
    }
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        let addr = searchResultController.address
        addressFld.text = addr
        if(addr != ""){
        self.getLocationFromAddressString(addressFld.text!)
        }
        return true;
    }
    func getLocationFromAddressString(loadAddressStr: String)  {
        var latitude :Double = 0
        var longitude : Double = 0
        let esc_addr = loadAddressStr.stringByAppendingFormat("%@","")
        let req = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=\(esc_addr)"
        let escapedAddress = req.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        let url: NSURL = NSURL(string: escapedAddress!)!
        let result:String? = try? String(contentsOfURL:url)
        if result != nil {
            let scanner: NSScanner = NSScanner(string: result!)
            if(scanner.scanUpToString("\"lat\" :", intoString: nil) && scanner.scanString("\"lat\" :", intoString: nil)){
                scanner.scanDouble(&latitude)
                if(scanner.scanUpToString("\"lng\" :", intoString: nil) && scanner.scanString("\"lng\" :", intoString: nil)){
                    scanner.scanDouble(&longitude)
                }
            }
        }
        var center :CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0);
        center.latitude = latitude;
        center.longitude = longitude;
        lat = String(format: "%.20f", center.latitude)
        long = String(format: "%.20f", center.longitude)
    }
    func handlePicker(sender: UIDatePicker){
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        dateS = dateFormatter.stringFromDate(sender.date)
        date2 = sender.date
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        if(textField == addressFld){
            fieldName = "Address"
//            let searchController = UISearchController(searchResultsController: searchResultController)
//            searchController.searchBar.delegate = self
//            self.presentViewController(searchController, animated: true, completion: nil)
        }
        else if(textField == firstNameFld){
            fieldName = "First Name"
        }
        else if(textField == lastNameFld){
            fieldName = "Last Name"
        }
        else if(textField == cityFld){
            fieldName = "City"
        }
        
      else if(textField == stateFld ){
        let InputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0.4))
        let PickerVw  : UIPickerView = UIPickerView(frame: CGRectMake(0, -10,UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height*0.32))
        PickerVw.delegate            = self
        PickerVw.dataSource          = self
        InputView.addSubview(PickerVw)
        textField.inputView = InputView
      }
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if(textField == cityFld || textField == firstNameFld || textField == lastNameFld ){
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
            if(newLength == 36){
               AppManager.sharedManager.Showalert("Alert", alertmessage: "\(fieldName) does not contain more than 35 characters.")
            }
        return newLength <= 35
        }
        if(textField == arCode){
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            if(newLength > 3){
                phnNumberFld.becomeFirstResponder()
            }
            return newLength <= 3
        }
        if(textField == addressFld){
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            if(newLength == 65){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "\(fieldName) does not contain more than 65 characters.")
            }
            return newLength <= 64
        }
        if(textField == phnNumberFld){
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            return newLength <= 7
        }
        if(textField == zipcodeFld){
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            return newLength <= 9
        }
        return true
    }
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return usState.count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return usState[row] as? String
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        stateFld.text = usState[row] as? String
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if(textField == birthTextField){
         birthTextField.text = dateS as String
        }
        let trimNameString1 = firstNameFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        let trimNameString2 = lastNameFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString3 = cityFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString4 = pswrdFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString5 = zipcodeFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString6 = emailadressFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString7 = addressFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString8 = phnNumberFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString9 = arCode.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if(socialMedia == false){
        if(trimNameString1 != "" && trimNameString2 != "" &&  trimNameString3 != "" && trimNameString4  != "" && stateFld.text != "" && trimNameString5  != "" && trimNameString6 != "" && trimNameString7 != "" && confrmPaswrdFld.text != "" && trimNameString8 != "" && birthTextField.text != "" && selectGender == true && trimNameString9 != ""){
            doneBtn.enabled = true
        }
        else if(trimNameString1.characters.count == 0 || trimNameString2.characters.count == 0 || trimNameString3.characters.count == 0 || trimNameString4.characters.count == 0 || stateFld.text == "" || trimNameString5.characters.count == 0 || trimNameString6.characters.count == 0 || trimNameString7.characters.count == 0 || confrmPaswrdFld.text == "" || trimNameString8.characters.count == 0 || birthTextField.text == "" || selectGender == false || trimNameString9.characters.count == 0){
            doneBtn.enabled = false
        }
        }else{
            if(trimNameString3 != "" &&  stateFld.text != "" && trimNameString5  != "" && trimNameString7 != "" &&  trimNameString8 != "" && birthTextField.text != "" && selectGender == true && trimNameString9 != ""){
                doneBtn.enabled = true
            }
            else if(trimNameString3.characters.count == 0 || stateFld.text == "" || trimNameString5.characters.count == 0 || trimNameString7.characters.count == 0 || trimNameString8.characters.count == 0 || birthTextField.text == "" || selectGender == false || trimNameString9.characters.count == 0){
                doneBtn.enabled = false
            }
        }
    }
   func okBtnAc(){
    let view = self.view.viewWithTag(2369)! as UIView
    view.removeFromSuperview()
   }
    func imageTapped(img: AnyObject)
    {
        self.view.endEditing(true)
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        actionSheet.showInView(self.view)
    }
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex){
        case 0:
            print("Cancel")
        case 1:
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.allowsEditing = false
                        imagePicker.sourceType = .Camera
                        self.presentViewController(imagePicker, animated: true, completion:nil)
                    }
                })
            }else{
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .Camera
                    presentViewController(imagePicker, animated: true, completion:nil)
                }
            }
        case 2:
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.allowsEditing = false
                        imagePicker.sourceType = .PhotoLibrary
                        self.presentViewController(imagePicker, animated: true, completion:nil)
                    }
                })
            }else{
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .PhotoLibrary
                    self.presentViewController(imagePicker, animated: true, completion:nil)
                }
            }
        default:
            print("Default")
            //Some code here..
        }
    }
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        let selectedImage : UIImage  = image
        patientProfile.image         = self.ResizeImage(selectedImage, targetSize: CGSizeMake(200.0, 200.0))
        patientProfile.clipsToBounds = true
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
@IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
func didSelectButton(aButton: UIButton?) {
        selectGender = false
        if(aButton == maleBtn){
           gender = "Male"
            selectGender = true
        }else if(aButton == femaleBtn){
            gender = "Female"
            selectGender = true
        }
    let trimNameString1 = firstNameFld.text!.stringByTrimmingCharactersInSet(
        NSCharacterSet.whitespaceAndNewlineCharacterSet());
    let trimNameString2 = lastNameFld.text!.stringByTrimmingCharactersInSet(
        NSCharacterSet.whitespaceAndNewlineCharacterSet())
    let trimNameString3 = cityFld.text!.stringByTrimmingCharactersInSet(
        NSCharacterSet.whitespaceAndNewlineCharacterSet())
    let trimNameString4 = pswrdFld.text!.stringByTrimmingCharactersInSet(
        NSCharacterSet.whitespaceAndNewlineCharacterSet())
    let trimNameString5 = zipcodeFld.text!.stringByTrimmingCharactersInSet(
        NSCharacterSet.whitespaceAndNewlineCharacterSet())
    let trimNameString6 = emailadressFld.text!.stringByTrimmingCharactersInSet(
        NSCharacterSet.whitespaceAndNewlineCharacterSet())
    let trimNameString7 = addressFld.text!.stringByTrimmingCharactersInSet(
        NSCharacterSet.whitespaceAndNewlineCharacterSet())
    let trimNameString8 = phnNumberFld.text!.stringByTrimmingCharactersInSet(
        NSCharacterSet.whitespaceAndNewlineCharacterSet())
    let trimNameString9 = arCode.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    if(socialMedia == false){
    if(trimNameString1 != "" && trimNameString2 != "" &&  trimNameString3 != "" && trimNameString4  != "" && stateFld.text != "" && trimNameString5  != "" && trimNameString6 != "" && trimNameString7 != "" && confrmPaswrdFld.text != "" && trimNameString8 != "" && birthTextField.text != "" && selectGender == true && trimNameString9 != ""){
        doneBtn.enabled = true
    }
    else if(trimNameString1.characters.count == 0 || trimNameString2.characters.count == 0 || trimNameString3.characters.count == 0 || trimNameString4.characters.count == 0 || stateFld.text == "" || trimNameString5.characters.count == 0 || trimNameString6.characters.count == 0 || trimNameString7.characters.count == 0 || confrmPaswrdFld.text == "" || trimNameString8.characters.count == 0 || birthTextField.text == "" || selectGender == false || trimNameString9.characters.count == 0){
        doneBtn.enabled = false
    }
    }
    else{
        if(trimNameString3 != "" &&  stateFld.text != "" && trimNameString5  != "" && trimNameString7 != "" &&  trimNameString8 != "" && birthTextField.text != "" && selectGender == true && trimNameString9 != ""){
            doneBtn.enabled = true
        }
        else if(trimNameString3.characters.count == 0 || stateFld.text == "" || trimNameString5.characters.count == 0 || trimNameString7.characters.count == 0 || trimNameString8.characters.count == 0 || birthTextField.text == "" || selectGender == false || trimNameString9.characters.count == 0){
            doneBtn.enabled = false
        }
    }
    }
    override func viewDidLayoutSubviews() {
        if((self.storyboard?.valueForKey("name"))! as! String == "Main"){
            scrollVwSignUp.scrollEnabled = true
                if(self.view.frame.size.height < 600){
                    if(socialMedia == false){
                    scrollVwSignUp.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1.5)
                    }else{
                     scrollVwSignUp.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1.22)
                    }
                }
                else if(self.view.frame.size.height > 600 && self.view.frame.size.height < 700){
                     if(socialMedia == false){
                    scrollVwSignUp.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1.4)
                     }else{
                        scrollVwSignUp.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1.1)
                    }
                }
                else{
                     if(socialMedia == false){
                    scrollVwSignUp.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1.3)
                     }else{
                        scrollVwSignUp.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1)
                    }
                }
          }
     }
    @IBAction func doneBtnAc(sender: AnyObject) {
        if(socialMedia == true){
            self.socialMediaTrue()
        }else{
            self.socialMediaFalse()
        }
      
        
    }
    func socialMediaTrue(){
        let compPh = "\(arCode.text! as String)\(phnNumberFld.text! as String)"
        if(!(arCod.containsObject(arCode.text!))){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            //let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.39))
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter a Valid Area Code."
            contentLbl.numberOfLines = 3
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PateintCreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(!(AppManager.sharedManager.isValidEmail(emailadressFld.text!))){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag             = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor    = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please enter valid email address."
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(zipcodeFld.text?.characters.count != 5 && zipcodeFld.text?.characters.count < 5){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter valid zipcode"
            contentLbl.numberOfLines = 1
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PateintCreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(zipcodeFld.text?.characters.count != 9 && zipcodeFld.text?.characters.count > 5){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.font = UIFont(name: "Helvetica", size:14)
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter valid zipcode"
            contentLbl.numberOfLines = 1
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.addTarget(self, action: #selector(PateintCreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(compPh.characters.count != 10){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please enter 10 digit phone number."
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if date2.earlierDate(date1).isEqualToDate(date1)  {
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter a valid date of birth"
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            
            return
        }
        else if(patientProfile.image == UIImage.init(named: "SignUpImg")){
            AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select Your Profile Picture")
            return
        }
        
        let num = Int(phnNumberFld.text!)
        if num == nil {
            AppManager.sharedManager.Showalert("Alert", alertmessage: "Please enter numeric value in phoneNumber Field.")
            return
        }
        AppManager.sharedManager.delegate=self
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(firstNameFld.text,forKey: "firstName")
        defaults.setObject(lastNameFld.text, forKey: "lastName")
        defaults.setObject(addressFld.text,  forKey: "address")
        let emailLowerCaseString:String = (emailadressFld.text?.lowercaseString)!
        let firstNameLowerCaseString:String = (firstNameFld.text?.lowercaseString)!
        let lastNameLowerCaseString:String = (lastNameFld.text?.lowercaseString)!
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Signing Up")
        let media = NSUserDefaults.standardUserDefaults().valueForKey("mediaType") as! String
        if(media == "facebook"){
            let testImage = NSData(contentsOfURL: urlImage)
            let base64String = testImage!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.EncodingEndLineWithLineFeed)
            let params = "FirstName=\(firstNameLowerCaseString)&LastName=\(lastNameLowerCaseString)&Gender=\(gender)&Mobile=\(phnNumberFld.text!)&Code=\(arCode.text!)&Address=\(addressFld.text!)&State=\(stateFld.text!)&City=\(cityFld.text!)&Zipcode=\(zipcodeFld.text!)&Lat=\(lat)&Long=\(long)&DOB=\(birthTextField.text!)&ProfilePic=\(base64String)&UserType=0&Email=\(emailLowerCaseString)&DeviceToken=123456&DeviceType=ios&MediaID=\(fbId)&MediaType=F"
            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/sign")
        }else{
            let testImage = NSData(contentsOfURL: urlImage)
            let base64String = testImage!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.EncodingEndLineWithLineFeed)
            let params = "FirstName=\(firstNameLowerCaseString)&LastName=\(lastNameLowerCaseString)&Gender=\(gender)&Mobile=\(phnNumberFld.text!)&Code=\(arCode.text!)&Address=\(addressFld.text!)&State=\(stateFld.text!)&City=\(cityFld.text!)&Zipcode=\(zipcodeFld.text!)&Lat=\(lat)&Long=\(long)&DOB=\(birthTextField.text!)&ProfilePic=\(base64String)&UserType=0&Email=\(emailLowerCaseString)&DeviceToken=123456&DeviceType=ios&MediaID=\(googleId)&MediaType=G"
            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/sign")
        }
    }
    func socialMediaFalse(){
        let compPh = "\(arCode.text! as String)\(phnNumberFld.text! as String)"
        if(!(arCod.containsObject(arCode.text!))){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            //let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.39))
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter a Valid Area Code."
            contentLbl.numberOfLines = 3
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PateintCreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(confrmPaswrdFld.text != pswrdFld.text){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.15)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor    = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            //let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.3)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor     = UIColor.grayColor()
            contentLbl.text          = "Password and Confirm Password does not match."
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(!(AppManager.sharedManager.isValidEmail(emailadressFld.text!))){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag             = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor    = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please enter valid email address."
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(zipcodeFld.text?.characters.count != 5 && zipcodeFld.text?.characters.count < 5){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter valid zipcode"
            contentLbl.numberOfLines = 1
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PateintCreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(zipcodeFld.text?.characters.count != 9 && zipcodeFld.text?.characters.count > 5){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.font = UIFont(name: "Helvetica", size:14)
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter valid zipcode"
            contentLbl.numberOfLines = 1
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.addTarget(self, action: #selector(PateintCreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(compPh.characters.count != 10){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please enter 10 digit phone number."
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if date2.earlierDate(date1).isEqualToDate(date1)  {
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.2)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter a valid date of birth"
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            
            return
        }
        else if(!(pswrdFld.text?.characters.count >= 6 && pswrdFld.text?.characters.count <= 15)){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.23)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.15)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor    = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.38)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.3)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor     = UIColor.grayColor()
            contentLbl.text          = "please enter password in between 6 to 15 digits"
            contentLbl.numberOfLines = 3
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(patientProfile.image == UIImage.init(named: "SignUpImg")){
            AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select Your Profile Picture")
            return
        }
        let num = Int(phnNumberFld.text!)
        if num == nil {
            AppManager.sharedManager.Showalert("Alert", alertmessage: "Please enter numeric value in phoneNumber Field.")
            return
        }
        AppManager.sharedManager.delegate=self
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(firstNameFld.text,forKey: "firstName")
        defaults.setObject(lastNameFld.text, forKey: "lastName")
        defaults.setObject(addressFld.text,  forKey: "address")
        let emailLowerCaseString:String = (emailadressFld.text?.lowercaseString)!
        let firstNameLowerCaseString:String = (firstNameFld.text?.lowercaseString)!
        let lastNameLowerCaseString:String = (lastNameFld.text?.lowercaseString)!
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Signing Up")
        let image : UIImage = patientProfile.image!
        let  mImageData              = UIImagePNGRepresentation(image)! as NSData
        let base64String = mImageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.EncodingEndLineWithLineFeed)
        NSUserDefaults.standardUserDefaults().setValue("manual", forKey: "mediaType")
        let params = "FirstName=\(firstNameLowerCaseString)&LastName=\(lastNameLowerCaseString)&Gender=\(gender)&Mobile=\(phnNumberFld.text!)&Code=\(arCode.text!)&Address=\(addressFld.text!)&State=\(stateFld.text!)&City=\(cityFld.text!)&Zipcode=\(zipcodeFld.text!)&Lat=\(lat)&Long=\(long)&DOB=\(birthTextField.text!)&ProfilePic=\(base64String)&UserType=0&Email=\(emailLowerCaseString)&DeviceToken=123456&DeviceType=ios&Password=\(pswrdFld.text!)&MediaType=M"
        AppManager.sharedManager.postDataOnserver(params, postUrl: "users/sign")
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString)
    {
        if((responseDict.valueForKey("error")) != nil){
             AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            dispatch_async(dispatch_get_main_queue()) {
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    contentLbl.font  = UIFont(name: "Helvetica", size:14)
                    contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.38)
            }else{
                    contentLbl.font  = UIFont(name: "Helvetica", size:19)
                    contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.21)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "This Email id is already taken."
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PateintCreateAccountViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            }
        }else{
            let defaults = NSUserDefaults.standardUserDefaults()
             defaults.setObject(responseDict.valueForKey("id"), forKey: "id")
             defaults.setObject(lat, forKey: "lat")
             defaults.setObject(long, forKey: "long")
            if(socialMedia == true){
              defaults.setValue("\(urlImage)", forKey: "profilePic")
                defaults.setValue(0, forKey: "usertype")
            }
        dispatch_async(dispatch_get_main_queue()) {
            //let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setBool(false, forKey: "searchDoc")
            defaults.setBool(false, forKey: "selectPharma")
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                backVw.tag = 2369
                self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.25)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.19)
            }
            
                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                alerVw.backgroundColor = UIColor.whiteColor()
                backVw.addSubview(alerVw)
                
                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                rightImg.image = UIImage(named: "verifyImg")
                alerVw.addSubview(rightImg)
                
                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                verificationLbl.text = " Alert"
                verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(verificationLbl)
                
                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                lineVw.backgroundColor = UIColor.grayColor()
                alerVw.addSubview(lineVw)
                
            
            let contentLbl = UILabel()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.05,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.1),alerVw.frame.size.height*0.4)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
                contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.05,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.1),alerVw.frame.size.height*0.35)
            }
                contentLbl.textAlignment = .Center
                contentLbl.textColor = UIColor.grayColor()
                contentLbl.text = "You have successfully created your Health4life Account! You will now be prompted to select your doctor."
                contentLbl.numberOfLines = 3
                alerVw.addSubview(contentLbl)
                
                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
                okLbl.setTitle("OK", forState: .Normal)
              if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                 okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
             }else{
                 okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
             }
                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                okLbl.titleLabel?.textAlignment = .Center
                okLbl.addTarget(self, action: #selector(PateintCreateAccountViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(okLbl)
        }
      }
    }
    func okBtnAc2(){
        let view = self.view.viewWithTag(2369)! as UIView
        view.removeFromSuperview()
        if(socialMedia == true){
        agreemntVw.hidden = false
        }else{
            let vc     = storyboard!.instantiateViewControllerWithIdentifier("SearchDoctorViewController") as! SearchDoctorViewController
            vc.searchDoc   = true
            vc.firstSearch = true
            self.navigationController!.pushViewController(vc, animated: true)
        }
        //let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }

    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        let placeClient = GMSPlacesClient()
        
        placeClient.autocompleteQuery(searchText, bounds: nil, filter: nil) { (data, error: NSError?) -> Void in
            self.resultsArray.removeAll()
        }
        placeClient.autocompleteQuery(searchText, bounds: nil, filter: nil) { (results, error: NSError?) -> Void in
            
            self.resultsArray.removeAll()
            if results == nil {
                return
            }
            
            for result in results! {
                if let result = result as? GMSAutocompletePrediction {
                    self.resultsArray.append(result.attributedFullText.string)
                }
            }
            
            self.searchResultController.reloadDataWithArray(self.resultsArray)
            
        }
    }
     override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
   
}
