

import UIKit
import SlideMenuControllerSwift
import SDWebImage
class DoctorMyAppointmentsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,WebServiceDelegate {
    @IBOutlet weak var appointmntSgmnt: UISegmentedControl!
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var backImageVw: UIImageView!
    var previouslySelectedHeaderIndex: Int?
    var selectedHeaderIndex: Int?
    var selectedItemIndex: Int?
    let cells = SwiftyAccordionCells()
    var selectStaff = Bool()
    var headerVw = UIView()
    var upcomingDate = NSString()
    var imageDrop =  UIImageView()
    var arrayAllData    = NSArray()
    var ageArray = NSMutableArray()
    var genderAr = NSMutableArray()
    var timeAr   = NSMutableArray()
    var scheduleAr = NSString()
    var sch = NSMutableArray()
    var serverInt = NSInteger()
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let params = "Dr_Id=\(userId)"
            serverInt = 1
            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrAppointmentList")
        }
        tableVw.delegate = self
        tableVw.dataSource = self
        self.navigationController?.navigationBarHidden = true
        tableVw.separatorStyle = .None
        
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            let font = UIFont.systemFontOfSize(20)
            appointmntSgmnt.setTitleTextAttributes([NSFontAttributeName: font],forState: UIControlState.Normal)
        }
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Selected)
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
    }
    func servicehit(){
        self.cells.removeAll()
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let params = "Dr_Id=\(userId)"
            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrAppointmentList")
        }
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        dispatch_async(dispatch_get_main_queue()) {
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            if(self.serverInt == 1){
                if(responseDict.objectForKey("data")?.count != 0){
                    let data  = responseDict.objectForKey("data")
                    self.arrayAllData  = data?.mutableCopy() as! NSArray
                    let bookingData = self.arrayAllData.valueForKey("Booking") as! NSArray
                    self.tableVw.hidden = false
                    if(((bookingData.valueForKey("past")).count) == 0 && self.appointmntSgmnt.selectedSegmentIndex == 1 && ((bookingData.valueForKey("past")) as! NSArray).objectAtIndex(0).count != 0 ){
                        self.tableVw.hidden = true
                        self.backImageVw.image = UIImage(named: "pastApp")
                    }
                    if(((bookingData.valueForKey("upcoming")).count) != 0 && self.appointmntSgmnt.selectedSegmentIndex == 0 && ((bookingData.valueForKey("upcoming")) as! NSArray).objectAtIndex(0).count == 0 ){
                        self.backImageVw.image = UIImage(named: "upcomingApp")
                        self.tableVw.hidden = true
                    }
                    self.setup()
                    let defaults = NSUserDefaults.standardUserDefaults()
                    if let staffId = defaults.valueForKey("staffId"){
                        AppManager.sharedManager.delegate=self
                        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                        let params1 = "StaffId=\(staffId)&Type=3"
                        self.serverInt = 2
                        AppManager.sharedManager.postDataOnserver(params1, postUrl: "staffapi/StaffScheduleRead")
                    }else{
                        let userId = defaults.valueForKey("id") as! NSString
                        AppManager.sharedManager.delegate=self
                        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                        let params1 = "UserId=\(userId)&Type=1"
                        self.serverInt = 2
                        AppManager.sharedManager.postDataOnserver(params1, postUrl: "users/scheduleread")
                    }
                }else if(responseDict.objectForKey("data")?.count == 0){
                    if(self.appointmntSgmnt.selectedSegmentIndex == 0){
                        self.backImageVw.image = UIImage(named: "upcomingApp")
                        self.tableVw.hidden = true
                    }
                    else{
                        self.tableVw.hidden = true
                        self.backImageVw.image = UIImage(named: "pastApp")
                    }
                    
                }
            }
            else{
                
            }
        }
        
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    func setup(){
        if(arrayAllData.count != 0){
            let arrayTempPast = arrayAllData.valueForKey("Booking") as! NSArray
            let upcomingData = arrayTempPast.valueForKey("upcoming") as! NSArray
            let arraTemp = upcomingData[0]
            let ar = arraTemp.valueForKey("ScheduleDate")! as! NSArray
            var duplicateAr = NSArray()
            duplicateAr = uniq(ar  as! [String])
            if(arraTemp.count != 0){
                duplicateAr.enumerateObjectsUsingBlock ({ (obj, index, value) in
                    
                    
                    self.cells.append(SwiftyAccordionCells.HeaderItem(value:obj))
                    arraTemp.enumerateObjectsUsingBlock ({ (obj1, index1, value1) in
                        let date = obj1.valueForKey("ScheduleDate") as! String
                        if date == String(obj){
                            self.cells.append(SwiftyAccordionCells.Item(value: obj1))
                        }
                    })
                })
                tableVw.reloadData()
                
            }
        }
        else{
            self.backImageVw.image = UIImage(named: "upcomingApp")
            self.tableVw.hidden = true
        }
    }
    func uniq<S: SequenceType, E: Hashable where E==S.Generator.Element>(source: S) -> [E] {
        var seen: [E:Bool] = [:]
        return source.filter { seen.updateValue(true, forKey: $0) == nil }
    }
    func pastSetUp(){
        if(arrayAllData.count != 0){
            let arrayTempPast = arrayAllData.valueForKey("Booking") as! NSArray
            let upcomingData = arrayTempPast.valueForKey("past") as! NSArray
            let arraTemp = upcomingData[0]
            let ar = arraTemp.valueForKey("ScheduleDate")! as! NSArray
            var duplicateAr = NSArray()
            duplicateAr = uniq(ar  as! [String])
            if(arraTemp.count != 0){
                duplicateAr.enumerateObjectsUsingBlock ({ (obj, index, value) in
                    self.cells.append(SwiftyAccordionCells.HeaderItem(value:obj))
                    arraTemp.enumerateObjectsUsingBlock ({ (obj1, index1, value1) in
                        let date = obj1.valueForKey("ScheduleDate") as! String
                        if date == String(obj){
                            self.cells.append(SwiftyAccordionCells.Item(value: obj1))
                        }
                    })
                })
                tableVw.reloadData()
            }
        }
        else{
            self.tableVw.hidden = true
            self.backImageVw.image = UIImage(named: "pastApp")
        }
    }
    @IBAction func appointmntSegmntAc(sender: AnyObject) {
        if(appointmntSgmnt.selectedSegmentIndex == 0){
            imageDrop.image = UIImage(named: "dropDown")
            self.cells.removeAll()
            self.selectedHeaderIndex = nil
            self.setup()
            self.tableVw.hidden = false
            if(self.cells.items.count == 0){
                self.backImageVw.image = UIImage(named: "upcomingApp")
                self.tableVw.hidden = true
            }
        }
        else{
            imageDrop.image = UIImage(named: "dropDown")
            self.cells.removeAll()
            self.selectedHeaderIndex = nil
            self.pastSetUp()
            self.tableVw.hidden = false
            if(self.cells.items.count == 0){
                self.tableVw.hidden = true
                self.backImageVw.image = UIImage(named: "pastApp")
            }
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(appointmntSgmnt.selectedSegmentIndex == 0){
            return self.cells.items.count
        }else{
            return self.cells.items.count
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let item = self.cells.items[indexPath.row]
        
        let value = item.value as? NSDictionary
        if(appointmntSgmnt.selectedSegmentIndex == 0){
            if(self.cells.items[indexPath.row] is SwiftyAccordionCells.HeaderItem){
                let cell = tableView.dequeueReusableCellWithIdentifier("cell")
                let dateLbl: UILabel = (cell!.viewWithTag(800) as! UILabel)
                cell?.userInteractionEnabled = true
                dateLbl.text = item.value as? String
                if(dateLbl.text != nil){
                    upcomingDate = dateLbl.text!
                }
                let vw: UIView = (cell!.viewWithTag(200))!
                vw.layer.cornerRadius = 7
                cell!.layer.masksToBounds = true
                headerVw = vw
                let dropDown: UIImageView = (cell!.viewWithTag(801) as! UIImageView)
                imageDrop = dropDown
                return cell!
            }else {
                let cell = tableView.dequeueReusableCellWithIdentifier("detail")
                if(indexPath.row == 3){
                    let imageView = UIImageView(frame: CGRectMake(0, 0, cell!.frame.width, cell!.frame.height))
                    let image = UIImage(named: "dropBox2")
                    imageView.image = image
                    cell!.backgroundView = imageView
                }
                let imageView = UIImageView(frame: CGRectMake(0, 0, cell!.frame.width, cell!.frame.height))
                let image = UIImage(named: "dropBox1")
                imageView.image = image
                cell!.backgroundView = imageView
                let PtLbl: UILabel = (cell!.viewWithTag(803) as! UILabel)
                print(value)
                let ptFirstName = value?.valueForKeyPath("PatientId.FirstName") as! NSString
                let ptLastName = value?.valueForKeyPath("PatientId.LastName") as! NSString
                
                PtLbl.text = "\(ptFirstName) \(ptLastName)" as String
                let ptgnderAge:UILabel = (cell!.viewWithTag(804) as! UILabel)
                let age = value?.valueForKey("Age") as! NSString
                let gnder = value?.valueForKeyPath("PatientId.Gender") as! NSString
                
                let fromStr = value?.valueForKey("From")  as? String
                let dateFormatter = NSDateFormatter()
                let timeZone = NSTimeZone(name: "GMT-8")
                dateFormatter.timeZone=timeZone
                dateFormatter.dateFormat = "HH:mm"
                let date24Hr = dateFormatter.dateFromString(fromStr! as String)!
                dateFormatter.dateFormat = "hh:mm a"
                let date12Hr = dateFormatter.stringFromDate(date24Hr)
                
                ptgnderAge.text = "\(gnder)-\(age)"
                let ptTime:UILabel = (cell!.viewWithTag(805) as! UILabel)
                ptTime.text = date12Hr as String
                let ptImg: UIImageView = (cell!.viewWithTag(802) as! UIImageView)
                let profilePic = value?.valueForKey("profile") as! NSString
                ptImg.layer.cornerRadius = ptImg.frame.size.height/2
                ptImg.layer.borderWidth = self.view.frame.size.height*0.008
                ptImg.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
                if let url = NSURL(string: "http://\(profilePic)") {
                    ptImg.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
                }
                return cell!
            }
        }else{
            if(self.cells.items[indexPath.row] is SwiftyAccordionCells.HeaderItem){
                let cell = tableView.dequeueReusableCellWithIdentifier("cell")
                let dateLbl: UILabel = (cell!.viewWithTag(800) as! UILabel)
                cell?.userInteractionEnabled = true
                dateLbl.text = item.value as? String
                let vw: UIView = (cell!.viewWithTag(200))!
                vw.layer.cornerRadius = 7
                cell!.layer.masksToBounds = true
                headerVw = vw
                return cell!
            }
            else{
                let cell = tableView.dequeueReusableCellWithIdentifier("detailPast")
                if(indexPath.row == 3){
                    let imageView = UIImageView(frame: CGRectMake(0, 0, cell!.frame.width, cell!.frame.height))
                    let image = UIImage(named: "dropBox2")
                    imageView.image = image
                    cell!.backgroundView = imageView
                }
                let imageView = UIImageView(frame: CGRectMake(0, 0, cell!.frame.width, cell!.frame.height))
                let image = UIImage(named: "dropBox1")
                imageView.image = image
                cell!.backgroundView = imageView
                let age = value?.valueForKey("Age") as! NSString
                let gnder = value?.valueForKeyPath("PatientId.Gender") as! NSString
                let fromStr = value?.valueForKey("From")  as? String
                let dateFormatter = NSDateFormatter()
                let timeZone = NSTimeZone(name: "GMT-8")
                dateFormatter.timeZone=timeZone
                dateFormatter.dateFormat = "HH:mm"
                let date24Hr = dateFormatter.dateFromString(fromStr! as String)!
                dateFormatter.dateFormat = "hh:mm a"
                let date12Hr = dateFormatter.stringFromDate(date24Hr)
                let time = date12Hr
                let status = value?.valueForKey("Status") as! NSInteger
                let PtLbl: UILabel = (cell!.viewWithTag(807) as! UILabel)
                let ptFirstName = value?.valueForKeyPath("PatientId.FirstName") as! NSString
                let ptLastName = value?.valueForKeyPath("PatientId.LastName") as! NSString
                PtLbl.text = "\(ptFirstName) \(ptLastName)" as String
                let ptgnderAge:UILabel = (cell!.viewWithTag(808) as! UILabel)
                ptgnderAge.text = "\(gnder)-\(age)"
                let ptTime:UILabel = (cell!.viewWithTag(810) as! UILabel)
                ptTime.text = time as String
                let ptStatus:UILabel = (cell!.viewWithTag(809) as! UILabel)
                if(status == 2){
                    ptStatus.text = "Status:Completed"
                }else if(status == 0){
                    ptStatus.text = "Status:Not Completed"
                }
                else{
                    ptStatus.text = "Status:Canceled by Doctor"
                }
                let ptImg: UIImageView = (cell!.viewWithTag(806) as! UIImageView)
                let profilePic = value?.valueForKey("profile") as! NSString
                ptImg.layer.cornerRadius = ptImg.frame.size.height/2
                ptImg.layer.borderWidth = self.view.frame.size.height*0.008
                ptImg.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
                if let url = NSURL(string: "http://\(profilePic)") {
                    ptImg.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
                }
                return cell!
            }
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let item = self.cells.items[indexPath.row]
        let value = item.value as? NSDictionary
        if item is SwiftyAccordionCells.HeaderItem {
            let indexPath = tableVw.indexPathForSelectedRow
            let currentCell = tableVw.cellForRowAtIndexPath(indexPath!)! as UITableViewCell
            headerVw.layer.cornerRadius = 7;
            imageDrop.image = UIImage(named: "dropDown")
            let vw: UIView = (currentCell.viewWithTag(200))!
            vw.layer.cornerRadius = 0;
            let path = UIBezierPath(roundedRect:vw.bounds, byRoundingCorners:[.TopLeft, .TopRight], cornerRadii: CGSize(width: 7, height:  7))
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.CGPath
            vw.layer.mask = maskLayer
            headerVw = vw
            let dropDown: UIImageView = (currentCell.viewWithTag(801) as! UIImageView)
            dropDown.image = UIImage(named: "dropUp")
            imageDrop = dropDown
            if self.selectedHeaderIndex == nil {
                self.selectedHeaderIndex = indexPath!.row
            } else {
                self.previouslySelectedHeaderIndex = self.selectedHeaderIndex
                self.selectedHeaderIndex = indexPath!.row
            }
            if let previouslySelectedHeaderIndex = self.previouslySelectedHeaderIndex {
                self.cells.collapse(previouslySelectedHeaderIndex)
            }
            if self.previouslySelectedHeaderIndex != self.selectedHeaderIndex {
                self.cells.expand(self.selectedHeaderIndex!)
            } else {
                self.selectedHeaderIndex = nil
                self.previouslySelectedHeaderIndex = nil
                headerVw.layer.cornerRadius = 0
                headerVw.layer.cornerRadius = 7;
                
                dropDown.image = UIImage(named: "dropDown")
            }
            self.tableVw.beginUpdates()
            self.tableVw.endUpdates()
        } else {
            if(appointmntSgmnt.selectedSegmentIndex == 0){
                if indexPath.row != self.selectedItemIndex {
                    let vc = storyboard!.instantiateViewControllerWithIdentifier("VideoCallViewController") as! VideoCallViewController
                    let schFrom = value?.valueForKey("From") as! String
                    let schTo = value?.valueForKey("To") as! String
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.timeStyle = .ShortStyle
                    dateFormatter.dateFormat = "k:mm" // k = Hour in 1~24, mm = Minute
                    let timeZone = NSTimeZone(name: "UTC")
                    dateFormatter.timeZone=timeZone
                    let timeTo = dateFormatter.dateFromString("\(schTo)")!
                    let timeFrom = dateFormatter.dateFromString("\(schFrom)")!
                    switch timeTo.compare(timeFrom) {
                    case .OrderedAscending     :
                        print("TimeTo is small than TimeFrom")
                    case .OrderedDescending
                        :   print("timeto is greater than date timefrom")
                        let calendar = NSCalendar.currentCalendar()
                        let datecomponenets = calendar.components(NSCalendarUnit.Minute, fromDate: timeFrom, toDate: timeTo, options: NSCalendarOptions.MatchLast)
                        let min = datecomponenets.minute
                        vc.scheduleMin = "\(min)"
                    case .OrderedSame          :
                        print("Same")
                    }
                    let ptFirstName = value?.valueForKeyPath("PatientId.FirstName") as! NSString
                    let ptLastName = value?.valueForKeyPath("PatientId.LastName") as! NSString
                    vc.name = "\(ptFirstName) \(ptLastName)" as NSString
                    vc.gender = value?.valueForKeyPath("PatientId.Gender") as! String
                    let mob = value?.valueForKeyPath("PatientId.Mobile") as! NSString
                    let code = value?.valueForKeyPath("PatientId.Code") as! NSString
                    let emailId = value?.valueForKeyPath("PatientId.Email") as! NSString
                    var compPh = "+1(\(code))\(mob)"
                    compPh = compPh.insert("-", ind: 10)
                    vc.mobile = compPh
                    vc.email_id = emailId
                    vc.address = value?.valueForKeyPath("PatientId.Address") as! String
                    vc.age = value?.valueForKey("Age") as! String
                    vc.date = value?.valueForKey("ScheduleDate") as! NSString
                    vc.compareDate = value?.valueForKey("Schedule") as! NSString
                    
                    let fromStr = value?.valueForKey("From")  as! NSString
                    let dateFormatter2 = NSDateFormatter()
                    let timeZone2 = NSTimeZone(name: "GMT-8")
                    dateFormatter2.timeZone=timeZone2
                    dateFormatter2.dateFormat = "HH:mm"
                    let date24Hr = dateFormatter2.dateFromString(fromStr as String)!
                    dateFormatter2.dateFormat = "hh:mm a"
                    let date12Hr = dateFormatter2.stringFromDate(date24Hr)
                    vc.time =  date12Hr
                    vc.ptId =  value?.valueForKeyPath("PatientId._id") as! String
                    vc.book_id = value?.valueForKey("BookingId") as! String
                    vc.fromTime =  value?.valueForKey("From") as! NSString
                    vc.profilePicUrl =  value?.valueForKey("profile") as! NSString
                    scheduleAr =  value?.valueForKey("drschedulesetsId") as! NSString
                    vc.schedule_Id = scheduleAr
                    vc.deviceTokn = value?.valueForKeyPath("PatientId.DeviceToken") as! String
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }else{
                if(selectStaff == false){
                    if indexPath.row != self.selectedItemIndex {
                        let vc = storyboard!.instantiateViewControllerWithIdentifier("PateintProfileViewController") as! PateintProfileViewController
                        let status = value?.valueForKey("Status") as! NSInteger
                        if(status == 4){
                            vc.patientNotSub = true
                        }
                        else{
                            vc.patientNotSub = false
                        }
                        vc.pending = true
                        vc.btnInt = 2
                        vc.address = value?.valueForKeyPath("PatientId.Address") as! String
                        vc.age = value?.valueForKey("Age") as! NSString
                        vc.gnder = value?.valueForKeyPath("PatientId.Gender") as! String
                        vc.name = value?.valueForKeyPath("PatientId.FirstName") as! String
                        let mob = value?.valueForKeyPath("PatientId.Mobile") as! NSString
                        let code = value?.valueForKeyPath("PatientId.Code") as! NSString
                        var compPh = "+1(\(code))\(mob)"
                        compPh = compPh.insert("-", ind: 10)
                        vc.phone = compPh
                        vc.ptId =  value?.valueForKeyPath("PatientId._id") as! String
                        vc.book_id = value?.valueForKey("BookingId") as! String
                        scheduleAr =  value?.valueForKey("drschedulesetsId") as! NSString
                        vc.schedule_Id = scheduleAr
                        vc.profilepicUrl =  value?.valueForKey("profile") as! NSString
                        vc.deviceTokn = value?.valueForKeyPath("PatientId.DeviceToken") as! String
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            
        }
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let item = self.cells.items[indexPath.row]
        if(appointmntSgmnt.selectedSegmentIndex == 0){
            if item is SwiftyAccordionCells.HeaderItem {
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    
                    return self.view.frame.size.width*0.16
                }else{
                    if(self.view.frame.size.height > 1365){
                        return self.view.frame.size.width*0.1
                    }else{
                        return self.view.frame.size.width*0.13
                    }
                }
            } else if (item.isHidden) {
                return 0
            } else {
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    return self.view.frame.size.width*0.2
                }else{
                    if(self.view.frame.size.height > 1365){
                        return self.view.frame.size.width*0.13
                    }else{
                        return self.view.frame.size.width*0.16
                    }
                }
                
            }
        }else{
            if item is SwiftyAccordionCells.HeaderItem {
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    
                    return self.view.frame.size.width*0.16
                }else{
                    if(self.view.frame.size.height > 1365){
                        return self.view.frame.size.width*0.1
                    }else{
                        return self.view.frame.size.width*0.13
                    }
                }
            } else if (item.isHidden) {
                return 0
            } else {
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    return self.view.frame.size.width*0.26
                }else{
                    if(self.view.frame.size.height > 1365){
                        return self.view.frame.size.width*0.13
                    }else{
                        return self.view.frame.size.width*0.16
                    }
                }
            }
        }
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension DoctorMyAppointmentsViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
