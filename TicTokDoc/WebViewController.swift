
import UIKit
import SlideMenuControllerSwift
class WebViewController: UIViewController,UIWebViewDelegate,WebServiceDelegate{
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var DoneBtn: UIButton!
    var url = NSURL()
    var ptId = NSString()
    var ptName = NSString()
    var address = NSString()
    var prescribeBool = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = NSUserDefaults.standardUserDefaults()
        if let Url = defaults.objectForKey("url") {
            let urlStr : NSString = Url.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
            let checkURL : NSURL = NSURL(string: urlStr as String)!
        
            let requestObj = NSURLRequest(URL: checkURL)
            webView.loadRequest(requestObj)
            //UIApplication.sharedApplication().openURL(checkURL)
        }
        webView.delegate = self
    }
    func webViewDidStartLoad(webView: UIWebView) {
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
   
    }
    func webViewDidFinishLoad(webView: UIWebView) {
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        let currentURL = (webView.request?.URL)! 
        url = currentURL
    }
    @IBAction func doneBtnAc(sender: AnyObject){
        if(prescribeBool == true){
            var storyboard = UIStoryboard()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                storyboard = UIStoryboard(name: "Main", bundle: nil)
            }else{
                storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
            }
            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorHomeViewController") as! DoctorHomeViewController
            let nav = UINavigationController(rootViewController: vc)
            appDelegate.window?.rootViewController = nav
            appDelegate.window?.makeKeyAndVisible()
        }else{
            let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To Exit??", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
                UIAlertAction in
                NSLog("OK Pressed")
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                let params = "Url=\(self.url)&PatientId=\(self.ptId)"
                AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/url")
            }
            let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        dispatch_async(dispatch_get_main_queue()) {
           // let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var storyboard = UIStoryboard()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                storyboard = UIStoryboard(name: "Main", bundle: nil)
            }else{
                storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
            }
            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorHomeViewController") as! DoctorHomeViewController
            let nav = UINavigationController(rootViewController: vc)
            appDelegate.window?.rootViewController = nav
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
