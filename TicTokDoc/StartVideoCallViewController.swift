

import UIKit
import SlideMenuControllerSwift
let videoWidth : CGFloat = 320
let videoHeight : CGFloat = 240



class StartVideoCallViewController: UIViewController,OTSessionDelegate, OTSubscriberKitDelegate, OTPublisherDelegate,WebServiceDelegate  {
    // *** Fill the following variables using your own Project info  ***
    // ***          https://dashboard.tokbox.com/projects            ***
    // Replace with your OpenTok API key
    var ApiKey = NSString()
    
    // Replace with your generated session ID
    
    
    var SessionID = NSString()
    // Replace with your generated token
    var Token = NSString()
    var url = NSString()
    //used for end call
    var availableID = NSString()
    var doctorId = NSString()
    var patientId = NSString()
    var patName = NSString()
    var ptAdd = NSString()
    var bokId = NSString()
    var schId = NSString()
    var email_id = NSString()
    var deviceToken = NSString()
    var drDetail = NSDictionary()
    var ptSchId = NSString()
    var bookId = NSString()
    var timer = NSTimer()
    // Change to YES to subscribe to your own stream.
    let SubscribeToSelf = false
    var session : OTSession?
    var publisher : OTPublisher?
    var subscriber : OTSubscriber?
    var subscribers: [NSIndexPath: OTSubscriber] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.sharedApplication().idleTimerDisabled = true
        self.view.backgroundColor = UIColor.whiteColor()
        let defaults = NSUserDefaults.standardUserDefaults()
        if let usertype = defaults.valueForKey("usertype") {
            if(usertype as! NSObject == 0){
        let endCallBtn = UIButton.init(frame: CGRectMake(self.view.frame.size.width/2-( self.view.frame.size.height*0.05), self.view.frame.size.height-(self.view.frame.size.width*0.2), self.view.frame.size.height*0.1, self.view.frame.size.height*0.1))
        endCallBtn.setBackgroundImage(UIImage(named: "endcallBtn"), forState: .Normal)
        endCallBtn.addTarget(self, action: #selector(StartVideoCallViewController.homeAc), forControlEvents: .TouchUpInside)
        endCallBtn.tag = 15301
        self.view.addSubview(endCallBtn)
        }
        }
        let viewBase = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - self.view.frame.size.height*0.15))
        self.view.addSubview(viewBase)
        AppManager.sharedManager.showActivityIndicatorInView(viewBase, withLabel: "Please Wait...")
        // Step 1: As the view is loaded initialize a new instance of OTSession
        session = OTSession(apiKey: ApiKey as String, sessionId: SessionID as String, delegate: self)
        
    }
    func homeAc(){
        let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To End The Call?", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default) {
            UIAlertAction in
        }
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            if AppManager.NetWorkReachability.isConnectedToNetwork()==true
            {
                    self.session!.disconnect(nil)
                    let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    appDelegate.window = UIWindow(frame: UIScreen.mainScreen().bounds)
                var storyboard = UIStoryboard()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                }else{
                    storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                }
                let vc = storyboard.instantiateViewControllerWithIdentifier("PatientHomeViewController") as! PatientHomeViewController
                    let nav = UINavigationController(rootViewController: vc)
                    appDelegate.window?.rootViewController = nav
                    appDelegate.window?.makeKeyAndVisible()
             }
            }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    func audioMuteBtnAc(){
         let btn = self.view.viewWithTag(402) as! UIButton
        if (subscriber!.subscribeToAudio == true) {
            subscriber!.subscribeToAudio = false
            btn.setBackgroundImage(UIImage(named: "volBtn"), forState: .Normal)
            btn.selected = true
        } else {
            subscriber!.subscribeToAudio = true;
            btn.setBackgroundImage(UIImage(named: "muteBtn"), forState: .Normal)
            btn.selected = false
        }
    }
    func flipCameraAc(){
        let btn = self.view.viewWithTag(401) as! UIButton
        if(publisher?.cameraPosition == .Back){
           self.publisher!.cameraPosition = .Front
            btn.selected = false
            btn.highlighted = false
        }
        else if (publisher?.cameraPosition == .Front) {
            self.publisher!.cameraPosition = .Back
            btn.selected = true
            btn.highlighted = true
        }
    }
    @IBAction func BackAction(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func viewWillAppear(animated: Bool) {
        // Step 2: As the view comes into the foreground, begin the connection process.
        doConnect()
        self.navigationController?.navigationBarHidden=true
        let defaults = NSUserDefaults.standardUserDefaults()
        if let usertype = defaults.valueForKey("usertype") {
            if(usertype as! NSObject  == 1){
             timer = NSTimer.scheduledTimerWithTimeInterval(50.0, target: self, selector: #selector(StartVideoCallViewController().disconnect), userInfo: nil, repeats: false)
            }
        }
        //self.performSelector(#selector(startVideoCall), withObject: nil, afterDelay: 2.0)
        
    }
    
    
    
    func startVideoCall()
    {
        doConnect()
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    // MARK: - OpenTok Methods
    
    /**
     * Asynchronously begins the session connect process. Some time later, we will
     * expect a delegate method to call us back with the results of this action.
     */
    func doConnect() {
        if let session = self.session {
            var maybeError : OTError?
            session.connectWithToken(Token as String, error: &maybeError)
            if let error = maybeError {
                showAlert(error.localizedDescription)
            }
        }
    }
    /**
     * Sets up an instance of OTPublisher to use with this session. OTPubilsher
     * binds to the device camera and microphone, and will provide A/V streams
     * to the OpenTok session.
     */
    func doPublish() {
        publisher = OTPublisher(delegate: self)
        
        var maybeError : OTError?
        session?.publish(publisher!, error: &maybeError)
        
        if let error = maybeError {
            showAlert(error.localizedDescription)
        }
        //if((self.storyboard?.valueForKey("name"))! as! String == "Main"){
            if(self.view.frame.size.height < 600){
              publisher!.view!.frame = CGRect(x: 0.0, y: (self.view.frame.size.height-135+70)-((self.view.frame.size.width/2)-10), width: (self.view.frame.size.width/2)-10, height: (self.view.frame.size.width/2)-10)
            }
            else if(self.view.frame.size.height > 600 && self.view.frame.size.height < 700){
               publisher!.view!.frame = CGRect(x: 10.0, y: (self.view.frame.size.height-145+70)-((self.view.frame.size.width/2)-10), width: (self.view.frame.size.width/2)-10, height: (self.view.frame.size.width/2)-10)
            }
            else{
              publisher!.view!.frame = CGRect(x: 0.0, y: (self.view.frame.size.height-155+70)-((self.view.frame.size.width/2)-10), width: (self.view.frame.size.width/2)-10, height: (self.view.frame.size.width/2)-10)
            }
        //}
        
        self.view.addSubview(publisher!.view!)
       
//        publisher?.view.bringSubviewToFront(endCallBtn)
//        publisher?.view.bringSubviewToFront(flipCameraBtn)
//        publisher?.view.bringSubviewToFront(audioMuteBtn)
//        publisher!.view.bringSubviewToFront(view)
    }
    
    /**
     * Instantiates a subscriber for the given stream and asynchronously begins the
     * process to begin receiving A/V content for this stream. Unlike doPublish,
     * this method does not add the subscriber to the view hierarchy. Instead, we
     * add the subscriber only after it has connected and begins receiving data.
     */
    func doSubscribe(stream : OTStream) {
        if let session = self.session {
            subscriber = OTSubscriber(stream: stream, delegate: self)
            
            var maybeError : OTError?
            session.subscribe(subscriber!, error: &maybeError)
            if let error = maybeError {
                showAlert(error.localizedDescription)
            }
        }
    }
    /**
     * Cleans the subscriber from the view hierarchy, if any.
     */
    func doUnsubscribe() {
        if let subscriber = self.subscriber {
            var maybeError : OTError?
            session?.unsubscribe(subscriber, error: &maybeError)
            if let error = maybeError {
              // showAlert("Connection Destroyed.")
            }
            let defaults = NSUserDefaults.standardUserDefaults()
            if let usertype = defaults.valueForKey("usertype") {
                if(usertype as! NSObject  == 1){
                    let firstname:String = defaults.valueForKey("firstName") as! String
                    let lastname:String = defaults.valueForKey("lastName") as! String
                    AppManager.sharedManager.delegate=self
                    let dateC = NSDate()
                    // "Jul 23, 2014, 11:01 AM" <-- looks local without seconds. But:
                    let formatter = NSDateFormatter()
                    formatter.dateFormat = "MMMM dd,yyyy"
                    let defaultTimeZoneStr = formatter.stringFromDate(dateC)
                  //  AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                    let params = "BookingId=\(bokId)&drschedulesetsId=\(schId)&DeviceToken=\(deviceToken)&Schedule=\(defaultTimeZoneStr)&PatientEmail=\(email_id)&DrName=\(firstname) \(lastname)&User=1"
                    AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/SchduleOver")
                }else{
                    AppManager.sharedManager.delegate=self
                    let username = defaults.valueForKey("username") as! NSString
                    let dateC = NSDate()
                    // "Jul 23, 2014, 11:01 AM" <-- looks local without seconds. But:
                    let formatter = NSDateFormatter()
                    formatter.dateFormat = "MMMM dd,yyyy"
                    let defaultTimeZoneStr = formatter.stringFromDate(dateC)
                    let firstname = drDetail.valueForKey("FirstName") as! NSString
                    let lastName = drDetail.valueForKey("LastName") as! NSString
                    //AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                    let params = "BookingId=\(bookId)&drschedulesetsId=\(ptSchId)&DeviceToken=\(deviceToken)&Schedule=\(defaultTimeZoneStr)&PatientEmail=\(username)&DrName=\(firstname) \(lastName)&User=0"
                    AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/SchduleOver")
                }
            }

            subscriber.view!.removeFromSuperview()
            self.subscriber = nil
        }
    }
    
    // MARK: - OTSession delegate callbacks
    
    func sessionDidConnect(session: OTSession) {
        NSLog("sessionDidConnect (\(session.sessionId))")
//        let defaults = NSUserDefaults.standardUserDefaults()
//        if let usertype = defaults.valueForKey("usertype") {
//            if(usertype as! NSObject  == 0){
        
//            }
  //      }
        // Step 2: We have successfully connected, now instantiate a publisher and
        // begin pushing A/V streams into OpenTok.
        doPublish()
    }
    
    func sessionDidDisconnect(session : OTSession) {
        NSLog("Session disconnected (\( session.sessionId))")
    }
    
    func session(session: OTSession, streamCreated stream: OTStream) {
        NSLog("session streamCreated (\(stream.streamId))")
        // Step 3a: (if NO == subscribeToSelf): Begin subscribing to a stream we
        // have seen on the OpenTok session.
        if subscriber == nil && !SubscribeToSelf {
            doSubscribe(stream)
        }
        
    }
    
    func session(session: OTSession, streamDestroyed stream: OTStream) {
        NSLog("session streamCreated (\(stream.streamId))")
        if (subscriber?.stream!.streamId)! == stream.streamId {
            doUnsubscribe()
        }
    }
    
    func session(session: OTSession, connectionCreated connection : OTConnection) {
        timer.invalidate()
        NSLog("session connectionCreated (\(connection.connectionId))")
    }
    
    func session(session: OTSession, connectionDestroyed connection : OTConnection) {
        NSLog("session connectionDestroyed (\(connection.connectionId))")
        
    }
    
    func session(session: OTSession, didFailWithError error: OTError) {
        NSLog("session didFailWithError (%@)", error)
    }
    
    // MARK: - OTSubscriber delegate callbacks
    
    func subscriberDidConnectToStream(subscriberKit: OTSubscriberKit) {
        NSLog("subscriberDidConnectToStream (\(subscriberKit))")
        if let view = subscriber?.view {
            //view.frame =  CGRect(x: 0.0, y: videoHeight, width: videoWidth, height: videoHeight)
          //  if((self.storyboard?.valueForKey("name"))! as! String == "Main"){
                if(self.view.frame.size.height < 600){
                     view.frame =  CGRect(x: 0.0, y:0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                }
                else if(self.view.frame.size.height > 600 && self.view.frame.size.height < 700){
                     view.frame =  CGRect(x: 0.0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                }
                else{
                     view.frame =  CGRect(x: 0.0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                }
           // }
            self.view.addSubview(view)
            self.view.addSubview(publisher!.view!)
            let defaults = NSUserDefaults.standardUserDefaults()
            if let usertype = defaults.valueForKey("usertype") {
            if(usertype as! NSObject == 0){
            let btn = self.view.viewWithTag(15301) as! UIButton
            btn.removeFromSuperview()
             }
            }
            let endCallBtn = UIButton.init(frame: CGRectMake(self.view.frame.size.width/2-( self.view.frame.size.height*0.05), self.view.frame.size.height-(self.view.frame.size.width*0.2), self.view.frame.size.height*0.1, self.view.frame.size.height*0.1))
            endCallBtn.setBackgroundImage(UIImage(named: "endcallBtn"), forState: .Normal)
            endCallBtn.addTarget(self, action: #selector(StartVideoCallViewController.DisconnectAction), forControlEvents: .TouchUpInside)
            self.view.addSubview(endCallBtn)
            
            let flipCameraBtn = UIButton.init(frame: CGRectMake(self.view.frame.size.width/2+( self.view.frame.size.width*0.16), self.view.frame.size.height-(self.view.frame.size.width*0.2), self.view.frame.size.height*0.1, self.view.frame.size.height*0.1))
            flipCameraBtn.tag = 401
            flipCameraBtn.setBackgroundImage(UIImage(named: "flipCameraBtn"), forState: .Normal)
            flipCameraBtn.addTarget(self, action: #selector(StartVideoCallViewController.flipCameraAc), forControlEvents: .TouchUpInside)
            self.view.addSubview(flipCameraBtn)
            
            let audioMuteBtn = UIButton.init(frame: CGRectMake(self.view.frame.size.width/2-(self.view.frame.size.width*0.325), self.view.frame.size.height-(self.view.frame.size.width*0.2), self.view.frame.size.height*0.1, self.view.frame.size.height*0.1))
            audioMuteBtn.tag = 402
            audioMuteBtn.setBackgroundImage(UIImage(named: "muteBtn"), forState: .Normal)
            audioMuteBtn.addTarget(self, action: #selector(StartVideoCallViewController.audioMuteBtnAc), forControlEvents: .TouchUpInside)
            self.view.addSubview(audioMuteBtn)
        }
    }
    func subscriber(subscriber: OTSubscriberKit, didFailWithError error : OTError) {
        NSLog("subscriber %@ didFailWithError %@", subscriber.stream!.streamId, error)
    }
    // MARK: - OTPublisher delegate callbacks
    func publisher(publisher: OTPublisherKit, streamCreated stream: OTStream) {
        NSLog("publisher streamCreated %@", stream)
        
        // Step 3b: (if YES == subscribeToSelf): Our own publisher is now visible to
        // all participants in the OpenTok session. We will attempt to subscribe to
        // our own stream. Expect to see a slight delay in the subscriber video and
        // an echo of the audio coming from the device microphone.
        if subscriber == nil && SubscribeToSelf {
            doSubscribe(stream)
        }
//        else{

    }
    func disconnect(){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        self.sessionDidDisconnect(session!)
        let alert = UIAlertController(title: "Alert", message: "Patient Did Not Picked The Call.Please Try After Sometime.", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default) {
            UIAlertAction in
             self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
        }
        let okAction = UIAlertAction(title: "Call Again", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Please Wait...")
            // Step 1: As the view is loaded initialize a new instance of OTSession
            let defaults = NSUserDefaults.standardUserDefaults()
            if let usertype = defaults.valueForKey("usertype") {
                if(usertype as! NSObject  == 1){
                    self.timer = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: #selector(StartVideoCallViewController().disconnect), userInfo: nil, repeats: false)
                }
            }
            self.session = OTSession(apiKey: self.ApiKey as String, sessionId: self.SessionID as String, delegate: self)
            self.doConnect()
        }
       // alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    func publisher(publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
        NSLog("publisher streamDestroyed %@", stream)
        if (subscriber?.stream!.streamId)! == stream.streamId {
            doUnsubscribe()
        }
    }
    
    func publisher(publisher: OTPublisherKit, didFailWithError error: OTError) {
        NSLog("publisher didFailWithError %@", error)
    }
    
    // MARK: - Helpers
    
    func showAlert(message: String) {
        // show alertview on main UI
        dispatch_async(dispatch_get_main_queue()) {
            let alertController = UIAlertController(title: "Oops",
                                                    message:message, preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
                NSLog("OK Pressed")

                 }
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
      }
    func DisconnectAction()
    {
        let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To End The Call?", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default) {
            UIAlertAction in
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            if AppManager.NetWorkReachability.isConnectedToNetwork()==true
            {
                if  self.session!.sessionConnectionStatus == .Connected {
                    self.session!.disconnect(nil)
                    let defaults = NSUserDefaults.standardUserDefaults()
                    if let usertype = defaults.valueForKey("usertype") {
                        if(usertype as! NSObject  == 1){
                            let firstname:String = defaults.valueForKey("firstName") as! String
                            let lastname:String = defaults.valueForKey("lastName") as! String
                            AppManager.sharedManager.delegate=self
                            let dateC = NSDate()
                            // "Jul 23, 2014, 11:01 AM" <-- looks local without seconds. But:
                            let formatter = NSDateFormatter()
                            formatter.dateFormat = "MMMM dd,yyyy"
                            let defaultTimeZoneStr = formatter.stringFromDate(dateC)
                            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                            let params = "BookingId=\(self.bokId)&drschedulesetsId=\(self.schId)&DeviceToken=\(self.deviceToken)&Schedule=\(defaultTimeZoneStr)&PatientEmail=\(self.email_id)&DrName=\(firstname) \(lastname)&User=1"
                            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/SchduleOver")
                        }else{
                            AppManager.sharedManager.delegate=self
                            let username = defaults.valueForKey("username") as! NSString
                            let dateC = NSDate()
                            // "Jul 23, 2014, 11:01 AM" <-- looks local without seconds. But:
                            let formatter = NSDateFormatter()
                            formatter.dateFormat = "MMMM dd,yyyy"
                            let defaultTimeZoneStr = formatter.stringFromDate(dateC)
                            let firstname = self.drDetail.valueForKey("FirstName") as! NSString
                            let lastName = self.drDetail.valueForKey("LastName") as! NSString
                            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                            let params = "BookingId=\(self.bookId)&drschedulesetsId=\(self.ptSchId)&DeviceToken=\(self.deviceToken)&Schedule=\(defaultTimeZoneStr)&PatientEmail=\(username)&DrName=\(firstname) \(lastName)&User=0"
                            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/SchduleOver")
                        }
                    }
                }
                else {
                    //all other cases just go back to home screen.
                    if self.navigationController!.viewControllers.indexOf(self) != NSNotFound {
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                }
            }
            else
            {
                AppManager.sharedManager.Showalert("No Internet Connection", alertmessage:  "Make sure your device is connected to the internet.")
            }
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    //MARK:webservice delegate
    
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        let defaults = NSUserDefaults.standardUserDefaults()
        if let usertype = defaults.valueForKey("usertype") {
         if(usertype as! NSObject == 0){
            dispatch_async(dispatch_get_main_queue()) {
                var storyboard = UIStoryboard()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                }else{
                    storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                }
                let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorRatingViewController") as! DoctorRatingViewController
                  vc.drDetailDict = self.drDetail
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        dispatch_async(dispatch_get_main_queue()) {
                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                backVw.tag = 2369
                self.view.addSubview(backVw)
                
                let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21))
                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                alerVw.backgroundColor = UIColor.whiteColor()
                backVw.addSubview(alerVw)
                
                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                rightImg.image = UIImage(named: "verifyImg")
                alerVw.addSubview(rightImg)
                
                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                verificationLbl.text = " Alert"
                verificationLbl.font = UIFont.init(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(verificationLbl)
                
                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                lineVw.backgroundColor = UIColor.grayColor()
                alerVw.addSubview(lineVw)
                
                let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
                contentLbl.font = UIFont.init(name: "Helvetica", size:14)
                contentLbl.textAlignment = .Center
                contentLbl.textColor = UIColor.grayColor()
                contentLbl.text = "Do you need to write a prescription for your Patient?"
                contentLbl.numberOfLines = 2
                alerVw.addSubview(contentLbl)
                
                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width/2, alerVw.frame.size.height*0.22))
                okLbl.setTitle("Yes", forState: .Normal)
                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                okLbl.titleLabel?.textAlignment = .Center
                okLbl.addTarget(self, action: #selector(StartVideoCallViewController.okBtnAc), forControlEvents: .TouchUpInside)
                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
             alerVw.addSubview(okLbl)
            let lineLbl = UILabel.init(frame: CGRectMake(okLbl.frame.size.width-2,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),1.2, alerVw.frame.size.height*0.22))
            lineLbl.backgroundColor = UIColor.lightGrayColor()
            alerVw.addSubview(lineLbl)
            
                let noLbl = UIButton.init(frame: CGRectMake(okLbl.frame.size.width,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width/2, alerVw.frame.size.height*0.22))
                noLbl.setTitle("No", forState: .Normal)
                noLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                noLbl.titleLabel?.textAlignment = .Center
                noLbl.addTarget(self, action: #selector(StartVideoCallViewController.noAc), forControlEvents: .TouchUpInside)
                noLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(noLbl)
            
            
          }
        }
      }
    }
    func okBtnAc(){
        let vc = storyboard!.instantiateViewControllerWithIdentifier("WebViewController") as! WebViewController
        vc.ptId = self.patientId
        vc.address = self.ptAdd
        vc.ptName = self.patName
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func noAc(){
        self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
    }

    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("Alert", alertmessage: failureError.localizedDescription)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
