
import UIKit
import SlideMenuControllerSwift
import IQKeyboardManagerSwift
class DoctorLogInViewController: UIViewController,UITextFieldDelegate,WebServiceDelegate,GIDSignInUIDelegate {
    @IBOutlet weak var btnLoginPswrd: UIButton!
    @IBOutlet weak var pswrdVw: UIView!
    @IBOutlet weak var usernameVw: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var textFieldLoginPswrd: UITextField!
    @IBOutlet weak var usernameFld: UITextField!
    @IBOutlet weak var rememberMeCheckBox: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var agreeDisagreeVw: UIView!
    @IBOutlet weak var agreeBtn: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var fbLoginBtn: UIButton!
    var serverWho = NSInteger()
    var textFieldSecureEntry = Bool()
    var docPatLogin = Bool()
    var firstTime = Bool()
    var snd = Bool()
    var activation = Bool()
    var responceDiction = NSMutableDictionary()
    var userId = NSString()
    var serverInt = NSInteger()
    var loginResponce = NSDictionary()
    var UserType = NSInteger()
    override func viewDidLoad(){
        super.viewDidLoad()
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().enable = true
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
        usernameFld.tintColor = UIColor.whiteColor()
        textFieldLoginPswrd.tintColor = UIColor.whiteColor()
        agreeDisagreeVw.hidden = true
        agreeDisagreeVw.layer.cornerRadius = self.agreeDisagreeVw.frame.size.height*0.04
        agreeDisagreeVw.layer.borderWidth = self.view.frame.size.height*0.0012
        agreeDisagreeVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        agreeDisagreeVw.clipsToBounds = true
        self.view.backgroundColor =  UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        agreeBtn.layer.cornerRadius = self.agreeBtn.frame.size.height*0.09
        agreeBtn.layer.borderWidth = self.view.frame.size.height*0.001
        agreeBtn.layer.borderColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1).CGColor
        agreeBtn.clipsToBounds = true
        
        usernameVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        usernameVw.layer.borderColor = UIColor.init(red: 45/255, green: 78/255, blue: 107/255, alpha: 1).CGColor
        usernameVw.backgroundColor = UIColor.init(red: 45/255, green: 78/255, blue: 107/255, alpha: 1)
        usernameVw.layer.masksToBounds = false;
        usernameVw.layer.borderWidth = 0.5
        usernameFld.autocapitalizationType = .Words
        pswrdVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        pswrdVw.layer.borderColor = UIColor.init(red: 45/255, green: 78/255, blue: 107/255, alpha: 1).CGColor
        pswrdVw.backgroundColor = UIColor.init(red: 45/255, green: 78/255, blue: 107/255, alpha: 1)
        pswrdVw.layer.masksToBounds = false;
        pswrdVw.layer.borderWidth = 0.5
       
        let defaults = NSUserDefaults.standardUserDefaults()
        if(docPatLogin == false){
            if let username = defaults.valueForKey("username") {
                if let pasword = defaults.valueForKey("password") {
                    textFieldLoginPswrd.text = pasword as? String
                    loginBtn.enabled = true
                    usernameFld.text = username as? String
                    rememberMeCheckBox.setImage(UIImage(named: "checked"), forState: .Normal)
                }
                else{
                    loginBtn.enabled = false
                    textFieldLoginPswrd.text = ""
                    usernameFld.text = ""
                    rememberMeCheckBox.setImage(UIImage(named: "emptyCheckBox"), forState: .Normal)
                }
            }else{
                loginBtn.enabled = false
                textFieldLoginPswrd.text = ""
                usernameFld.text = ""
                rememberMeCheckBox.setImage(UIImage(named: "emptyCheckBox"), forState: .Normal)
            }
        }else{
            if let username = defaults.valueForKey("usernameDoc") {
                let pasword = defaults.valueForKey("passwordDoc") as! String
                textFieldLoginPswrd.text = pasword
                loginBtn.enabled = true
                usernameFld.text = username as? String
                rememberMeCheckBox.setImage(UIImage(named: "emptyCheckBox"), forState: .Normal)
            }else{
                loginBtn.enabled = false
                textFieldLoginPswrd.text = ""
                usernameFld.text = ""
                rememberMeCheckBox.setImage(UIImage(named: "checked"), forState: .Normal)
            }
        }
        textFieldSecureEntry = true
        if(snd == true){
            backBtn.removeFromSuperview()
        }
        textFieldLoginPswrd.delegate = self
        usernameFld.delegate = self
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DoctorLogInViewController.apiHitGoogle), name: "signUpStatus", object: nil)
    }
    
    override func viewWillAppear(animated: Bool){
        serverWho = 0
        agreeDisagreeVw.hidden = true
        let defaults = NSUserDefaults.standardUserDefaults()
        if(docPatLogin == false){
            if let username = defaults.valueForKey("username") {
                if let pasword = defaults.valueForKey("password") {
                    textFieldLoginPswrd.text = pasword as? String
                    loginBtn.enabled = true
                    usernameFld.text = username as? String
                    rememberMeCheckBox.setImage(UIImage(named: "checked"), forState: .Normal)
                }
                else{
                    loginBtn.enabled = false
                    textFieldLoginPswrd.text = ""
                    usernameFld.text = ""
                    rememberMeCheckBox.setImage(UIImage(named: "emptyCheckBox"), forState: .Normal)
                }
            }else{
                loginBtn.enabled = false
                textFieldLoginPswrd.text = ""
                usernameFld.text = ""
                rememberMeCheckBox.setImage(UIImage(named: "emptyCheckBox"), forState: .Normal)
            }
        }
        else{
            if let username = defaults.valueForKey("usernameDoc") {
                let pasword = defaults.valueForKey("passwordDoc") as! String
                textFieldLoginPswrd.text = pasword
                loginBtn.enabled = true
                usernameFld.text = username as? String
                rememberMeCheckBox.setImage(UIImage(named: "checked"), forState: .Normal)
            }else{
                loginBtn.enabled = false
                textFieldLoginPswrd.text = ""
                usernameFld.text = ""
                rememberMeCheckBox.setImage(UIImage(named: "emptyCheckBox"), forState: .Normal)
            }
        }
    }
    
    //MARK:- LoginThroughFB
    @IBAction func fbLoginAction(sender: AnyObject) {
        NSUserDefaults.standardUserDefaults().setValue("facebook", forKey: "mediaType")
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        
        fbLoginManager.logInWithReadPermissions(["email"], fromViewController: self, handler: {(result, error) -> Void in
            if error == nil {
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading")
                self.getFBUserData()
                fbLoginManager.logOut()
            }
            else {
                print("Facebook Login Error----\n",error)
            }
            }
        )
    }
    func getFBUserData () {
        if((FBSDKAccessToken.currentAccessToken()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil){
                    let defaults = NSUserDefaults.standardUserDefaults()
                    var token =  NSString()
                    if(UIDevice.isSimulator == true){
                        token = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
                    }else{
                        token = defaults.valueForKey("deviceToken") as! NSString
                    }
                    let id = result.valueForKey("id") as! String
                    let email = result.valueForKey("email") as! String
                    if(self.docPatLogin == true){
                        self.doRequestPost("\(Header.BASE_URL)users/Login1", data: ["DeviceToken":"\(token)","DeviceType":"ios","LoginType":"F","LoginKey":"\(id)","Email":"\(email)","UserType":"1"])
                        self.serverWho = 0
                    }else{
                        self.doRequestPost("\(Header.BASE_URL)users/Login1", data: ["DeviceToken":"\(token)","DeviceType":"ios","LoginType":"F","LoginKey":"\(id)","Email":"\(email)","UserType":"0"])
                        self.serverWho = 0
                    }
                    NSUserDefaults.standardUserDefaults().setValue(result.valueForKey("first_name"), forKey: "firstName")
                    NSUserDefaults.standardUserDefaults().setValue(result.valueForKey("last_name"), forKey: "lastName")
                    NSUserDefaults.standardUserDefaults().setValue(result.valueForKey("email"), forKey: "email")
                    NSUserDefaults.standardUserDefaults().setValue(result.valueForKey("id"), forKey: "fbId")
                    let url = result.valueForKeyPath("picture.data.url") as! NSString
                    let imageUrl = NSURL(string: url as String)
                    NSUserDefaults.standardUserDefaults().setURL(imageUrl, forKey: "imageUrl")
                    NSUserDefaults.standardUserDefaults().setValue("", forKey: "googleId")
                }
            })
        }else{
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        }
    }
    
    //MARK:- LoginThroughGoogle
    func apiHitGoogle(){
        let defaults = NSUserDefaults.standardUserDefaults()
        let id = defaults.valueForKey("googleId") as! String
        let email = defaults.valueForKey("email") as! String
        var token =  NSString()
        if(UIDevice.isSimulator == true){
            token = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
        }else{
            token = defaults.valueForKey("deviceToken") as! NSString
        }
        if(docPatLogin == true){
            self.doRequestPost("\(Header.BASE_URL)users/Login1", data: ["DeviceToken":"\(token)","DeviceType":"ios","LoginType":"G","LoginKey":"\(id)","Email":"\(email)","UserType":"1"])
            self.serverWho = 0
        }
        else{
            self.doRequestPost("\(Header.BASE_URL)users/Login1", data: ["DeviceToken":"\(token)","DeviceType":"ios","LoginType":"G","LoginKey":"\(id)","Email":"\(email)","UserType":"0"])
            self.serverWho = 0
        }
        
    }
    
    @IBAction func googleSignIn(sender: AnyObject) {
        NSUserDefaults.standardUserDefaults().setValue("google", forKey: "mediaType")
        GIDSignIn.sharedInstance().signIn()
    }
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        //myActivityIndicator.stopAnimating()
    }
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
        self.presentViewController(viewController, animated: true, completion: nil)
    }
    func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
        self.dismissViewControllerAnimated(true, completion: nil)
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
    }
    
    //MARK:- SignupThroughSocialMedia
    func signUp(){
        if(docPatLogin == true){
            let vc = storyboard!.instantiateViewControllerWithIdentifier("CreateAccountViewController") as! CreateAccountViewController
            vc.firstName = NSUserDefaults.standardUserDefaults().valueForKey("firstName") as! String
            vc.emailId = NSUserDefaults.standardUserDefaults().valueForKey("email") as! String
            vc.lastName = NSUserDefaults.standardUserDefaults().valueForKey("lastName") as! String
            vc.fbId = NSUserDefaults.standardUserDefaults().valueForKey("fbId") as! String
            vc.googleId = NSUserDefaults.standardUserDefaults().valueForKey("googleId") as! String
            vc.urlImage = NSUserDefaults.standardUserDefaults().URLForKey("imageUrl")!
            vc.socialMedia = true
            self.navigationController?.pushViewController(vc, animated: true)
            vc.createAc = true
        }else{
            let vc = storyboard!.instantiateViewControllerWithIdentifier("PateintCreateAccountViewController") as! PateintCreateAccountViewController
            vc.firstName = NSUserDefaults.standardUserDefaults().valueForKey("firstName") as! String
            vc.emailId = NSUserDefaults.standardUserDefaults().valueForKey("email") as! String
            vc.lastName = NSUserDefaults.standardUserDefaults().valueForKey("lastName") as! String
            vc.fbId = NSUserDefaults.standardUserDefaults().valueForKey("fbId") as! String
            vc.googleId = NSUserDefaults.standardUserDefaults().valueForKey("googleId") as! String
            vc.urlImage = NSUserDefaults.standardUserDefaults().URLForKey("imageUrl")!
            vc.socialMedia = true
            self.navigationController?.pushViewController(vc, animated: true)
            vc.createAc = true
        }
    }
    
    //MARK:- ServerResponce
    func doRequestPost(url:String,data:[String: NSObject]){
        let requestDic = data
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
            let _: NSError?
            let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                if(self.serverWho == 1){
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Email has been sent successfully.")
                }else if(self.serverWho == 2){
                    
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        if let dic:String = responseDict.valueForKey("error") as? String {
                            if (dic == "Password wrong"){
                                dispatch_async(dispatch_get_main_queue()) {
                                    AppManager.sharedManager.Showalert("Alert", alertmessage:"Incorrect Password")
                                }
                            }
                            else if (dic == "No Any Email"){
                                dispatch_async(dispatch_get_main_queue()) {
                                    self.signUp()
                                }
                            }
                            else if(dic == "Inactive User"){
                                dispatch_async(dispatch_get_main_queue()) {
                                    AppManager.sharedManager.Showalert("Alert", alertmessage:"Please verify your email address before logging into Health4Life!")
                                }
                            }
                            else if(dic == "Admin Inactive User"){
                                dispatch_async(dispatch_get_main_queue()) {
                                    AppManager.sharedManager.Showalert("Alert", alertmessage:"Health4Life has not yet verified your account. Please call them at 702.626.8200 to expedite this process.")
                                }
                            }
                            else if(dic == "All ready email another account"){
                                AppManager.sharedManager.Showalert("Alert", alertmessage: "Email id already exist with another account.")
                            }
                            else{
                                dispatch_async(dispatch_get_main_queue()) {
                                    self.signUp()
                                }
                            }
                        }else if (responseDict.valueForKey("status") as! NSInteger  == 200){
                            let dict = responseDict.valueForKey("data") as! NSDictionary
                            self.loginResponce = dict
                            let alreadylogin = dict["LoginAllready"] as! NSInteger
                            if(alreadylogin == 0){
                                dispatch_async(dispatch_get_main_queue()) {
                                    self.responceDiction = responseDict as! NSMutableDictionary
                                    self.agreeDisagreeVw.hidden = false
                                    let crossBtn = UIButton()
                                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                                        crossBtn.frame = CGRectMake(self.agreeDisagreeVw.frame.size.width-(self.agreeDisagreeVw.frame.size.width*0.12),0, self.agreeDisagreeVw.frame.size.width*0.12,self.agreeDisagreeVw.frame.size.width*0.12)
                                    }
                                    else{
                                        crossBtn.frame = CGRectMake(self.agreeDisagreeVw.frame.size.width-(self.agreeDisagreeVw.frame.size.width*0.1),0, self.agreeDisagreeVw.frame.size.width*0.1,self.agreeDisagreeVw.frame.size.width*0.1)
                                    }
                                    
                                    crossBtn.setBackgroundImage(UIImage(named: "crossBtn"), forState: .Normal)
                                    crossBtn.addTarget(self, action: #selector(DoctorLogInViewController.crossBtnAc), forControlEvents: .TouchUpInside)
                                    crossBtn.tag = 4
                                    self.agreeDisagreeVw.addSubview(crossBtn)
                                    let id:String = responseDict.valueForKeyPath("data._id") as! String
                                    self.userId = id
                                }
                            }
                            else{
                                let usertype:Int = dict["UserType"]  as! NSInteger
                                let id:String = responseDict.valueForKeyPath("data._id") as! String
                                self.doRequestPost("\(Header.BASE_URL)messageChat/sessionChange", data: ["UserId":"\(id)"])
                                self.serverWho = 2
                                
                                let defaults = NSUserDefaults.standardUserDefaults()
                                defaults.setValue(id, forKey: "id")
                                defaults.setValue(usertype, forKey: "usertype")
                                let pic = dict["ProfilePic"] as! String
                                if(usertype == 1 && self.docPatLogin == true ){
                                    dispatch_async(dispatch_get_main_queue()) {
                                        let firstName: String = dict["FirstName"] as! String
                                        let lastName:String = dict["LastName"] as! String
                                        let specialityTxt:String = dict["Speciality"] as! String
                                        let qul:String = dict["Qualification"] as! String
                                        defaults.setObject(pic, forKey: "profilePic")
                                        defaults.setObject(firstName, forKey: "firstName")
                                        defaults.setObject(lastName, forKey: "lastName")
                                        defaults.setObject(specialityTxt, forKey: "Speciality")
                                        defaults.setObject(qul, forKey: "qualification")
                                        if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginSelectcheckBox")){
                                            defaults.setObject(self.usernameFld.text!, forKey: "usernameDoc")
                                            defaults.setObject(self.textFieldLoginPswrd.text!, forKey: "passwordDoc")
                                        }else if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginCheckBox")){
                                            defaults.removeObjectForKey("usernameDoc")
                                            defaults.removeObjectForKey("passwordDoc")
                                        }
                                        defaults.setObject(responseDict.valueForKeyPath("data.UserType"), forKey: "usertype")
                                        var storyboard = UIStoryboard()
                                        switch UIDevice.currentDevice().userInterfaceIdiom {
                                        case .Phone:
                                            storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        case .Pad:
                                            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                                        default:
                                            break
                                        }
                                        let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorHomeViewController") as! DoctorHomeViewController
                                        let drType:String = dict["DrType"] as! String
                                        defaults.setValue(drType, forKey: "drType")
                                        if(drType == "MedicinePrescribers"){
                                            vc.tableVwRow = 4
                                        }else{
                                            vc.tableVwRow = 3
                                        }
                                        self.navigationController!.pushViewController(vc, animated: true)
                                    }
                                }
                                    
                                else if(usertype == 0 && self.docPatLogin == false){
                                    let firstName: String = dict["FirstName"] as! String
                                    let lastName:String = dict["LastName"] as! String
                                    let address:String = dict["Address"] as! String
                                    defaults.setObject(firstName, forKey: "firstName")
                                    defaults.setObject(lastName, forKey: "lastName")
                                    defaults.setObject(address, forKey: "address")
                                    defaults.setObject(pic, forKey: "profilePic")
                                    defaults.setObject(self.usernameFld.text!, forKey: "username")
                                    let reqInt = responseDict.valueForKeyPath("data.DrRequest") as! NSInteger
                                    let pharma = responseDict.valueForKeyPath("data.PharmacyName") as! NSString
                                    if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginSelectcheckBox")){
                                        defaults.setObject(self.textFieldLoginPswrd.text!, forKey: "password")
                                    }else if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginCheckBox")){
                                        defaults.removeObjectForKey("password")
                                    }
                                    var storyboard = UIStoryboard()
                                    switch UIDevice.currentDevice().userInterfaceIdiom {
                                    case .Phone:
                                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    case .Pad:
                                        storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                                    default:
                                        break
                                    }
                                    let lat:String = responseDict.valueForKeyPath("data.Lat") as! String
                                    defaults.setObject(lat, forKey: "lat")
                                    let long:String = responseDict.valueForKeyPath("data.Long") as! String
                                    defaults.setObject(long, forKey: "long")
                                    if(reqInt == 0){
                                        dispatch_async(dispatch_get_main_queue()) {
                                            let vc     = storyboard.instantiateViewControllerWithIdentifier("SearchDoctorViewController") as! SearchDoctorViewController
                                            vc.searchDoc   = true
                                            vc.firstSearch = true
                                            self.navigationController!.pushViewController(vc, animated: true)
                                        }
                                    }
                                    else if(pharma == ""){
                                        dispatch_async(dispatch_get_main_queue()) {
                                            defaults.setBool(true, forKey: "searchDoc")
                                            let vc = storyboard.instantiateViewControllerWithIdentifier("SelectPharmacyViewController") as! SelectPharmacyViewController
                                            vc.nextBool = true
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    }
                                    else{
                                        defaults.setBool(true, forKey: "searchDoc")
                                        defaults.setBool(true, forKey: "selectPharma")
                                        dispatch_async(dispatch_get_main_queue()) {
                                            let vc     = storyboard.instantiateViewControllerWithIdentifier("PatientHomeViewController") as! PatientHomeViewController
                                            self.navigationController!.pushViewController(vc, animated: true)
                                        }
                                    }
                                }
                                    
                                else{
                                    dispatch_async(dispatch_get_main_queue()) {
                                        AppManager.sharedManager.Showalert("Alert", alertmessage:"User Not Found")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        dataTask.resume()
    }
    
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString)
    {
        if(serverInt == 1){
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            if let dic:String = responseDict.valueForKey("error") as? String {
                if (dic == "Password wrong"){
                    dispatch_async(dispatch_get_main_queue()) {
                        AppManager.sharedManager.Showalert("Alert", alertmessage:"Incorrect Password")
                    }
                }
                else if (dic == "No Any Email"){
                    dispatch_async(dispatch_get_main_queue()) {
                        AppManager.sharedManager.Showalert("Alert", alertmessage:"Email id does not exist")
                    }
                }
                else if(dic == "Inactive User"){
                    dispatch_async(dispatch_get_main_queue()) {
                        let alert = UIAlertController(title: "Alert", message: "Please verify your email address before logging into Health4Life!", preferredStyle: UIAlertControllerStyle.Alert)
                        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default) {
                            UIAlertAction in
                        }
                        let okAction = UIAlertAction(title: "Resend Email", style: UIAlertActionStyle.Cancel) {
                            UIAlertAction in
                            let emailLowerCaseString:String = (self.usernameFld.text?.lowercaseString)!
                            self.serverWho = 1
                            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                            self.doRequestPost("\(Header.BASE_URL)users/resendmail", data: ["Email":"\(emailLowerCaseString)"])
                        }
                        alert.addAction(okAction)
                        alert.addAction(cancelAction)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
                else if(dic == "Admin Inactive User"){
                    dispatch_async(dispatch_get_main_queue()) {
                        AppManager.sharedManager.Showalert("Alert", alertmessage:"Health4Life has not yet verified your account. Please call them at 702.626.8200 to expedite this process.")
                    }
                }
                else if(dic == "All ready email another account"){
                    
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Email id already exist with another account.")
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        AppManager.sharedManager.Showalert("Alert", alertmessage:"Email id does not exist")
                    }
                }
            }else if let dict = responseDict["data"]{
                loginResponce = dict as! NSDictionary
                let alreadylogin = dict["LoginAllready"] as! NSInteger
                if(alreadylogin == 0){
                    dispatch_async(dispatch_get_main_queue()){
                        self.responceDiction = responseDict
                        self.agreeDisagreeVw.hidden = false
                        let crossBtn = UIButton()
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            crossBtn.frame = CGRectMake(self.agreeDisagreeVw.frame.size.width-(self.agreeDisagreeVw.frame.size.width*0.12),0, self.agreeDisagreeVw.frame.size.width*0.12,self.agreeDisagreeVw.frame.size.width*0.12)
                        }
                        else{
                            crossBtn.frame = CGRectMake(self.agreeDisagreeVw.frame.size.width-(self.agreeDisagreeVw.frame.size.width*0.1),0, self.agreeDisagreeVw.frame.size.width*0.1,self.agreeDisagreeVw.frame.size.width*0.1)
                        }
                        
                        crossBtn.setBackgroundImage(UIImage(named: "crossBtn"), forState: .Normal)
                        crossBtn.addTarget(self, action: #selector(DoctorLogInViewController.crossBtnAc), forControlEvents: .TouchUpInside)
                        crossBtn.tag = 4
                        self.agreeDisagreeVw.addSubview(crossBtn)
                        let id:String = responseDict.valueForKeyPath("data._id") as! String
                        self.userId = id
                    }
                }
                else{
                    let usertype:Int = dict["UserType"]  as! NSInteger
                    let id:String = responseDict.valueForKeyPath("data._id") as! String
                    self.doRequestPost("\(Header.BASE_URL)messageChat/sessionChange", data: ["UserId":"\(id)"])
                    serverWho = 2
                    let defaults = NSUserDefaults.standardUserDefaults()
                    defaults.setValue(id, forKey: "id")
                    defaults.setValue(usertype, forKey: "usertype")
                    defaults.setInteger(dict["docShow"] as! NSInteger, forKey: "msgCount")
                    let pic = dict["ProfilePic"] as! String
                    if(usertype == 1 && docPatLogin == true){
                        dispatch_async(dispatch_get_main_queue()) {
                            let firstName: String = dict["FirstName"] as! String
                            let lastName:String = dict["LastName"] as! String
                            let specialityTxt:String = dict["Speciality"] as! String
                            let qul:String = dict["Qualification"] as! String
                            defaults.setObject(pic, forKey: "profilePic")
                            defaults.setObject(firstName, forKey: "firstName")
                            defaults.setObject(lastName, forKey: "lastName")
                            defaults.setObject(specialityTxt, forKey: "Speciality")
                            defaults.setObject(qul, forKey: "qualification")
                            if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginSelectcheckBox")){
                                defaults.setObject(self.usernameFld.text!, forKey: "usernameDoc")
                                defaults.setObject(self.textFieldLoginPswrd.text!, forKey: "passwordDoc")
                            }else if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginCheckBox")){
                                defaults.removeObjectForKey("usernameDoc")
                                defaults.removeObjectForKey("passwordDoc")
                            }
                            defaults.setObject(responseDict.valueForKeyPath("data.UserType"), forKey: "usertype")
                            var storyboard = UIStoryboard()
                            switch UIDevice.currentDevice().userInterfaceIdiom {
                            case .Phone:
                                storyboard = UIStoryboard(name: "Main", bundle: nil)
                            case .Pad:
                                storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                            default:
                                break
                            }
                            
                            
                            let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorHomeViewController") as! DoctorHomeViewController
                            let drType:String = dict["DrType"] as! String
                            //let drType:String = "MedicinePrescribers"
                            defaults.setValue(drType, forKey: "drType")
                            if(drType == "MedicinePrescribers"){
                                vc.tableVwRow = 4
                            }else{
                                vc.tableVwRow = 3
                            }
                            
                            self.navigationController!.pushViewController(vc, animated: true)
                        }
                    }
                    else if(usertype == 0 && docPatLogin == false){
                        let firstName: String = dict["FirstName"] as! String
                        let lastName:String = dict["LastName"] as! String
                        let address:String = dict["Address"] as! String
                        defaults.setObject(firstName, forKey: "firstName")
                        defaults.setObject(lastName, forKey: "lastName")
                        defaults.setObject(address, forKey: "address")
                        defaults.setObject(pic, forKey: "profilePic")
                        defaults.setObject(self.usernameFld.text!, forKey: "username")
                        let reqInt = responseDict.valueForKeyPath("data.DrRequest") as! NSInteger
                        let pharma = responseDict.valueForKeyPath("data.PharmacyName") as! NSString
                        if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginSelectcheckBox")){
                            defaults.setObject(self.textFieldLoginPswrd.text!, forKey: "password")
                        }else if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginCheckBox")){
                            defaults.removeObjectForKey("password")
                        }
                        var storyboard = UIStoryboard()
                        switch UIDevice.currentDevice().userInterfaceIdiom {
                        case .Phone:
                            storyboard = UIStoryboard(name: "Main", bundle: nil)
                        case .Pad:
                            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                        default:
                            break
                        }
                        let lat:String = responseDict.valueForKeyPath("data.Lat") as! String
                        defaults.setObject(lat, forKey: "lat")
                        let long:String = responseDict.valueForKeyPath("data.Long") as! String
                        defaults.setObject(long, forKey: "long")
                        if(reqInt == 0){
                            dispatch_async(dispatch_get_main_queue()) {
                                let vc     = storyboard.instantiateViewControllerWithIdentifier("SearchDoctorViewController") as! SearchDoctorViewController
                                vc.searchDoc   = true
                                vc.firstSearch = true
                                self.navigationController!.pushViewController(vc, animated: true)
                            }
                        }
                        else if(pharma == ""){
                            dispatch_async(dispatch_get_main_queue()) {
                                defaults.setBool(true, forKey: "searchDoc")
                                let vc = storyboard.instantiateViewControllerWithIdentifier("SelectPharmacyViewController") as! SelectPharmacyViewController
                                vc.nextBool = true
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        else{
                            defaults.setBool(true, forKey: "searchDoc")
                            defaults.setBool(true, forKey: "selectPharma")
                            dispatch_async(dispatch_get_main_queue()) {
                                let vc     = storyboard.instantiateViewControllerWithIdentifier("PatientHomeViewController") as! PatientHomeViewController
                                self.navigationController!.pushViewController(vc, animated: true)
                            }
                        }
                    }
                        
                    else{
                        dispatch_async(dispatch_get_main_queue()) {
                            AppManager.sharedManager.Showalert("Alert", alertmessage:"User Not Found")
                        }
                    }
                }
            }
        }
        else if(serverInt == 2){
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            let dict = responceDiction["data"]! as AnyObject
            loginResponce = dict as! NSDictionary
            let usertype:Int = dict["UserType"]  as! NSInteger
            let id:String = responceDiction.valueForKeyPath("data._id") as! String
            self.doRequestPost("\(Header.BASE_URL)messageChat/sessionChange", data: ["UserId":"\(id)"])
            serverWho = 2
            
            
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setValue(id, forKey: "id")
            defaults.setValue(usertype, forKey: "usertype")
            let pic = dict["ProfilePic"] as! String
            if(usertype == 1 && docPatLogin == true ){
                dispatch_async(dispatch_get_main_queue()) {
                    let firstName: String = dict["FirstName"] as! String
                    let lastName:String = dict["LastName"] as! String
                    let specialityTxt:String = dict["Speciality"] as! String
                    let qul:String = dict["Qualification"] as! String
                    defaults.setObject(pic, forKey: "profilePic")
                    defaults.setObject(firstName, forKey: "firstName")
                    defaults.setObject(lastName, forKey: "lastName")
                    defaults.setObject(specialityTxt, forKey: "Speciality")
                    defaults.setObject(qul, forKey: "qualification")
                    if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginSelectcheckBox")){
                        defaults.setObject(self.usernameFld.text!, forKey: "usernameDoc")
                        defaults.setObject(self.textFieldLoginPswrd.text!, forKey: "passwordDoc")
                    }else if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginCheckBox")){
                        defaults.removeObjectForKey("usernameDoc")
                        defaults.removeObjectForKey("passwordDoc")
                    }
                    defaults.setObject(self.responceDiction.valueForKeyPath("data.UserType"), forKey: "usertype")
                    var storyboard = UIStoryboard()
                    switch UIDevice.currentDevice().userInterfaceIdiom {
                    case .Phone:
                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                    case .Pad:
                        storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    default:
                        break
                    }
                    let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorHomeViewController") as! DoctorHomeViewController
                    let drType:String = dict["DrType"] as! String
                    // let drType:String = "MedicinePrescribers"
                    defaults.setValue(drType, forKey: "drType")
                    if(drType == "MedicinePrescribers"){
                        vc.tableVwRow = 4
                    }else{
                        vc.tableVwRow = 3
                    }
                    self.navigationController!.pushViewController(vc, animated: true)
                }
            }
            else if(usertype == 0 && docPatLogin == false){
                let firstName: String = dict["FirstName"] as! String
                let lastName:String = dict["LastName"] as! String
                let address:String = dict["Address"] as! String
                defaults.setObject(firstName, forKey: "firstName")
                defaults.setObject(lastName, forKey: "lastName")
                defaults.setObject(address, forKey: "address")
                defaults.setObject(pic, forKey: "profilePic")
                defaults.setObject(self.usernameFld.text!, forKey: "username")
                let reqInt = responceDiction.valueForKeyPath("data.DrRequest") as! NSInteger
                let pharma = responceDiction.valueForKeyPath("data.PharmacyName") as! NSString
                if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginSelectcheckBox")){
                    defaults.setObject(self.textFieldLoginPswrd.text!, forKey: "password")
                }else if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginCheckBox")){
                    defaults.removeObjectForKey("password")
                }
                var storyboard = UIStoryboard()
                switch UIDevice.currentDevice().userInterfaceIdiom {
                case .Phone:
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                case .Pad:
                    storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                default:
                    break
                }
                let lat:String = responceDiction.valueForKeyPath("data.Lat") as! String
                defaults.setObject(lat, forKey: "lat")
                let long:String = responceDiction.valueForKeyPath("data.Long") as! String
                defaults.setObject(long, forKey: "long")
                if(reqInt == 0){
                    dispatch_async(dispatch_get_main_queue()) {
                        let vc     = storyboard.instantiateViewControllerWithIdentifier("SearchDoctorViewController") as! SearchDoctorViewController
                        vc.searchDoc   = true
                        vc.firstSearch = true
                        self.navigationController!.pushViewController(vc, animated: true)
                    }
                }
                else if(pharma == ""){
                    dispatch_async(dispatch_get_main_queue()) {
                        defaults.setBool(true, forKey: "searchDoc")
                        let vc = storyboard.instantiateViewControllerWithIdentifier("SelectPharmacyViewController") as! SelectPharmacyViewController
                        vc.nextBool = true
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else{
                    defaults.setBool(true, forKey: "searchDoc")
                    defaults.setBool(true, forKey: "selectPharma")
                    dispatch_async(dispatch_get_main_queue()) {
                        let vc     = storyboard.instantiateViewControllerWithIdentifier("PatientHomeViewController") as! PatientHomeViewController
                        self.navigationController!.pushViewController(vc, animated: true)
                    }
                }
            }
        }else{
            let usertype:Int = loginResponce["UserType"]  as! NSInteger
            let id:String = loginResponce.valueForKeyPath("_id") as! String
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setValue(id, forKey: "id")
            defaults.setValue(usertype, forKey: "usertype")
            let pic = loginResponce["ProfilePic"] as! String
            if(usertype == 1 && docPatLogin == true ){
                dispatch_async(dispatch_get_main_queue()) {
                    let firstName: String = self.loginResponce["FirstName"] as! String
                    let lastName:String = self.loginResponce["LastName"] as! String
                    let specialityTxt:String = self.loginResponce["Speciality"] as! String
                    let qul:String = self.loginResponce["Qualification"] as! String
                    defaults.setObject(pic, forKey: "profilePic")
                    defaults.setObject(firstName, forKey: "firstName")
                    defaults.setObject(lastName, forKey: "lastName")
                    defaults.setObject(specialityTxt, forKey: "Speciality")
                    defaults.setObject(qul, forKey: "qualification")
                    // let drType:String = "MedicinePrescribers"
                    let drType:String = self.loginResponce["DrType"] as! String
                    defaults.setValue(drType, forKey: "drType")
                    if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginSelectcheckBox")){
                        defaults.setObject(self.usernameFld.text!, forKey: "usernameDoc")
                        defaults.setObject(self.textFieldLoginPswrd.text!, forKey: "passwordDoc")
                    }else if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginCheckBox")){
                        defaults.removeObjectForKey("usernameDoc")
                        defaults.removeObjectForKey("passwordDoc")
                    }
                    defaults.setObject(self.loginResponce.valueForKey("UserType"), forKey: "usertype")
                    var storyboard = UIStoryboard()
                    switch UIDevice.currentDevice().userInterfaceIdiom {
                    case .Phone:
                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                    case .Pad:
                        storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    default:
                        break
                    }
                    let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorHomeViewController") as! DoctorHomeViewController
                    if(drType == "MedicinePrescribers"){
                        vc.tableVwRow = 4
                    }else{
                        vc.tableVwRow = 3
                    }
                    self.navigationController!.pushViewController(vc, animated: true)
                }
            }
                
            else if(usertype == 0 && docPatLogin == false){
                let firstName: String = loginResponce["FirstName"] as! String
                let lastName:String = loginResponce["LastName"] as! String
                let address:String = loginResponce["Address"] as! String
                defaults.setObject(firstName, forKey: "firstName")
                defaults.setObject(lastName, forKey: "lastName")
                defaults.setObject(address, forKey: "address")
                defaults.setObject(pic, forKey: "profilePic")
                defaults.setObject(self.usernameFld.text!, forKey: "username")
                let reqInt = loginResponce.valueForKey("DrRequest") as! NSInteger
                let pharma = loginResponce.valueForKey("PharmacyName") as! NSString
                if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginSelectcheckBox")){
                    defaults.setObject(self.textFieldLoginPswrd.text!, forKey: "password")
                }else if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginCheckBox")){
                    defaults.removeObjectForKey("password")
                }
                var storyboard = UIStoryboard()
                switch UIDevice.currentDevice().userInterfaceIdiom {
                case .Phone:
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                case .Pad:
                    storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                default:
                    break
                }
                let lat:String = loginResponce.valueForKey("Lat") as! String
                defaults.setObject(lat, forKey: "lat")
                let long:String = loginResponce.valueForKey("Long") as! String
                defaults.setObject(long, forKey: "long")
                if(reqInt == 0){
                    dispatch_async(dispatch_get_main_queue()) {
                        let vc     = storyboard.instantiateViewControllerWithIdentifier("SearchDoctorViewController") as! SearchDoctorViewController
                        vc.searchDoc   = true
                        vc.firstSearch = true
                        self.navigationController!.pushViewController(vc, animated: true)
                    }
                }
                else if(pharma == ""){
                    dispatch_async(dispatch_get_main_queue()) {
                        defaults.setBool(true, forKey: "searchDoc")
                        let vc = storyboard.instantiateViewControllerWithIdentifier("SelectPharmacyViewController") as! SelectPharmacyViewController
                        vc.nextBool = true
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else{
                    defaults.setBool(true, forKey: "searchDoc")
                    defaults.setBool(true, forKey: "selectPharma")
                    dispatch_async(dispatch_get_main_queue()) {
                        let vc     = storyboard.instantiateViewControllerWithIdentifier("PatientHomeViewController") as! PatientHomeViewController
                        self.navigationController!.pushViewController(vc, animated: true)
                    }
                }
            }
                
            else{
                dispatch_async(dispatch_get_main_queue()) {
                    AppManager.sharedManager.Showalert("Alert", alertmessage:"User Not Found")
                }
            }
            
            
        }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    
    //MARK:- TextFieldMethod
    func textFieldDidEndEditing(textField: UITextField){
        let trimNameString1 = usernameFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        trimNameString1
        if(trimNameString1 != "" && textFieldLoginPswrd.text! != ""){
            loginBtn.enabled = true
        }else if(trimNameString1 == "" || textFieldLoginPswrd.text! == ""){
            loginBtn.enabled = false
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- ButtonAction
    @IBAction func agreeBtnAc(sender: AnyObject){
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
        let params = "UserId=\(userId)&agreement=1"
        serverInt = 2
        AppManager.sharedManager.postDataOnserver(params, postUrl: "users/agreement")
        agreeDisagreeVw.hidden = true
    }
    @IBAction func createAcountAc(sender: AnyObject) {
        if(docPatLogin == true){
            let vc = storyboard!.instantiateViewControllerWithIdentifier("CreateAccountViewController") as! CreateAccountViewController
            self.navigationController?.pushViewController(vc, animated: true)
            vc.createAc = true
        }else{
            let vc = storyboard!.instantiateViewControllerWithIdentifier("PateintCreateAccountViewController") as! PateintCreateAccountViewController
            self.navigationController?.pushViewController(vc, animated: true)
            vc.createAc = true
        }
    }
    @IBAction func remembermeAction(sender: AnyObject) {
        
        if(sender.currentImage == UIImage(named: "emptyCheckBox")){
            sender.setImage(UIImage(named: "checked"), forState: .Normal)
        }else if(sender.currentImage == UIImage(named: "checked")){
            sender.setImage(UIImage(named: "emptyCheckBox"), forState: .Normal)
        }
//        if(sender.currentImage == UIImage(named: "loginCheckBox")){
//            sender.setImage(UIImage(named: "loginSelectcheckBox"), forState: .Normal)
//        }else if(sender.currentImage == UIImage(named: "loginSelectcheckBox")){
//            sender.setImage(UIImage(named: "loginCheckBox"), forState: .Normal)
//        }
    }
    @IBAction func loginAc(sender: AnyObject) {
        NSUserDefaults.standardUserDefaults().setValue("manual", forKey: "mediaType")
        let emailLowerCaseString:String = (usernameFld.text?.lowercaseString)!
        let defaults = NSUserDefaults.standardUserDefaults()
        if((AppManager.sharedManager.isValidEmail(emailLowerCaseString))){
            var deviceToken =  NSString()
            if(UIDevice.isSimulator == true){
                deviceToken = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
            }else{
                deviceToken = defaults.valueForKey("deviceToken") as! NSString
            }
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loging In")
            let deviceName = UIDevice.currentDevice().name
            
            let params = "Email=\(emailLowerCaseString)&LoginType=M&LoginKey=\(textFieldLoginPswrd.text!)&DeviceToken=\(deviceToken)&DeviceType=ios&Device=\(deviceName)&UserType=\(UserType)"
            serverInt = 1
            defaults.setObject(emailLowerCaseString,forKey: "email")
            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/Login1")
        }
        else{
            AppManager.sharedManager.Showalert("Alert", alertmessage: "please enter correct email format like, 'abc@gmail.com'")
        }
    }
    
    func crossBtnAc(){
        agreeDisagreeVw.hidden = true
        let crossBtn = self.view.viewWithTag(4) as! UIButton
        crossBtn.removeFromSuperview()
    }
    func okBtnAc(){
        let view = self.view.viewWithTag(2369)! as UIView
        view.removeFromSuperview()
    }
    
    @IBAction func btnLoginPswrdAc(sender: AnyObject) {
        if(textFieldSecureEntry == true){
            textFieldSecureEntry = false
            textFieldLoginPswrd.secureTextEntry = false
            btnLoginPswrd.setImage(UIImage(named: "whiteEyeClose"), forState: UIControlState.Normal)
        }else{
            textFieldSecureEntry = true
            textFieldLoginPswrd.secureTextEntry = true
            btnLoginPswrd.setImage(UIImage(named: "whiteEyeOpen"), forState: UIControlState.Normal)
        }
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
