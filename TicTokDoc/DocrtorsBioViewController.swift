

import UIKit
import SlideMenuControllerSwift
import RealmSwift
class DocrtorsBioViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,WebServiceDelegate {
    var imgLogoAr = ["qualification","language","phone","Address"]
    
    var adress = NSString()
    var bio = NSString()
    var lang = NSString()
    var quali = NSString()
    var drname = NSString()
    var phone = NSString()
    var speciality = NSString()
    var drEmail = NSString()
    var docBio = Bool()
    var sndReq = Bool()
    var data = []
    var dr_Id = NSString()
    var deviceToken = NSString()
    var profilrPicUrl = NSString()
    var docDocSearch = Bool()
    var deviceType = NSString()
    var realm = try! Realm()
    @IBOutlet weak var docBioTableVw: UITableView!
    @IBOutlet weak var docBioImgVw: UIImageView!
    @IBOutlet weak var sndReqBtn: UIButton!
    @IBOutlet weak var showStaffBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        data = [quali,lang,phone,adress]
        docBioTableVw.delegate = self
        docBioTableVw.dataSource = self
        docBioImgVw.layer.cornerRadius = docBioImgVw.frame.size.height/2
        docBioImgVw.layer.borderWidth = docBioImgVw.frame.size.height*0.06
        docBioImgVw.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        docBioImgVw.clipsToBounds = true
        if let url = NSURL(string: "http://\(profilrPicUrl)") {
            docBioImgVw.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        sndReqBtn.hidden = true
        showStaffBtn.hidden = true
        if(sndReq == true || docBio == true){
            sndReqBtn.hidden = false
        }
        else{
            showStaffBtn.hidden = false
        }
        if(docDocSearch == true){
            sndReqBtn.hidden = false
            showStaffBtn.hidden = false
            sndReqBtn.setImage(UIImage(named: "chatNwBtn"), forState: .Normal)
        }
    }
    
    @IBAction func showStaffBtnAc(sender: AnyObject){
        var storyboard = UIStoryboard()
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        }else{
            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
        }
        let vc = storyboard.instantiateViewControllerWithIdentifier("StaffListingViewController") as! StaffListingViewController
        let defaults = NSUserDefaults.standardUserDefaults()
        let usertype = defaults.valueForKey("usertype") as! NSInteger
        if(usertype == 1){
            vc.doctorItself = false
            vc.showStaff = false
        }else if(usertype == 0){
            vc.showStaff = false
        }
        else if(usertype == 3){
            vc.showStaff = true
        }
        vc.drName = drname
        vc.searchDocUserId = dr_Id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func sndReqBtnAc(sender: AnyObject) {
        if(docDocSearch == true){
            let defaults = NSUserDefaults.standardUserDefaults()
            let userType = defaults.valueForKey("usertype") as! Int
            if(userType == 1 || userType == 0){
                if let userId = defaults.valueForKey("id"){
                    AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                    self.doRequestPost("\(Header.BASE_URL)docter/DRdrcreatetoken", data: ["From":"\(userId)","To":"\(dr_Id)"])
                }
            }else{
                if let userId = defaults.valueForKey("staffId"){
                    AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                    self.doRequestPost("\(Header.BASE_URL)docter/DRdrcreatetoken", data: ["From":"\(userId)","To":"\(dr_Id)"])
                }
            }
        }
        else if(docBio == true || sndReq == true){
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let defaults = NSUserDefaults.standardUserDefaults()
            let firstname = defaults.valueForKey("firstName") as! String
            let lastname = defaults.valueForKey("lastName") as! String
            if let userId = defaults.valueForKey("id"){
                let params = "Patient_id=\(userId)&Dr_id=\(dr_Id)&DeviceToken=\(deviceToken)&Email=\(drEmail)&DrName=\(drname)&PatientName=\(firstname) \(lastname)&DeviceType=\(deviceType)"
                AppManager.sharedManager.postDataOnserver(params, postUrl:"patients/request")
            }
        }
    }
    func doRequestPost(url:String,data:[String: NSObject]){
        print("URL FOR DEVICETOKEN ==> \(url)")
        let requestDic = data
        print(requestDic)
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        
        print("Request Object:\(data)")
        print("Request string = \(jsonString!)")
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                print("Succes:")
                let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                print(responceString)
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
              
                dispatch_async(dispatch_get_main_queue())
                {
                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                    let responceData = responseDict.valueForKey("data") as! NSDictionary
                    let tokenData = responceData.valueForKey("TokenData") as! String
                    let apiKey = responceData.valueForKey("apiKey") as! String
                    let groupId = responceData.valueForKey("groupId") as! String
                    let sessionData = responceData.valueForKey("sessionData") as! String
                    
                    let firstname:String = ("\(self.drname)")
                    
                    let user = NSMutableDictionary()
                    user.setValue(tokenData, forKey: "TokenData")
                    user.setValue(apiKey, forKey: "ApiKey")
                    user.setValue(groupId, forKey: "GroupId")
                    user.setValue(sessionData, forKey: "sessionData")
                    user.setValue(firstname, forKey: "Name")
                    self.msgCountList(groupId) { (result) in
                        let chatCount = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", groupId ))
                        if(chatCount.count < result.valueForKey("count") as! Int){
                            var userId = NSString()
                            let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
                            if(userType == 3){
                                userId = NSUserDefaults.standardUserDefaults().valueForKey("staffId") as! String
                            }else{
                                userId = NSUserDefaults.standardUserDefaults().valueForKey("id") as! String
                            }
                            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                            RealmInteractor.shared.getMsgsFromServer(userId as String, groupId:groupId) { (result) in
                                AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                                
                                if(result.valueForKey("status") as! Int == 200)
                                {
                                    let chatResults = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", groupId))
                                    try! self.realm.write({
                                        self.realm.delete(chatResults)
                                    })
                                    
                                    let chatArray = result.objectForKey("data") as! NSArray
                                    
                                    for dict in chatArray
                                    {
                                        try! self.realm.write({
                                            
                                            let group = Group()
                                            group.displayName = dict.valueForKey("DisplayName") as! String
                                            group.message = dict.valueForKey("message") as! String
                                            group.messageType = dict.valueForKey("messageType") as! String
                                            group.fromId = dict.valueForKey("From") as! String
                                            group.groupId = dict.valueForKey("GroupId") as! String
                                            self.realm.add(group)
                                            
                                        })
                                        
                                    }
                                    self.navigateToChatView(user)
                                }
                            }
                        }
                        else
                        {
                            self.navigateToChatView(user)
                        }
                    }
                }
            }
        }
        dataTask.resume()
    }
    func navigateToChatView(user:NSDictionary) -> Void {
        
        let chatResults = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", user.valueForKey("GroupId") as! String))
        print(chatResults.count)
        
        let msgArray = NSMutableArray()
        
        for group in chatResults
        {
            var mySelf = false
            let msgDict = group
            
            let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
            if(userType == 1 || userType == 0 || userType == 2){
                if(msgDict.fromId == (NSUserDefaults.standardUserDefaults().valueForKey("id") as! String))
                {
                    mySelf = true
                }
            }else if(userType == 3){
                if(msgDict.fromId == (NSUserDefaults.standardUserDefaults().valueForKey("staffId") as! String))
                {
                    mySelf = true
                }
            }
            
            let mdict = ["msg":msgDict.message,"bool":mySelf, "msgType":msgDict.messageType,"name":msgDict.displayName]
            msgArray.addObject(mdict)
        }
        
        let firstname:String = NSUserDefaults.standardUserDefaults().valueForKey("firstName") as! String
        
        let vc = storyboard!.instantiateViewControllerWithIdentifier("ChatViewController") as! ChatViewController
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let usertype = defaults.valueForKey("usertype") as! NSInteger
        if(usertype == 3){
            vc.userId = defaults.valueForKey("staffId") as! String
        }else{
            vc.userId = defaults.valueForKey("id") as! String
        }
        
        vc.chatUserId = user.valueForKey("GroupId") as! String
        
        vc.userName = "\(user.valueForKey("Name"))" as! String
        
        
        vc.Token = user.valueForKey("TokenData") as! String
        vc.SessionID = user.valueForKey("sessionData") as! String
        vc.apiKey = user.valueForKey("ApiKey") as! String
        vc.msgAr = msgArray
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func msgCountList(groupId:String,completion: (result: NSDictionary)->()) -> Void {
        let paramString = "GroupId=\(groupId)"
        AppManager.sharedManager.generateGroupIdForUser("messageChat/listChat", params: paramString) { (result) in
            print("\(result)")
            
            completion(result: result)
        }
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString)
    {
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        if((responseDict.valueForKey("error")) != nil){
            
            dispatch_async(dispatch_get_main_queue()) {
                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                backVw.tag = 2356
                self.view.addSubview(backVw)
                
                let alerVw = UIView()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.07,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.14), self.view.frame.size.height*0.184)
                }else{
                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
                }
                
                alerVw.backgroundColor = UIColor.whiteColor()
                backVw.addSubview(alerVw)
                
                let rightImg = UIImageView.init(frame: CGRectMake(12,12, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                rightImg.image = UIImage(named: "confirm")
                alerVw.addSubview(rightImg)
                
                let verificationLbl  = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,12, self.view.frame.size.width*0.45, self.view.frame.size.width*0.06))
                verificationLbl.text = "Alert"
                verificationLbl.font = UIFont(name: "Bold", size: self.view.frame.size.height * 0.02)
                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(verificationLbl)
                
                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+20,alerVw.frame.size.width,1))
                lineVw.backgroundColor = UIColor.grayColor()
                alerVw.addSubview(lineVw)
                
                let contentLbl = UILabel()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    contentLbl.font  = UIFont(name: "Helvetica", size:14)
                    contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+22, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
                }else{
                    contentLbl.font  = UIFont(name: "Helvetica", size:19)
                    contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.21)
                }
                contentLbl.textAlignment = .Center
                contentLbl.textColor = UIColor.grayColor()
                contentLbl.text = "Request has already been sent."
                contentLbl.numberOfLines = 2
                alerVw.addSubview(contentLbl)
                
                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.2))
                okLbl.setTitle("OK", forState: .Normal)
                if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                }else{
                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                }
                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                okLbl.titleLabel?.textAlignment = .Center
                okLbl.addTarget(self, action: #selector(DocrtorsBioViewController.okBtnAc), forControlEvents: .TouchUpInside)
                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(okLbl)
            }
            
        }
        else{
            dispatch_async(dispatch_get_main_queue()) {
                if(self.view.frame.size.height < 600){
                    let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                    backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                    backVw.tag = 2356
                    self.view.addSubview(backVw)
                    
                    let alerVw = UIView()
                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.07,self.view.frame.size.height/2-((self.view.frame.size.height*0.38)/2), self.view.frame.size.width-(self.view.frame.size.width*0.14), self.view.frame.size.height*0.38)
                    alerVw.backgroundColor = UIColor.whiteColor()
                    backVw.addSubview(alerVw)
                    
                    let rightImg = UIImageView.init(frame: CGRectMake(12,12, self.view.frame.size.width*0.06, self.view.frame.size.width*0.06))
                    rightImg.image = UIImage(named: "confirm")
                    alerVw.addSubview(rightImg)
                    
                    let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,12, self.view.frame.size.width*0.45, self.view.frame.size.width*0.06))
                    verificationLbl.text = " Request Sent"
                    verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                    verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(verificationLbl)
                    
                    let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+20,alerVw.frame.size.width,1))
                    lineVw.backgroundColor = UIColor.grayColor()
                    alerVw.addSubview(lineVw)
                    
                    let contentLbl = UILabel()
                    contentLbl.font  = UIFont(name: "Helvetica", size:14)
                    contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+20, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.56)
                    contentLbl.textAlignment = .Center
                    contentLbl.textColor = UIColor.grayColor()
                    let trimNameString = self.drname.stringByTrimmingCharactersInSet(
                        NSCharacterSet.whitespaceAndNewlineCharacterSet());
                    contentLbl.text = "You can self-schedule your video visits with Dr. \(trimNameString) once they accept your request. You will be notified via a pop-up notification and/or an email as soon as your account has been verified by your doctor."
                    contentLbl.numberOfLines = 7
                    alerVw.addSubview(contentLbl)
                    
                    let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.2))
                    okLbl.setTitle("OK", forState: .Normal)
                    okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                    okLbl.titleLabel?.textAlignment = .Center
                    okLbl.addTarget(self, action: #selector(DoctorRatingViewController.okBtnAc), forControlEvents: .TouchUpInside)
                    okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(okLbl)
                }
                else if(self.view.frame.size.height > 600 && self.view.frame.size.height < 700){
                    let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                    backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                    backVw.tag = 2356
                    self.view.addSubview(backVw)
                    
                    let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.07,self.view.frame.size.height/2-((self.view.frame.size.height*0.38)/2), self.view.frame.size.width-(self.view.frame.size.width*0.14), self.view.frame.size.height*0.33))
                    alerVw.backgroundColor = UIColor.whiteColor()
                    backVw.addSubview(alerVw)
                    
                    let rightImg = UIImageView.init(frame: CGRectMake(12,12, self.view.frame.size.width*0.06, self.view.frame.size.width*0.06))
                    rightImg.image = UIImage(named: "confirm")
                    alerVw.addSubview(rightImg)
                    
                    let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,12, self.view.frame.size.width*0.45, self.view.frame.size.width*0.06))
                    verificationLbl.text = " Request Sent"
                    verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                    verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(verificationLbl)
                    
                    let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+20,alerVw.frame.size.width,1))
                    lineVw.backgroundColor = UIColor.grayColor()
                    alerVw.addSubview(lineVw)
                    
                    let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+20, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.56))
                    contentLbl.font = UIFont(name: "Helvetica", size:14)
                    contentLbl.textAlignment = .Center
                    contentLbl.textColor = UIColor.grayColor()
                    let trimNameString = self.drname.stringByTrimmingCharactersInSet(
                        NSCharacterSet.whitespaceAndNewlineCharacterSet());
                    contentLbl.text = "You can self-schedule your video visits with Dr. \(trimNameString) once they accept your request. You will be notified via a pop-up notification and/or an email as soon as your account has been verified by your doctor."
                    contentLbl.numberOfLines = 7
                    alerVw.addSubview(contentLbl)
                    
                    let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.18),alerVw.frame.size.width, alerVw.frame.size.height*0.18))
                    okLbl.setTitle("OK", forState: .Normal)
                    okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                    okLbl.titleLabel?.textAlignment = .Center
                    okLbl.addTarget(self, action: #selector(DoctorRatingViewController.okBtnAc), forControlEvents: .TouchUpInside)
                    okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(okLbl)
                }else{
                    let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                    backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                    backVw.tag = 2356
                    self.view.addSubview(backVw)
                    let alerVw = UIView()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        alerVw.frame = CGRectMake(self.view.frame.size.width*0.07,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.14), self.view.frame.size.height*0.3)
                    }else{
                        alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.2)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.24)
                    }
                    alerVw.backgroundColor = UIColor.whiteColor()
                    backVw.addSubview(alerVw)
                    
                    let rightImg = UIImageView.init(frame: CGRectMake(12,12, self.view.frame.size.width*0.06, self.view.frame.size.width*0.06))
                    rightImg.image = UIImage(named: "confirm")
                    alerVw.addSubview(rightImg)
                    
                    let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,12, self.view.frame.size.width*0.45, self.view.frame.size.width*0.06))
                    verificationLbl.text = " Request Sent"
                    verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                    verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(verificationLbl)
                    
                    let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+20,alerVw.frame.size.width,1))
                    lineVw.backgroundColor = UIColor.grayColor()
                    alerVw.addSubview(lineVw)
                    
                    
                    let contentLbl = UILabel()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        contentLbl.font  = UIFont(name: "Helvetica", size:14)
                        contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+20, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.56)
                    }else{
                        contentLbl.font  = UIFont(name: "Helvetica", size:19)
                        contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+22, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.45)
                    }
                    contentLbl.textAlignment = .Center
                    contentLbl.textColor = UIColor.grayColor()
                    let trimNameString = self.drname.stringByTrimmingCharactersInSet(
                        NSCharacterSet.whitespaceAndNewlineCharacterSet());
                    contentLbl.text = "You can self-schedule your video visits with Dr. \(trimNameString) once they accept your request. You will be notified via a pop-up notification and/or an email as soon as your account has been verified by your doctor."
                    contentLbl.numberOfLines = 7
                    alerVw.addSubview(contentLbl)
                    
                    let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.18),alerVw.frame.size.width, alerVw.frame.size.height*0.18))
                    okLbl.setTitle("OK", forState: .Normal)
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                        okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                    }else{
                        okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                    }
                    okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                    okLbl.titleLabel?.textAlignment = .Center
                    okLbl.addTarget(self, action: #selector(DoctorRatingViewController.okBtnAc), forControlEvents: .TouchUpInside)
                    okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(okLbl)
                }
            }
        }
    }
    
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    
    func okBtnAc(){
        if(docBio == true){
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setBool(true, forKey: "searchDoc")
            let vc = storyboard!.instantiateViewControllerWithIdentifier("SelectPharmacyViewController") as! SelectPharmacyViewController
            vc.nextBool = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let view = self.view.viewWithTag(2356)! as UIView
            view.removeFromSuperview()
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCellWithIdentifier("cell")
            cell!.layer.cornerRadius = 7.0
            cell!.layer.masksToBounds = true
            let backBoxVw: UIView = (cell?.viewWithTag(1600))!
            backBoxVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            backBoxVw.layer.borderColor = UIColor.darkGrayColor().CGColor
            backBoxVw.layer.shadowOffset = CGSizeMake(10, 5);
            backBoxVw.layer.shadowRadius = 1;
            backBoxVw.layer.shadowOpacity = 0.06;
            backBoxVw.backgroundColor = UIColor.whiteColor()
            backBoxVw.layer.masksToBounds = false;
            backBoxVw.layer.shadowColor = UIColor.lightGrayColor().CGColor
            backBoxVw.layer.borderWidth = 0.5
            let name = cell?.viewWithTag(5006) as! UILabel
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: "DocIcon")
            let offsetY: CGFloat = -6.0
            attachment.bounds = CGRectMake(-10, offsetY, attachment.image!.size.width, attachment.image!.size.height)
            let attachmentString = NSAttributedString(attachment: attachment)
            let myString = NSMutableAttributedString(string:"")
            myString.appendAttributedString(attachmentString)
            let myString1 = NSMutableAttributedString(string:"Dr. \(drname)" as String)
            myString.appendAttributedString(myString1)
            name.textAlignment = .Center
            name.attributedText = myString
            let special = cell?.viewWithTag(5007) as! UILabel
            special.text = speciality as String
            return cell!
        }
        else if(indexPath.row == 5){
            let cell = tableView.dequeueReusableCellWithIdentifier("lastcell")
            cell!.layer.cornerRadius = 7.0
            cell!.layer.masksToBounds = true
            let backBoxVw: UIView = (cell?.viewWithTag(1604))!
            backBoxVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            backBoxVw.layer.borderColor = UIColor.darkGrayColor().CGColor
            backBoxVw.layer.shadowOffset = CGSizeMake(10, 5);
            backBoxVw.layer.shadowRadius = 1;
            backBoxVw.layer.shadowOpacity = 0.06;
            backBoxVw.backgroundColor = UIColor.whiteColor()
            backBoxVw.layer.masksToBounds = false;
            backBoxVw.layer.shadowColor = UIColor.lightGrayColor().CGColor
            backBoxVw.layer.borderWidth = 0.5
            let bio1 = cell?.viewWithTag(5012) as! UILabel
            bio1.text = "\(bio)"
            return cell!
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("leftcell")
            cell!.layer.cornerRadius = 7.0
            cell!.layer.masksToBounds = true
            let backBoxVw: UIView = (cell?.viewWithTag(1603))!
            backBoxVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            backBoxVw.layer.borderColor = UIColor.darkGrayColor().CGColor
            backBoxVw.layer.shadowOffset = CGSizeMake(10, 5);
            backBoxVw.layer.shadowRadius = 1;
            backBoxVw.layer.shadowOpacity = 0.06;
            backBoxVw.backgroundColor = UIColor.whiteColor()
            backBoxVw.layer.masksToBounds = false;
            backBoxVw.layer.shadowColor = UIColor.lightGrayColor().CGColor
            backBoxVw.layer.borderWidth = 0.5
            let qualification = cell?.viewWithTag(5008) as! UILabel
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: imgLogoAr[indexPath.row-1])
            let offsetY: CGFloat = -8.0
            attachment.bounds = CGRectMake(0, offsetY, attachment.image!.size.width, attachment.image!.size.height)
            let attachmentString = NSAttributedString(attachment: attachment)
            let myString = NSMutableAttributedString(string:"")
            myString.appendAttributedString(attachmentString)
            let myString1 = NSMutableAttributedString(string:"  \(data[indexPath.row-1])")
            myString.appendAttributedString(myString1)
            qualification.textAlignment = .Center
            qualification.attributedText = myString
            return cell!
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return self.view.frame.size.width*0.2
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.row == 3){
            let alert = UIAlertController(title: "Please confirm your call", message: "You’re about to call the following:\n\(phone)", preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
                UIAlertAction in
            }
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
                UIAlertAction in
                if let url = NSURL(string: "tel://\(self.phone)") {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
