
import UIKit
import SlideMenuControllerSwift
class ViewScheduleViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,WebServiceDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{
    @IBOutlet weak var yearTxtFld: UITextField!
    @IBOutlet weak var monthTxtFld: UITextField!
    @IBOutlet weak var dateTxtFld: UITextField!
    @IBOutlet weak var viewSchTableVw: UITableView!
    
    var fromToTime = NSMutableArray()
    var duration = NSMutableArray()
    var dateAr1 = NSArray()
    var monthAr1 = NSArray()
    var yearAr1 = NSArray()
    var dataArr = NSArray()
    var dateY = NSString()
    var dateM = NSString()
    var dateD = NSString()
    var monthArr = NSArray()
    var yearArr = NSArray()
    var dayArr = NSArray()
    var server = Bool()
    var fromAr = NSMutableArray()
    var schIdAr = NSMutableArray()
    var toAr = NSMutableArray()
    var stausAr = NSMutableArray()
    var scheduleId = NSMutableArray()
    var yearCollectionAr  = NSArray()
    var dayCollectionAr   = NSArray()
    var monthCollectionAr = NSArray()
    var removeIndex = NSIndexPath()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        viewSchTableVw.delegate = self
        viewSchTableVw.dataSource = self
        viewSchTableVw.allowsSelection = true
        yearTxtFld.delegate = self
        monthTxtFld.delegate = self
        dateTxtFld.delegate = self
        
        yearTxtFld.text = ""
        monthTxtFld.text = ""
        dateTxtFld.text = ""
        
        yearTxtFld.userInteractionEnabled = true
        monthTxtFld.userInteractionEnabled = true
        dateTxtFld.userInteractionEnabled = true
        yearTxtFld.tintColor     = UIColor.clearColor()
        monthTxtFld.tintColor    = UIColor.clearColor()
        dateTxtFld.tintColor = UIColor.clearColor()
    }
    override func viewWillAppear(animated: Bool) {
         self.tabBarController!.tabBar.userInteractionEnabled = false;
        duration.removeAllObjects()
        fromToTime.removeAllObjects()
         self.tabBarController?.tabBar.hidden = false
        yearTxtFld.text = ""
        monthTxtFld.text = ""
        dateTxtFld.text = ""
        schIdAr.removeAllObjects()
        viewSchTableVw.reloadData()
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
            let params = "Dr_id=\(userId)"
            server = false
            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/ScheduleList")
        }
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        if(server == false){
         dispatch_async(dispatch_get_main_queue()) {
             self.tabBarController!.tabBar.userInteractionEnabled = true
          let data = responseDict.valueForKey("data") as! NSArray
            if(data.count != 0){
            self.dataArr = data
            let yearAr = NSMutableArray()
            for i in 0...data.count-1 {
              let date = data[i].valueForKeyPath("schedule.Date") as! NSDictionary
                let year = date.valueForKey("Year") as! NSString
                yearAr.addObject(year)
            }
                self.yearAr1 = []
            let yearFilter = yearAr as NSArray as! [String]
            self.yearAr1 = self.uniq(yearFilter)
            }else{
                self.dataArr = []
                self.yearAr1 = []
                self.monthAr1 = []
                self.dateAr1 = []
               AppManager.sharedManager.Showalert("Alert", alertmessage: "Currently You Have No Schedule")
            }
        }
        }else{
          dispatch_async(dispatch_get_main_queue()) {
            self.duration.removeObjectAtIndex(self.removeIndex.row)
            self.schIdAr.removeObjectAtIndex(self.removeIndex.row)
            self.fromToTime.removeObjectAtIndex(self.removeIndex.row)
            self.fromAr.removeObjectAtIndex(self.removeIndex.row)
            self.toAr.removeObjectAtIndex(self.removeIndex.row)
            if(self.duration.count == 0){
            self.yearTxtFld.text = ""
            self.monthTxtFld.text = ""
            self.dateTxtFld.text = ""
             }
            self.viewSchTableVw.deleteRowsAtIndexPaths([self.removeIndex], withRowAnimation: .Right)
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.18)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.18)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.2)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.16)
            }
           
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor    = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.38))
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
             contentLbl.font          = UIFont(name: "Helvetica", size:14)
            }else{
             contentLbl.font          = UIFont(name: "Helvetica", size:19)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor     = UIColor.grayColor()
            contentLbl.text          = "schedule deleted successfully"
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(ViewScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
          }
        }
    }
    func okBtnAc(){
        let view = self.view.viewWithTag(2369)! as UIView
        view.removeFromSuperview()
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
            let params = "Dr_id=\(userId)"
            self.server = false
            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/ScheduleList")
        }
    }
    func uniq<S: SequenceType, E: Hashable where E==S.Generator.Element>(source: S) -> [E] {
        var seen: [E:Bool] = [:]
        return source.filter { seen.updateValue(true, forKey: $0) == nil }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    func pickerShow(){
        let InputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0.4))
        
        let PickerVw  : UIPickerView = UIPickerView(frame: CGRectMake(0, 40,UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height*0.32))
        PickerVw.delegate            = self
        PickerVw.dataSource          = self
        InputView.addSubview(PickerVw)
        
        let doneButton = UIButton(frame: CGRectMake((self.view.frame.size.width-100), 0, 100, 50))
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        doneButton.setTitle("Done", forState: UIControlState.Highlighted)
        doneButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        doneButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        InputView.addSubview(doneButton)
        doneButton.addTarget(self, action: #selector(ViewScheduleViewController.doneButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let cancelBtn = UIButton(frame: CGRectMake(0, 0, 100, 50))
        cancelBtn.setTitle("Cancel", forState: UIControlState.Normal)
        cancelBtn.setTitle("Cancel", forState: UIControlState.Highlighted)
        cancelBtn.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        cancelBtn.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        InputView.addSubview(cancelBtn)
        cancelBtn.addTarget(self, action: #selector(ViewScheduleViewController.cancelBtn(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        if(yearTxtFld.isFirstResponder()){
            yearTxtFld.inputView = InputView
        }else if(monthTxtFld.isFirstResponder()){
            monthTxtFld.inputView = InputView
        }else if(dateTxtFld.isFirstResponder()){
            dateTxtFld.inputView = InputView
        }
    }
    func pickerShowHide(){
        let InputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0.0))
        let PickerVw  : UIPickerView = UIPickerView(frame: CGRectMake(0, 40,UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height*0.32))
        PickerVw.delegate            = self
        PickerVw.dataSource          = self
        InputView.addSubview(PickerVw)
        if(yearTxtFld.isFirstResponder()){
            yearTxtFld.inputView = InputView
        }else if(monthTxtFld.isFirstResponder()){
            monthTxtFld.inputView = InputView
        }else if(dateTxtFld.isFirstResponder()){
            dateTxtFld.inputView = InputView
        }
    }
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(yearTxtFld.isFirstResponder()){
            return yearAr1.count
        }else if(monthTxtFld.isFirstResponder()){
            return monthAr1.count
        }else if(dateTxtFld.isFirstResponder()){
            return dateAr1.count
        }
        return 0
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(yearTxtFld.isFirstResponder()){
             return yearAr1[row] as? String
        }else if(monthTxtFld.isFirstResponder()){
             return monthAr1[row] as? String
        }else if(dateTxtFld.isFirstResponder()){
             return dateAr1[row] as? String
        }
        return nil
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if(yearTxtFld.isFirstResponder()){
            let monthAr = NSMutableArray()
            dateY = yearAr1[row] as! String
            let yearValue = dateY
            
            let predicate = NSPredicate(format:"Year == %@",yearValue)
            let yearAr = dataArr.filteredArrayUsingPredicate(predicate)
            yearArr = yearAr
            
            for i in 0...yearAr.count-1 {
                let d = yearAr[i]
                monthAr.addObject(d.valueForKey("Month")!)
            }
            monthAr1 = uniq(monthAr as NSArray as! [String])
            
        }else if(monthTxtFld.isFirstResponder()){
            let days = NSMutableArray()
            dateM = monthAr1[row] as! String
            let mnthValue = dateM
            let predicate = NSPredicate(format:"Month = %@",mnthValue)
            let mnthAr = yearArr.filteredArrayUsingPredicate(predicate)
            monthArr = mnthAr
            for i in 0...mnthAr.count-1 {
                let d = mnthAr[i]
                days.addObject(d.valueForKey("day")!)
            }
            dateAr1 = uniq(days as NSArray as! [String])
        }else if(dateTxtFld.isFirstResponder()){
            fromToTime.removeAllObjects()
            duration.removeAllObjects()
           dateD = dateAr1[row] as! String
            let dateValue = dateD
            let predicate = NSPredicate(format:"day = %@",dateValue)
            let mnthAr = monthArr.filteredArrayUsingPredicate(predicate) as NSArray
            let schedule = mnthAr[0].valueForKeyPath("schedule.Schedule") as! NSArray
            let schId = mnthAr[0].valueForKeyPath("schedule._id") as! NSString
            scheduleId.addObject(schId)
            for j in 0...schedule.count-1 {
                let dic = schedule[j]
                let status = dic.valueForKey("Status") as! NSInteger
                if(status == 1){
                    stausAr.addObject(status)
                    let from = dic.valueForKeyPath("Time.From") as! NSString
                    let to = dic.valueForKeyPath("Time.To") as! NSString
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "HH:mm"
                    let timeZone = NSTimeZone(name: "GMT-8")
                    dateFormatter.timeZone=timeZone
                    let schF:NSDate = dateFormatter.dateFromString("\(from)")!
                    let schT:NSDate = dateFormatter.dateFromString("\(to)")!
                    dateFormatter.dateFormat = "hh:mm a"
                    let from12hrStr = dateFormatter.stringFromDate(schF) as NSString
                    let to12hrStr = dateFormatter.stringFromDate(schT) as NSString
                    let fromTo = "\(from12hrStr)-\(to12hrStr)"
                    fromToTime.addObject(fromTo)
                    fromAr.addObject(from)
                    toAr.addObject(to)
                    let meetingSlot = dic.valueForKey("MeetingSlot") as! NSString
                    duration.addObject(meetingSlot)
                    let sch_id = dic.valueForKey("_id") as! NSString
                    schIdAr.addObject(sch_id)
                }
            }
        }
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        if(yearTxtFld.isFirstResponder()){
            monthTxtFld.text = ""
            dateTxtFld.text = ""
            dateY = ""
            dateM = ""
            dateD = ""
            monthAr1 = []
            duration.removeAllObjects()
            fromToTime.removeAllObjects()
            fromAr.removeAllObjects()
            toAr.removeAllObjects()
            scheduleId.removeAllObjects()
            schIdAr.removeAllObjects()
            viewSchTableVw.reloadData()
            if(yearAr1.count == 0){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Currently You Have No Schedule")
            }else{
               self.pickerShow()
            }
        }else if(monthTxtFld.isFirstResponder()){
            dateM = ""
            dateD = ""
            dateTxtFld.text = ""
            dateAr1 = []
            duration.removeAllObjects()
            fromToTime.removeAllObjects()
            fromToTime.removeAllObjects()
            fromAr.removeAllObjects()
            scheduleId.removeAllObjects()
            schIdAr.removeAllObjects()
            viewSchTableVw.reloadData()
            if(monthAr1.count == 0){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Select Year")
            }else{
             self.pickerShow()
            }
        }else if(dateTxtFld.isFirstResponder()){
            dateD = ""
            duration.removeAllObjects()
            fromToTime.removeAllObjects()
            fromToTime.removeAllObjects()
            fromAr.removeAllObjects()
            scheduleId.removeAllObjects()
            schIdAr.removeAllObjects()
            if(dateAr1.count == 0){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Select Month")
            }else{
             self.pickerShow()
            }
        }
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if((yearTxtFld.text != "") && (monthTxtFld.text != "") && (dateTxtFld.text != "")){
          viewSchTableVw.reloadData()
        }
    }
    func doneButton(sender:UIButton)
    {
        if(yearTxtFld.isFirstResponder()){
            if(dateY == ""){
                let monthAr = NSMutableArray()
                dateY = yearAr1[0] as! String
                let yearValue = dateY
                yearTxtFld.text = dateY as String
                let predicate = NSPredicate(format:"Year == %@",yearValue)
                let yearAr = dataArr.filteredArrayUsingPredicate(predicate)
                yearArr = yearAr
                
                for i in 0...yearAr.count-1 {
                    let d = yearAr[i]
                    monthAr.addObject(d.valueForKey("Month")!)
                }
                monthAr1 = uniq(monthAr as NSArray as! [String])
            }else{
             yearTxtFld.text = dateY as String
            }
        }else if(monthTxtFld.isFirstResponder()){
            if(dateM == ""){
                let days = NSMutableArray()
                dateM = monthAr1[0] as! String
                monthTxtFld.text = dateM as String
                let mnthValue = dateM
                let predicate = NSPredicate(format:"Month = %@",mnthValue)
                let mnthAr = yearArr.filteredArrayUsingPredicate(predicate)
                monthArr = mnthAr
                for i in 0...mnthAr.count-1 {
                    let d = mnthAr[i]
                    days.addObject(d.valueForKey("day")!)
                }
                dateAr1 = uniq(days as NSArray as! [String])
            }else{
             monthTxtFld.text = dateM as String
            }
        }else if(dateTxtFld.isFirstResponder()){
            if(dateD == ""){
                fromToTime.removeAllObjects()
                duration.removeAllObjects()
                dateD = dateAr1[0] as! String
                dateTxtFld.text = dateD as String
                let dateValue = dateD
                let predicate = NSPredicate(format:"day = %@",dateValue)
                let mnthAr = monthArr.filteredArrayUsingPredicate(predicate) as NSArray
                let schedule = mnthAr[0].valueForKeyPath("schedule.Schedule") as! NSArray
                let schId = mnthAr[0].valueForKeyPath("schedule._id") as! NSString
                scheduleId.addObject(schId)
                for j in 0...schedule.count-1 {
                    let dic = schedule[j]
                    let status = dic.valueForKey("Status") as! NSInteger
                    if(status == 1){
                        stausAr.addObject(status)
                        let from = dic.valueForKeyPath("Time.From") as! NSString
                        let to = dic.valueForKeyPath("Time.To") as! NSString
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "HH:mm"
                        let timeZone = NSTimeZone(name: "GMT-8")
                        dateFormatter.timeZone=timeZone
                        let schF:NSDate = dateFormatter.dateFromString("\(from)")!
                        let schT:NSDate = dateFormatter.dateFromString("\(to)")!
                        dateFormatter.dateFormat = "hh:mm a"
                        let from12hrStr = dateFormatter.stringFromDate(schF) as NSString
                        let to12hrStr = dateFormatter.stringFromDate(schT) as NSString
                        let fromTo = "\(from12hrStr)-\(to12hrStr)"
                        fromToTime.addObject(fromTo)
                        fromAr.addObject(from)
                        toAr.addObject(to)
                        let meetingSlot = dic.valueForKey("MeetingSlot") as! NSString
                        duration.addObject(meetingSlot)
                        let sch_id = dic.valueForKey("_id") as! NSString
                        schIdAr.addObject(sch_id)
                    }
                }
            }else{
             dateTxtFld.text = dateD as String
            }
        }
        dateTxtFld.resignFirstResponder()
        monthTxtFld.resignFirstResponder()
        yearTxtFld.resignFirstResponder()
    }
    func cancelBtn(sender:UIButton)
    {
        dateTxtFld.resignFirstResponder()
        monthTxtFld.resignFirstResponder()
        yearTxtFld.resignFirstResponder()
    }
    @IBAction func deleteBtnAc(sender: AnyObject) {
        let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To Delete This Schedule.", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default) {
            UIAlertAction in
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            let btnPos = sender.convertPoint(CGPointZero, toView: self.viewSchTableVw)
            let indexPath:NSIndexPath = self.viewSchTableVw.indexPathForRowAtPoint(btnPos)!
            self.removeIndex = indexPath
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("id") {
                AppManager.sharedManager.delegate=self
                let userType = defaults.valueForKey("usertype") as! NSInteger
                var createdId = NSString()
                if(userType == 1){
                    let userId = defaults.valueForKey("id") as! String
                    createdId = userId
                }else{
                    let userId = defaults.valueForKey("staffId") as! String
                    createdId = userId
                }
                let dateComp = "\(self.monthTxtFld.text!) \(self.dateTxtFld.text!), \(self.yearTxtFld.text!)"
                let firstname:String = defaults.valueForKey("firstName") as! String
                let lastname:String = defaults.valueForKey("lastName") as! String
                let fullname:String = "\(firstname) \(lastname)"
                let params = "Dr_id=\(userId)&DrScheduleId=\(self.scheduleId[0])&DrName=\(firstname)&From=\(self.fromAr[indexPath.row])&To=\(self.toAr[indexPath.row])&TimeSchedule=\(self.schIdAr[indexPath.row])&ScheduledDate=\(dateComp)&CancellationUserType=\(userType)&CancellationId=\(createdId)&CancellationName=\(fullname)"
                self.server = true
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrCancel")
            }
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    @IBAction func backBtnAc(sender: AnyObject) {
            self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return duration.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        cell!.layer.cornerRadius = 7.0
        cell!.layer.masksToBounds = true
        
        let imageView: UIView = (cell?.viewWithTag(1900))!
        imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
        imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
        imageView.layer.shadowOffset = CGSizeMake(10, 5);
        imageView.layer.shadowRadius = 1;
        imageView.layer.shadowOpacity = 0.06;
        imageView.layer.masksToBounds = false;
        imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        imageView.layer.borderWidth = 0.5
        
        let delBtn = cell?.viewWithTag(1904) as! UIButton
        delBtn.layer.cornerRadius = self.view.frame.size.height*0.009
        if(indexPath.row%2 == 0){
            imageView.backgroundColor = UIColor.whiteColor()
        }else{
            imageView.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
        }
        let timeLbl: UILabel = (cell!.viewWithTag(1902) as! UILabel)
        timeLbl.text = fromToTime[indexPath.row] as? String
        let durationlbl: UILabel = (cell!.viewWithTag(1903) as! UILabel)
        durationlbl.text = duration[indexPath.row] as? String
        return cell!
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let vc = storyboard!.instantiateViewControllerWithIdentifier("DetailScheduleViewController") as! DetailScheduleViewController
        let defaults = NSUserDefaults.standardUserDefaults()
        let userId = defaults.valueForKey("id") as! String
        vc.drId = userId
        vc.drSchId = scheduleId[0] as! NSString
        vc.sh_Id = schIdAr[indexPath.row] as! NSString
        vc.fromT = fromAr[indexPath.row] as! NSString
        vc.toT = toAr[indexPath.row] as! NSString
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return self.view.frame.size.width*0.1
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
