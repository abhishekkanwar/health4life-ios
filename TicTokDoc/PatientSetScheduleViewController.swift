

import UIKit
import SlideMenuControllerSwift
import IQKeyboardManagerSwift
import EventKit

class PatientSetScheduleViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,WebServiceDelegate {
    @IBOutlet weak var doctorVw: UIView!
    @IBOutlet weak var durationVw: UIView!
    @IBOutlet weak var timeVw: UIView!
    @IBOutlet weak var dateVw: UIView!
    @IBOutlet weak var monthVw: UIView!
    @IBOutlet weak var yearVw: UIView!
    
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var timeFld: UITextField!
    @IBOutlet weak var durationFld: UITextField!
    @IBOutlet weak var dateFld: UITextField!
    @IBOutlet weak var monthFld: UITextField!
    @IBOutlet weak var doctorFld: UITextField!
    @IBOutlet weak var yearFld: UITextField!
    
    var dateY             = String()
    var dateM             = String()
    var dateD             = String()
    var time              = String()
    var duration          = String()
    var doc               = String()
    var doctorEmail       = String()
    var appendString1     = String()
    var dr_id             = NSString()
    var docPicker         = NSArray()
    var dr_idAr           = NSArray()
    var docEmailAr        = NSArray()
    var dataArr           = NSArray()
    var yearCollectionAr  = NSArray()
    var dayCollectionAr   = NSArray()
    var monthCollectionAr = NSArray()
    var meetingPicker     = NSArray()
    var years             = NSArray()
    var months            = NSArray()
    var day               = NSArray()
    var bookAr            = NSString()
    var imgIcon           = ["bigSte","dateIcon","myAppointmnt","fromIcon"]
    var lblItem           = ["Doctor's Name","Date of appointment","Time","Duration"]
    var timeAr            = NSMutableArray()
    var timeAr2            = NSMutableArray()
    var userDr_id         = NSInteger()
    var timeFrom          = NSDate()
    var timeTo            = NSDate()
    var timeInterval      = NSDate()
    var minutesSlot       = NSInteger()
    var scheduleData      = NSArray()
    var deviceTknData     = NSMutableArray()
    var deviceTkn         = NSString()
    var alertInt = Double()
    @IBOutlet weak var setScheduleTableVw: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBarHidden = true
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            let userEmail = defaults.valueForKey("email")  as! String
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let params = "Patient_id=\(userId)"
            userDr_id = 1
            AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/PatientDrList")
        }
        
        setScheduleTableVw.delegate = self
        setScheduleTableVw.dataSource = self
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        doctorFld.delegate   = self
        yearFld.delegate     = self
        monthFld.delegate    = self
        durationFld.delegate = self
        timeFld.delegate     = self
        dateFld.delegate     = self
        
        doctorFld.tintColor   = UIColor.clearColor()
        yearFld.tintColor     = UIColor.clearColor()
        monthFld.tintColor    = UIColor.clearColor()
        durationFld.tintColor = UIColor.clearColor()
        timeFld.tintColor     = UIColor.clearColor()
        dateFld.tintColor     = UIColor.clearColor()
        
        doctorVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        doctorVw.layer.borderColor   = UIColor.whiteColor().CGColor
        doctorVw.layer.shadowOffset  = CGSizeMake(10, 5);
        doctorVw.layer.shadowRadius  = 1;
        doctorVw.layer.shadowOpacity = 0.06;
        doctorVw.backgroundColor     = UIColor.clearColor()
        doctorVw.layer.masksToBounds = false;
        doctorVw.layer.shadowColor   = UIColor.lightGrayColor().CGColor
        doctorVw.layer.borderWidth   = 0.5
        
        yearVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        yearVw.layer.borderColor   = UIColor.whiteColor().CGColor
        yearVw.layer.shadowOffset  = CGSizeMake(10, 5);
        yearVw.layer.shadowRadius  = 1;
        yearVw.layer.shadowOpacity = 0.06;
        yearVw.backgroundColor     = UIColor.clearColor()
        yearVw.layer.masksToBounds = false;
        yearVw.layer.shadowColor   = UIColor.lightGrayColor().CGColor
        yearVw.layer.borderWidth   = 0.5
        
        monthVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        monthVw.layer.borderColor   = UIColor.whiteColor().CGColor
        monthVw.layer.shadowOffset  = CGSizeMake(10, 5);
        monthVw.layer.shadowRadius  = 1;
        monthVw.layer.shadowOpacity = 0.06;
        monthVw.backgroundColor     = UIColor.clearColor()
        monthVw.layer.masksToBounds = false;
        monthVw.layer.shadowColor   = UIColor.lightGrayColor().CGColor
        monthVw.layer.borderWidth   = 0.5
        
        dateVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        dateVw.layer.borderColor   = UIColor.whiteColor().CGColor
        dateVw.layer.shadowOffset  = CGSizeMake(10, 5);
        dateVw.layer.shadowRadius  = 1;
        dateVw.layer.shadowOpacity = 0.06;
        dateVw.backgroundColor     = UIColor.clearColor()
        dateVw.layer.masksToBounds = false;
        dateVw.layer.shadowColor   = UIColor.lightGrayColor().CGColor
        dateVw.layer.borderWidth   = 0.5
        
        timeVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        timeVw.layer.borderColor   = UIColor.whiteColor().CGColor
        timeVw.layer.shadowOffset  = CGSizeMake(10, 5);
        timeVw.layer.shadowRadius  = 1;
        timeVw.layer.shadowOpacity = 0.06;
        timeVw.backgroundColor     = UIColor.clearColor()
        timeVw.layer.masksToBounds = false;
        timeVw.layer.shadowColor   = UIColor.lightGrayColor().CGColor
        timeVw.layer.borderWidth   = 0.5
        
        durationVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        durationVw.layer.borderColor   = UIColor.whiteColor().CGColor
        durationVw.layer.shadowOffset  = CGSizeMake(10, 5);
        durationVw.layer.shadowRadius  = 1;
        durationVw.layer.shadowOpacity = 0.06;
        durationVw.backgroundColor     = UIColor.clearColor()
        durationVw.layer.masksToBounds = false;
        durationVw.layer.shadowColor   = UIColor.lightGrayColor().CGColor
        durationVw.layer.borderWidth   = 0.5
        
        
        if(yearFld.text == "" && monthFld.text == "" && doctorFld.text == "" && durationFld.text == "" && timeFld.text == "" && dateFld.text == ""){
            confirmBtn.enabled = false
        }
        
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString)
    {
        let docMutableAr = NSMutableArray()
        let docidAr = NSMutableArray()
        let docEmail = NSMutableArray()
        let yearAr = NSMutableArray()
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        if(responseDict.objectForKey("data") != nil){
        if(userDr_id == 1){
        let dataDict = responseDict.objectForKey("data") as! NSArray
            deviceTknData.removeAllObjects()
         for i in 0...dataDict.count-1 {
           let d = dataDict[i]
            let status = d.valueForKey("status") as! NSInteger
            if(status == 1){
               docMutableAr.addObject(d.valueForKey("Name")!)
               docidAr.addObject(d.valueForKey("Dr_id")!)
                docEmail.addObject(d.valueForKey("Email")!)
                deviceTknData.addObject(d.valueForKey("DeviceToken")!)
            }
          }
            let docAr = docMutableAr as NSArray as! [String]
            docPicker  = uniq(docAr)
            let doc_Id_Ar = docidAr as NSArray as! [String]
            dr_idAr = uniq(doc_Id_Ar)
            let docemail = docEmail as NSArray as! [String]
            docEmailAr = uniq(docemail)
        }
        else if(userDr_id == 2){
           let dataDict = responseDict.objectForKey("data") as! NSArray
            dataArr = dataDict
            for i in 0...dataDict.count-1 {
               let d = dataDict[i]
               yearAr.addObject(d.valueForKey("Year")!)
            }
            let yearsFilter = yearAr as NSArray as! [String]
            years  = uniq(yearsFilter)
            }
          }
        if(userDr_id == 3){
            dispatch_async(dispatch_get_main_queue()) {
                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                backVw.tag = 2357
                self.view.addSubview(backVw)
                let alerVw = UIView()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.35)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.35)
                }else{
                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.2)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.27)
                }
                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                alerVw.backgroundColor = UIColor.whiteColor()
                backVw.addSubview(alerVw)
                
                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                rightImg.image = UIImage(named: "confirm")
                alerVw.addSubview(rightImg)
                
                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.6, self.view.frame.size.width*0.05))
                verificationLbl.text = " Appointment Scheduled"
                verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(verificationLbl)
                
                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                lineVw.backgroundColor = UIColor.grayColor()
                alerVw.addSubview(lineVw)
                let docName:String = self.doctorFld.text!
               
                let contentLbl = UILabel()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    contentLbl.font  = UIFont(name: "Helvetica", size:14)
                    contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.05,verificationLbl.frame.size.height+20, alerVw.frame.size.width-(alerVw.frame.size.width*0.1),alerVw.frame.size.height*0.6)
                }else{
                    contentLbl.font  = UIFont(name: "Helvetica", size:19)
                    contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.05,verificationLbl.frame.size.height+25, alerVw.frame.size.width-(alerVw.frame.size.width*0.1),alerVw.frame.size.height*0.48)
                }
                contentLbl.textAlignment = .Center
                contentLbl.textColor = UIColor.grayColor()
                contentLbl.text = "You have successfully scheduled a Video Visit Appointment with Dr. \(docName).Your doctor will be notified of your scheduled appointment.\nPlease log into Health4life App 10-15 minutes prior to your scheduled appointment and Dr. \(docName) will video call you!"
                contentLbl.numberOfLines = 11
                alerVw.addSubview(contentLbl)
                
                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.16),alerVw.frame.size.width*0.3, alerVw.frame.size.height*0.16))
                okLbl.setTitle("OK", forState: .Normal)
                if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                }else{
                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                }
                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                okLbl.titleLabel?.textAlignment = .Center
                okLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(okLbl)
                
                
                let lineLbl = UILabel.init(frame: CGRectMake(okLbl.frame.size.width-2,alerVw.frame.size.height-(alerVw.frame.size.height*0.16),1.2, alerVw.frame.size.height*0.16))
                lineLbl.backgroundColor = UIColor.lightGrayColor()
                alerVw.addSubview(lineLbl)
                
                
                
                let calenderInviteLbl = UIButton.init(frame: CGRectMake(okLbl.frame.size.width,alerVw.frame.size.height-(alerVw.frame.size.height*0.16),alerVw.frame.size.width*0.7, alerVw.frame.size.height*0.16))
                if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                    calenderInviteLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 18)
                }else{
                    calenderInviteLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 13)
                }
                calenderInviteLbl.setTitle("Copy Appointment to Calendar", forState: .Normal)
                calenderInviteLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                calenderInviteLbl.titleLabel?.textAlignment = .Center
                calenderInviteLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.calenderInvite), forControlEvents: .TouchUpInside)
                calenderInviteLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(calenderInviteLbl)
            }
        }
    }
    func calenderInvite(){
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd,yyyy, hh:mm a"
        let combinedString = "\(appendString1), \(time)"
        let timeAlert = dateFormatter.dateFromString("\(combinedString)")!
        if(duration == "15 Min"){
            alertInt = 15
        }
        else if(duration == "30 Min"){
            alertInt = 30
        }
        else if(duration == "90 Min"){
            alertInt = 90
        }
        else if(duration == "60 Min"){
            alertInt = 60
        }
            let store = EKEventStore()
            store.requestAccessToEntityType(.Event) {(granted, error) in
                    if !granted {
                        return
                      }
                        let event = EKEvent(eventStore: store)
                        event.title = "You Have an Appointment."
                        event.startDate = timeAlert //today
                        event.endDate = event.startDate.dateByAddingTimeInterval(self.alertInt*60) //1 hour long meeting
                        event.calendar = store.defaultCalendarForNewEvents
                        do {
                            try store.saveEvent(event, span: .ThisEvent, commit: true)
                            let savedEventId = event.eventIdentifier //save event id to access this particular event later
                            print(savedEventId)
                        } catch {
                            // Display error to user
                        }
                    }
        let view = self.view.viewWithTag(2357)! as UIView
        view.removeFromSuperview()
        yearFld.text     = ""
        monthFld.text    = ""
        doctorFld.text   = ""
        durationFld.text = ""
        timeFld.text     = ""
        dateFld.text     = ""
        
        dateY    = ""
        dateM    = ""
        dateD    = ""
        time     = ""
        duration = ""
        appendString1 = ""
        doc      = ""
        if(yearFld.text == "" && monthFld.text == "" && doctorFld.text == "" && durationFld.text == "" && timeFld.text == "" && dateFld.text == ""){
            confirmBtn.enabled = false
        }
        let myIP1 = NSIndexPath(forRow: 0, inSection: 0)
        let cell1 = setScheduleTableVw.cellForRowAtIndexPath(myIP1)
        let lbl1 = cell1?.viewWithTag(40) as! UILabel
        lbl1.text = nil
        
        let myIP2 = NSIndexPath(forRow: 1, inSection: 0)
        let cell2 = setScheduleTableVw.cellForRowAtIndexPath(myIP2)
        let lbl2 = cell2?.viewWithTag(41) as! UILabel
        lbl2.text = nil
        
        let myIP3 = NSIndexPath(forRow: 2, inSection: 0)
        let cell3 = setScheduleTableVw.cellForRowAtIndexPath(myIP3)
        let lbl3 = cell3?.viewWithTag(42) as! UILabel
        lbl3.text = nil
        
        let myIP4 = NSIndexPath(forRow: 3, inSection: 0)
        let cell4 = setScheduleTableVw.cellForRowAtIndexPath(myIP4)
        let lbl4 = cell4?.viewWithTag(43) as! UILabel
        lbl4.text = nil
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().enable = true
        let alert = UIAlertController(title: "Confirmed", message:"Your appointment was successfully saved to your default calendar.", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default) {
            UIAlertAction in
             self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
        }
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    func uniq<S: SequenceType, E: Hashable where E==S.Generator.Element>(source: S) -> [E] {
        var seen: [E:Bool] = [:]
        return source.filter { seen.updateValue(true, forKey: $0) == nil }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        if(textField == doctorFld){
            if(docPicker.count == 0){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "You are not Verified by any doctor yet. kindly wait or send a new request")
            }else{
                self.pickerShow()
                doctorFld.text = ""
                yearFld.text = ""
                monthFld.text = ""
                dateFld.text = ""
                durationFld.text = ""
                timeFld.text = ""
                dateY = ""
                dateM = ""
                dateD = ""
                time = ""
                duration = ""
                doc = ""
            }
        }else if(textField == yearFld){
            if(doctorFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Select The Doctor First.")
                return
            }
            else if(years.count == 0){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Sorry for the inconvenience! Doctor has yet to set their available timings.")
                return
            }else{
                self.pickerShow()
                monthFld.text = ""
                yearFld.text = ""
                dateFld.text = ""
                timeFld.text = ""
                durationFld.text = ""
                dateY = ""
                dateM = ""
                dateD = ""
                time = ""
                duration = ""
            }
        }else if(textField == monthFld){
            if(doctorFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Select The Doctor First.")
                return
            }
           else if(yearFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select year")
                return
            }
            else if(months.count == 0){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Sorry for the inconvenience! Doctor has yet to set their available timings.")
                return
            }else{
                self.pickerShow()
                monthFld.text = ""
                dateFld.text = ""
                timeFld.text = ""
                durationFld.text = ""
                dateM = ""
                dateD = ""
                time = ""
                duration = ""
            }
        }else if(textField == dateFld){
            if(doctorFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Select The Doctor First.")
                return
            }
            else if(yearFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select year")
                return
            }
           else if(monthFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select Month")
                return
            }
            else if(day.count == 0){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Sorry for the inconvenience! Doctor has yet to set their available timings.")
                return
            }else{
                self.pickerShow()
                dateFld.text = ""
                timeFld.text = ""
                durationFld.text = ""
                dateD = ""
                time = ""
                duration = ""
                
            }
        }else if(textField == durationFld){
            if(doctorFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Select The Doctor First.")
                return
            }
            else if(yearFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select year")
                return
            }
            else if(monthFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select Month")
                return
            }
            else if(dateFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select Date")
                return
            }
            else if(meetingPicker.count == 0){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Sorry for the inconvenience! Doctor has yet to set their available timings.")
                return
            }else{
                self.pickerShow()
                timeFld.text = ""
                durationFld.text = ""
                time = ""
                duration = ""
            }
        }else if(textField == timeFld){
            if(doctorFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Select The Doctor First.")
                return
            }
            else if(yearFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select Year")
                return
            }
            else if(monthFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select Month")
                return
            }
            else if(dateFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select Date")
                return
            }
           else if(durationFld.text == ""){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select Duration")
            }
            else if(timeAr.count == 0){
                self.pickerShowHide()
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Sorry for the inconvenience! Doctor has yet to set their available timings.")
            }else{
                self.pickerShow()
                timeFld.text = ""
                time = ""
            }
        }
    }
     func pickerShow(){
        let InputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0.4))
        
        let PickerVw  : UIPickerView = UIPickerView(frame: CGRectMake(0, 40,UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height*0.32))
        PickerVw.delegate            = self
        PickerVw.dataSource          = self
        InputView.addSubview(PickerVw)
        
        let doneButton = UIButton(frame: CGRectMake((self.view.frame.size.width-100), 0, 100, 50))
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        doneButton.setTitle("Done", forState: UIControlState.Highlighted)
        doneButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        doneButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        InputView.addSubview(doneButton)
        doneButton.addTarget(self, action: #selector(PatientSetScheduleViewController.doneButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let cancelBtn = UIButton(frame: CGRectMake(0, 0, 100, 50))
        cancelBtn.setTitle("Cancel", forState: UIControlState.Normal)
        cancelBtn.setTitle("Cancel", forState: UIControlState.Highlighted)
        cancelBtn.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        cancelBtn.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        InputView.addSubview(cancelBtn)
        cancelBtn.addTarget(self, action: #selector(PatientSetScheduleViewController.cancelBtn(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        if(doctorFld.isFirstResponder()){
             doctorFld.inputView = InputView
        }else if(yearFld.isFirstResponder()){
             yearFld.inputView = InputView
        }else if(monthFld.isFirstResponder()){
             monthFld.inputView = InputView
        }else if(dateFld.isFirstResponder()){
             dateFld.inputView = InputView
        }else if(durationFld.isFirstResponder()){
             durationFld.inputView = InputView
        }else if(timeFld.isFirstResponder()){
             timeFld.inputView = InputView
        }
       
 
    }
    func pickerShowHide(){
        let InputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0.0))
        
        let PickerVw  : UIPickerView = UIPickerView(frame: CGRectMake(0, 40,UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height*0.32))
        PickerVw.delegate            = self
        PickerVw.dataSource          = self
        InputView.addSubview(PickerVw)
        
        let doneButton = UIButton(frame: CGRectMake((self.view.frame.size.width-100), 0, 100, 50))
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        doneButton.setTitle("Done", forState: UIControlState.Highlighted)
        doneButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        doneButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        InputView.addSubview(doneButton)
        doneButton.addTarget(self, action: #selector(PatientSetScheduleViewController.doneButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let cancelBtn = UIButton(frame: CGRectMake(0, 0, 100, 50))
        cancelBtn.setTitle("Cancel", forState: UIControlState.Normal)
        cancelBtn.setTitle("Cancel", forState: UIControlState.Highlighted)
        cancelBtn.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        cancelBtn.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        InputView.addSubview(cancelBtn)
        cancelBtn.addTarget(self, action: #selector(PatientSetScheduleViewController.cancelBtn(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        if(doctorFld.isFirstResponder()){
            doctorFld.inputView = InputView
        }else if(yearFld.isFirstResponder()){
            yearFld.inputView = InputView
        }else if(monthFld.isFirstResponder()){
            monthFld.inputView = InputView
        }else if(dateFld.isFirstResponder()){
            dateFld.inputView = InputView
        }else if(durationFld.isFirstResponder()){
            durationFld.inputView = InputView
        }else if(timeFld.isFirstResponder()){
            timeFld.inputView = InputView
        }
        
        
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(durationFld.isFirstResponder()){
          return meetingPicker.count
        }
        else if(doctorFld.isFirstResponder()){
          return docPicker.count
        }
        else if(monthFld.isFirstResponder()){
            return months.count
        }else if(yearFld.isFirstResponder()){
            return years.count
        }else if(dateFld.isFirstResponder()){
            return day.count
        }
        else{
            return timeAr.count
        }
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(durationFld.isFirstResponder()){
            return meetingPicker[row] as? String
        }
        else if(doctorFld.isFirstResponder()){
            return docPicker[row] as? String
        }
        else if(monthFld.isFirstResponder()){
            return months[row] as? String
        }else if(yearFld.isFirstResponder()){
            return years[row] as? String
        }else if(dateFld.isFirstResponder()){
            return day[row] as? String
        }
        else if(timeFld.isFirstResponder()){
            return timeAr[row] as? String
        }
        return nil
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if(durationFld.isFirstResponder()){
            let statusAr = NSMutableArray()
            var durationInt = NSInteger()
            timeAr.removeAllObjects()
            timeAr2.removeAllObjects()
            var meetingSlotAr = []
            duration = (meetingPicker[row] as? String)!
            var addTime = Double()
            if(duration == "15 Min"){
                durationInt = 15
                addTime = 15
                let durationValue = duration
                let predicate = NSPredicate(format:"MeetingSlot CONTAINS[cd] %@",durationValue)
                meetingSlotAr = scheduleData.filteredArrayUsingPredicate(predicate)
            }
            else if(duration == "30 Min"){
                durationInt = 30
                addTime = 15
                let durationValue = duration
                let predicate = NSPredicate(format:"MeetingSlot CONTAINS[cd] %@",durationValue)
                meetingSlotAr = scheduleData.filteredArrayUsingPredicate(predicate)
            }
            else if(duration == "90 Min"){
                durationInt = 90
                addTime = 75
                let durationValue = duration
                let predicate = NSPredicate(format:"MeetingSlot CONTAINS[cd] %@",durationValue)
                meetingSlotAr = scheduleData.filteredArrayUsingPredicate(predicate)
            }
            else if(duration == "60 Min"){
                durationInt = 60
                addTime = 45
                let durationValue = duration
                let predicate = NSPredicate(format:"MeetingSlot CONTAINS[cd] %@",durationValue)
                meetingSlotAr = scheduleData.filteredArrayUsingPredicate(predicate)
                
            }
            let booked = dayCollectionAr[0].objectForKey("Booking") as! NSArray
            var fromTime = NSDate()
            var toTime = NSDate()
            if (booked.count != 0){
                for bookMin in 0...booked.count-1 {
                    statusAr.addObject(booked[bookMin].objectForKey("Status")!)
                    if(statusAr[bookMin] as! NSObject == 1){
                        let bookedfrom = booked[bookMin].objectForKey("From") as! String
                        let bookedTo = booked[bookMin].objectForKey("To") as! String
                        let dateFormatter1 = NSDateFormatter()
                        dateFormatter1.timeStyle = .ShortStyle
                        dateFormatter1.dateFormat = "HH:mm" // k = Hour in 1~24, mm = Minute
                        let timeZone = NSTimeZone(name: "GMT-8")
                        dateFormatter1.timeZone=timeZone
                        fromTime = dateFormatter1.dateFromString("\(bookedfrom)")!
                        toTime = dateFormatter1.dateFromString("\(bookedTo)")!
                        let timeDif =  (toTime.timeIntervalSinceDate(fromTime))
                        let i = Int(timeDif)
                        var arCount = (i/60)/15
                        var q = NSDate()
                        if(duration != "15 Min"){
                            fromTime = fromTime.dateByAddingTimeInterval(addTime*(-60))
                            if(addTime == 15){
                                arCount += 1
                            }
                            else if(addTime == 45){
                                arCount += 3
                            }
                            else if(addTime == 75){
                                arCount += 5
                            }
                             q = fromTime
                        }
                        else{
                            q = fromTime
                        }
                        for i in 0...arCount-1 {
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "hh:mm a"
                            let timeZone = NSTimeZone(name: "GMT-8")
                            dateFormatter.timeZone=timeZone
                            let timestring = dateFormatter.stringFromDate(q)
                            timeAr2.addObject(timestring)
                            q = q.dateByAddingTimeInterval(15*60)
                        }
                    }
                }
            }
            for min in 0...meetingSlotAr.count-1 {
                let meetingSlots = meetingSlotAr[min]
                let timeSlot = meetingSlots.objectForKey("Time") as! NSDictionary
                let toTimeString = timeSlot.valueForKey("To") as! NSString
                let fromTimeString = timeSlot.valueForKey("From") as! NSString
                let dateFormatter = NSDateFormatter()
                dateFormatter.timeStyle = .ShortStyle
                dateFormatter.dateFormat = "HH:mm" // k = Hour in 1~24, mm = Minute
                let timeZone = NSTimeZone(name: "GMT-8")
                dateFormatter.timeZone=timeZone
                timeTo = dateFormatter.dateFromString("\(toTimeString)")!
                timeFrom = dateFormatter.dateFromString("\(fromTimeString)")!
                let timeI =  (timeTo.timeIntervalSinceDate(timeFrom))
                let i = Int(timeI)
                minutesSlot = i/60
                let arCount = (minutesSlot-durationInt)/15
                var q = timeFrom
                if(arCount != 0 && arCount > 0){
                    for i in 0...arCount {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "hh:mm a"
                        let timeZone = NSTimeZone(name: "GMT-8")
                        dateFormatter.timeZone=timeZone
                        let timestring = dateFormatter.stringFromDate(q)
                        timeAr.addObject(timestring)
                        if(timeAr2.count != 0){
                            if(timeAr.containsObject(timeAr2.objectAtIndex(0))){
                                timeAr.removeObject(timeAr2.objectAtIndex(0))
                                timeAr2.removeObjectAtIndex(0)
                            }
                        }
                        q = q.dateByAddingTimeInterval(15*60)
                    }
                }
                else{
                    let dateFormatter2 = NSDateFormatter()
                    dateFormatter2.dateFormat = "hh:mm a"
                    let timeZone = NSTimeZone(name: "GMT-8")
                    dateFormatter2.timeZone=timeZone
                    let timeF = dateFormatter2.stringFromDate(q)
                    var hours = timeF.componentsSeparatedByString(":")
                    if(hours[0] == "24"){
                        if(timeF.containsString("24")){
                            let hoursf = (timeF as NSString).stringByReplacingOccurrencesOfString("24", withString: "00")
                            timeAr.addObject(hoursf)
                        }
                    }else{
                        timeAr.addObject(dateFormatter2.stringFromDate(q))
                    }
                }
            }
            if(timeAr2.count != 0){
                for removeAr in 0...timeAr2.count-1 {
                    if(timeAr.containsObject(timeAr2.objectAtIndex(removeAr))){
                        timeAr.removeObject(timeAr2.objectAtIndex(removeAr))
                    }
                }
            }
        }
        else if(doctorFld.isFirstResponder()){
            doc = docPicker[row] as! String
            dr_id = dr_idAr[row] as! NSString
            doctorEmail = docEmailAr[row] as! String
            deviceTkn = deviceTknData[row] as! NSString
        }
        else if(monthFld.isFirstResponder()){
            let days = NSMutableArray()
            dateM = months[row] as! String
            let mnthValue = dateM
            let predicate = NSPredicate(format:"Month = %@",mnthValue)
            let mnthAr = yearCollectionAr.filteredArrayUsingPredicate(predicate)
            monthCollectionAr = mnthAr
            for i in 0...mnthAr.count-1 {
                let d = mnthAr[i]
                days.addObject(d.valueForKey("Date")!)
            }
            day = uniq(days as NSArray as! [String])
        }
        else if(yearFld.isFirstResponder()){
            let mnths = NSMutableArray()
            dateY = years[row] as! String
            let yearValue = dateY
            let predicate = NSPredicate(format:"Year == %@",yearValue)
            let yearAr = dataArr.filteredArrayUsingPredicate(predicate)
            yearCollectionAr = yearAr
            for i in 0...yearAr.count-1 {
                let d = yearAr[i]
                mnths.addObject(d.valueForKey("Month")!)
            }
            months = uniq(mnths as NSArray as! [String])
        }
        else if(dateFld.isFirstResponder()){
            let scheduleAr = NSMutableArray()
            dateD = day[row] as! String
            let dayValue = dateD
            let predicate = NSPredicate(format:"Date == %@",dayValue)
            let dayAr = monthCollectionAr.filteredArrayUsingPredicate(predicate)
            dayCollectionAr = dayAr
            for i in 0...dayAr.count-1 {
                let d = dayAr[i]
                bookAr = d.valueForKey("BookId") as! NSString
                scheduleAr.addObject(d.valueForKey("Sloat")!)
                scheduleData = d.valueForKey("Schedule") as! NSArray
            }
            meetingPicker = scheduleAr[0] as! NSArray
        }
        else if(timeFld.isFirstResponder()){
            time = timeAr[row] as! String
        }
    }
   
    func doneButton(sender:UIButton)
    {
        if( monthFld.isFirstResponder() ){
            if(dateM == ""){
              monthFld.text = months[0] as? String
                let days = NSMutableArray()
                let mnthValue =  monthFld.text
                let predicate = NSPredicate(format:"Month = %@",mnthValue!)
                let mnthAr = yearCollectionAr.filteredArrayUsingPredicate(predicate)
                monthCollectionAr = mnthAr
                for i in 0...mnthAr.count-1 {
                    let d = mnthAr[i]
                    days.addObject(d.valueForKey("Date")!)
                }
                day = uniq(days as NSArray as! [String])
            }else{
               monthFld.text = dateM
            }
            monthFld.resignFirstResponder()
        }
        else if(dateFld.isFirstResponder()){
            if(dateD == ""){
              dateFld.text = day[0] as? String
                let scheduleAr = NSMutableArray()
                let dayValue = dateFld.text
                let predicate = NSPredicate(format:"Date == %@",dayValue!)
                let dayAr = monthCollectionAr.filteredArrayUsingPredicate(predicate)
                dayCollectionAr = dayAr
                for i in 0...dayAr.count-1 {
                    let d = dayAr[i]
                    bookAr = d.valueForKey("BookId") as! NSString
                    scheduleAr.addObject(d.valueForKey("Sloat")!)
                    scheduleData = d.valueForKey("Schedule") as! NSArray
                }
                meetingPicker = scheduleAr[0] as! NSArray
            }else{
              dateFld.text = dateD
            }
            dateFld.resignFirstResponder()
        }
        else if( yearFld.isFirstResponder()){
            if(dateY == ""){
                yearFld.text = years[0] as? String
                let mnths = NSMutableArray()
                let yearValue = yearFld.text
                let predicate = NSPredicate(format:"Year == %@",yearValue!)
                let yearAr = dataArr.filteredArrayUsingPredicate(predicate)
                yearCollectionAr = yearAr
                for i in 0...yearAr.count-1 {
                    let d = yearAr[i]
                    mnths.addObject(d.valueForKey("Month")!)
                }
                months = uniq(mnths as NSArray as! [String])
            }else{
                 yearFld.text = dateY
            }
           
            yearFld.resignFirstResponder()
        }
        else if(timeFld.isFirstResponder()){
            if(time == ""){
                timeFld.text = timeAr[0] as? String
                time = timeFld.text!
            }else{
                timeFld.text = time
            }
            timeFld.resignFirstResponder()
        }
        else if(doctorFld.isFirstResponder()){
            if(doc == ""){
                dr_id = dr_idAr[0] as! NSString
                doctorEmail = docEmailAr[0] as! String
                doctorFld.text = docPicker[0] as? String
            }else{
               doctorFld.text = doc
            }
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let params = "Dr_Id=\(dr_id)"
            userDr_id = 2
            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrScheduleList")
            doctorFld.resignFirstResponder()
        }
        else if(durationFld.isFirstResponder()){
            if(duration == ""){
                let statusAr = NSMutableArray()
                var durationInt = NSInteger()
                timeAr.removeAllObjects()
                timeAr2.removeAllObjects()
                var meetingSlotAr = []
                durationFld.text = meetingPicker[0] as? String
                duration = durationFld.text!
                var addTime = Double()
                if(duration == "15 Min"){
                    durationInt = 15
                    addTime = 15
                    let durationValue = duration
                    let predicate = NSPredicate(format:"MeetingSlot CONTAINS[cd] %@",durationValue)
                    meetingSlotAr = scheduleData.filteredArrayUsingPredicate(predicate)
                }
                else if(duration == "30 Min"){
                    durationInt = 30
                    addTime = 15
                    let durationValue = duration
                    let predicate = NSPredicate(format:"MeetingSlot CONTAINS[cd] %@",durationValue)
                    meetingSlotAr = scheduleData.filteredArrayUsingPredicate(predicate)
                }
                else if(duration == "90 Min"){
                    durationInt = 90
                    addTime = 75
                    let durationValue = duration
                    let predicate = NSPredicate(format:"MeetingSlot CONTAINS[cd] %@",durationValue)
                    meetingSlotAr = scheduleData.filteredArrayUsingPredicate(predicate)
                }
                else if(duration == "60 Min"){
                    durationInt = 60
                    addTime = 45
                    let durationValue = duration
                    let predicate = NSPredicate(format:"MeetingSlot CONTAINS[cd] %@",durationValue)
                    meetingSlotAr = scheduleData.filteredArrayUsingPredicate(predicate)
                    
                }
                let booked = dayCollectionAr[0].objectForKey("Booking") as! NSArray
                var fromTime = NSDate()
                var toTime = NSDate()
                if (booked.count != 0){
                    for bookMin in 0...booked.count-1 {
                        statusAr.addObject(booked[bookMin].objectForKey("Status")!)
                        if(statusAr[bookMin] as! NSObject == 1){
                            let bookedfrom = booked[bookMin].objectForKey("From") as! String
                            let bookedTo = booked[bookMin].objectForKey("To") as! String
                            let dateFormatter1 = NSDateFormatter()
                            dateFormatter1.timeStyle = .ShortStyle
                            dateFormatter1.dateFormat = "HH:mm" // k = Hour in 1~24, mm = Minute
                            let timeZone = NSTimeZone(name: "GMT-8")
                            dateFormatter1.timeZone=timeZone
                            fromTime = dateFormatter1.dateFromString("\(bookedfrom)")!
                            toTime = dateFormatter1.dateFromString("\(bookedTo)")!
                            let timeDif =  (toTime.timeIntervalSinceDate(fromTime))
                            let i = Int(timeDif)
                            var arCount = (i/60)/15
                            var q = NSDate()
                            
                            if(duration != "15 Min"){
                                fromTime = fromTime.dateByAddingTimeInterval(addTime*(-60))
                                if(addTime == 15){
                                    arCount += 1
                                }
                                else if(addTime == 45){
                                    arCount += 3
                                }
                                else if(addTime == 75){
                                    arCount += 5
                                }
                                q = fromTime
                            }
                            else{
                                q = fromTime
                            }
                            for i in 0...arCount-1 {
                                let dateFormatter = NSDateFormatter()
                                dateFormatter.dateFormat = "hh:mm a"
                                let timeZone = NSTimeZone(name: "GMT-8")
                                dateFormatter.timeZone=timeZone
                                let timestring = dateFormatter.stringFromDate(q)
                                timeAr2.addObject(timestring)
                                q = q.dateByAddingTimeInterval(15*60)
                            }
                        }
                    }
                }
                
                for min in 0...meetingSlotAr.count-1 {
                    let meetingSlots = meetingSlotAr[min]
                    let timeSlot = meetingSlots.objectForKey("Time") as! NSDictionary
                    let toTimeString = timeSlot.valueForKey("To") as! NSString
                    let fromTimeString = timeSlot.valueForKey("From") as! NSString
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.timeStyle = .ShortStyle
                    dateFormatter.dateFormat = "HH:mm" // k = Hour in 1~24, mm = Minute
                    let timeZone = NSTimeZone(name: "GMT-8")
                    dateFormatter.timeZone=timeZone
                    timeTo = dateFormatter.dateFromString("\(toTimeString)")!
                    timeFrom = dateFormatter.dateFromString("\(fromTimeString)")!
                    let timeI =  (timeTo.timeIntervalSinceDate(timeFrom))
                    let i = Int(timeI)
                    minutesSlot = i/60
                    let arCount = (minutesSlot-durationInt)/15
                    var q = timeFrom
                    if(arCount != 0 && arCount > 0){
                            for i in 0...arCount {
                                let dateFormatter2 = NSDateFormatter()
                                dateFormatter2.dateFormat = "hh:mm a"
                                let timeZone = NSTimeZone(name: "GMT-8")
                                dateFormatter2.timeZone=timeZone
                                let timestring = dateFormatter2.stringFromDate(q)
                                timeAr.addObject(timestring)
                                if(timeAr2.count != 0){
                                    if(timeAr.containsObject(timeAr2.objectAtIndex(0))){
                                        timeAr.removeObject(timeAr2.objectAtIndex(0))
                                        timeAr2.removeObjectAtIndex(0)
                                    }
                                }
                                q = q.dateByAddingTimeInterval(15*60)
                            }
                    }
                    else{
                        let dateFormatter2 = NSDateFormatter()
                        dateFormatter2.dateFormat = "hh:mm a"
                        let timeZone = NSTimeZone(name: "GMT-8")
                        dateFormatter2.timeZone=timeZone
                            let timeF = dateFormatter2.stringFromDate(q)
                            var hours = timeF.componentsSeparatedByString(":")
                            if(hours[0] == "24"){
                                if(timeF.containsString("24")){
                                    let hoursf = (timeF as NSString).stringByReplacingOccurrencesOfString("24", withString: "00")
                                    timeAr.addObject(hoursf)
                                }
                            }else{
                               timeAr.addObject(dateFormatter2.stringFromDate(q))
                            }
                        }
                    }
                    if(timeAr2.count != 0){
                        for removeAr in 0...timeAr2.count-1 {
                            if(timeAr.containsObject(timeAr2.objectAtIndex(removeAr))){
                                timeAr.removeObject(timeAr2.objectAtIndex(removeAr))
                            }
                        }
                    }
            }else{
               durationFld.text = duration
            }
            durationFld.resignFirstResponder()
        }
        if(yearFld.text != "" && monthFld.text != "" && doctorFld.text != "" && durationFld.text != "" && timeFld.text != "" && dateFld.text != ""){
            confirmBtn.enabled = true
        }
        if(yearFld.text == "" || monthFld.text == "" || doctorFld.text == "" ||  durationFld.text == "" || timeFld.text == "" || dateFld.text == ""){
            confirmBtn.enabled = false
        }
        if(dateFld.text != "" && monthFld.text != "" && yearFld.text != ""){
            let mnth = monthFld.text!
            let yr = yearFld.text!
            let dat = dateFld.text!
        appendString1 = "\(mnth) \(dat),\(yr)"
        }
        yearFld.resignFirstResponder()
        monthFld.resignFirstResponder()
        doctorFld.resignFirstResponder()
        durationFld.resignFirstResponder()
        timeFld.resignFirstResponder()
        dateFld.resignFirstResponder()
        setScheduleTableVw.reloadData()
    }
    func cancelBtn(sender:UIButton)
    {
        yearFld.resignFirstResponder()
        monthFld.resignFirstResponder()
        doctorFld.resignFirstResponder()
        durationFld.resignFirstResponder()
        timeFld.resignFirstResponder()
        dateFld.resignFirstResponder()
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().enable = true
         self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(indexPath.row == 0){
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        cell!.layer.cornerRadius = 7.0
        cell!.layer.masksToBounds = true
        let imageView: UIView = (cell?.viewWithTag(2360))!
        imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
        imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
        imageView.layer.shadowOffset = CGSizeMake(10, 5);
        imageView.layer.shadowRadius = 1;
        imageView.layer.shadowOpacity = 0.06;
        imageView.backgroundColor = UIColor.whiteColor()
        imageView.layer.masksToBounds = false;
        imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        imageView.layer.borderWidth = 0.5
        imageView.backgroundColor = UIColor.whiteColor()
            let docLbl: UILabel = (cell!.viewWithTag(40) as! UILabel)
            docLbl.text = doctorFld.text!
        return cell!
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCellWithIdentifier("cell1")
            cell!.layer.cornerRadius = 7.0
            cell!.layer.masksToBounds = true
            let imageView: UIView = (cell?.viewWithTag(2370))!
            imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
            imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
            imageView.layer.shadowOffset = CGSizeMake(10, 5);
            imageView.layer.shadowRadius = 1;
            imageView.layer.shadowOpacity = 0.06;
            imageView.backgroundColor = UIColor.whiteColor()
            imageView.layer.masksToBounds = false;
            imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
            imageView.layer.borderWidth = 0.5
            imageView.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            let dateLbl: UILabel = (cell!.viewWithTag(41) as! UILabel)
            dateLbl.text = appendString1

            return cell!
        }else if(indexPath.row == 2){
            let cell = tableView.dequeueReusableCellWithIdentifier("cell2")
            cell!.layer.cornerRadius = 7.0
            cell!.layer.masksToBounds = true
            let imageView: UIView = (cell?.viewWithTag(2380))!
            imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
            imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
            imageView.layer.shadowOffset = CGSizeMake(10, 5);
            imageView.layer.shadowRadius = 1;
            imageView.layer.shadowOpacity = 0.06;
            imageView.backgroundColor = UIColor.whiteColor()
            imageView.layer.masksToBounds = false;
            imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
            imageView.layer.borderWidth = 0.5
            imageView.backgroundColor = UIColor.whiteColor()
            let timeLbl: UILabel = (cell!.viewWithTag(42) as! UILabel)
            timeLbl.text = time
            return cell!
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("cell3")
            cell!.layer.cornerRadius = 7.0
            cell!.layer.masksToBounds = true
            let imageView: UIView = (cell?.viewWithTag(2390))!
            imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
            imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
            imageView.layer.shadowOffset = CGSizeMake(10, 5);
            imageView.layer.shadowRadius = 1;
            imageView.layer.shadowOpacity = 0.06;
            imageView.backgroundColor = UIColor.whiteColor()
            imageView.layer.masksToBounds = false;
            imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
            imageView.layer.borderWidth = 0.5
            imageView.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            let durationLbl: UILabel = (cell!.viewWithTag(43) as! UILabel)
            durationLbl.text = duration
            return cell!
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return setScheduleTableVw.frame.size.height*0.25
    }
    @IBAction func confirmBtnAc(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd,yyyy"
        let timeZone = NSTimeZone(name: "GMT-8")
        dateFormatter.timeZone=timeZone
        let schDate = dateFormatter.dateFromString("\(appendString1)")!
        let dateC = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "MMMM dd,yyyy"
        let defaultTimeZoneStr = formatter.stringFromDate(dateC)
        let currentD1 = dateFormatter.dateFromString("\(defaultTimeZoneStr)")!
        switch schDate.compare(currentD1) {
        case .OrderedAscending     :
            print("schDate is small than currentD1")
        case .OrderedDescending
            :   print("schDate is greater than date currentD1")
            dateFormatter.dateFormat = "hh:mm a"
            let schT = dateFormatter.dateFromString(timeFld.text! as String)!
            
        case .OrderedSame          :
            print("The two dates are the same")
            let timestamp1 = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .NoStyle, timeStyle: .ShortStyle)
            dateFormatter.timeStyle = .ShortStyle
            let fmt = NSDateFormatter.dateFormatFromTemplate("jm", options: 0, locale: NSLocale.currentLocale())!
            dateFormatter.dateFormat = "\(fmt)"
            let currentT:NSDate = dateFormatter.dateFromString("\(timestamp1)")!
            dateFormatter.dateFormat = "hh:mm a"
            let date12 = dateFormatter.dateFromString(timeFld.text! as String)!
            dateFormatter.dateFormat = "hh:mm a"
            let date22 = dateFormatter.stringFromDate(date12)
            let schT:NSDate = dateFormatter.dateFromString("\(date22)")!
            switch schT.compare(currentT) {
            case .OrderedAscending     :
                print("schT is small than currentT")
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select a time greater than the current time.")
                return
            case .OrderedDescending
                :   print("schT is greater than currentT")
            case .OrderedSame          :
                print("The two dates are the same")
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select a time greater than the Current time.")
                return
            }
        }
        dateFormatter.dateFormat = "hh:mm a"
        let date12Hr = dateFormatter.dateFromString(timeFld.text! as String)!
        dateFormatter.dateFormat = "HH:mm"
        let date24Hr = dateFormatter.stringFromDate(date12Hr)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
        AppManager.sharedManager.delegate=self
        let strSplit = durationFld.text!.characters.split(" ")
       let splitDuration = String(strSplit.first!)
        let splitIntDur:Int = Int(splitDuration)!
        let userEmail = defaults.valueForKey("email")  as! String
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
            let firstname:String = defaults.valueForKey("firstName") as! String
            let lastname:String = defaults.valueForKey("lastName") as! String
        let params = "Book_Id=\(bookAr)&PatientId=\(userId)&DrId=\(dr_id)&From=\(date24Hr)&Sloat=\(splitIntDur)&Date=\(appendString1)&DeviceToken=\(deviceTkn)&PatientName=\(firstname) \(lastname)&DrEmail=\(doctorEmail)&PatientEmail=\(userEmail)"
        userDr_id = 3
        AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/PatientBookDr")
        }
    }
    func okBtnAc(){
      let view = self.view.viewWithTag(2357)! as UIView
        view.removeFromSuperview()
        yearFld.text     = ""
        monthFld.text    = ""
        doctorFld.text   = ""
        durationFld.text = ""
        timeFld.text     = ""
        dateFld.text     = ""
        
        dateY    = ""
        dateM    = ""
        dateD    = ""
        time     = ""
        duration = ""
        appendString1 = ""
        doc      = ""
        if(yearFld.text == "" && monthFld.text == "" && doctorFld.text == "" && durationFld.text == "" && timeFld.text == "" && dateFld.text == ""){
            confirmBtn.enabled = false
        }
        let myIP1 = NSIndexPath(forRow: 0, inSection: 0)
        let cell1 = setScheduleTableVw.cellForRowAtIndexPath(myIP1)
        let lbl1 = cell1?.viewWithTag(40) as! UILabel
        lbl1.text = nil
        
        let myIP2 = NSIndexPath(forRow: 1, inSection: 0)
        let cell2 = setScheduleTableVw.cellForRowAtIndexPath(myIP2)
        let lbl2 = cell2?.viewWithTag(41) as! UILabel
        lbl2.text = nil
        
        let myIP3 = NSIndexPath(forRow: 2, inSection: 0)
        let cell3 = setScheduleTableVw.cellForRowAtIndexPath(myIP3)
        let lbl3 = cell3?.viewWithTag(42) as! UILabel
        lbl3.text = nil
        
        let myIP4 = NSIndexPath(forRow: 3, inSection: 0)
        let cell4 = setScheduleTableVw.cellForRowAtIndexPath(myIP4)
        let lbl4 = cell4?.viewWithTag(43) as! UILabel
        lbl4.text = nil
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        IQKeyboardManager.sharedManager().enable = false
        self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension Array where Element : Hashable {
    var unique: [Element] {
        return Array(Set(self))
    }
}
extension PatientSetScheduleViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
