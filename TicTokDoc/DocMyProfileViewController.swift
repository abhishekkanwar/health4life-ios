

import UIKit
import SlideMenuControllerSwift
import IQKeyboardManagerSwift
import CoreLocation
import GooglePlaces
import Toast_Swift
class DocMyProfileViewController: UIViewController,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,WebServiceDelegate,UISearchBarDelegate,CLLocationManagerDelegate,UIActionSheetDelegate{
    
    @IBOutlet weak var firstNameVw: UIView!
    @IBOutlet weak var lastNameVw: UIView!
    @IBOutlet weak var emailAddVw: UIView!
    @IBOutlet weak var codeVw: UIView!
    @IBOutlet weak var phonenumberVw: UIView!
    @IBOutlet weak var addressVw: UIView!
    @IBOutlet weak var zipcodeVw: UIView!
    @IBOutlet weak var qualificationVw: UIView!
    @IBOutlet weak var specialityVw: UIView!
    @IBOutlet weak var languageVw: UIView!
    @IBOutlet weak var bioVw: UIView!
    
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    
    @IBOutlet weak var dr_Code: UITextView!
    @IBOutlet weak var imgVwProfile: UIImageView!
    @IBOutlet weak var phCodeText: UITextView!
    @IBOutlet weak var changePswrdBtn: UIButton!
    @IBOutlet weak var myProfileScrollVw: UIScrollView!
    @IBOutlet weak var nameText: UITextView!
    @IBOutlet weak var emailAddText: UITextView!
    @IBOutlet weak var phnoText: UITextView!
    @IBOutlet weak var addText: UITextView!
    @IBOutlet weak var zipcodeText: UITextView!
    @IBOutlet weak var qualificationText: UITextView!
    @IBOutlet weak var specialityText: UITextView!
    @IBOutlet weak var languageText: UITextView!
    @IBOutlet weak var bioText: UITextView!
    
    var bioTextView : UITextView!
    
    @IBOutlet weak var scrollContentVw: UIView!
    @IBOutlet weak var myProfileTittleLbl: UILabel!
    @IBOutlet weak var lastNameTextVw: UITextView!
    @IBOutlet weak var nameLbl: UILabel!
    var fieldName = NSString()
    var firstNm = NSString()
    var lastNm = NSString()
    var textView = UITextView()
    var editable = Bool()
    var response = NSDictionary()
    var signup = CreateAccountViewController()
    var resultsArray = [String]()
    var lat = String()
    var long = String()
    var arCode = []
    var topConstraint = NSLayoutConstraint()
    var docCode = NSString()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saveBtn.hidden = true
        arCode = ["201", "202", "203", "204", "205", "206", "207", "208", "209", "210", "212", "213", "214", "215", "216", "217", "218", "219", "224", "225", "226", "228", "229", "231", "234", "239", "240", "242", "246", "248", "250", "251", "252", "253", "254", "256", "260", "262", "264", "267", "268", "269", "270", "276", "281", "284", "289", "301", "302", "303", "304", "305", "306", "307", "308", "309", "310", "312", "313", "314", "315", "316", "317", "318", "319", "320", "321", "323", "325", "330", "331", "334", "336", "337", "339", "340", "343", "345", "347", "351", "352", "360", "361", "385", "386", "401", "402", "403", "404", "405", "406", "407", "408", "409", "410", "412", "413", "414", "415", "416", "417", "418", "419", "423", "424", "425", "430", "432", "434", "435", "438", "440", "441", "442", "443", "450", "456", "458", "469", "470", "473", "475", "478", "479", "480", "484", "500", "501", "502", "503", "504", "505", "506", "507", "508", "509", "510", "512", "513", "514", "515", "516", "517", "518", "519", "520", "530", "533", "534", "539", "540", "541", "551", "559", "561", "562", "563", "567", "570", "571", "573", "574", "575", "579", "580", "581", "585", "586", "587", "600", "601", "602", "603", "604", "605", "606", "607", "608", "609", "610", "612", "613", "614", "615", "616", "617", "618", "619", "620", "623", "626", "630", "631", "636", "641", "646", "647", "649", "650", "651", "657", "660", "661", "662", "664", "670", "671", "678", "681", "682", "684", "669", "700", "701", "702", "703", "704", "705", "706", "707", "708", "709", "710", "712", "713", "714", "715", "716", "717", "718", "719", "720", "724", "727", "731", "732", "734", "740", "747", "754", "757", "758", "760", "762", "763", "765", "767", "769", "770", "772", "773", "774", "775", "778", "779", "780", "781", "784", "785", "786", "787", "800", "801", "802", "803", "804", "805", "806", "807", "808", "809", "810", "812", "813", "814", "815", "816", "817", "818", "819", "828", "829", "830", "831", "832", "843", "845", "847", "848", "849", "850", "855", "856", "857", "858", "859", "860", "862", "863", "864", "865", "866", "867", "868", "869", "870", "872", "876", "877", "878", "888", "900", "901", "902", "903", "904", "905", "906", "907", "908", "909", "910", "912", "913", "914", "915", "916", "917", "918", "919", "920", "925", "928", "931", "936", "937", "938", "939", "940", "941", "947", "949", "951", "952", "954", "956", "970", "971", "972", "973", "978", "979", "980", "985", "989"]
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        addText.delegate = self
        nameText.delegate = self
        phnoText.delegate = self
        zipcodeText.delegate = self
        qualificationText.delegate = self
        specialityText.delegate = self
        languageText.delegate = self
        bioText.delegate = self
        phCodeText.delegate = self
        
        dr_Code.delegate = self
        self.dr_Code.layoutManager.allowsNonContiguousLayout = false
        let stringLength:Int = self.dr_Code.text.characters.count
        self.dr_Code.scrollRangeToVisible(NSMakeRange(stringLength-1, 0))
        self.dr_Code.selectable = false
        
        
        bioText.tintColor = UIColor.blackColor()
        //bioText.appearance().tintColor = UIColor.blackColor()
        
        addText.autocapitalizationType = .Words
        nameText.autocapitalizationType = .Words
        lastNameTextVw.autocapitalizationType = .Words
        qualificationText.autocapitalizationType = .Words
        specialityText.autocapitalizationType = .Words
        languageText.autocapitalizationType = .Words
        bioText.autocapitalizationType = .Words
        
        addText.tintColor = UIColor.blackColor()
        nameText.tintColor = UIColor.blackColor()
        phnoText.tintColor = UIColor.blackColor()
        zipcodeText.tintColor = UIColor.blackColor()
        lastNameTextVw.tintColor = UIColor.blackColor()
        qualificationText.tintColor = UIColor.blackColor()
        specialityText.tintColor = UIColor.blackColor()
        languageText.tintColor = UIColor.blackColor()
        bioText.tintColor = UIColor.blackColor()
        phCodeText.tintColor = UIColor.blackColor()
        self.viewSet()
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.85)
            backVw.tag = 2359
            self.view.addSubview(backVw)
            AppManager.sharedManager.showActivityIndicatorInView(backVw, withLabel: "Loading..")
            let params = "id=\(userId)"
            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/details")
        }
        
        nameText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        emailAddText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        phnoText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        phCodeText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        addText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        zipcodeText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        lastNameTextVw.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        qualificationText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        specialityText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        languageText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        bioText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        
        let height = bioText.contentSize.height
        print(height)
        
        //        bioText.backgroundColor = UIColor.redColor()
        //        bioText.frame = CGRectMake(bioText.frame.origin.x, bioText.frame.origin.y, bioText.frame.size.width, 50)
        //        bioText.inputView?.frame = bioText.frame
        //self.bioVw(bioText.text)
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            self.bioVw.hidden = true
        }
        //self.bioVw.frame = CGRectMake(languageVw.frame.origin.x, languageVw.frame.origin.y+languageVw.frame.size.height+15, languageVw.frame.size.width,height+100)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(DocMyProfileViewController.imageTapped(_:)))
        imgVwProfile.addGestureRecognizer(tapGestureRecognizer)
        
        self.navigationController?.navigationBarHidden = true
        imgVwProfile.layer.cornerRadius = imgVwProfile.frame.size.height/2
        imgVwProfile.layer.borderWidth = imgVwProfile.frame.size.height*0.07
        imgVwProfile.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        imgVwProfile.clipsToBounds = true
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            topConstraint = NSLayoutConstraint(item:emailAddVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: firstNameVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 30)
            lastNameVw.hidden = true
            self.view.addConstraints([topConstraint])
        }
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target:self, action:#selector(DocMyProfileViewController.codeCopy(_:)))
        dr_Code.addGestureRecognizer(tapGestureRecognizer2)
    }
    
    
    func bioVw(text:String){
        
        var height = bioText.contentSize.height
        
        if(height <= 33){
            height = 33
        }
        
        print(height)
        
        let biotxtVw = UIView.init(frame: CGRectMake(languageVw.frame.origin.x,languageVw.frame.origin.y+languageVw.frame.size.height+15, self.view.frame.size.width-(2*languageVw.frame.origin.x),height+50))
        biotxtVw.tag = 70
        biotxtVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        biotxtVw.layer.shadowOffset = CGSizeMake(7, 5);
        biotxtVw.layer.shadowRadius = 0.5;
        biotxtVw.layer.shadowOpacity = 0.06;
        biotxtVw.layer.masksToBounds = false;
        biotxtVw.layer.borderWidth = 0.5
        myProfileScrollVw.addSubview(biotxtVw)
        
        let bioImgVw = UIImageView.init(frame: CGRectMake(10,5,40,40))
        bioImgVw.image = UIImage.init(named: "bio")
        biotxtVw.addSubview(bioImgVw)
        
        let biolbl = UILabel.init(frame: CGRectMake(bioImgVw.frame.origin.x+bioImgVw.frame.size.width+20,5,biotxtVw.frame.size.width-(bioImgVw.frame.origin.x+bioImgVw.frame.size.width+40),30))
        biolbl.text = "Bio"
        biolbl.textColor = UIColor.blackColor()
        biotxtVw.addSubview(biolbl)
        
        
        bioTextView = UITextView.init(frame: CGRectMake(bioImgVw.frame.origin.x+bioImgVw.frame.size.width+20,biolbl.frame.size.height+5,biotxtVw.frame.size.width-(bioImgVw.frame.origin.x+bioImgVw.frame.size.width+40),height))
        bioTextView.text = text
        bioTextView.tintColor = UIColor.blackColor()
        bioTextView.font = UIFont(name: "Helvetica", size: 14)
        bioTextView.backgroundColor = UIColor.clearColor()
        bioTextView.textColor = UIColor.darkGrayColor()
        
        bioTextView.tag = 5001
        
        bioTextView.delegate = self
        
        //bioTextView = bioTxt
        
        biotxtVw.addSubview(bioTextView)
        
        
    }
    
    func textViewDidChange(textView: UITextView) {
        //do continue your work
        
        if(textView.tag == 5001){
            print("\(textView.contentSize.height)")
            
            var height = textView.contentSize.height
            
            if(height <= 33){
                height = 33
            }
            
            textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y, textView.frame.size.width, height)
            
            let textViewParent = textView.superview! as UIView
            
            textViewParent.frame = CGRectMake(textViewParent.frame.origin.x, textViewParent.frame.origin.y, textViewParent.frame.size.width, height+50)
            
            bioText.text = textView.text
            
            myProfileScrollVw.contentSize = CGSizeMake(myProfileScrollVw.contentSize.width, textViewParent.frame.origin.y+textViewParent.frame.size.height+100)
        }
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        if(editable == true){
            lastNameVw.hidden = false
            saveBtn.hidden = false
            imgVwProfile.userInteractionEnabled = true
            editBtn.hidden = true
            editable = true
            nameText.editable = true
            phnoText.editable = true
            phCodeText.editable = true
            addText.editable = true
            lastNameTextVw.editable = true
            nameLbl.text = "First Name"
            zipcodeText.editable = true
            nameText.text = firstNm as String
            firstNameVw.backgroundColor = UIColor.whiteColor()
            lastNameTextVw.text = lastNm as String
            qualificationText.editable = true
            specialityText.editable = true
            languageText.editable = true
            bioText.editable = true
            myProfileTittleLbl.text = "Edit Profile"
            changePswrdBtn.hidden = true
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                let topConstraint1 = NSLayoutConstraint(item:lastNameVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: firstNameVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 30)
                self.view.addConstraints([topConstraint1])
                self.view.removeConstraint(topConstraint)
                let bioVw = myProfileScrollVw.viewWithTag(70)! as? UIView
                bioVw?.frame = CGRectMake(languageVw.frame.origin.x,languageVw.frame.origin.y+(2*languageVw.frame.size.height)+30, self.view.frame.size.width-(2*languageVw.frame.origin.x),(bioVw?.frame.size.height)!)
                    
                
            }
            saveBtn.userInteractionEnabled = true
        }
    }
    
    override func viewDidLayoutSubviews() {
        if((self.storyboard?.valueForKey("name"))! as! String == "Main"){
            //            if(editable == true){
            //                myProfileScrollVw.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1.34)
            //            }else{
            //                myProfileScrollVw.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1.24)
            //            }
        }
    }
    
    
    //MARK:- GetLocFromAdd
    func getLocationFromAddressString(loadAddressStr: String)  {
        var latitude :Double = 0
        var longitude : Double = 0
        let esc_addr = loadAddressStr.stringByAppendingFormat("%@","")
        let req = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=\(esc_addr)"
        let escapedAddress = req.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        let url: NSURL = NSURL(string: escapedAddress!)!
        let result:String? = try? String(contentsOfURL:url)
        if result != nil {
            let scanner: NSScanner = NSScanner(string: result!)
            if(scanner.scanUpToString("\"lat\" :", intoString: nil) && scanner.scanString("\"lat\" :", intoString: nil)){
                scanner.scanDouble(&latitude)
                if(scanner.scanUpToString("\"lng\" :", intoString: nil) && scanner.scanString("\"lng\" :", intoString: nil)){
                    scanner.scanDouble(&longitude)
                }
            }
        }
        var center :CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0);
        center.latitude = latitude;
        center.longitude = longitude;
        
        lat = String(format: "%.20f", center.latitude)
        long = String(format: "%.20f", center.longitude)
    }
    
    //MARK:- TextViewMethod
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(DocMyProfileViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        IQKeyboardManager.sharedManager().enable = true
        if(textView.tag == 5001){
            self.hideKeyboard()
            IQKeyboardManager.sharedManager().enable = false
            self.textViewDidChange(self.bioTextView)
            IQKeyboardManager.sharedManager().preventShowingBottomBlankSpace = true
            let bioVw = self.myProfileScrollVw.viewWithTag(70)! as? UIView
            myProfileScrollVw.contentOffset = CGPointMake(0,languageVw.frame.origin.y+languageVw.frame.size.height+(bioVw?.frame.size.height)!-80)
        }
        if(textView == addText){
            fieldName = "Address"
            
        }
        else if(textView == phCodeText){
            fieldName = "Phone Number"
        }
        else if(textView == qualificationText){
            fieldName = "Qualification"
        }
        else if(textView == bioText){
            fieldName = "Bio"
        }
        else if(textView == languageText){
            fieldName = "language"
        }
        else if(textView == specialityText){
            fieldName = "Speciality"
        }
        else if(textView == lastNameTextVw){
            fieldName = "Last Name"
        }
        else if(textView == nameText){
            fieldName = "First Name"
        }
    }
    func textViewDidEndEditing(textView: UITextView) {
        
        if(textView.tag == 5001){
            self.textViewDidChange(self.bioTextView)
            myProfileScrollVw.contentOffset = CGPointMake(0,0)
        }
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(textView == addText || textView == nameText || textView == qualificationText || textView == languageText || textView == specialityText || textView == lastNameTextVw){
            let currentCharacterCount = textView.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + text.characters.count - range.length
            if(newLength == 36){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "\(fieldName) does not contain more than 35 characters.")
            }
            return newLength <= 35
        }
        if(textView == zipcodeText){
            let currentCharacterCount = textView.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + text.characters.count - range.length
            return newLength <= 9
        }
        if(textView == phnoText){
            let currentCharacterCount = textView.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + text.characters.count - range.length
            return newLength <= 7
        }
        if(textView == phCodeText){
            let currentCharacterCount = textView.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + text.characters.count - range.length
            return newLength <= 3
        }
        if(textView.tag == 5001){
            self.textViewDidChange(self.bioTextView)
        }
        return true
    }
    //MARK:- ServerResponce
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString)
    {
            let dict = responseDict["data"]
            response = responseDict
            dispatch_async(dispatch_get_main_queue()) {
                
                let userdefaults = NSUserDefaults.standardUserDefaults()
                let imgUrl:String = userdefaults.valueForKey("profilePic") as! String
                if let url = NSURL(string: "http://\(imgUrl)") {
                    self.imgVwProfile.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
                }
                
                
                let view = self.view.viewWithTag(2359)! as UIView
                view.removeFromSuperview()
                AppManager.sharedManager.hideActivityIndicatorInView(view)
                let name:String = dict!["FirstName"]  as! String
                self.firstNm = "\(name)"
                let lastName:String = dict!["LastName"] as! String
                self.lastNm = "\(lastName)"
                self.nameText.text = self.firstNm as String
                self.lastNameTextVw.text = self.lastNm as String
                let Qualification:String = dict!["Qualification"] as! String
                let email:String = dict!["Email"] as! String
                let phone = dict!["Mobile"] as! NSString
                let code = dict!["Code"] as! NSString
                let zipcode:String = dict!["Zipcode"] as! String
                let address:String = dict!["Address"] as! String
                let Speciality:String = dict!["Speciality"] as! String
                let Languages:String = dict!["Languages"] as! String
                let Bio:String = dict!["Bio"] as! String
                let drCode:String = dict!["DrCode"] as! String
                self.dr_Code.text = "Doctor Code : \(drCode)"
                self.docCode = "\(drCode)"
                
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(address, forKey: "address")
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    if(self.editable == true){
                        self.nameText.text = self.firstNm as String
                    }else{
                        self.nameText.text = "\(name) \(lastName)"
                    }
                }
                else{
                    self.nameText.text = "\(name)"
                    self.lastNameTextVw.text = "\(lastName)"
                }
                self.emailAddText.text = email
                self.phnoText.text = "\(phone)"
                self.addText.text = address
                self.zipcodeText.text = "\(zipcode)"
                self.qualificationText.text = Qualification
                self.specialityText.text = Speciality
                self.languageText.text = Languages
                self.bioText.text = Bio
                
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    self.bioVw(self.bioText.text)
                    self.textViewDidChange(self.bioTextView)
                }
                
                
                self.phCodeText.text = "\(code)"
                
            }
            
    }
    func failureRsponseError(failureError: NSError) {
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    func doRequestPost(url:String,data:[String: NSObject]){
        let requestDic = data
        print(requestDic)
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSUTF8StringEncoding)
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                print(responceString)
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
               
                dispatch_async(dispatch_get_main_queue()) {
                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                    dispatch_async(dispatch_get_main_queue()) {
                        let data = responseDict.valueForKey("data") as! NSDictionary
                        let imgUrl = data.valueForKey("ProfilePic") as! NSString
                        let defaults = NSUserDefaults.standardUserDefaults()
                        defaults.setObject(imgUrl, forKey: "profilePic")
                        self.imgVwProfile.userInteractionEnabled = false
                        self.editBtn.hidden = false
                        self.editable = false
                        self.backBtn.hidden = false
                        self.nameText.editable = false
                        self.emailAddText.editable = false
                        self.phnoText.editable = false
                        self.phCodeText.editable = false
                        self.addText.editable = false
                        self.zipcodeText.editable = false
                        self.qualificationText.editable = false
                        self.specialityText.editable = false
                        self.changePswrdBtn.hidden = false
                        self.languageText.editable = false
                        self.bioText.editable = false
                        self.firstNameVw.backgroundColor = UIColor.whiteColor()
                        self.myProfileTittleLbl.text = "My Profile"
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            self.nameLbl.text = "Name"
                            self.view.addConstraints([self.topConstraint])
                            self.nameText.text = "\(self.nameText.text!) \(self.lastNameTextVw.text!)"
                            self.lastNameVw.hidden = true
                            let bioVw = self.myProfileScrollVw.viewWithTag(70)! as? UIView
                            bioVw?.frame = CGRectMake(self.languageVw.frame.origin.x,self.bioVw.frame.origin.y-self.languageVw.frame.size.height-15, self.view.frame.size.width-(2*self.languageVw.frame.origin.x),(bioVw?.frame.size.height)!)
                        }
                        self.saveBtn.hidden = true
                        
                        AppManager.sharedManager.Showalert("Alert", alertmessage: "Your Profile has been updated successfully")
                    }
                }
            }
        }
        dataTask.resume()
    }
    
    //MARK:- ActionSheetMethod
    func imageTapped(img: AnyObject)
    {
        self.view.endEditing(true)
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        actionSheet.showInView(self.view)
    }
    func codeCopy(img: AnyObject){
        UIPasteboard.generalPasteboard().string = "\(docCode)"
        self.view.makeToast("Doctor Code Copied")
    }
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex){
            
        case 0:
            print("Cancel")
        case 1:
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.allowsEditing = false
                        imagePicker.sourceType = .Camera
                        self.presentViewController(imagePicker, animated: true, completion:nil)
                    }
                })
            }else{
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .Camera
                    presentViewController(imagePicker, animated: true, completion:nil)
                }
            }
        case 2:
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.allowsEditing = false
                        imagePicker.sourceType = .PhotoLibrary
                        self.presentViewController(imagePicker, animated: true, completion:nil)
                    }
                })
            }else{
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .PhotoLibrary
                    self.presentViewController(imagePicker, animated: true, completion:nil)
                }
            }
        default:
            print("Default")
        }
    }
    
    
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        let selectedImage : UIImage = image
        imgVwProfile.image=self.ResizeImage(selectedImage, targetSize: CGSizeMake(200.0, 200.0))
        imgVwProfile.clipsToBounds = true
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK:- ResizeImage
    func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    //MARK:- UI
    func viewSet(){
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone)
        {
            firstNameVw.backgroundColor = UIColor.whiteColor()
            lastNameVw.backgroundColor = UIColor.whiteColor()
            addressVw.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            codeVw.backgroundColor = UIColor.whiteColor()
            phonenumberVw.backgroundColor = UIColor.whiteColor()
            emailAddVw.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            languageVw.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            qualificationVw.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            bioVw.backgroundColor = UIColor.whiteColor()
            zipcodeVw.backgroundColor = UIColor.whiteColor()
            specialityVw.backgroundColor = UIColor.whiteColor()
        }else{
            firstNameVw.backgroundColor = UIColor.whiteColor()
            lastNameVw.backgroundColor = UIColor.whiteColor()
            addressVw.backgroundColor = UIColor.whiteColor()
            codeVw.backgroundColor = UIColor.whiteColor()
            phonenumberVw.backgroundColor = UIColor.whiteColor()
            emailAddVw.backgroundColor = UIColor.whiteColor()
            languageVw.backgroundColor = UIColor.whiteColor()
            qualificationVw.backgroundColor = UIColor.whiteColor()
            bioVw.backgroundColor = UIColor.whiteColor()
            zipcodeVw.backgroundColor = UIColor.whiteColor()
            specialityVw.backgroundColor = UIColor.whiteColor()
        }
        firstNameVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        firstNameVw.layer.shadowOffset = CGSizeMake(7, 5);
        firstNameVw.layer.shadowRadius = 0.5;
        firstNameVw.layer.shadowOpacity = 0.06;
        firstNameVw.layer.masksToBounds = false;
        firstNameVw.layer.borderWidth = 0.5
        
        lastNameVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        lastNameVw.layer.shadowOffset = CGSizeMake(7, 5);
        lastNameVw.layer.shadowRadius = 0.5;
        lastNameVw.layer.shadowOpacity = 0.06;
        lastNameVw.layer.masksToBounds = false;
        lastNameVw.layer.borderWidth = 0.5
        
        addressVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        addressVw.layer.shadowOffset = CGSizeMake(7, 5);
        addressVw.layer.shadowRadius = 0.5;
        addressVw.layer.shadowOpacity = 0.06;
        addressVw.layer.masksToBounds = false;
        addressVw.layer.borderWidth = 0.5
        
        codeVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        codeVw.layer.shadowOffset = CGSizeMake(7, 5);
        codeVw.layer.shadowRadius = 0.5;
        codeVw.layer.shadowOpacity = 0.06;
        codeVw.layer.masksToBounds = false;
        codeVw.layer.borderWidth = 0.5
        
        phonenumberVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        phonenumberVw.layer.shadowOffset = CGSizeMake(7, 5);
        phonenumberVw.layer.shadowRadius = 0.5;
        phonenumberVw.layer.shadowOpacity = 0.06;
        phonenumberVw.layer.masksToBounds = false;
        phonenumberVw.layer.borderWidth = 0.5
        
        emailAddVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        emailAddVw.layer.shadowOffset = CGSizeMake(7, 5);
        emailAddVw.layer.shadowRadius = 0.5;
        emailAddVw.layer.shadowOpacity = 0.06;
        emailAddVw.layer.masksToBounds = false;
        emailAddVw.layer.borderWidth = 0.5
        
        qualificationVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        qualificationVw.layer.shadowOffset = CGSizeMake(7, 5);
        qualificationVw.layer.shadowRadius = 0.5;
        qualificationVw.layer.shadowOpacity = 0.06;
        qualificationVw.layer.masksToBounds = false;
        qualificationVw.layer.borderWidth = 0.5
        
        languageVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        languageVw.layer.shadowOffset = CGSizeMake(7, 5);
        languageVw.layer.shadowRadius = 0.5;
        languageVw.layer.shadowOpacity = 0.06;
        languageVw.layer.masksToBounds = false;
        languageVw.layer.borderWidth = 0.5
        
        bioVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        bioVw.layer.shadowOffset = CGSizeMake(7, 5);
        bioVw.layer.shadowRadius = 0.5;
        bioVw.layer.shadowOpacity = 0.06;
        bioVw.layer.masksToBounds = false;
        bioVw.layer.borderWidth = 0.5
        
        zipcodeVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        zipcodeVw.layer.shadowOffset = CGSizeMake(7, 5);
        zipcodeVw.layer.shadowRadius = 0.5;
        zipcodeVw.layer.shadowOpacity = 0.06;
        zipcodeVw.layer.masksToBounds = false;
        zipcodeVw.layer.borderWidth = 0.5
        
        specialityVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        specialityVw.layer.shadowOffset = CGSizeMake(7, 5);
        specialityVw.layer.shadowRadius = 0.5;
        specialityVw.layer.shadowOpacity = 0.06;
        specialityVw.layer.masksToBounds = false;
        specialityVw.layer.borderWidth = 0.5
    }
    
    
    //MARK:- ButtonAction
    @IBAction func saveBtnAc(sender: AnyObject) {
        saveBtn.userInteractionEnabled = false
        if(!(arCode.containsObject(phCodeText.text!))){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            
            let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21))
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.39))
            contentLbl.font = UIFont(name: "Helvetica", size:14)
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter a Valid Area Code."
            contentLbl.numberOfLines = 3
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(DocMyProfileViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(zipcodeText.text?.characters.count != 5 && zipcodeText.text?.characters.count < 5){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            
            let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21))
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont.init(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
            contentLbl.font = UIFont(name: "Helvetica", size:14)
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter valid zipcode"
            contentLbl.numberOfLines = 1
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(DocMyProfileViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(zipcodeText.text?.characters.count != 9 && zipcodeText.text?.characters.count > 5){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            
            let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21))
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
            contentLbl.font = UIFont(name: "Helvetica", size:14)
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please Enter valid zipcode"
            contentLbl.numberOfLines = 1
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(DocMyProfileViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        else if(phnoText.text.characters.count != 7 || phCodeText.text.characters.count != 3){
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            
            let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21))
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
            contentLbl.font = UIFont(name: "Helvetica", size:14)
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Please enter 10 digit phone number."
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
            okLbl.setTitle("OK", forState: .Normal)
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            return
        }
        if(lat == ""){
            self.getLocationFromAddressString(addText.text!)
        }
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            let image : UIImage = imgVwProfile.image!
            
            let  mImageData              = UIImagePNGRepresentation(image)! as NSData
            
            let base64String = mImageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.EncodingEndLineWithLineFeed)
            
            let firstName = nameText.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let phCode = phCodeText.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let lastName = lastNameTextVw.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let trimNameString1 = phnoText.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let trimNameString2 = addText.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let trimNameString3 = zipcodeText.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let trimNameString4 = qualificationText.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let trimNameString5 = specialityText.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let trimNameString6 = languageText.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let trimNameString7 = bioText.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            if(firstName == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the FirstName")
                return
            }else if(lastName == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the LastName")
                return
            }else if(trimNameString3 == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the Zipcode")
                return
            }else if(trimNameString4 == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the Qualification")
                return
            }else if(trimNameString5 == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the Speciality")
                return
            }else if(trimNameString6 == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the Language")
                return
            }else if(trimNameString7 == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the Bio")
                return
            }else if(trimNameString1 == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the phone number")
                return
            }else if(trimNameString2 == ""){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the Address")
                return
            }
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let firstNameLowerCaseString:String = (firstName.lowercaseString)
            let lastNameLowerCaseString:String = (lastName.lowercaseString)
            
            let params = ["FirstName":"\(firstNameLowerCaseString)" ,"LastName":"\(lastNameLowerCaseString)", "Mobile":"\(trimNameString1)", "Code":"\(phCode)", "Address":"\(trimNameString2)" , "Zipcode":"\(trimNameString3)", "Qualification":"\(trimNameString4)", "ProfilePic":"\(base64String)",  "Speciality":"\(trimNameString5)", "Languages":"\(trimNameString6)", "Bio":"\(trimNameString7)","id":"\(userId)"]
            
            self.doRequestPost("\(Header.BASE_URL)users/useredit", data: params)
            
           // let params = "FirstName=\(firstNameLowerCaseString)&LastName=\(lastNameLowerCaseString)&Mobile=\(trimNameString1)&Code=\(phCode)&Address=\(trimNameString2)&Lat=\(lat)&Long=\(long)&id=\(userId)&Zipcode=\(trimNameString3)&ProfilePic=\(base64String)&Qualification=\(trimNameString4)&Speciality=\(trimNameString5)&Languages=\(trimNameString6)&Bio=\(trimNameString7)"
            defaults.setObject(nameText.text!, forKey: "firstName")
            defaults.setObject(lastNameTextVw.text!, forKey: "lastName")
            defaults.setObject(specialityText.text, forKey: "Speciality")
            defaults.setObject(qualificationText.text, forKey: "qualification")
           // server = false
            firstNm = firstName
            lastNm = lastName
           // AppManager.sharedManager.postDataOnserver(params, postUrl: "users/useredit")
        }
    }
    @IBAction func editBtnAc(sender: AnyObject) {
        lastNameVw.hidden = false
        imgVwProfile.userInteractionEnabled = true
        editBtn.hidden = true
        editable = true
        nameText.editable = true
        phnoText.editable = true
        phCodeText.editable = true
        addText.editable = true
        lastNameTextVw.editable = true
        nameLbl.text = "First Name"
        zipcodeText.editable = true
        nameText.text = firstNm as String
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            firstNameVw.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
        }
        lastNameTextVw.text = lastNm as String
        qualificationText.editable = true
        specialityText.editable = true
        languageText.editable = true
        bioText.editable = true
        myProfileTittleLbl.text = "Edit Profile"
        changePswrdBtn.hidden = true
        //nameText.becomeFirstResponder()
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            let topConstraint1 = NSLayoutConstraint(
                item:lastNameVw,
                attribute: NSLayoutAttribute.TopMargin,
                relatedBy: NSLayoutRelation.Equal,
                toItem: firstNameVw,
                attribute: NSLayoutAttribute.BottomMargin,
                multiplier: 1,
                constant: 30)
            self.view.addConstraints([topConstraint1])
            self.view.removeConstraint(topConstraint)
            
            let bioVw = myProfileScrollVw.viewWithTag(70)! as? UIView
            bioVw?.frame = CGRectMake(languageVw.frame.origin.x,languageVw.frame.origin.y+(2*languageVw.frame.size.height)+30, self.view.frame.size.width-(2*languageVw.frame.origin.x),(bioVw?.frame.size.height)!)
            
        }
        saveBtn.hidden = false
        saveBtn.userInteractionEnabled = true
    }
    func okBtnAc(){
        saveBtn.userInteractionEnabled = true
        let view = self.view.viewWithTag(2369)! as UIView
        view.removeFromSuperview()
    }
    
    @IBAction func backBtnAc(sender: AnyObject) {
        self.slideMenuController()!.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension DocMyProfileViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
