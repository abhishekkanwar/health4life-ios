

import UIKit
import RealmSwift
enum PatRightMenu: Int {
    case Info = 0
    case Appointmnt
    case Docs
    case PatNotifications
    case contact
    case logout
}

protocol PatientRightMenuProtocol : class {
    func changeViewController(menu: PatRightMenu)
}
class PatientSideBarViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,WebServiceDelegate {
    @IBOutlet weak var patientSidebarTableVw: UITableView!
    @IBOutlet weak var PatientSideBarImgVW: UIImageView!
    @IBOutlet weak var patientNameLbl: UILabel!
    @IBOutlet weak var patientAddressLbl: UILabel!
    
    var mainViewController: UIViewController!
    var myInfo : UIViewController!
    var myAppointment : UIViewController!
    var myDocs : UIViewController!
    var setSchedule : UIViewController!
    var patNoti : UIViewController!
    var logoutC : UIViewController!
    var contactUs : UIViewController!
    var editInfoVw : UIViewController!
    var schVw : UIViewController!
    var serverInt = NSInteger()
    var countNoti = NSInteger()
    let realm = try! Realm()
    var arMenu = ["My Information", "My Appointments","My Doctors", "Notifications","Contact Us","Logout"]
    var arMenuImages = ["infoIcon", "myAppointmnt","myDocs","notification","ContactUs", "logout"]
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = NSUserDefaults.standardUserDefaults()
        let firstname:String = defaults.valueForKey("firstName") as! String
        let lastname:String = defaults.valueForKey("lastName") as! String
        let add:String = defaults.valueForKey("address") as! String
        let imgUrl:String = defaults.valueForKey("profilePic") as! String
        if let url = NSURL(string: "http://\(imgUrl)") {
            PatientSideBarImgVW.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        patientNameLbl.text = "\(firstname) \(lastname)"
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: "cityState")
        let offsetY: CGFloat = -8.0
        attachment.bounds = CGRectMake(0, offsetY, attachment.image!.size.width, attachment.image!.size.height)
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string:"")
        myString.appendAttributedString(attachmentString)
        let myString1 = NSMutableAttributedString(string:add)
        myString.appendAttributedString(myString1)
        patientAddressLbl.textAlignment = .Center
        patientAddressLbl.attributedText = myString
        patientSidebarTableVw.delegate = self
        patientSidebarTableVw.dataSource = self
        self.patientSidebarTableVw.tableFooterView = UIView(frame: CGRectMake(0, 0, self.patientSidebarTableVw.frame.size.width, 1))
        PatientSideBarImgVW.layer.cornerRadius = PatientSideBarImgVW.frame.size.height/2
        PatientSideBarImgVW.layer.borderWidth = PatientSideBarImgVW.frame.size.height*0.03
        PatientSideBarImgVW.layer.borderColor = UIColor.init(red: 206/255, green: 205/255, blue: 205/255, alpha: 1).CGColor
        PatientSideBarImgVW.clipsToBounds = true
       
        
        //if((self.storyboard?.valueForKey("name"))! as! String == "Main"){
           // let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let myAppointmentVw = storyboard!.instantiateViewControllerWithIdentifier("PatientAppointmentViewController") as! PatientAppointmentViewController
            self.myAppointment = UINavigationController(rootViewController: myAppointmentVw)
            
            let myInfoVw = storyboard!.instantiateViewControllerWithIdentifier("MyInformationViewController") as! MyInformationViewController
            self.myInfo = UINavigationController(rootViewController: myInfoVw)
            
            let myDocsVw = storyboard!.instantiateViewControllerWithIdentifier("MyDoctorsViewController") as! MyDoctorsViewController
            myDocsVw.statusBool = false
            self.myDocs = UINavigationController(rootViewController: myDocsVw)
            
            let patNotificationVw = storyboard!.instantiateViewControllerWithIdentifier("PatientNotificationViewController") as! PatientNotificationViewController
            patNotificationVw.notiBool = false
            self.patNoti = UINavigationController(rootViewController: patNotificationVw)
            
            let logoutVw = storyboard!.instantiateViewControllerWithIdentifier("mainViewController") as! ViewController
            self.logoutC = UINavigationController(rootViewController: logoutVw)
            
            let contactus = storyboard!.instantiateViewControllerWithIdentifier("ContactUsViewController") as! ContactUsViewController
            contactus.contactSwich = false
            self.contactUs = UINavigationController(rootViewController: contactus)
       // }
    }
    @IBAction func editBtnAc(sender: AnyObject) {
       // let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let editVw = storyboard!.instantiateViewControllerWithIdentifier("MyInformationViewController") as! MyInformationViewController
        self.editInfoVw = UINavigationController(rootViewController: editVw)
        editVw.editabl = true
        self.slideMenuController()?.changeMainViewController(self.editInfoVw, close: true)
    }
    func changeViewController(menu: PatRightMenu) {
        switch menu {
        case .Info:
            self.slideMenuController()?.changeMainViewController(self.myInfo, close: true)
        case .Appointmnt:
            self.slideMenuController()?.changeMainViewController(self.myAppointment, close: true)
        case .Docs:
            self.slideMenuController()?.changeMainViewController(self.myDocs, close: true)
        case .PatNotifications:
            self.slideMenuController()?.changeMainViewController(self.patNoti, close: true)
        case.contact:
            self.slideMenuController()?.changeMainViewController(self.contactUs, close: true)
        case.logout:
            let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To LogOut?", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
                UIAlertAction in
                NSLog("OK Pressed")
                let defaults = NSUserDefaults.standardUserDefaults()
                if let userId = defaults.valueForKey("id") {
                    var token =  NSString()
                    if(UIDevice.isSimulator == true){
                        token = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
                    }else{
                        token = defaults.valueForKey("deviceToken") as! NSString
                    }
                    AppManager.sharedManager.delegate=self
                    AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Log Out")
                    let params = "UserId=\(userId)&DeviceToken=\(token)"
                    self.serverInt = 1
                    AppManager.sharedManager.postDataOnserver(params, postUrl: "users/logout")
                }
            }
            let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        if(serverInt == 1){
        dispatch_async(dispatch_get_main_queue()){
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.removeObjectForKey("id")
            defaults.removeObjectForKey("searchDoc")
            defaults.removeObjectForKey("selectPharma")
            defaults.removeObjectForKey("usertype")
            defaults.removeObjectForKey("msgCount")
            try! self.realm.write({
                self.realm.deleteAll()
            })
            UIApplication.sharedApplication().cancelAllLocalNotifications()
          self.slideMenuController()?.changeMainViewController(self.logoutC, close: true)
        }
        }else{
            if( responseDict.objectForKey("data") != nil){
                let docMutableAr = NSMutableArray()
                let docidAr = NSMutableArray()
                let dataDict = responseDict.objectForKey("data") as! NSArray
                for i in 0...dataDict.count-1 {
                    let d = dataDict[i]
                    let status = d.valueForKey("status") as! NSInteger
                    if(status == 1){
                        docMutableAr.addObject(d.valueForKey("Name")!)
                        docidAr.addObject(d.valueForKey("Dr_id")!)
                    }
                }
                if(docMutableAr.count != 0){
                    dispatch_async(dispatch_get_main_queue()) {
                      //  let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        var storyboard = UIStoryboard()
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            storyboard = UIStoryboard(name: "Main", bundle: nil)
                        }else{
                            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                        }
                        let vc = storyboard.instantiateViewControllerWithIdentifier("PatientSetScheduleViewController") as! PatientSetScheduleViewController
                        self.schVw = UINavigationController(rootViewController: vc)
                       self.slideMenuController()?.changeMainViewController(self.schVw, close: true)
                    }
                }else{
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Your doctor has not verified your account yet. Please contact your doctor’s office to expedite this process or send a new request.")
                }
            }
            else{
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Your doctor has not verified your account yet. Please contact your doctor’s office to expedite this process or send a new request.")
            }
        }
    }
    @IBAction func schAction(sender: AnyObject) {
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let params = "Patient_id=\(userId)"
            serverInt = 2
            AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/PatientDrList")
        }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arMenu.count
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
        let  cellId:String = "cell"+String(indexPath.row)
        var cell  = tableView.dequeueReusableCellWithIdentifier(cellId)
       
        if (cell == nil){
            cell = UITableViewCell(style: .Default, reuseIdentifier:cellId)
        }
        let lbl_title = UILabel(frame: CGRectMake(self.view.frame.size.width*0.25, view.frame.size.height*0.01, view.frame.size.width*0.55,  view.frame.size.height*0.05))
        lbl_title.text = arMenu[indexPath.row]
        lbl_title.font = UIFont(name: "Helvetica", size: self.view.frame.height*0.025)
        lbl_title.textColor = UIColor.blackColor()
        let image = UIImage(named:arMenuImages[indexPath.row])
        let imageVW = UIImageView(frame: CGRectMake(self.view.frame.size.width*0.1,view.frame.size.height*0.012,view.frame.size.height*0.04, view.frame.size.height*0.04))
        imageVW.image = image
        
        let vwSelect = UIView(frame: CGRectMake(self.view.frame.size.width*0,view.frame.size.height*0,self.view.frame.size.width*0.027, self.view.frame.size.width*0.19))
        vwSelect.backgroundColor = UIColor.clearColor()
        vwSelect.tag = 30+indexPath.row
        cell?.contentView.addSubview(vwSelect)
        cell?.contentView.addSubview(lbl_title)
        cell?.contentView.addSubview(imageVW)
        if(indexPath.row == 3){
            let countLabel = UILabel.init(frame: CGRectMake(tableView.frame.size.width-(self.view.frame.size.width*0.12),10, self.view.frame.size.width*0.1, self.view.frame.size.width*0.1))
            countLabel.text = "\(countNoti)"
            countLabel.textAlignment = .Center
            countLabel.layer.cornerRadius = countLabel.frame.size.height/2
            countLabel.clipsToBounds = true
            countLabel.backgroundColor = UIColor.blackColor()
            countLabel.textColor = UIColor.whiteColor()
            //cell?.addSubview(countLabel)
        }
        return cell! as UITableViewCell
    } else{
            let cell = tableView.dequeueReusableCellWithIdentifier("cell")
            let ptImg: UIImageView = (cell!.viewWithTag(11) as! UIImageView)
            ptImg.image = UIImage(named: arMenuImages[indexPath.row])
            
            let nameLbl: UILabel = (cell!.viewWithTag(12) as! UILabel)
            nameLbl.text = arMenu[indexPath.row] as? String
            let vwSelect = UIView(frame: CGRectMake(self.view.frame.size.width*0,view.frame.size.height*0,self.view.frame.size.width*0.027, self.view.frame.size.width*0.2))
            vwSelect.backgroundColor = UIColor.clearColor()
            vwSelect.tag = 30+indexPath.row
            cell?.contentView.addSubview(vwSelect)
            return cell! as UITableViewCell
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let  vw = self.view.viewWithTag(30+indexPath.row)
        vw?.backgroundColor = UIColor.init(red: 10/255, green: 4/255, blue: 88/255, alpha: 1)
        if let menu = PatRightMenu(rawValue: indexPath.item) {
            self.changeViewController(menu)
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.view.frame.size.width*0.19
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
