

import UIKit
import SlideMenuControllerSwift
class StaffLoginViewController: UIViewController,WebServiceDelegate,UITextFieldDelegate {
    @IBOutlet weak var btnLoginPswrd: UIButton!
    @IBOutlet weak var pswrdVw: UIView!
    @IBOutlet weak var usernameVw: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var textFieldLoginPswrd: UITextField!
    @IBOutlet weak var usernameFld: UITextField!
    @IBOutlet weak var rememberMeCheckBox: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var agreeDisagreeVw: UIView!
    @IBOutlet weak var agreeBtn: UIButton!
    
    var textFieldSecureEntry = Bool()
    var userId = NSString()
    var serverInt = NSInteger()
    var serverWho = NSInteger()
    var responceDiction = NSMutableDictionary()
    var nameAr = NSMutableArray()
    var specialAr = NSMutableArray()
    var imgAr = NSMutableArray()
    var idAr = NSMutableArray()
    override func viewDidLoad(){
        super.viewDidLoad()
        agreeDisagreeVw.hidden = true
        self.uiRoundView()
        loginBtn.enabled = false
        usernameFld.delegate = self
        textFieldLoginPswrd.delegate = self
        
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let username = defaults.valueForKey("staffEmail") {
            if let pasword = defaults.valueForKey("staffPswrd") {
                textFieldLoginPswrd.text = pasword as? String
                loginBtn.enabled = true
                usernameFld.text = username as? String
                rememberMeCheckBox.setImage(UIImage(named: "checked"), forState: .Normal)
            }
            else{
                loginBtn.enabled = false
                textFieldLoginPswrd.text = ""
                usernameFld.text = ""
                rememberMeCheckBox.setImage(UIImage(named: "emptyCheckBox"), forState: .Normal)
            }
        }else{
            loginBtn.enabled = false
            textFieldLoginPswrd.text = ""
            usernameFld.text = ""
            rememberMeCheckBox.setImage(UIImage(named: "emptyCheckBox"), forState: .Normal)
        }
        
    }
    func textFieldDidEndEditing(textField: UITextField){
        let trimNameString1 = usernameFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        trimNameString1
        if(trimNameString1 != "" && textFieldLoginPswrd.text! != ""){
            loginBtn.enabled = true
        }else if(trimNameString1 == "" || textFieldLoginPswrd.text! == ""){
            loginBtn.enabled = false
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        dispatch_async(dispatch_get_main_queue()) {
            if(self.serverInt == 1){
                if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 200){
                    let data = responseDict.valueForKey("data") as! NSDictionary
                    let loginAlready = data.valueForKey("LoginAllready") as! NSInteger
                    let staffDrStatus = data.valueForKey("StaffDrStatus") as! NSInteger
                    if(staffDrStatus == 1){
                        if(loginAlready == 0){
                            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                            self.responceDiction = responseDict
                            self.agreeDisagreeVw.hidden = false
                            let crossBtn = UIButton()
                            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                                crossBtn.frame = CGRectMake(self.agreeDisagreeVw.frame.size.width-(self.agreeDisagreeVw.frame.size.width*0.12),0, self.agreeDisagreeVw.frame.size.width*0.12,self.agreeDisagreeVw.frame.size.width*0.12)
                            }
                            else{
                                crossBtn.frame = CGRectMake(self.agreeDisagreeVw.frame.size.width-(self.agreeDisagreeVw.frame.size.width*0.1),0, self.agreeDisagreeVw.frame.size.width*0.1,self.agreeDisagreeVw.frame.size.width*0.1)
                            }
                            
                            crossBtn.setBackgroundImage(UIImage(named: "crossBtn"), forState: .Normal)
                            crossBtn.addTarget(self, action: #selector(StaffLoginViewController.crossBtnAc), forControlEvents: .TouchUpInside)
                            crossBtn.tag = 4
                            self.agreeDisagreeVw.addSubview(crossBtn)
                            let id:String = responseDict.valueForKeyPath("data._id") as! String
                            self.userId = id
                        }
                        else{
                            let usertype:Int = data["UserType"]  as! NSInteger
                            let id:String = responseDict.valueForKeyPath("data._id") as! String
//                            self.doRequestPost("\(Header.BASE_URL)messageChat/sessionChange", data: ["UserId":"\(id)"])
//                            self.serverWho = 2
                            
                            let defaults = NSUserDefaults.standardUserDefaults()
                            defaults.setValue(id, forKey: "staffId")
                            defaults.setValue(usertype, forKey: "usertype")
                            defaults.setInteger(data["docShow"] as! NSInteger, forKey: "msgCount")
                            let pic = data["ProfilePic"] as! String
                            let firstName: String = data["FirstName"] as! String
                            let lastName:String = data["LastName"] as! String
                            let designationTxt:String = data["Designation"] as! String
                            defaults.setObject(pic, forKey: "profilePic")
                            defaults.setObject(firstName, forKey: "firstName")
                            defaults.setObject(lastName, forKey: "lastName")
                            defaults.setObject(designationTxt, forKey: "designation")
                            if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginSelectcheckBox")){
                                defaults.setObject(self.usernameFld.text!, forKey: "staffEmail")
                                defaults.setObject(self.textFieldLoginPswrd.text!, forKey: "staffPswrd")
                            }else if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginCheckBox")){
                                defaults.removeObjectForKey("staffEmail")
                                defaults.removeObjectForKey("staffPswrd")
                            }
                            NSUserDefaults.standardUserDefaults().setValue(0, forKey: "indexPath")
                            self.doRequestPost("\(Header.BASE_URL)staffapi/staffDrList", data: ["StaffId":"\(id)"])
                            self.serverWho = 1
                        }
                    }
                    else{
                        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                        AppManager.sharedManager.Showalert("Alert", alertmessage: "Your Doctor has Suspended your account, Please contact your doctor to get your account reinstated.")
                    }
                }
                else if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 400){
                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                    AppManager.sharedManager.Showalert("Alert", alertmessage:"Email id does not exist")
                }
                else if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 401){
                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                    AppManager.sharedManager.Showalert("Alert", alertmessage:"Incorrect Password")
                }
                else if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 601){
                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                    AppManager.sharedManager.Showalert("Alert", alertmessage:"Email id already exist with another account.")
                }
                else{
                    dispatch_async(dispatch_get_main_queue()) {
                        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                        AppManager.sharedManager.Showalert("Alert", alertmessage:"Email id does not exist")
                    }
                }
            }
            else{
                if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 200){
                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                    print(self.responceDiction)
                    let data = self.responceDiction.valueForKey("data") as! NSDictionary
                    let usertype:Int = data["UserType"]  as! NSInteger
                    let id:String = self.responceDiction.valueForKeyPath("data._id") as! String
                    let defaults = NSUserDefaults.standardUserDefaults()
                    defaults.setValue(id, forKey: "staffId")
                    defaults.setValue(usertype, forKey: "usertype")
                    let pic = data["ProfilePic"] as! String
                    let firstName: String = data["FirstName"] as! String
                    let lastName:String = data["LastName"] as! String
                    let designationTxt:String = data["Designation"] as! String
                    defaults.setObject(pic, forKey: "profilePic")
                    defaults.setObject(firstName, forKey: "firstName")
                    defaults.setObject(lastName, forKey: "lastName")
                    defaults.setObject(designationTxt, forKey: "designation")
                    if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginSelectcheckBox")){
                        defaults.setObject(self.usernameFld.text!, forKey: "staffEmail")
                        defaults.setObject(self.textFieldLoginPswrd.text!, forKey: "staffPswrd")
                    }else if(self.rememberMeCheckBox.currentImage == UIImage(named: "loginCheckBox")){
                        defaults.removeObjectForKey("staffEmail")
                        defaults.removeObjectForKey("staffPswrd")
                    }
                    NSUserDefaults.standardUserDefaults().setValue(0, forKey: "indexPath")
                    self.doRequestPost("\(Header.BASE_URL)staffapi/staffDrList", data: ["StaffId":"\(id)"])
                    self.serverWho = 1
                    
                    
                }
            }
        }
    }
    func doRequestPost(url:String,data:[String: NSObject]){
        print("URL FOR DEVICETOKEN ==> \(url)")
        let requestDic = data
        print(requestDic)
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        
        print("Request Object:\(data)")
        print("Request string = \(jsonString!)")
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil)
            {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
                print("Succes:")
                let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                print(responceString)
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                dispatch_async(dispatch_get_main_queue())
                {
                    if(self.serverWho == 1){
                        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                        let id = NSUserDefaults.standardUserDefaults().valueForKey("staffId") as! String
                        self.doRequestPost("\(Header.BASE_URL)messageChat/sessionChange", data: ["UserId":"\(id)"])
                        self.serverWho = 2
                        self.nameAr.removeAllObjects()
                        self.specialAr.removeAllObjects()
                        self.imgAr.removeAllObjects()
                        self.idAr.removeAllObjects()
                        if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 200)
                        {
                            let data = responseDict.valueForKey("data") as! NSMutableArray
                            for i in 0...data.count-1{
                                let dict = data.objectAtIndex(i) as! NSDictionary
                                if(dict.valueForKey("StaffStatus") as! Int == 1){
                                    self.nameAr.addObject(dict.valueForKey("Name")!)
                                    self.specialAr.addObject(dict.valueForKey("Speciality")!)
                                    self.imgAr.addObject(dict.valueForKey("ProfilePic")!)
                                    self.idAr.addObject(dict.valueForKey("DrId")!)
                                    
                                }
                            }
                            NSUserDefaults.standardUserDefaults().setValue(self.nameAr.objectAtIndex(0), forKey: "selectDrName")
                            NSUserDefaults.standardUserDefaults().setValue(self.specialAr.objectAtIndex(0), forKey: "selectDrSpeciality")
                            NSUserDefaults.standardUserDefaults().setValue(self.imgAr.objectAtIndex(0), forKey: "selectDrImg")
                            NSUserDefaults.standardUserDefaults().setValue(self.idAr.objectAtIndex(0), forKey: "id")
                        }
                        var storyboard = UIStoryboard()
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            storyboard = UIStoryboard(name: "Main", bundle: nil)
                        }else{
                            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                        }
                        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("StaffHomeViewController") as! StaffHomeViewController
                        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("StaffSelectDoctorViewController") as! StaffSelectDoctorViewController
                        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                        leftViewController.mainViewController = nvc
                        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController:leftViewController)
                        slideMenuController.automaticallyAdjustsScrollViewInsets = true
                        slideMenuController.delegate = mainViewController
                        self.navigationController?.pushViewController(slideMenuController, animated: true)
                    }
                    else{
                        
                        
                    }
                }
            }
        }
        dataTask.resume()
    }
    func crossBtnAc(){
        agreeDisagreeVw.hidden = true
        let crossBtn = self.view.viewWithTag(4) as! UIButton
        crossBtn.removeFromSuperview()
    }
    @IBAction func agreeBtnAc(sender: AnyObject){
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
        let params = "UserId=\(userId)&agreement=1"
        serverInt = 2
        AppManager.sharedManager.postDataOnserver(params, postUrl: "users/agreement")
        agreeDisagreeVw.hidden = true
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    @IBAction func btnLoginPswrdAc(sender: AnyObject) {
        if(textFieldSecureEntry == true){
            textFieldSecureEntry = false
            textFieldLoginPswrd.secureTextEntry = false
            btnLoginPswrd.setImage(UIImage(named: "whiteEyeClose"), forState: UIControlState.Normal)
        }else{
            textFieldSecureEntry = true
            textFieldLoginPswrd.secureTextEntry = true
            btnLoginPswrd.setImage(UIImage(named: "whiteEyeOpen"), forState: UIControlState.Normal)
        }
    }
    @IBAction func remembermeAction(sender: AnyObject){
        if(sender.currentImage == UIImage(named: "loginCheckBox")){
            sender.setImage(UIImage(named: "loginSelectcheckBox"), forState: .Normal)
        }else if(sender.currentImage == UIImage(named: "loginSelectcheckBox")){
            sender.setImage(UIImage(named: "loginCheckBox"), forState: .Normal)
        }
    }
    @IBAction func loginAc(sender: AnyObject){
        let emailLowerCaseString:String = (usernameFld.text?.lowercaseString)!
        let defaults = NSUserDefaults.standardUserDefaults()
        if((AppManager.sharedManager.isValidEmail(emailLowerCaseString))){
            var deviceToken =  NSString()
            if(UIDevice.isSimulator == true){
                deviceToken = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
            }else{
                deviceToken = defaults.valueForKey("deviceToken") as! NSString
            }
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loging In")
            let deviceName = UIDevice.currentDevice().name
            serverInt = 1
            let params = "Email=\(emailLowerCaseString)&LoginType=M&LoginKey=\(textFieldLoginPswrd.text!)&DeviceToken=\(deviceToken)&DeviceType=ios&Device=\(deviceName)&UserType=3"
            defaults.setObject(emailLowerCaseString,forKey: "email")
            AppManager.sharedManager.postDataOnserver(params, postUrl: "staffapi/Login")
        }
        else{
            AppManager.sharedManager.Showalert("Alert", alertmessage: "please enter correct email format like, 'abc@gmail.com'")
        }
    }
    func uiRoundView(){
        textFieldSecureEntry = true
        usernameFld.tintColor = UIColor.blackColor()
        textFieldLoginPswrd.tintColor = UIColor.blackColor()
        
        
        agreeDisagreeVw.layer.cornerRadius = self.agreeDisagreeVw.frame.size.height*0.04
        agreeDisagreeVw.layer.borderWidth = self.view.frame.size.height*0.0012
        agreeDisagreeVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        agreeDisagreeVw.clipsToBounds = true
        
        self.view.backgroundColor =  UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        
        agreeBtn.layer.cornerRadius = self.agreeBtn.frame.size.height*0.09
        agreeBtn.layer.borderWidth = self.view.frame.size.height*0.001
        agreeBtn.layer.borderColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1).CGColor
        agreeBtn.clipsToBounds = true
        
        usernameVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        usernameVw.layer.borderColor = UIColor.init(red: 45/255, green: 78/255, blue: 107/255, alpha: 1).CGColor
        usernameVw.backgroundColor = UIColor.init(red: 45/255, green: 78/255, blue: 107/255, alpha: 1)
        usernameVw.layer.masksToBounds = false;
        usernameVw.layer.borderWidth = 0.5
        usernameFld.autocapitalizationType = .Words
        
        pswrdVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        pswrdVw.layer.borderColor = UIColor.init(red: 45/255, green: 78/255, blue: 107/255, alpha: 1).CGColor
        pswrdVw.backgroundColor = UIColor.init(red: 45/255, green: 78/255, blue: 107/255, alpha: 1)
        pswrdVw.layer.masksToBounds = false;
        pswrdVw.layer.borderWidth = 0.5
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
