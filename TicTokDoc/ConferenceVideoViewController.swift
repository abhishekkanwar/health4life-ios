

import UIKit
import OpenTok
import SafariServices
import RealmSwift
import IQKeyboardManagerSwift
import KMPlaceholderTextView
class ConferenceVideoViewController: UIViewController,WebServiceDelegate,UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet weak var msgTableVw: UITableView!
    @IBOutlet var endCallButton: UIButton!
    @IBOutlet var swapCameraButton: UIButton!
    @IBOutlet var muteMicButton: UIButton!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet weak var addUserButton: UIButton!
    @IBOutlet weak var endCallBackVw: UIView!
    @IBOutlet weak var tempEndBtn: UIButton!
    @IBOutlet weak var chatHideShowBtn: UIButton!
    @IBOutlet weak var textMsgTxtFld: KMPlaceholderTextView!
    @IBOutlet weak var chatTxtVw: UIView!
    
    
    var realm = try! Realm()
    var groupId = String()
    var displayName = String()
    var disconnectSelf = false
    var msgClr = Bool()
    var msgLbl = UILabel()
    var labelBounds = CGRect()
    var rowHeight = CGFloat()
    
    var msgAr = NSMutableArray()
    
    var kApiKey = NSString()
    var kSessionId = NSString()
    var kToken = NSString()
    var drDetail = NSDictionary()
    var redirectUrl = NSString()
    var url = NSString()
    var inviteUrl = NSString()
    var availableID = NSString()
    var doctorId = NSString()
    var patientId = NSString()
    var patName = NSString()
    var ptAdd = NSString()
    var bokId = NSString()
    var schId = NSString()
    var email_id = NSString()
    var deviceToken = NSString()
    var ptSchId = NSString()
    var bookId = NSString()
    var timer = NSTimer()
    
    var connectingPerson = NSString()
    var subscribers: [NSIndexPath: OTSubscriber] = [:]
    var session: OTSession?
    lazy var publisher: OTPublisher = {
        let settings = OTPublisherSettings()
        settings.name = UIDevice.currentDevice().name
        return OTPublisher(delegate: self, settings: settings)!
    }()
    var error: OTError?
    let chatModule =  ChatViewController()
    
    
    
    @IBAction func hideShowChatView(sender:UIButton)
    {
        if(self.msgTableVw.hidden == true)
        {
            chatHideShowBtn.setImage(UIImage(named:"chatInactive"), forState: .Normal)
            UIView.animateWithDuration(0.5, animations: {
                
                self.msgTableVw.alpha = 1.0
                self.collectionView.frame = CGRectMake(0.0,0.0,self.collectionView.frame.size.width,self.collectionView.frame.size.height-self.msgTableVw.frame.size.height)
                self.endCallBackVw.frame = CGRectMake(0.0,self.endCallBackVw.frame.origin.y-self.msgTableVw.frame.size.height,self.endCallBackVw.frame.size.width,self.endCallBackVw.frame.size.height )
                
                if let pubView  = self.publisher.view {
                    
                    
                    pubView.frame = CGRect(origin: CGPoint(x:10,y:30),size: CGSizeMake(100.0, 100.0))
                    pubView.layer.cornerRadius = 50.0
                    pubView.clipsToBounds = true
                    pubView.layer.borderColor = UIColor.init(colorLiteralRed: 0.0/255.0, green: 204.0/255.0, blue: 175.0/255.0, alpha: 1.0).CGColor
                    pubView.layer.borderWidth = 0.5
                    
//                    let publisherDimensions = CGSize(width: self.view.bounds.size.width/2 ,
//                        height: self.collectionView.bounds.size.height/2)
//                    if(self.view.frame.size.height < 600){
//                        pubView.frame = CGRect(origin: CGPoint(x:0,y:self.collectionView.bounds.size.height-publisherDimensions.height),
//                            size: publisherDimensions)
//                    }else{
//                        pubView.frame = CGRect(origin: CGPoint(x:0,y:self.collectionView.bounds.size.height-publisherDimensions.height-20),size: publisherDimensions)
//                    }
//                    pubView.frame = CGRect(origin: CGPoint(x:0,y:self.collectionView.bounds.size.height-publisherDimensions.height),
//                        size: publisherDimensions)
                }
                
                }, completion: { (finished) in
                    
                    self.collectionView.reloadData()
                    self.msgTableVw.hidden = false
                    self.view.layoutIfNeeded()
            })
            
            
            
        }
        else
        {
            chatHideShowBtn.setImage(UIImage(named:"chatActive"), forState: .Normal)
            UIView.animateWithDuration(0.5, animations: {
                
                
                self.msgTableVw.alpha = 0.0
                self.collectionView.frame = CGRectMake(0.0,0.0,self.collectionView.frame.size.width,self.collectionView.frame.size.height+self.msgTableVw.frame.size.height)
                self.endCallBackVw.frame = CGRectMake(0.0,self.endCallBackVw.frame.origin.y+self.msgTableVw.frame.size.height,self.endCallBackVw.frame.size.width,self.endCallBackVw.frame.size.height )
                
                
                if let pubView = self.publisher.view {
                    
                    pubView.frame = CGRect(origin: CGPoint(x:10,y:30),size: CGSizeMake(100.0, 100.0))
                    pubView.layer.cornerRadius = 50.0
                    pubView.clipsToBounds = true
                    pubView.layer.borderColor = UIColor.init(colorLiteralRed: 0.0/255.0, green: 204.0/255.0, blue: 175.0/255.0, alpha: 1.0).CGColor
                    pubView.layer.borderWidth = 0.5
                    
//                    let publisherDimensions = CGSize(width: self.view.bounds.size.width/2 ,
//                        height: self.collectionView.bounds.size.height/2)
//                    if(self.view.frame.size.height < 600){
//                        pubView.frame = CGRect(origin: CGPoint(x:0,y:self.collectionView.bounds.size.height-publisherDimensions.height),
//                            size: publisherDimensions)
//                    }else{
//                        pubView.frame = CGRect(origin: CGPoint(x:0,y:self.collectionView.bounds.size.height-publisherDimensions.height-20),size: publisherDimensions)
//                    }
//                    pubView.frame = CGRect(origin: CGPoint(x:0,y:self.collectionView.bounds.size.height-publisherDimensions.height),
//                        size: publisherDimensions)
                }
                
                
                }, completion: { (finished) in
                    
                    self.collectionView.reloadData()
                    self.msgTableVw.hidden = true
                    self.view.layoutIfNeeded()
            })
            
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        addUserButton.layer.borderWidth = 0.5
        addUserButton.layer.cornerRadius = addUserButton.frame.size.width/2
        addUserButton.layer.borderColor = UIColor.init(colorLiteralRed: 0.0/255.0, green: 204.0/255.0, blue: 175.0/255.0, alpha: 1.0).CGColor
        
        
        IQKeyboardManager.sharedManager().enable = true
        chatHideShowBtn.setImage(UIImage(named:"chatActive"), forState: .Normal)
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Connecting...")
        
        chatTxtVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        chatTxtVw.layer.borderColor   = UIColor.darkGrayColor().CGColor
        chatTxtVw.layer.masksToBounds = false;
        chatTxtVw.layer.borderWidth   = 0.8
        textMsgTxtFld.tintColor = UIColor.blackColor()
        
        UIApplication.sharedApplication().idleTimerDisabled = true
        tempEndBtn.hidden = true
        self.view.backgroundColor = UIColor.whiteColor()
        self.msgTableVw.frame = CGRectMake(8.0, UIScreen.mainScreen().bounds.size.height-180-55, UIScreen.mainScreen().bounds.size.width - 16, 180)
        msgTableVw.layer.borderColor   = UIColor.darkGrayColor().CGColor
        msgTableVw.layer.borderWidth   = 0.5
        
        if let firstname = NSUserDefaults.standardUserDefaults().valueForKey("firstName") as? String
        {
            if let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype"){
                if(userType as! NSObject == 1){
                    let lasName = NSUserDefaults.standardUserDefaults().valueForKey("lastName") as! String
                    self.displayName = "Dr. \(lasName)"
                }else{
                    self.displayName = firstname
                }
            }
        }
        else
        {
            self.displayName = "Guest"
        }
        
//        if let firstname = NSUserDefaults.standardUserDefaults().valueForKey("firstName") as? String
//        {
//            if let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype"){
//                if(userType as! NSObject == 1){
//                    self.displayName = "\(firstname)"
//                }else{
//                    self.displayName = firstname
//                }
//            }
//        }
//        else
//        {
//            self.displayName = "Guest"
//        }
        
        
        msgTableVw.delegate = self
        msgTableVw.dataSource = self
        
        
        self.collectionView.frame = CGRectMake(0.0, 0.0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height-180-55-59)
        
        self.endCallBackVw.frame = CGRectMake(0.0, self.collectionView.frame.size.height,UIScreen.mainScreen().bounds.size.width, 59 )
        
        
        endCallButton.frame = CGRectMake((UIScreen.mainScreen().bounds.size.width-endCallButton.frame.size.width)/2, (self.endCallBackVw.frame.size.height-self.endCallButton.frame.size.height)/2, self.endCallButton.frame.size.width, self.endCallButton.frame.size.height)
        
        swapCameraButton.frame = CGRectMake(endCallButton.frame.origin.x-self.swapCameraButton.frame.size.width*1.5, (self.endCallBackVw.frame.size.height-self.swapCameraButton.frame.size.height)/2, self.swapCameraButton.frame.size.width, self.swapCameraButton.frame.size.height)
        
        muteMicButton.frame = CGRectMake(endCallButton.frame.origin.x+self.swapCameraButton.frame.size.width*1.5, (self.endCallBackVw.frame.size.height-self.muteMicButton.frame.size.height)/2, self.muteMicButton.frame.size.width, self.muteMicButton.frame.size.height)
        chatHideShowBtn.frame = CGRectMake(UIScreen.mainScreen().bounds.size.width - chatHideShowBtn.frame.size.width-5, (self.endCallBackVw.frame.size.height-self.muteMicButton.frame.size.height)/2, self.chatHideShowBtn.frame.size.width, self.chatHideShowBtn.frame.size.height)
        
        
        self.navigationController?.navigationBarHidden = true
        self.collectionView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0)
        session =  OTSession(apiKey: kApiKey as String, sessionId: kSessionId as String, delegate: self)!
        session!.connectWithToken(kToken as String, error: &error)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let usertype = defaults.valueForKey("usertype") {
            if(usertype as! NSObject == 0){
                addUserButton.hidden = true
            }
            if(usertype as! NSObject  == 2){
                addUserButton.hidden = true
            }
            if(usertype as! NSObject  == 1){
                addUserButton.enabled = false
                timer = NSTimer.scheduledTimerWithTimeInterval(40.0, target: self, selector: #selector(ConferenceVideoViewController().disconnect), userInfo: nil, repeats: false)
            }
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ConferenceVideoViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = true
        msgTableVw.addGestureRecognizer(tapGesture)
        
    }
    
    func hideKeyboard() {
        textMsgTxtFld.resignFirstResponder()
    }
    
    @IBAction func userBtnAction(sender: AnyObject) {
        let completeMsg = "You have been invited to join a Health4life Concierge Telemedicine Video Call! \nPlease follow these instructions for joining the Video Call on a MOBILE DEVICE:\n1. Click on this link: - \(redirectUrl)\n2. The Health4life App will open, the URL will auto populate and then click ‘Connect Now’\n\nPlease follow these instructions for joining the Video Call on a COMPUTER:\n1. Click on this link: - \(inviteUrl)\n2.You will be automatically connected into this Video Call. \n\nIf you do not have the Health4life App on your device, you can download it here:\n\nIOS Device: https://itunes.apple.com/us/app/health4life-concierge-telemedicine/id1269065511?l=es&mt=8\n\nAndroid Device: https://play.google.com/store/apps/details?id=com.trigma.tiktok&hl=en\n\nSincerely,\nHealth4life Admin"
        //let completemsg = "Please click on the link below! This will route you to the Video Visit that you have been invited to.\n\n \(inviteUrl) \n\n Sincerely,\n Health4life Admin"
        let textToShare = [ completeMsg ]
        let activityController = UIActivityViewController(activityItems:textToShare, applicationActivities: nil)
        activityController.setValue("Please join my secure Video Visit through Health4Life", forKey: "Subject")
        activityController.popoverPresentationController?.sourceView = self.view
        activityController.excludedActivityTypes = [UIActivityTypeAirDrop, UIActivityTypeAddToReadingList]
        activityController.completionWithItemsHandler = { activity, success, items, error in
            if activity == UIActivityTypeMail {
                print("mail")
            }
            print("activity: \(activity), success: \(success), items: \(items), error: \(error)")
        }
        self.presentViewController(activityController, animated: true, completion: nil)
        
    }
    
    func homeAc(){
        let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To End The Call?", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default) {
            UIAlertAction in
        }
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            if AppManager.NetWorkReachability.isConnectedToNetwork()==true
            {
                self.session!.disconnect(nil)
                let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.window = UIWindow(frame: UIScreen.mainScreen().bounds)
                var storyboard = UIStoryboard()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                }else{
                    storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                }
                let vc = storyboard.instantiateViewControllerWithIdentifier("PatientHomeViewController") as! PatientHomeViewController
                let nav = UINavigationController(rootViewController: vc)
                appDelegate.window?.rootViewController = nav
                appDelegate.window?.makeKeyAndVisible()
            }
        }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    override func viewDidAppear(animated: Bool) {
        guard let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        layout.itemSize = CGSize(width: collectionView.bounds.size.width / 2,
                                 height: collectionView.bounds.size.height / 2)
        
        if(msgAr.count == 0)
        {
            self.getMsgsFromServer()
        }
        
    }
    
    
    
    func disconnect(){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        let alert = UIAlertController(title: "Alert", message: "Patient Did Not Picked The Call.Please Try After Sometime.", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
        }
        let okAction = UIAlertAction(title: "Call Again", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Please Wait...")
            let defaults = NSUserDefaults.standardUserDefaults()
            if let usertype = defaults.valueForKey("usertype") {
                if(usertype as! NSObject  == 1){
                    self.timer = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: #selector(ConferenceVideoViewController().disconnect), userInfo: nil, repeats: false)
                }
            }
        }
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func swapCameraAction(sender: AnyObject) {
        if publisher.cameraPosition == .Front {
            publisher.cameraPosition = .Back
        } else {
            publisher.cameraPosition = .Front
        }
    }
    @IBAction func muteMicAction(sender: AnyObject) {
        if(publisher.publishAudio == true){
            publisher.publishAudio = false
            muteMicButton.setBackgroundImage(UIImage(named: "muteBtn"), forState: .Normal)
            muteMicButton.selected = true
            
        }else{
            publisher.publishAudio = true
            muteMicButton.setBackgroundImage(UIImage(named: "volBtn"), forState: .Normal)
            muteMicButton.selected = false
        }
    }
    
    @IBAction func endCallAction(sender: AnyObject) {
        let defaults = NSUserDefaults.standardUserDefaults()
        print(defaults.valueForKey("usertype"))
        let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To End The Call?", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default) {
            UIAlertAction in
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            if AppManager.NetWorkReachability.isConnectedToNetwork()==true
            {
                if  self.session!.sessionConnectionStatus == .Connected {
                    let defaults = NSUserDefaults.standardUserDefaults()
                    if let usertype = defaults.valueForKey("usertype") {
                        if(usertype as! NSObject  == 1){
                            self.session!.disconnect(&self.error)
                        }else if(usertype as! NSObject  == 0){
                            self.session!.disconnect(&self.error)
                        }else if(usertype as! NSObject  == 2){
                            self.disconnectSelf = true
                            self.session!.disconnect(&self.error)
                        }
                    }
                }
                else {
                    if self.navigationController!.viewControllers.indexOf(self) != NSNotFound {
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                }
            }
            else
            {
                AppManager.sharedManager.Showalert("No Internet Connection", alertmessage:  "Make sure your device is connected to the internet.")
            }
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
        
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        let defaults = NSUserDefaults.standardUserDefaults()
        if let usertype = defaults.valueForKey("usertype") {
            if(usertype as! NSObject == 0){
                dispatch_async(dispatch_get_main_queue()) {
                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                    var storyboard = UIStoryboard()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                    }else{
                        storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    }
                    let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorRatingViewController") as! DoctorRatingViewController
                    vc.drDetailDict = self.drDetail
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }else{
                AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                dispatch_async(dispatch_get_main_queue()) {
                    let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                    backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                    backVw.tag = 2369
                    self.view.addSubview(backVw)
                    
                    let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21))
                    alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                    alerVw.backgroundColor = UIColor.whiteColor()
                    backVw.addSubview(alerVw)
                    
                    let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                    rightImg.image = UIImage(named: "verifyImg")
                    alerVw.addSubview(rightImg)
                    
                    let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                    verificationLbl.text = " Alert"
                    verificationLbl.font = UIFont.init(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                    verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(verificationLbl)
                    
                    let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                    lineVw.backgroundColor = UIColor.grayColor()
                    alerVw.addSubview(lineVw)
                    
                    let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
                    contentLbl.font = UIFont.init(name: "Helvetica", size:14)
                    contentLbl.textAlignment = .Center
                    contentLbl.textColor = UIColor.grayColor()
                    contentLbl.text = "Do you need to write a prescription for your Patient?"
                    contentLbl.numberOfLines = 2
                    alerVw.addSubview(contentLbl)
                    
                    let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width/2, alerVw.frame.size.height*0.22))
                    okLbl.setTitle("Yes", forState: .Normal)
                    okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                    okLbl.titleLabel?.textAlignment = .Center
                    okLbl.addTarget(self, action: #selector(ConferenceVideoViewController.okBtnAc), forControlEvents: .TouchUpInside)
                    okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(okLbl)
                    let lineLbl = UILabel.init(frame: CGRectMake(okLbl.frame.size.width-2,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),1.2, alerVw.frame.size.height*0.22))
                    lineLbl.backgroundColor = UIColor.lightGrayColor()
                    alerVw.addSubview(lineLbl)
                    
                    let noLbl = UIButton.init(frame: CGRectMake(okLbl.frame.size.width,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width/2, alerVw.frame.size.height*0.22))
                    noLbl.setTitle("No", forState: .Normal)
                    noLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                    noLbl.titleLabel?.textAlignment = .Center
                    noLbl.addTarget(self, action: #selector(ConferenceVideoViewController.noAc), forControlEvents: .TouchUpInside)
                    noLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(noLbl)
                    
                    
                }
            }
        }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("Alert", alertmessage: failureError.localizedDescription)
    }
    func okBtnAc(){
        let vc = storyboard!.instantiateViewControllerWithIdentifier("WebViewController") as! WebViewController
        vc.ptId = self.patientId
        vc.address = self.ptAdd
        vc.ptName = self.patName
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func noAc(){
        self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
    }
    func reloadCollectionView() {
        collectionView.hidden = subscribers.count == 0
        collectionView.reloadData()
    }
    //   MARK:- Chat module
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.textMsgTxtFld.endEditing(true)
    }
    
    
    
    @IBAction func sendBtnAc(sender: AnyObject){
         textMsgTxtFld.resignFirstResponder()
        if(textMsgTxtFld.text == ""){
            textMsgTxtFld.resignFirstResponder()
        }else{
            self.sendChatMessage()
        }
    }
    
    func sendChatMessage() {
        let error: OTError? = nil
        do {
            let defaults = NSUserDefaults.standardUserDefaults()
            let usertype = defaults.valueForKey("usertype")
            
            var userId = "guest_userId"
            
            if let id = defaults.valueForKey("id") as? String
            {
                userId = id
            }
            
            let msgId = "\(usertype!)_"+"\(NSDate())"
            let chatMsg = NSMutableDictionary()
            chatMsg.setValue(msgId, forKey: "msgId")
            chatMsg.setValue(self.groupId, forKey: "groupId")
            chatMsg.setValue(userId, forKey: "fromId")
            chatMsg.setValue("\(self.displayName):-\n\(textMsgTxtFld.text!)", forKey: "message")
            chatMsg.setValue("text", forKey: "messageType")
            chatMsg.setValue(self.displayName, forKey: "displayName")
            chatMsg.setValue(false, forKey: "readStatus")
            
            let strinData = try! NSJSONSerialization.dataWithJSONObject(chatMsg, options: NSJSONWritingOptions.PrettyPrinted)
            let str = NSString(data: strinData, encoding: NSUTF8StringEncoding)
            
            session!.signalWithType("chat", string:  str as? String, connection: nil ,error: nil)
        }
        if error != nil {
            print("Signal error: \(error!)")
        }
        else {
            print("Signal sent: \(textMsgTxtFld.text)")
        }
        self.textMsgTxtFld.text = ""
    }
    
    func logSignalString(string: String, fromSelf: Bool, msgType:String,name:String) {
        let dict = ["msg":string,"bool":fromSelf, "msgType":msgType,"name":name]
        
        self.msgAr.addObject(dict)
        self.msgTableVw.reloadData()
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            let indexPath = NSIndexPath.init(forRow: self.msgAr.count-1, inSection: 0)
            self.msgTableVw.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
            
        }
    }
    
    func tableViewScrollToBottom(animated: Bool) {
        
        let delay = 2.0 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        
        dispatch_after(time, dispatch_get_main_queue(), {
            
            let numberOfSections = 1
            let numberOfRows = self.msgAr.count
            
            if numberOfRows > 0 {
                
            }
            
        })
    }
    
    
    func getMsgsFromServer() -> Void {
        let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as? NSInteger
        if(userType != 2){
        var userId = "guest_userId"
        
        if let id = NSUserDefaults.standardUserDefaults().valueForKey("id") as? String
        {
            userId = id
            
            RealmInteractor.shared.getMsgsFromServer(userId, groupId: self.groupId) {(result) in
                
                if(result.valueForKey("status") as! Int == 200)
                {
                    self.msgAr.removeAllObjects()
                    
                    let chatResults = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", self.groupId))
                    
                    let chatArray = result.objectForKey("data") as! NSArray
                    
                    if(chatResults.count<chatArray.count)
                    {
                        try! self.realm.write({
                            self.realm.delete(chatResults)
                        })
                        
                        for dict in chatArray
                        {
                            try! self.realm.write({
                                
                                let group = Group()
                                group.displayName = dict.valueForKey("DisplayName") as! String
                                group.message = dict.valueForKey("message") as! String
                                group.messageType = dict.valueForKey("messageType") as! String
                                group.fromId = dict.valueForKey("From") as! String
                                group.groupId = dict.valueForKey("GroupId") as! String
                                self.realm.add(group)
                                
                            })
                            
                        }
                    }
                    
                    
                    for dict in chatArray
                    {
                        var mySelf = false
                        let msgDict = dict as! NSDictionary
                        
                        if(msgDict.valueForKey("From") as! String == userId)
                        {
                            mySelf = true
                        }
                        
                        let mdict = ["msg":msgDict.valueForKey("message") as! String,"bool":mySelf, "msgType":msgDict.valueForKey("messageType") as! String, "name":msgDict.valueForKey("DisplayName") as! String]
                        self.msgAr.addObject(mdict)
                    }
                    
                    if(self.msgAr.count != 0)
                    {
                        dispatch_async(dispatch_get_main_queue(),{
                             self.msgTableVw.reloadData()
                            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2.0 * Double(NSEC_PER_SEC)))
                            dispatch_after(delayTime, dispatch_get_main_queue()) {
                            let indexPath = NSIndexPath.init(forRow: self.msgAr.count-1, inSection: 0)
                            self.msgTableVw.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom
                                , animated: false)
                            }
                        })
                    }
                    
                 }
              }
            }
          }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return msgAr.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("MessageTableViewCell", forIndexPath: indexPath) as! LGChatMessageCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        let dic = self.msgAr[indexPath.row]
        
        let message = dic["msg"] as! String
        
        
        if((dic["msgType"] as! String) == "link")
        {
            msgClr = dic["bool"] as! Bool
            cell.msgcolor = msgClr
            cell.setupThumbnail("message",name: dic["name"] as! String)
        }
        else
        {
            let padding: CGFloat = 10.0
            
            
            let height = cell.setupWithMessage(message).height
            rowHeight = height + padding
            
            msgClr = dic["bool"] as! Bool
            cell.msgcolor = msgClr
            cell.setupWithMessage(message)
            cell.selectionStyle = .None
        }
        
        return cell
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let dic = self.msgAr[indexPath.row]
        if((dic["msgType"] as! String) == "link")
        {
            return 110
        }
        return rowHeight
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.textMsgTxtFld.endEditing(true)
        
        let dic = self.msgAr[indexPath.row]
        
        
        if((dic["msgType"] as! String) == "link")
        {
            if #available(iOS 9.0, *) {
                let controller = SFSafariViewController(URL: NSURL.init(string: dic["msg"] as! String)!)
                self.presentViewController(controller, animated: true, completion: nil)
            } else {
            }
            
        }
    }
    @IBAction func attachmentBtnAc(sender: AnyObject){
        self.view.endEditing(true)
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        actionSheet.showInView(self.view)
    }
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex){
        case 0:
            print("Cancel")
        case 1:
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.allowsEditing = false
                        imagePicker.sourceType = .Camera
                        self.presentViewController(imagePicker, animated: true, completion:nil)
                    }
                })
            }else{
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .Camera
                    presentViewController(imagePicker, animated: true, completion:nil)
                }
            }
        case 2:
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.allowsEditing = false
                        imagePicker.sourceType = .PhotoLibrary
                        imagePicker.mediaTypes = ["public.image", "public.movie"]
                        self.presentViewController(imagePicker, animated: true, completion:nil)
                    }
                })
            }else{
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .PhotoLibrary
                    imagePicker.mediaTypes = ["public.image", "public.movie"]
                    self.presentViewController(imagePicker, animated: true, completion:nil)
                }
            }
        default:
            print("Default")
            //Some code here..
        }
    }
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        let selectedImage : UIImage = image
        // let resizeImage = selectedImage.resizeWithPercentage(0.1)
        let imageData = UIImageJPEGRepresentation(selectedImage, 0.4)
        
        self.uploadFile(imageData!,ext: "jpg")
        
       
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        print(mediaType)
        if(mediaType == "public.image"){
            let imgUrl =  info["UIImagePickerControllerOriginalImage"] as! UIImage
             //let resizeImage = imgUrl.resizeWithPercentage(0.1)
            do {
                let imageData = UIImageJPEGRepresentation(imgUrl, 0.4)
                //let imageData: NSData = UIImagePNGRepresentation(resizeImage!)!
                self.uploadFile(imageData!,ext: "jpg")
                
            } catch {
                print(error)
                return
            }
            
        }else{
            let videoURL = info["UIImagePickerControllerMediaURL"] as? NSURL
            
            let fileExt  = videoURL!.lastPathComponent
            
            
            var movieData: NSData?
            do {
                let video = try NSData(contentsOfURL: videoURL!, options: .DataReadingMappedIfSafe)
                self.uploadFile(video,ext: "mp4")
            } catch {
                print(error)
                return
            }
        }
          self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    func uploadFile(docData: NSData, ext:String) {
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "uploading...")
        var string: String = ""
        
       
        
        let request = NSMutableURLRequest(URL: NSURL(string: "\(Header.BASE_URL)videoChat/uploadApi")!, cachePolicy: NSURLRequestCachePolicy(rawValue: UInt(0))!, timeoutInterval: 10.0)
        
        request.HTTPMethod = "POST"
        request.addValue("0", forHTTPHeaderField: "devicetype")
        
        let body = NSMutableData()
        let my_time: Double = NSDate().timeIntervalSince1970
        
        let imageName: String = "\(1)\(Int(my_time))"
        let imagetag: String = "Content-Disposition: form-data; name=\"File\"; filename=\""
        string = "\(imagetag)\(imageName)\(".\(ext)\"\r\n\"")"
        let boundary: String = "---------------------------14737809831466499882746641449"
        let contentType: String = "multipart/form-data; boundary=\(boundary)"
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        
        body.appendData("\r\n--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData(string.dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData("Content-Type: application/octet-stream\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        body.appendData(docData)
        body.appendData("\r\n--\(boundary)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        request.HTTPBody = body
        
        NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            dispatch_async(dispatch_get_main_queue(),{
                AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                
                if(data?.length != 0 && error == nil)
                {
                    let str = String(data: data!, encoding: NSUTF8StringEncoding)
                    print("\(str)")
                    let result = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: UInt(0)))
                    
                    if let resultDict = result as? NSDictionary{
                        
                        let error: OTError? = nil
                        do {
                            var userId = "guest_userId"
                            
                            if let id = NSUserDefaults.standardUserDefaults().valueForKey("id") as? String
                            {
                                userId = id
                            }
                            let docLink = resultDict.valueForKey("link")
                            
                            let defaults = NSUserDefaults.standardUserDefaults()
                            let usertype = defaults.valueForKey("usertype")
                            let msgId = "\(usertype!)_"+"\(NSDate())"
                            let chatMsg = NSMutableDictionary()
                            chatMsg.setValue(msgId, forKey: "msgId")
                            chatMsg.setValue(self.groupId, forKey: "groupId")
                            chatMsg.setValue(userId, forKey: "fromId")
                            chatMsg.setValue(docLink, forKey: "message")
                            chatMsg.setValue("link", forKey: "messageType")
                            chatMsg.setValue(self.displayName, forKey: "displayName")
                            chatMsg.setValue(false, forKey: "readStatus")
                            
                            let strinData = try! NSJSONSerialization.dataWithJSONObject(chatMsg, options: NSJSONWritingOptions.PrettyPrinted)
                            let str = NSString(data: strinData, encoding: 0)
                            
                            self.session!.signalWithType("chat", string:  str as? String, connection: nil ,error: nil)
                        }
                        if error != nil {
                            print("Signal error: \(error!)")
                        }
                        else {
                            print("Signal sent: ")
                        }
                        
                        
                    }
                    
                }
                else{
                    let alert = UIAlertController(title: "Alert", message: "Sending Failed.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let okAction = UIAlertAction(title: "Retry", style: UIAlertActionStyle.Cancel) {
                        UIAlertAction in
                        self.uploadFile(docData,ext: ext)
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                        NSLog("Cancel Pressed")
                    }
                    alert.addAction(okAction)
                    alert.addAction(cancelAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            })
            }.resume()
        
        
    }
}

extension ConferenceVideoViewController: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subscribers.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("subscriberCell", forIndexPath: indexPath) as! SubscriberCollectionCell
        cell.subscriber = subscribers[indexPath]
        return cell
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        if(self.msgTableVw.hidden == true)
        {
            if(subscribers.count <= 1){
                let width = self.view.frame.size.width
                return CGSizeMake(width, width+180)
            }else{
                let width = self.view.frame.size.width/2
                return CGSizeMake(width, self.view.frame.size.width+180)
            }
        }
        else
        {
            if(subscribers.count <= 1){
                let width = self.view.frame.size.width
                return CGSizeMake(width, width)
            }else{
                let width = self.view.frame.size.width/2
                return CGSizeMake(width, self.view.frame.size.width)
            }
        }
        
    }
}

extension ConferenceVideoViewController: UICollectionViewDelegate {
}

// MARK: - Subscriber Cell
class SubscriberCollectionCell: UICollectionViewCell {
    @IBOutlet var muteButton: UIButton!
    
    var subscriber: OTSubscriber?
    
    @IBAction func muteSubscriberAction(sender: AnyObject) {
        subscriber?.subscribeToAudio = !(subscriber?.subscribeToAudio ?? true)
        
    }
    
    override func layoutSubviews() {
        if let sub = subscriber, let subView = sub.view {
            subView.frame = bounds
            contentView.insertSubview(subView, belowSubview: muteButton)
            
            muteButton.enabled = true
            muteButton.hidden = false
        }
    }
}
// MARK: - OpenTok Methods
extension ConferenceVideoViewController {
    func doPublish() {
        
        swapCameraButton.enabled = true
        muteMicButton.enabled = true
        endCallButton.enabled = true
        if let pubView = publisher.view {
            
            pubView.frame = CGRect(origin: CGPoint(x:10,y:30),size: CGSizeMake(100.0, 100.0))
            pubView.layer.cornerRadius = 50.0
            pubView.clipsToBounds = true
            pubView.layer.borderColor = UIColor.init(colorLiteralRed: 0.0/255.0, green: 204.0/255.0, blue: 175.0/255.0, alpha: 1.0).CGColor
            pubView.layer.borderWidth = 0.5
            
//            let publisherDimensions = CGSize(width: view.bounds.size.width/2 ,
//                                             height: collectionView.bounds.size.height/2)
//            if(self.view.frame.size.height < 600){
//                pubView.frame = CGRect(origin: CGPoint(x:0,y:collectionView.bounds.size.height-publisherDimensions.height),
//                                       size: publisherDimensions)
//            }else{
//                pubView.frame = CGRect(origin: CGPoint(x:0,y:collectionView.bounds.size.height-publisherDimensions.height-20),size: publisherDimensions)
//            }
            
            
//            pubView.frame = CGRect(origin: CGPoint(x:0,y:collectionView.bounds.size.height-publisherDimensions.height),
//                                   size: publisherDimensions)
            
            view.addSubview(pubView)
        }
        session!.publish(publisher, error: &error)
    }
    
    func doSubscribe(to stream: OTStream) {
        if let subscriber = OTSubscriber(stream: stream, delegate: self) {
            let indexPath = NSIndexPath.init(forItem: subscribers.count, inSection: 0)
            subscribers[indexPath] = subscriber
            session!.subscribe(subscriber, error: &error)
        }
        collectionView.reloadData()
    }
    
    func findSubscriber(byStreamId id: String) -> (NSIndexPath, OTSubscriber)? {
        for (_, entry) in subscribers.enumerate() {
            if let stream = entry.1.stream where stream.streamId == id {
                return (entry.0, entry.1)
            }
           
        }
        
        
        return nil
    }
    
    func findSubscriberCell(byStreamId id: String) -> SubscriberCollectionCell? {
        for cell in collectionView.visibleCells() {
            if let subscriberCell = cell as? SubscriberCollectionCell,
                let subscriberOfCell = subscriberCell.subscriber where
                (subscriberOfCell.stream?.streamId ?? "") == id
            {
                return subscriberCell
            }
        }
        return nil
    }
}

// MARK: - OTSession delegate callbacks
extension ConferenceVideoViewController: OTSessionDelegate {
    
    func session(session: OTSession, receivedSignalType type: String?, fromConnection connection: OTConnection?, withString string: String?) {
        print("Received signal \(string!)")
        
        let recData = string?.dataUsingEncoding(NSUTF8StringEncoding)
        let recDictionary = try! NSJSONSerialization.JSONObjectWithData(recData!, options: NSJSONReadingOptions(rawValue: UInt(0)))
        
        var userId = "guest_userId"
        
        if let id = NSUserDefaults.standardUserDefaults().valueForKey("id") as? String
        {
            userId = id
            
            try! self.realm.write({
                let group = Group()
                group.displayName = recDictionary.valueForKey("displayName") as! String
                group.message = recDictionary.valueForKey("message") as! String
                group.messageType = recDictionary.valueForKey("messageType") as! String
                group.msgId = recDictionary.valueForKey("msgId") as! String
                group.fromId = recDictionary.valueForKey("fromId") as! String
                group.groupId = self.groupId
                self.realm.add(group)
            })
        }
        
        
        
        var fromSelf = false
        if (connection!.connectionId == session.connection!.connectionId) {
            fromSelf = true
        }
        
        
        let groupDict = NSMutableDictionary()
        groupDict.setValue(self.groupId, forKey:"GroupId")
        groupDict.setValue((recDictionary.valueForKey("fromId") as! String), forKey:"From")
        groupDict.setValue((recDictionary.valueForKey("message") as! String), forKey:"message")
        groupDict.setValue((recDictionary.valueForKey("msgId") as! String), forKey:"messageId")
        groupDict.setValue((recDictionary.valueForKey("messageType") as! String), forKey:"messageType")
        groupDict.setValue((recDictionary.valueForKey("displayName") as! String), forKey:"DisplayName")
        
        let groupArray = NSMutableArray()
        groupArray.addObject(groupDict)
        
        let chatDict = NSDictionary(objects: [groupArray,Int(0),userId,self.groupId], forKeys: ["Data","BatchPush","PushUserId","GroupId"])
        if(fromSelf)
        {
            RealmInteractor.shared.syncMsgsWithMsg(chatDict, push: false)
        }
      
        self.logSignalString(recDictionary.valueForKey("message") as! String, fromSelf: fromSelf, msgType: recDictionary.valueForKey("messageType") as! String, name: recDictionary.valueForKey("displayName") as! String)
        
    }
    
    
    
    func sessionDidConnect(session: OTSession) {
        print("Session connected")
        
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        doPublish()
    }
    
    func sessionDidDisconnect(session: OTSession) {
        print("Session disconnected")
        subscribers.removeAll()
        reloadCollectionView()
        textMsgTxtFld.resignFirstResponder()
        let defaults = NSUserDefaults.standardUserDefaults()
        if let usertype = defaults.valueForKey("usertype") {
            if(usertype as! NSObject  == 1){
                let firstname:String = defaults.valueForKey("firstName") as! String
                let lastname:String = defaults.valueForKey("lastName") as! String
                AppManager.sharedManager.delegate=self
                let dateC = NSDate()
                let formatter = NSDateFormatter()
                formatter.dateFormat = "MMMM dd,yyyy"
                let defaultTimeZoneStr = formatter.stringFromDate(dateC)
                let params = "BookingId=\(bokId)&drschedulesetsId=\(schId)&DeviceToken=\(deviceToken)&Schedule=\(defaultTimeZoneStr)&PatientEmail=\(email_id)&DrName=\(firstname) \(lastname)&User=1&GroupId=\(self.groupId)"
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/SchduleOver")
            }else if(usertype as! NSObject  == 0){
                AppManager.sharedManager.delegate=self
                let username = defaults.valueForKey("email") as! NSString
                let dateC = NSDate()
                let formatter = NSDateFormatter()
                formatter.dateFormat = "MMMM dd,yyyy"
                let defaultTimeZoneStr = formatter.stringFromDate(dateC)
                let firstname = drDetail.valueForKey("FirstName") as! NSString
                let lastName = drDetail.valueForKey("LastName") as! NSString
                let params = "BookingId=\(bookId)&drschedulesetsId=\(ptSchId)&DeviceToken=\(deviceToken)&Schedule=\(defaultTimeZoneStr)&PatientEmail=\(username)&DrName=\(firstname) \(lastName)&User=0&GroupId=\(self.groupId)"
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/SchduleOver")
            }
            else if(usertype as! NSObject  == 2){
                if(disconnectSelf == false){
                    let alert = UIAlertController(title: "Alert", message: "Thanks for using Health4Life, the call has been disconnected now. If the call has been disconnected due to some network issue please copy and paste the link below to join the call.", preferredStyle: UIAlertControllerStyle.Alert)
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel) {
                        UIAlertAction in
                        var storyboard = UIStoryboard()
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            storyboard = UIStoryboard(name: "Main", bundle: nil)
                        }else{
                            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                        }
                        if(self.connectingPerson == "Doctor"){
                            defaults.setValue(1, forKey: "usertype")
                            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                            let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorHomeViewController") as! DoctorHomeViewController
                            let nav = UINavigationController(rootViewController: vc)
                            appDelegate.window?.rootViewController = nav
                            appDelegate.window?.makeKeyAndVisible()
                        }
                        else{
                            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                            let vc = storyboard.instantiateViewControllerWithIdentifier("mainViewController") as! ViewController
                            let nav = UINavigationController(rootViewController: vc)
                            appDelegate.window?.rootViewController = nav
                            appDelegate.window?.makeKeyAndVisible()
                            
                        }
                    }
                    alert.addAction(okAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }else{
                    if(connectingPerson != "Doctor"){
                    defaults.removeObjectForKey("usertype")
                    }
                    var storyboard = UIStoryboard()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                    }else{
                        storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    }
                    if(self.connectingPerson == "Doctor"){
                        defaults.setValue(1, forKey: "usertype")
                        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorHomeViewController") as! DoctorHomeViewController
                        let nav = UINavigationController(rootViewController: vc)
                        appDelegate.window?.rootViewController = nav
                        appDelegate.window?.makeKeyAndVisible()
                    }
                    else{
                        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        let vc = storyboard.instantiateViewControllerWithIdentifier("mainViewController") as! ViewController
                        let nav = UINavigationController(rootViewController: vc)
                        appDelegate.window?.rootViewController = nav
                        appDelegate.window?.makeKeyAndVisible()
                        
                    }
                }
            }
        }
    }
    
    func session(session: OTSession, streamCreated stream: OTStream) {
        print("Session streamCreated: \(stream.streamId)")
        if subscribers.count > 2 {
            print("Sorry this sample only supports up to 4 subscribers :)")
            return
        }
        doSubscribe(to: stream)
    }
    
    func session(session: OTSession, streamDestroyed stream: OTStream) {
        print("Session streamDestroyed: \(stream.streamId)")
        
        guard let (index, subscriber) = findSubscriber(byStreamId: stream.streamId) else {
            return
        }
        var maybeError : OTError?
        session.unsubscribe(subscriber, error: &maybeError)
        if let error = maybeError {
        }
        
       // subscriber.view?.removeFromSuperview()
        subscribers.removeValueForKey(index)
        reloadCollectionView()
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let usertype = defaults.valueForKey("usertype") {
            
            if(subscribers.count == 0){
                if(usertype as! NSObject  == 1){
                    self.session!.disconnect(&self.error)
                    
                }else if(usertype as! NSObject  == 0){
                    self.session!.disconnect(&self.error)
                    
                }
            }
            if(usertype as! NSObject  == 2){
                self.session!.disconnect(&self.error)
            }
            
        }
        
        
        
    }
    
    func session(session: OTSession, didFailWithError error: OTError) {
        print("session Failed to connect: \(error.localizedDescription)")
       
        let alert = UIAlertController(title: "Alert", message: "No internet connection.", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.session!.disconnect(&self.error)
            
        }
        alert.addAction(okAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

// MARK: - OTPublisher delegate callbacks
extension ConferenceVideoViewController: OTPublisherDelegate {
    func publisher(publisher: OTPublisherKit, streamCreated stream: OTStream) {
    }
    
    func publisher(publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
    }
    
    func publisher(publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("Publisher failed: \(error.localizedDescription)")
        
        let alert = UIAlertController(title: "Alert", message: "No internet connection.", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.session!.disconnect(&self.error)
            
        }
        alert.addAction(okAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

// MARK: - OTSubscriber delegate callbacks
extension ConferenceVideoViewController: OTSubscriberDelegate {
    func subscriberDidConnectToStream(subscriber: OTSubscriberKit) {
        print("Subscriber connected")
        timer.invalidate()
        addUserButton.enabled = true
        let defaults = NSUserDefaults.standardUserDefaults()
        if let usertype = defaults.valueForKey("usertype") {
            if(usertype as! NSObject == 0){
                
            }
        }
        reloadCollectionView()
    }
    
    func subscriber(subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("Subscriber failed: \(error.localizedDescription)")
        let defaults = NSUserDefaults.standardUserDefaults()
        print(defaults.valueForKey("usertype"))
        let alert = UIAlertController(title: "Alert", message: "No internet connection.", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.session!.disconnect(&self.error)
                       
        }
        alert.addAction(okAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func subscriberVideoDataReceived(subscriber: OTSubscriber) {
    }
    
    
}


