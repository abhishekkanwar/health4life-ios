//
//  UIViewControllerExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/19/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setNavigationBarItem() {

        //self.addRightBarButtonWithImage(UIImage(named: "menu")!)
        self.title = "My Appoinments"
         self.navigationController?.navigationBar.barTintColor = UIColor.blackColor()
        let attributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont(name: "Helvetica", size: 22)!
        ]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        if(self.title == "My Appoinments"){
            self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addRightGestures()
        }
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
      self.slideMenuController()?.removeRightGestures()
    }
}
//public func addLeftBarButtonWithImage(buttonImage: UIImage) {
//    let leftButton: UIBarButtonItem = UIBarButtonItem(image: buttonImage, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.toggleLeft))
//    navigationItem.leftBarButtonItem = leftButton;
//}

////    public func addRightBarButtonWithImage(buttonImage: UIImage) {
////        let rightButton: UIBarButtonItem = UIBarButtonItem(image: buttonImage, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.toggleRight))
////        navigationItem.rightBarButtonItem = rightButton;
////    }
//@IBAction func toggleRight1(sender: AnyObject){
//    slideMenuController()?.toggleRight()
//}
//public func toggleLeft() {
//    slideMenuController()?.toggleLeft()
//}
//
////    public func toggleRight() {
////        slideMenuController()?.toggleRight()
////    }
