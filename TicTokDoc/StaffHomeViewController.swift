

import UIKit
import SlideMenuControllerSwift
import EventKit
import RealmSwift
import AudioToolbox
class StaffHomeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,WebServiceDelegate{
        @IBOutlet weak var profilePicImgVw: UIImageView!
        @IBOutlet weak var drName: UILabel!
        @IBOutlet weak var speciality: UILabel!
        @IBOutlet weak var chatNotifBadge : UILabel!
        var arMenu = ["My Appointments", "My Schedules","My Patients","Search Doctors/Staff"]
        var arMenuImages = ["HomeClock", "HomeCalender","HomePateints","HomesearchDoc"]
        var serverInt = NSInteger()
        var count = NSInteger()
        var pndingPatCount = NSInteger()
        var schCount = NSInteger()
        var timer = NSTimer()
        var logoutCon = Bool()
        let realm = try! Realm()
        var tableVwRow = NSInteger()
        var serverint = NSInteger()
    var nameAr = NSMutableArray()
    var specialityAr = NSMutableArray()
    var imgAr = NSMutableArray()
    var drIdAr = NSMutableArray()
    var showDotAr = NSMutableArray()
    var player: AVAudioPlayer?
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        @IBOutlet weak var homeTableVw: UITableView!
        override func viewDidLoad(){
            super.viewDidLoad()
             self.navigationController?.navigationBarHidden = true
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            
            profilePicImgVw.layer.cornerRadius = self.profilePicImgVw.frame.size.height/2
            profilePicImgVw.layer.borderWidth = self.view.frame.size.height*0.012
            profilePicImgVw.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
            profilePicImgVw.clipsToBounds = true
            profilePicImgVw.setShowActivityIndicatorView(true)
            profilePicImgVw.setIndicatorStyle(.Gray)
            let defaults = NSUserDefaults.standardUserDefaults()
            let imgUrl:String = defaults.valueForKey("selectDrImg") as! String
            if let url = NSURL(string: "http://\(imgUrl)") {
                profilePicImgVw.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
            }
            profilePicImgVw.clipsToBounds = true
            
            chatNotifBadge.layer.cornerRadius = self.chatNotifBadge.frame.size.height/2
            chatNotifBadge.clipsToBounds = true
            
            homeTableVw.dataSource = self
            homeTableVw.delegate = self
           
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(chatRecieveNotification), name: "chatRecieveNotification", object: nil)
        }
    
    func drListHit(){
        timer.invalidate()
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("staffId")
        {
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
            serverint = 1
            self.doRequestPost("\(Header.BASE_URL)staffapi/staffDrList", data: ["StaffId":"\(userId)"])
        }
    }
    @IBAction func selectDocBtnAction(sender: AnyObject){
        slideMenuController()?.toggleLeft()
    }
        //    MARK: - post service
        func tokenGetHit(){
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("staffId"){
                var token =  NSString()
                if(UIDevice.isSimulator == true){
                    token = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
                }else{
                    token = defaults.valueForKey("deviceToken") as! NSString
                }
                serverint = 2
                self.doRequestPost("\(Header.BASE_URL)users/LoginLogout", data: ["DeviceToken":"\(token)","DeviceType":"ios","UserId":"\(userId)"])
            }
        }
    
        func updateChatBadge() -> Void {
            
            if(UIApplication.sharedApplication().applicationIconBadgeNumber > 0)
            {
                self.chatNotifBadge.hidden = false
            }
            else
            {
                self.chatNotifBadge.hidden = true
            }
        }
    
        func chatRecieveNotification(){
            if(appdelegate.openMsgFlag == false){
                if(self.navigationController?.viewControllers.count > 2){
                    self.navigationController?.popToViewController(self.navigationController?.viewControllers[2] as! ViewController, animated:false)
                }
            }
            else{
                appdelegate.openMsgFlag = false
            }
            
            self.messageListAc(self)
        }
        func doRequestPost(url:String,data:[String: NSObject]){
            print("URL FOR DEVICETOKEN ==> \(url)")
            let requestDic = data
            print(requestDic)
            let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
                data ,
                options: NSJSONWritingOptions(rawValue: 0))
            let jsonString = NSString(data: theJSONData!,
                                      encoding: NSASCIIStringEncoding)
            
            print("Request Object:\(data)")
            print("Request string = \(jsonString!)")
            
            let session = NSURLSession.sharedSession()
            
            let urlPath = NSURL(string: url)
            let request = NSMutableURLRequest(URL: urlPath!)
            request.timeoutInterval = 60
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.HTTPMethod = "POST"
            let postLength = NSString(format:"%lu", jsonString!.length) as String
            request.setValue(postLength, forHTTPHeaderField:"Content-Length")
            request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
            
            let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
                if((error) != nil) {
                    print(error!.localizedDescription)
                    var alert = UIAlertController()
                    if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                        alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                        
                    }else{
                        alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                        self.doRequestPost(url, data: requestDic)
                    }
                    alert.addAction(cancelAction)
                    UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
                }else {
                    print("Succes:")
                    let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                    print(responceString)
                    let _: NSError?
                    let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                    dispatch_async(dispatch_get_main_queue()) {
                        if(self.serverint == 2){
                        let status = responseDict.valueForKey("userStatus") as! NSInteger
                        if(status == 0){
                            let alert = UIAlertController(title: "Tik Tok Doc", message: "Your account has been deleted or deactivated, please contact our support for more information.", preferredStyle: UIAlertControllerStyle.Alert)
                            let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                                UIAlertAction in
                                self.deleLogOut()
                            }
                            alert.addAction(cancelAction)
                            self.presentViewController(alert, animated: true, completion: nil)
                         }
                            self.drListHit()
                        }else if(self.serverint == 1){
                            self.nameAr.removeAllObjects()
                            self.specialityAr.removeAllObjects()
                            self.imgAr.removeAllObjects()
                            self.drIdAr.removeAllObjects()
                            self.showDotAr.removeAllObjects()
                            
                            if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 200)
                            {
                                let data = responseDict.valueForKey("data") as! NSMutableArray
                                for i in 0...data.count-1{
                                    let dict = data.objectAtIndex(i) as! NSDictionary
                                    if(dict.valueForKey("StaffStatus") as! Int == 1){
                                        self.nameAr.addObject(dict.valueForKey("Name")!)
                                        self.specialityAr.addObject(dict.valueForKey("Speciality")!)
                                        self.imgAr.addObject(dict.valueForKey("ProfilePic")!)
                                        self.drIdAr.addObject(dict.valueForKey("DrId")!)
                                        self.showDotAr.addObject(dict.valueForKey("showDot")!)
                                    }
                                }
                                
                                if(self.nameAr.count == 0){
                                    let alert = UIAlertController(title: "Tik Tok Doc", message: "Your account has been deleted or deactivated, please contact our support for more information.", preferredStyle: UIAlertControllerStyle.Alert)
                                    let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                                        UIAlertAction in
                                        self.deleLogOut()
                                    }
                                    alert.addAction(cancelAction)
                                    self.presentViewController(alert, animated: true, completion: nil)
                                }
                                else{
                                    let defaults = NSUserDefaults.standardUserDefaults()
                                    if let idGet = defaults.valueForKey("id"){
                                        let idDrGet = defaults.valueForKey("id") as! String
                                        if(self.drIdAr.containsObject(idDrGet)){
                                            let drIdIndex = self.drIdAr.indexOfObject(idDrGet)
                                            let row = Int(drIdIndex)
                                            let indexPath = NSIndexPath(forRow: row, inSection: 0)
                                            let  vw = self.view.viewWithTag(30+indexPath.row)
                                            vw?.backgroundColor = UIColor.init(red: 10/255, green: 4/255, blue: 88/255, alpha: 1)
                                            NSUserDefaults.standardUserDefaults().setValue(indexPath.row, forKey: "indexPath")
                                        }
                                        else{
                                            NSUserDefaults.standardUserDefaults().setValue(self.nameAr[0], forKey: "selectDrName")
                                            NSUserDefaults.standardUserDefaults().setValue(self.specialityAr[0], forKey: "selectDrSpeciality")
                                            NSUserDefaults.standardUserDefaults().setValue(self.imgAr[0], forKey: "selectDrImg")
                                            NSUserDefaults.standardUserDefaults().setValue(self.drIdAr[0], forKey: "id")
                                            let defaults = NSUserDefaults.standardUserDefaults()
                                            let imgUrl:String = defaults.valueForKey("selectDrImg") as! String
                                            if let url = NSURL(string: "http://\(imgUrl)") {
                                                self.profilePicImgVw.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
                                            }
                                            let firstname:String = defaults.valueForKey("selectDrName") as! String
                                            let special:String = defaults.valueForKey("selectDrSpeciality") as! String
                                            self.drName.text = "Dr. \(firstname)"
                                            let attachment = NSTextAttachment()
                                            attachment.image = UIImage(named: "HomeStethoscope")
                                            let offsetY: CGFloat = -8.0
                                            attachment.bounds = CGRectMake(0, offsetY, attachment.image!.size.width, attachment.image!.size.height)
                                            let attachmentString = NSAttributedString(attachment: attachment)
                                            let myString = NSMutableAttributedString(string:"")
                                            myString.appendAttributedString(attachmentString)
                                            let myString1 = NSMutableAttributedString(string:" \(special)")
                                            myString.appendAttributedString(myString1)
                                            self.speciality.textAlignment = .Center
                                            self.speciality.attributedText = myString
                                        }
                                    }
                                }
                            }
                            else{
                                let alert = UIAlertController(title: "Tik Tok Doc", message: "Your account has been deleted or deactivated, please contact our support for more information.", preferredStyle: UIAlertControllerStyle.Alert)
                                let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                                    UIAlertAction in
                                    self.deleLogOut()
                                }
                                alert.addAction(cancelAction)
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                            let defaults = NSUserDefaults.standardUserDefaults()
                            if let userId = defaults.valueForKey("staffId") {
                                let drId = defaults.valueForKey("id") as! String
                                AppManager.sharedManager.delegate=self
                                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                                let params = "StaffId=\(userId)&Drid=\(drId)"
                                self.serverInt = 1
                                AppManager.sharedManager.postDataOnserver(params, postUrl: "staffapi/StaffCount")
                            }
                        }else{
                            dispatch_async(dispatch_get_main_queue()){
                                let defaults = NSUserDefaults.standardUserDefaults()
                                defaults.removeObjectForKey("id")
                                defaults.removeObjectForKey("usertype")
                                defaults.removeObjectForKey("firstName")
                                defaults.removeObjectForKey("drType")
                                defaults.removeObjectForKey("staffId")
                                defaults.removeObjectForKey("selectDrName")
                                defaults.removeObjectForKey("selectDrImg")
                                defaults.removeObjectForKey("selectDrSpeciality")
                                defaults.removeObjectForKey("selectDrId")
                                try! self.realm.write({
                                  self.realm.deleteAll()
                                  })
                      self.performSelector(#selector(self.changeController), withObject: nil, afterDelay: 2.0)
                    }
                }
            }
                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        }
    }
            dataTask.resume()
}
    func changeController(){
        let vc = storyboard!.instantiateViewControllerWithIdentifier("mainViewController") as! ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func deleLogOut(){
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("staffId") {
            var token =  NSString()
            if(UIDevice.isSimulator == true){
                token = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
            }else{
                token = defaults.valueForKey("deviceToken") as! NSString
            }
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
            self.serverint = 3
            self.doRequestPost("\(Header.BASE_URL)users/logout", data: ["UserId":"\(userId)","DeviceToken":"\(token)"])
        }
        return
    }
        //    MARK: Functionality
        override func viewWillAppear(animated: Bool) {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(StaffHomeViewController.drListHit), name: "Staff notification status", object: nil)
            self.tokenGetHit()
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            let defaults = NSUserDefaults.standardUserDefaults()
            let imgUrl:String = defaults.valueForKey("selectDrImg") as! String
            if let url = NSURL(string: "http://\(imgUrl)") {
                profilePicImgVw.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
            }
            let firstname:String = defaults.valueForKey("selectDrName") as! String
            let special:String = defaults.valueForKey("selectDrSpeciality") as! String
            drName.text = "Dr. \(firstname)"
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: "HomeStethoscope")
            let offsetY: CGFloat = -8.0
            attachment.bounds = CGRectMake(0, offsetY, attachment.image!.size.width, attachment.image!.size.height)
            let attachmentString = NSAttributedString(attachment: attachment)
            let myString = NSMutableAttributedString(string:"")
            myString.appendAttributedString(attachmentString)
            let myString1 = NSMutableAttributedString(string:" \(special)")
            myString.appendAttributedString(myString1)
            speciality.textAlignment = .Center
            speciality.attributedText = myString
            
            timer = NSTimer.scheduledTimerWithTimeInterval(90.0, target: self, selector: #selector(StaffHomeViewController().apiHit), userInfo: nil, repeats: true)
            
           
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updateChatBadge), name: "ChatBadgeNotif", object: nil)
            
            if let msgCount =  NSUserDefaults.standardUserDefaults().integerForKey("msgCount") as? Int{
                if(msgCount == 1){
                    self.chatNotifBadge.hidden = false
                }else{
                    self.chatNotifBadge.hidden = true
                }
            }
        }
        func drStatusFunc(){
            self.apiHit()
        }
        override func viewWillDisappear(animated: Bool) {
            
            NSNotificationCenter.defaultCenter().removeObserver(self)
            
            self.navigationController?.navigationBarHidden = true
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(chatRecieveNotification), name: "chatRecieveNotification", object: nil)
            
        }
    
        func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
            dispatch_async(dispatch_get_main_queue()){
                
                if(self.serverInt == 2){
                    let defaults = NSUserDefaults.standardUserDefaults()
                    defaults.removeObjectForKey("id")
                    defaults.removeObjectForKey("usertype")
                    defaults.removeObjectForKey("firstName")
                    defaults.removeObjectForKey("drType")
                    defaults.removeObjectForKey("staffId")
                    defaults.removeObjectForKey("selectDrName")
                    defaults.removeObjectForKey("selectDrImg")
                    defaults.removeObjectForKey("selectDrSpeciality")
                    defaults.removeObjectForKey("selectDrId")
                    defaults.removeObjectForKey("msgCount")
                    var storyboard = UIStoryboard()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                    }else{
                        storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    }
                    try! self.realm.write({
                        self.realm.deleteAll()
                    })
                    let vc = storyboard.instantiateViewControllerWithIdentifier("mainViewController") as! ViewController
                    if(self.logoutCon == true){
                        let alert = UIAlertController(title: "Tik Tok Doc", message: "Your account has been deleted or deactivated, please contact our support for more information.", preferredStyle: UIAlertControllerStyle.Alert)
                        let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                            UIAlertAction in
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        alert.addAction(cancelAction)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }else{
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else if( self.serverInt == 3){
                    UIApplication.sharedApplication().cancelAllLocalNotifications()
                    if(responseDict.valueForKey("data")!.count != 0){
                        let data = responseDict.valueForKey("data") as! NSArray
                        for i in 0...data.count-1{
                            let schSeq = data[i] as! NSDictionary
                            let schF = schSeq.valueForKey("From") as! NSString
                            let schD = schSeq.valueForKey("Schedule") as! NSString
                            let schT = schSeq.valueForKey("To") as! NSString
                            var minutes = Double()
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "dd-MMMM-yyyy ,HH:mm"
                            let timeZone = NSTimeZone(name: "GMT-8")
                            dateFormatter.timeZone=timeZone
                            let schDate = dateFormatter.dateFromString("\(schD) ,\(schF)")!
                            let notification = UILocalNotification()
                            notification.fireDate = schDate
                            notification.alertBody = "You have a scheduled video visit with your Patient in 5 minutes. Please log into Tik Tok Doc App"
                            notification.timeZone = timeZone
                            notification.alertAction = "be awesome!"
                            notification.soundName = "iphonenoti_cRjTITC7.wav"
                            notification.userInfo = ["Status":"DoctorLocal"]
                            UIApplication.sharedApplication().scheduleLocalNotification(notification)
                            
                            dateFormatter.dateFormat = "HH:mm" // k = Hour in 1~24, mm = Minute
                            let timeTo = dateFormatter.dateFromString("\(schT)")!
                            let timeFrom = dateFormatter.dateFromString("\(schF)")!
                            let plusTimeFrom = timeFrom.dateByAddingTimeInterval(60*5)
                            let plusString = dateFormatter.stringFromDate(plusTimeFrom)
                            dateFormatter.dateFormat = "hh:mm a"
                            let plusFromString = dateFormatter.stringFromDate(plusTimeFrom)
                            let plustoStr = dateFormatter.stringFromDate(timeTo)
                            let finalTo = dateFormatter.dateFromString(plustoStr)
                            let finalfrom = dateFormatter.dateFromString(plusFromString)
                            let timeI =  (finalTo!.timeIntervalSinceDate(finalfrom!))
                            let i = Int(timeI)
                            let minutesSlot = i/60
                            minutes = Double(minutesSlot)
                            
                            dateFormatter.dateFormat = "dd-MMMM-yyyy ,HH:mm"
                            let schDate2 = dateFormatter.dateFromString("\(schD) ,\(plusString)")!
                            let store = EKEventStore()
                            let event = EKEvent(eventStore: store)
                            let startDate=schDate2
                            let endDate=startDate.dateByAddingTimeInterval(minutes*60)
                            let predicate2 = store.predicateForEventsWithStartDate(startDate, endDate: endDate, calendars: nil)
                            
                            let eV = store.eventsMatchingPredicate(predicate2) as [EKEvent]!
                            
                            if eV != nil {
                                for i in eV {
                                    print("Title  \(i.title)" )
                                    print("stareDate: \(i.startDate)" )
                                    print("endDate: \(i.endDate)" )
                                    do{
                                        (try store.removeEvent(i, span: EKSpan.ThisEvent, commit: true))
                                    }
                                    catch let error {
                                    }
                                    
                                }
                            }
                            store.requestAccessToEntityType(.Event) {(granted, error) in
                                if !granted {
                                    return
                                }
                                event.title = "Tik Tok Doc Video Visit with your Doctor."
                                event.startDate = schDate2 //today
                                event.endDate = event.startDate.dateByAddingTimeInterval(minutes*60) //1 hour long meeting
                                event.calendar = store.defaultCalendarForNewEvents
                                do {
                                    try store.saveEvent(event, span: .ThisEvent, commit: true)
                                    let savedEventId = event.eventIdentifier //save event id to access this particular event later
                                    print(savedEventId)
                                } catch {
                                    // Display error to user
                                }
                            }
                        }
                    }
                }
                else{
                    let status = responseDict.valueForKey("status") as! NSInteger
                    if(status == 200){
                    self.pndingPatCount = responseDict.valueForKey("requestCount") as! NSInteger
                    self.schCount = responseDict.valueForKey("ScheduleCount") as! NSInteger
                        let msgCount = responseDict.valueForKey("Messagedotshow") as! NSInteger
                        if(msgCount == 1){
                            self.chatNotifBadge.hidden = false
                        }else{
                            self.chatNotifBadge.hidden = true
                        }
                    dispatch_async(dispatch_get_main_queue()){
                        self.homeTableVw.reloadData()
                        let defaults = NSUserDefaults.standardUserDefaults()
                        if let userId = defaults.valueForKey("id") {
                            AppManager.sharedManager.delegate=self
                            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                            let params = "Dr_Id=\(userId)"
                            self.serverInt = 3
                            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrAppointmentDate")
                        }
                    }
                  }
                }
                AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            }
        }
        func apiHit(){
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("staffId") {
                  let drId = defaults.valueForKey("id") as! String
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                let params = "StaffId=\(userId)&Drid=\(drId)"
                serverInt = 1
                AppManager.sharedManager.postDataOnserver(params, postUrl: "staffapi/StaffCount")
            }
        }
    
    @IBAction func staffAction(sender: AnyObject) {
        var storyboard = UIStoryboard()
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        }else{
            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
        }
        let vc = storyboard.instantiateViewControllerWithIdentifier("StaffListingViewController") as! StaffListingViewController
        vc.showStaff = true
        let firstname:String = NSUserDefaults.standardUserDefaults().valueForKey("selectDrName") as! String
        vc.drName = "Dr. \(firstname)"
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
          vc.searchDocUserId = userId as! NSString
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
        @IBAction func logoutAction(sender: AnyObject) {
            timer.invalidate()
            let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To Logout?", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
                UIAlertAction in
                NSLog("OK Pressed")
                let defaults = NSUserDefaults.standardUserDefaults()
                if let userId = defaults.valueForKey("staffId") {
                    var token =  NSString()
                    if(UIDevice.isSimulator == true){
                        token = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
                    }else{
                        token = defaults.valueForKey("deviceToken") as! NSString
                    }
                    AppManager.sharedManager.delegate=self
                    AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Log Out")
                    let params = "UserId=\(userId)&DeviceToken=\(token)"
                    self.serverInt = 2
                    self.logoutCon = false
                    AppManager.sharedManager.postDataOnserver(params, postUrl: "users/logout")
                }
            }
            let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        @IBAction func myProfileAc(sender: AnyObject) {
            timer.invalidate()
            let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("StaffProfileViewController") as! StaffProfileViewController
            let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("StaffSlideViewController") as! StaffSlideViewController
            let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
            leftViewController.mainViewController = nvc
            let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            slideMenuController.delegate = mainViewController
            self.navigationController?.pushViewController(slideMenuController, animated: true)
        }
        @IBAction func contactAc(sender: AnyObject){
            timer.invalidate()
            let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("ContactUsViewController") as! ContactUsViewController
            mainViewController.contactSwich = true
            let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("StaffSlideViewController") as! StaffSlideViewController
            let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
            leftViewController.mainViewController = nvc
            let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            slideMenuController.delegate = mainViewController
            self.navigationController?.pushViewController(slideMenuController, animated: true)
        }
        @IBAction func messageListAc(sender: AnyObject){
            timer.invalidate()
            let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("MessageListingViewController") as! MessageListingViewController
            let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("StaffSlideViewController") as! StaffSlideViewController
            let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
            leftViewController.mainViewController = nvc
            let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            slideMenuController.delegate = mainViewController
            self.navigationController?.pushViewController(slideMenuController, animated: true)
        }
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 4
        }
        
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
            let cell = tableView.dequeueReusableCellWithIdentifier("cell")
            let imageView: UIView = (cell?.viewWithTag(9111))!
            switch UIDevice.currentDevice().userInterfaceIdiom {
            case .Phone:
                imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
                imageView.layer.shadowOffset = CGSizeMake(10, 5);
                imageView.layer.shadowRadius = 0.5;
                imageView.layer.shadowOpacity = 0.06;
                imageView.layer.masksToBounds = false;
                imageView.layer.borderWidth = 0.5
            case .Pad:
                imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
                imageView.layer.shadowOffset = CGSizeMake(7, 3);
                imageView.layer.shadowRadius = 0.5;
                imageView.layer.shadowOpacity = 0.02;
                imageView.layer.masksToBounds = false;
                imageView.layer.borderWidth = 0.5
            default:
                break
            }
            
            if(indexPath.row == 0){
                imageView.backgroundColor = UIColor.init(red: 10/255, green: 187/255, blue: 167/255, alpha: 1)
            }
            else if(indexPath.row == 1){
                imageView.backgroundColor = UIColor.init(red: 0/255, green: 174/255, blue: 153/255, alpha: 1)
            }
            else if(indexPath.row == 2){
                imageView.backgroundColor = UIColor.init(red: 55/255, green: 132/255, blue: 143/255, alpha: 1)
            }
            else{
                imageView.backgroundColor = UIColor.init(red: 76/255, green: 109/255, blue: 132/255, alpha: 1)
            }
            let img: UIImageView = (cell!.viewWithTag(9112) as! UIImageView)
            img.image = UIImage(named:arMenuImages[indexPath.row])
            let valueLbl: UILabel = (cell!.viewWithTag(9113) as! UILabel)
            valueLbl.text = arMenu[indexPath.row]
            
                if(indexPath.row == 3){
                    let lineLbl1: UILabel = (cell!.viewWithTag(1920) as! UILabel)
                    lineLbl1.backgroundColor = UIColor.clearColor()
                    
                    let lineLbl2: UILabel = (cell!.viewWithTag(1921) as! UILabel)
                    lineLbl2.backgroundColor = UIColor.clearColor()
                }
                else{
                    let lineLbl1: UILabel = (cell!.viewWithTag(1920) as! UILabel)
                    lineLbl1.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
                    
                    let lineLbl2: UILabel = (cell!.viewWithTag(1921) as! UILabel)
                    lineLbl2.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
                }
                
                if(indexPath.row == 0){
                    let lineLbl1: UILabel = (cell!.viewWithTag(1922) as! UILabel)
                    lineLbl1.backgroundColor = UIColor.clearColor()
                    
                    let lineLbl2: UILabel = (cell!.viewWithTag(1923) as! UILabel)
                    lineLbl2.backgroundColor = UIColor.clearColor()
                }
                else{
                    let lineLbl1: UILabel = (cell!.viewWithTag(1922) as! UILabel)
                    lineLbl1.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
                    
                    let lineLbl2: UILabel = (cell!.viewWithTag(1923) as! UILabel)
                    lineLbl2.backgroundColor = UIColor.init(red: 0/255, green: 204/255, blue: 175/255, alpha: 1)
                }
            
            let countLabel: UILabel = (cell!.viewWithTag(9114) as! UILabel)
            countLabel.layer.cornerRadius = countLabel.frame.size.height/2
            countLabel.clipsToBounds = true
            if(indexPath.row == 2){
                if(pndingPatCount != 0){
                    countLabel.backgroundColor = UIColor.redColor()
                }
                else{
                    countLabel.text = ""
                    countLabel.backgroundColor = UIColor.clearColor()
                    countLabel.textColor = UIColor.clearColor()
                }
                
            }
            if(indexPath.row == 0){
                if(schCount != 0){
                    countLabel.backgroundColor = UIColor.redColor()
                }
                else{
                    countLabel.text = ""
                    countLabel.backgroundColor = UIColor.clearColor()
                    countLabel.textColor = UIColor.clearColor()
                }
            }
            return cell! as UITableViewCell
        }
        func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
            switch UIDevice.currentDevice().userInterfaceIdiom {
            case .Phone:
                if(self.view.frame.size.height < 600){
                    return homeTableVw.frame.size.height*0.25
                }else{
                    return homeTableVw.frame.size.height * 0.23
                }
            case .Pad:
                return homeTableVw.frame.size.height * 0.22
            default:
                break
            }
            return 0
        }
        func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
            if(indexPath.row == 0){
                timer.invalidate()
                let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("DoctorMyAppointmentsViewController") as! DoctorMyAppointmentsViewController
                mainViewController.selectStaff = true
                let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("StaffSlideViewController") as! StaffSlideViewController
                let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                leftViewController.mainViewController = nvc
                let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
                slideMenuController.automaticallyAdjustsScrollViewInsets = true
                slideMenuController.delegate = mainViewController
                self.navigationController?.pushViewController(slideMenuController, animated: true)
            }else if(indexPath.row == 1){
                timer.invalidate()
                let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("TabBar") as! TabBarViewController
                let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("StaffSlideViewController") as! StaffSlideViewController
                let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                nvc.navigationBarHidden = true
                leftViewController.mainViewController = nvc
                let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
                slideMenuController.automaticallyAdjustsScrollViewInsets = true
                slideMenuController.delegate = mainViewController
                self.navigationController?.pushViewController(slideMenuController, animated: true)
            }else if(indexPath.row == 2){
                timer.invalidate()
                let mainViewController = storyboard!.instantiateViewControllerWithIdentifier("DocMyPateintViewController") as! DocMyPateintViewController
                mainViewController.staff = true
                let leftViewController = storyboard!.instantiateViewControllerWithIdentifier("StaffSlideViewController") as! StaffSlideViewController
                let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                leftViewController.mainViewController = nvc
                let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
                slideMenuController.automaticallyAdjustsScrollViewInsets = true
                slideMenuController.delegate = mainViewController
                self.navigationController?.pushViewController(slideMenuController, animated: true)
            }else if(indexPath.row == 3){
                timer.invalidate()
                let vc = storyboard!.instantiateViewControllerWithIdentifier("SearchDoctorViewController") as! SearchDoctorViewController
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "searchDoc")
                vc.sndRequ = false
                vc.searchWho = "doctor"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        func failureRsponseError(failureError:NSError){
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        
        
    }
/*extension UIDevice {
        static var isSimulator: Bool {
            return NSProcessInfo.processInfo().environment["SIMULATOR_DEVICE_NAME"] != nil
        }
}*/
extension StaffHomeViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
