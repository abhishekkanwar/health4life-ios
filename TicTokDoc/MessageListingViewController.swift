

import UIKit
import SlideMenuControllerSwift
import RealmSwift
import IQKeyboardManagerSwift
class MessageListingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,WebServiceDelegate {
    
    @IBOutlet weak var noMsgImageVw: UIImageView!
    @IBOutlet weak var messageUserList: UITableView!
    var userListAr = NSMutableArray()
    var serverInt = NSInteger()
    var specialValueAr = NSMutableArray()
    var realm = try! Realm()
    var chatPush = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.sharedManager().enable = true
        self.navigationController?.navigationBarHidden = true
        messageUserList.delegate = self
        messageUserList.dataSource = self
        noMsgImageVw.hidden = true
    }
    override func viewWillAppear(animated: Bool) {
        self.userListAr.removeAllObjects()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(msgListRefresh), name: "msgListRefresh", object: nil)
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let usertype = defaults.valueForKey("usertype")
        if(usertype as! NSObject == 1){
            if let userId = defaults.valueForKey("id") {
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                let params = "UserId=\(userId)&UserType=1"
                serverInt = 1
                AppManager.sharedManager.postDataOnserver(params, postUrl: "users/DrpatientChatlist")
            }
        }else if(usertype as! NSObject == 3){
            if let userId = defaults.valueForKey("staffId") {
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                let params = "UserId=\(userId)&UserType=3"
                serverInt = 3
                AppManager.sharedManager.postDataOnserver(params, postUrl: "users/DrpatientChatlist")
            }
        }
        else{
            if let userId = defaults.valueForKey("id") {
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                let params = "UserId=\(userId)&UserType=0"
                serverInt = 2
                AppManager.sharedManager.postDataOnserver(params, postUrl: "users/DrpatientChatlist")
            }
        }
        
    }
    func msgListRefresh(){
        let defaults = NSUserDefaults.standardUserDefaults()
        let usertype = defaults.valueForKey("usertype")
        if(usertype as! NSObject == 1){
            if let userId = defaults.valueForKey("id") {
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                let params = "UserId=\(userId)&UserType=1"
                serverInt = 1
                AppManager.sharedManager.postDataOnserver(params, postUrl: "users/DrpatientChatlist")
            }
        }else if(usertype as! NSObject == 3){
            if let userId = defaults.valueForKey("staffId") {
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                let params = "UserId=\(userId)&UserType=3"
                serverInt = 3
                AppManager.sharedManager.postDataOnserver(params, postUrl: "users/DrpatientChatlist")
            }
        }
        else{
            if let userId = defaults.valueForKey("id") {
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                let params = "UserId=\(userId)&UserType=0"
                serverInt = 2
                AppManager.sharedManager.postDataOnserver(params, postUrl: "users/DrpatientChatlist")
            }
        }
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        userListAr.removeAllObjects()
        dispatch_async(dispatch_get_main_queue(),{
            if(responseDict.valueForKey("status") as! Int == 200){
                let dataArray = responseDict.valueForKey("user") as! NSArray
                
                let arrayRead   = NSMutableArray()
                let arrayUnread = NSMutableArray()
                dataArray.enumerateObjectsUsingBlock({ (obj, index, stop) in
                    
                    let user = dataArray[index] as! NSDictionary
                    let dotShow = user.valueForKey("messagedotshow") as! NSInteger
                    if(dotShow == 0){
                        arrayUnread.addObject(obj)
                    }else{
                        arrayRead.addObject(obj)
                    }
                    
                })
                
                self.userListAr.addObjectsFromArray(arrayUnread as [AnyObject])
                self.userListAr.addObjectsFromArray(arrayRead as [AnyObject])
                
                let usersList = self.realm.objects(User.self)
                if(usersList.count != 0){
                    try! self.realm.write({
                        self.realm.delete(usersList)
                        self.realm.refresh()
                    })
                }
                for i in 0..<self.userListAr.count {
                    let data = self.userListAr[i]
                    try! self.realm.write({
                        let newUser = User()
                        newUser.userId = data.valueForKey("_id") as! String
                        newUser.groupId = data.valueForKey("GroupId") as! String
                        newUser.gender = data.valueForKeyPath("Gender") as! String
                        newUser.name = data.valueForKey("Name") as! String
                        newUser.profilePic = data.valueForKeyPath("ProfilePic") as! String
                        newUser.openTokToken = data.valueForKey("TokenData") as! String
                        newUser.openTokSession = data.valueForKey("sessionData") as! String
                        newUser.openTokAPIKey = data.valueForKey("ApiKey") as! String
                        newUser.userType = data.valueForKey("UserType") as! NSInteger
                        newUser.chatCount = data.valueForKey("count") as! NSInteger
                        let type = data.valueForKey("UserType") as! NSInteger
                        if(type as NSObject == 0){
                            newUser.age = data.valueForKey("DOB") as! String
                            self.specialValueAr.addObject(data.valueForKey("DOB")!)
                        }
                        else if(type as NSObject == 1){
                            newUser.age = data.valueForKey("Speciality") as! String
                            self.specialValueAr.addObject(data.valueForKey("Speciality")!)
                        }
                        else{
                            newUser.age = data.valueForKey("Designation") as! String
                            self.specialValueAr.addObject(data.valueForKey("Designation")!)
                        }
                        self.realm.add(newUser)
                    })
                }
                if(arrayUnread.count != 0){
                    NSUserDefaults.standardUserDefaults().setInteger(1, forKey: "msgCount")
                }
                else{
                    NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "msgCount")
                }
            }else{
                let noChatLbl = UIImageView()
                noChatLbl.frame = CGRectMake(10, (self.view.frame.size.height/2)-(noChatLbl.frame.size.height/2), self.view.frame.size.width-20, self.view.frame.size.height*0.16)
                noChatLbl.tag = 44
                noChatLbl.image = UIImage(named: "nochatUser")
                noChatLbl.contentMode = UIViewContentMode.ScaleAspectFit
                self.view.addSubview(noChatLbl)
            }
            self.messageUserList.reloadData()
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        })
    }
    func removeStringFromDOB(string: String) -> Int {
        let result = String(string.characters.filter { "01234567890.".characters.contains($0) })
        return Int(result)!
    }
    
    
    
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("Alert", alertmessage: failureError.localizedDescription)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userListAr.count
    }
    // MARK:- Msg count list
    func msgCountList(groupId:String,completion: (result: NSDictionary)->()) -> Void {
        let paramString = "GroupId=\(groupId)"
        AppManager.sharedManager.generateGroupIdForUser("messageChat/listChat", params: paramString) { (result) in
            
            completion(result: result)
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let user = userListAr[indexPath.row] as! NSDictionary
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        let imageView: UIView = (cell?.viewWithTag(11))!
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Phone:
            imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
            imageView.layer.shadowOffset = CGSizeMake(7, 3);
            imageView.layer.shadowRadius = 0.2;
            imageView.layer.shadowOpacity = 0.06;
            imageView.layer.masksToBounds = false;
            imageView.layer.borderWidth = 0.5
        case .Pad:
            imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
            imageView.layer.shadowOffset = CGSizeMake(7, 3);
            imageView.layer.shadowRadius = 0.5;
            imageView.layer.shadowOpacity = 0.02;
            imageView.layer.masksToBounds = false;
            imageView.layer.borderWidth = 0.5
        default:
            break
        }
        let img: UIImageView = (cell!.viewWithTag(12) as! UIImageView)
        img.layer.cornerRadius = img.frame.size.height/2
        img.layer.borderWidth = self.view.frame.size.height*0.008
        img.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        
        let profilPic = user.valueForKey("ProfilePic") as! String
        
        if let url = NSURL(string: "http://\(profilPic)") {
            img.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        let docImg: UIImageView = (cell!.viewWithTag(50) as! UIImageView)
        if(user.valueForKey("UserType")  as! NSObject == 1){
            docImg.hidden = false
        }else{
            docImg.hidden = true
        }
        
        let nameLbl: UILabel = (cell!.viewWithTag(13) as! UILabel)
        nameLbl.text = user.valueForKey("Name") as? String
        
        let ageLbl: UILabel = (cell!.viewWithTag(14) as! UILabel)
        
        if(specialValueAr.count == 0){
            ageLbl.text = user.valueForKey("DOB") as? String
        }
        else{
            ageLbl.text = specialValueAr[indexPath.row] as? String
        }
        let msgCount: UILabel = (cell!.viewWithTag(18) as! UILabel)
        msgCount.layer.cornerRadius = msgCount.frame.size.height/2
        msgCount.backgroundColor = UIColor.redColor()
        msgCount.clipsToBounds = true
        msgCount.hidden = true
        msgCount.text = "0"
        
        let gendrLbl: UILabel = (cell!.viewWithTag(15) as! UILabel)
        gendrLbl.text = user.valueForKey("Gender") as? String
        
        let dotShow = user.valueForKey("messagedotshow") as! NSInteger
        if(dotShow == 0){
            msgCount.hidden = false
        }else{
            msgCount.hidden = true
        }
       
        
        return cell! as UITableViewCell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Phone:
            return messageUserList.frame.size.height*0.22
        case .Pad:
            return messageUserList.frame.size.height * 0.18
        default:
            break
        }
        return 0
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let user = userListAr[indexPath.row] as! NSDictionary
        let defaults = NSUserDefaults.standardUserDefaults()
        let usertype = defaults.valueForKey("usertype")
        if((user.valueForKey("GroupId") as! String).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).characters.count == 0)
        {
            if(usertype as! NSObject == 1){
                if let userId = defaults.valueForKey("id") {
                    AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                    let params = "DrId=\(userId)&PatientId=\(user.valueForKey("Patient_id"))&Group=0"
                    AppManager.sharedManager.generateGroupIdForUser("messageChat/createsession", params: params, completion: { (result) in
                        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                        let tokenDict = NSMutableDictionary()
                        tokenDict.setValue(result.valueForKey("GroupId"), forKey: "groupId")
                        tokenDict.setValue(result.valueForKey("TokenData"), forKey: "openTokToken")
                        tokenDict.setValue(result.valueForKey("sessionData"), forKey: "openTokSession")
                        tokenDict.setValue(result.valueForKey("ApiKey"), forKey: "openTokAPIKey")
                        self.navigateToChatView(user)
                        
                    })
                }
            }else  if(usertype as! NSObject == 0){
                if let userId = defaults.valueForKey("id") {
                    AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                    let params = "PatientId=\(userId)&DrId=\(user.valueForKey("Dr_id"))&Group=0"
                    AppManager.sharedManager.generateGroupIdForUser("messageChat/createsession", params: params, completion: { (result) in
                        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                        let tokenDict = NSMutableDictionary()
                        tokenDict.setValue(result.valueForKey("GroupId"), forKey: "groupId")
                        tokenDict.setValue(result.valueForKey("TokenData"), forKey: "openTokToken")
                        tokenDict.setValue(result.valueForKey("sessionData"), forKey: "openTokSession")
                        tokenDict.setValue(result.valueForKey("ApiKey"), forKey: "openTokAPIKey")
                        self.navigateToChatView(user)
                    })
                }
            }
            else{
                if let userId = defaults.valueForKey("staffId") {
                    AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                    let params = "PatientId=\(userId)&DrId=\(user.valueForKey("Dr_id"))&Group=0"
                    AppManager.sharedManager.generateGroupIdForUser("messageChat/createsession", params: params, completion: { (result) in
                        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                        let tokenDict = NSMutableDictionary()
                        tokenDict.setValue(result.valueForKey("GroupId"), forKey: "groupId")
                        tokenDict.setValue(result.valueForKey("TokenData"), forKey: "openTokToken")
                        tokenDict.setValue(result.valueForKey("sessionData"), forKey: "openTokSession")
                        tokenDict.setValue(result.valueForKey("ApiKey"), forKey: "openTokAPIKey")
                        self.navigateToChatView(user)
                    })
                }
            }
        }
        else
        {
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            let msgCount: UILabel = (cell!.viewWithTag(18) as! UILabel)
            if(msgCount.hidden == false)
            {
                var userId = NSString()
                let userType = defaults.valueForKey("usertype") as! NSInteger
                if(userType == 3){
                    userId = defaults.valueForKey("staffId") as! String
                }else{
                    userId = defaults.valueForKey("id") as! String
                }
                
                
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                
                RealmInteractor.shared.getMsgsFromServer(userId as String, groupId: user.valueForKey("GroupId") as! String) { (result) in
                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                    
                    if(result.valueForKey("status") as! Int == 200)
                    {
                        let chatResults = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", user.valueForKey("GroupId") as! String))
                        try! self.realm.write({
                            self.realm.delete(chatResults)
                        })
                        
                        let chatArray = result.objectForKey("data") as! NSArray
                        
                        for dict in chatArray
                        {
                            try! self.realm.write({
                                
                                let group = Group()
                                group.displayName = dict.valueForKey("DisplayName") as! String
                                group.message = dict.valueForKey("message") as! String
                                group.messageType = dict.valueForKey("messageType") as! String
                                group.fromId = dict.valueForKey("From") as! String
                                group.groupId = dict.valueForKey("GroupId") as! String
                                self.realm.add(group)
                                
                            })
                            
                        }
                        
                        if(usertype as! NSObject == 1){
                            self.navigateToChatView(user)
                        }else{
                            self.navigateToChatView(user)
                        }
                        
                    }
                    
                }
                
            }
            else
            {
                if(usertype as! NSObject == 1){
                    self.navigateToChatView(user)
                }else{
                    self.navigateToChatView(user)
                }
            }
            
        }
    }
    func navigateToChatView(user:NSDictionary) -> Void {
        print(user)
        let chatResults = realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", user.valueForKey("GroupId") as! String))
        print(chatResults.count)
        
        let msgArray = NSMutableArray()
        
        for group in chatResults
        {
            var mySelf = false
            let msgDict = group
            
            let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
            if(userType == 1 || userType == 0 || userType == 2){
                if(msgDict.fromId == (NSUserDefaults.standardUserDefaults().valueForKey("id") as! String))
                {
                    mySelf = true
                }
            }else if(userType == 3){
                if(msgDict.fromId == (NSUserDefaults.standardUserDefaults().valueForKey("staffId") as! String))
                {
                    mySelf = true
                }
            }
            
            let mdict = ["msg":msgDict.message,"bool":mySelf, "msgType":msgDict.messageType,"name":msgDict.displayName]
            msgArray.addObject(mdict)
        }
        
        let firstname:String = NSUserDefaults.standardUserDefaults().valueForKey("firstName") as! String
        
        let vc = storyboard!.instantiateViewControllerWithIdentifier("ChatViewController") as! ChatViewController
        let defaults = NSUserDefaults.standardUserDefaults()
        let usertype = defaults.valueForKey("usertype") as! NSInteger
        if(usertype == 3){
            vc.userId = defaults.valueForKey("staffId") as! String
        }else{
            vc.userId = defaults.valueForKey("id") as! String
        }
        vc.chatUserId = user.valueForKey("GroupId") as! String
        
        vc.userName = user.valueForKey("Name") as! String
        
        vc.anotherUserId = user.valueForKeyPath("user._id") as! String
        vc.Token = user.valueForKey("TokenData") as! String
        vc.SessionID = user.valueForKey("sessionData") as! String
        vc.apiKey = user.valueForKey("ApiKey") as! String
        vc.msgAr = msgArray
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func sideBarBtn(sender: AnyObject) {
        slideMenuController()?.toggleRight()
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        if(chatPush == true){
            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window = UIWindow(frame: UIScreen.mainScreen().bounds)
            var vc = UIViewController()
            let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! Int
            if(userType == 0){
                vc = storyboard!.instantiateViewControllerWithIdentifier("PatientHomeViewController") as! PatientHomeViewController
            }else if(userType == 1){
                vc = storyboard!.instantiateViewControllerWithIdentifier("DoctorHomeViewController") as! DoctorHomeViewController
            }else if(userType == 3){
                vc = storyboard!.instantiateViewControllerWithIdentifier("StaffHomeViewController") as! StaffHomeViewController
            }
            let nav = UINavigationController(rootViewController: vc)
            appDelegate.window?.rootViewController = nav
            appDelegate.window?.makeKeyAndVisible()
        }else{
            self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
extension MessageListingViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}


