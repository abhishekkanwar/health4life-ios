
import UIKit

class PatientWaitingRoomViewController: UIViewController {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var waitingBoxVw: UIView!
    @IBOutlet weak var bigBoxVw: UIView!
    @IBOutlet weak var docNumber: UIButton!
    var drname = NSString()
    var drSpecial = NSString()
    var drScheduleDate = NSString()
    var drscheduleTime = NSString()
    var scheduleMin = NSString()
    var drQual = NSString()
    var drLang = NSString()
    var drMobil = NSString()
    var drAddres = NSString()
    var drId = NSString()
    var profilepicurl = NSString()
    var attrs = [
        NSFontAttributeName : UIFont.systemFontOfSize(15.0),
        NSForegroundColorAttributeName : UIColor.init(red: 51/255, green: 82/255, blue: 109/255, alpha: 1),
        NSUnderlineStyleAttributeName : 1]
    var attributedString = NSMutableAttributedString(string:"")
    override func viewDidLoad() {
        super.viewDidLoad()
        //let myNormalAttributedTitle = NSAttributedString(string: "\(drMobil)")
        let buttonTitleStr = NSMutableAttributedString(string:"\(drMobil)", attributes:attrs)
        attributedString.appendAttributedString(buttonTitleStr)
        docNumber.setAttributedTitle(attributedString, forState: .Normal)
    
       // docNumber.setAttributedTitle(myNormalAttributedTitle, forState: .Normal)
        //docNumber.setTitle("\(drMobil)", forState: .Normal)
        waitingBoxVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        waitingBoxVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        waitingBoxVw.layer.shadowOffset = CGSizeMake(10, 5);
        waitingBoxVw.layer.shadowRadius = 1;
        waitingBoxVw.layer.shadowOpacity = 0.06;
        waitingBoxVw.backgroundColor = UIColor.whiteColor()
        waitingBoxVw.layer.masksToBounds = false;
        waitingBoxVw.layer.shadowColor = UIColor.lightGrayColor().CGColor
        waitingBoxVw.layer.borderWidth = 0.5
        
        bigBoxVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        bigBoxVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        bigBoxVw.layer.shadowOffset = CGSizeMake(10, 5);
        bigBoxVw.layer.shadowRadius = 1;
        bigBoxVw.layer.shadowOpacity = 0.06;
        bigBoxVw.backgroundColor = UIColor.whiteColor()
        bigBoxVw.layer.masksToBounds = false;
        bigBoxVw.layer.shadowColor = UIColor.lightGrayColor().CGColor
        bigBoxVw.layer.borderWidth = 0.5
        
        let drNameLbl = waitingBoxVw.viewWithTag(311) as! UILabel
        drNameLbl.text = "Dr. \(drname),\(drQual)"
        let drSpeciality = waitingBoxVw.viewWithTag(312) as! UILabel
        drSpeciality.text = "\(drSpecial)"
        let drSchedule  = waitingBoxVw.viewWithTag(313) as! UILabel
        drSchedule.text = "\(drScheduleDate) at \(drscheduleTime) (\(scheduleMin) Min)"
        let drQualification = bigBoxVw.viewWithTag(314) as! UILabel
        drQualification.text = "\(drQual)"
        let drLanguage = bigBoxVw.viewWithTag(315) as! UILabel
        drLanguage.text = "\(drLang)"
        let drMobile = bigBoxVw.viewWithTag(316) as! UILabel
        drMobile.text = "\(drMobil)"
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(PatientWaitingRoomViewController.imageTapped(_:)))
        drMobile.userInteractionEnabled = true
        drMobile.addGestureRecognizer(tapGestureRecognizer)
        let drAddress = bigBoxVw.viewWithTag(317) as! UILabel
        drAddress.text = "\(drAddres)"
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(drId, forKey: "drId")
        imgProfile.layer.cornerRadius = imgProfile.frame.size.height/2
        imgProfile.layer.borderWidth = self.view.frame.size.height*0.01
        imgProfile.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        if let url = NSURL(string: "http://\(profilepicurl)") {
            imgProfile.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
    }
    func imageTapped(img: AnyObject){
        let alert = UIAlertController(title: "Please confirm your call", message: "You’re about to call the following:\n\(drMobil)", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            if let url = NSURL(string: "tel://\(self.drMobil)") {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    @IBAction func phoneAction(sender: AnyObject) {
        let alert = UIAlertController(title: "Please confirm your call", message: "You’re about to call the following:\n\(drMobil)", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            if let url = NSURL(string: "tel://\(self.drMobil)") {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func phone2Action(sender: AnyObject) {
        let alert = UIAlertController(title: "Please confirm your call", message: "You’re about to call the following:\n\(drMobil)", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            if let url = NSURL(string: "tel://\(self.drMobil)") {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
}
class UnderlinedLabel: UILabel {
    
    override var text: String? {
        didSet {
            guard let text = text else { return }
            let textRange = NSMakeRange(0, text.characters.count)
            let attributedText = NSMutableAttributedString(string: text)
            attributedText.addAttribute(NSUnderlineStyleAttributeName, value:NSUnderlineStyle.StyleSingle.rawValue, range: textRange)
            // Add other attributes if needed
            
            self.attributedText = attributedText
        }
    }
}
