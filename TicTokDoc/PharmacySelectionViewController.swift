

import UIKit
import SlideMenuControllerSwift
class PharmacySelectionViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,WebServiceDelegate{

    @IBOutlet weak var pharmacyNameLbl: UILabel!
    @IBOutlet weak var pharmacySelectionTableVw: UITableView!
    @IBOutlet weak var selctPharmaBtn: UIButton!
    var pharmacyName = NSString()
    var pharmaAdd = NSString()
    var pharmaCity = NSString()
    var pharmaState = NSString()
    var pharmaZipcode = NSString()
    var pharmaPhone = NSString()
    var pharmaFax = NSString()
    var pharmaHours = NSString()
    var pharmaCertified = NSString()
    var certifiedBool = NSString()
    var pharmacyId = NSString()
    var storeNumber = NSString()
    var pharma_id = NSString()
    var nxt = Bool()
    var btnExist = Bool()
    var imgAr = ["Address","cityState","cityState","zipCode","phone","Fax","timings","Certified"]
    var valueAr = ["Address","City","State","Zipcode","Phone Primary","Fax","Timing","Certified"]
    var valueTextAr = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(pharmaCertified == "True"){
          certifiedBool = "Yes"
        }else{
          certifiedBool = "No"
        }
         valueTextAr = ["\(pharmaAdd)","\(pharmaCity)","\(pharmaState)","\(pharmaZipcode)","\(pharmaPhone)","\(pharmaFax)","\(pharmaHours)","\(certifiedBool)"]
        let footerView: UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height*0.12))
        footerView.tag = 99
        if(btnExist == true){
            selctPharmaBtn.hidden = false
        let selectBtn: UIButton = UIButton(frame: CGRectMake(self.view.frame.size.width*0.12, self.view.frame.size.height*0.035, self.view.frame.size.width-(2*(self.view.frame.size.width*0.12)), self.view.frame.size.height*0.07))
        selectBtn.tag = 96
        selectBtn.setBackgroundImage(UIImage(named: "selectPharmacy"), forState: .Normal)
        selectBtn.addTarget(self, action: #selector(PharmacySelectionViewController.selectBtnAc), forControlEvents: .TouchUpInside)
         }
        else{
           selctPharmaBtn.hidden = true
        }
        //footerView.addSubview(selectBtn)
        pharmacySelectionTableVw.tableFooterView = footerView
    
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: "smallRedPlus")
        let offsetY: CGFloat = -6.0
        attachment.bounds = CGRectMake(0, offsetY, attachment.image!.size.width, attachment.image!.size.height)
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string:"")
        myString.appendAttributedString(attachmentString)
        let myString1 = NSMutableAttributedString(string:" \(pharmacyName)")
        myString.appendAttributedString(myString1)
        pharmacyNameLbl.textAlignment = .Center
        pharmacyNameLbl.attributedText = myString
        pharmacySelectionTableVw.delegate = self
        pharmacySelectionTableVw.dataSource = self
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = pharmacySelectionTableVw.dequeueReusableCellWithIdentifier("cell")
        cell!.layer.cornerRadius = 7.0
        cell!.layer.masksToBounds = true
        
        let imageView: UIView = (cell?.viewWithTag(370))!
        imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
        imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
        imageView.layer.shadowOffset = CGSizeMake(10, 5);
        imageView.layer.shadowRadius = 1;
        imageView.layer.shadowOpacity = 0.06;
        imageView.layer.masksToBounds = false;
        imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        imageView.layer.borderWidth = 0.5
        if(indexPath.row%2 == 0){
            imageView.backgroundColor = UIColor.whiteColor()
        }else{
            imageView.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
        }
       let itemImg: UIImageView = (cell!.viewWithTag(371) as! UIImageView)
       itemImg.image = UIImage(named: imgAr[indexPath.row])!
        let valueLbl: UILabel = (cell!.viewWithTag(373) as! UILabel)
        valueLbl.text = valueTextAr[indexPath.row] as? String
        if(indexPath.row == 4){
            let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(PharmacySelectionViewController.imageTapped(_:)))
            itemImg.userInteractionEnabled = true
            itemImg.addGestureRecognizer(tapGestureRecognizer)
            
            valueLbl.userInteractionEnabled = true
            valueLbl.addGestureRecognizer(tapGestureRecognizer)
        }
        let lbl: UILabel = (cell!.viewWithTag(372) as! UILabel)
        lbl.text = valueAr[indexPath.row]
        
        return cell!
    }
    func imageTapped(img: AnyObject){
        let alert = UIAlertController(title: "Please confirm your call", message: "You’re about to call the following:\n\(pharmaPhone)", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            if let url = NSURL(string: "tel://\(self.pharmaPhone)") {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return self.view.frame.size.width*0.1
    }
    @IBAction func selectPharmacyAc(sender: AnyObject) {
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
            let params = "UserId=\(userId)&PharmacyId=\(storeNumber)&PharmacyName=\(pharmacyName)&PharmacySelect=\(pharma_id)"
            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/PharmacyAdd")
        }
    }
    func selectBtnAc(){
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        let params = "UserId=\(userId)&PharmacyId=\(storeNumber)&PharmacyName=\(pharmacyName)&PharmacySelect=\(pharma_id)"
        AppManager.sharedManager.postDataOnserver(params, postUrl: "users/PharmacyAdd")
        }
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        if(nxt == false){
        dispatch_async(dispatch_get_main_queue()){
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if(aViewController is MyInformationViewController){
                    self.navigationController!.popToViewController(aViewController, animated: true);
                    NSNotificationCenter.defaultCenter().postNotificationName("UpdatePharmacy", object: self.pharmacyName)
                }
            }
          }
        }else{
            dispatch_async(dispatch_get_main_queue()){
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setBool(true, forKey: "selectPharma")
                let mediaType = defaults.valueForKey("mediaType") as! String
                if let userId = defaults.valueForKey("usertype"){
                    print(userId)
                    var storyboard = UIStoryboard()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                    }else{
                        storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    }
                    let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    let vc = storyboard.instantiateViewControllerWithIdentifier("PatientHomeViewController") as! PatientHomeViewController
                    let nav = UINavigationController(rootViewController: vc)
                    appDelegate.window?.rootViewController = nav
                    appDelegate.window?.makeKeyAndVisible()
                }else if(mediaType == "manual"){
                    if(self.view.frame.size.height < 600){
                        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                        backVw.tag = 2369
                        self.view.addSubview(backVw)
                        
                        let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.48)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.49))
                        alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                        alerVw.backgroundColor = UIColor.whiteColor()
                        backVw.addSubview(alerVw)
                        
                        let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                        rightImg.image = UIImage(named: "confirm")
                        alerVw.addSubview(rightImg)
                        
                        let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.5, self.view.frame.size.width*0.05))
                        verificationLbl.text = " Pharmacy Selected"
                        verificationLbl.font = UIFont.init(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                        verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(verificationLbl)
                        
                        let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                        lineVw.backgroundColor = UIColor.grayColor()
                        alerVw.addSubview(lineVw)
                        
                        let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+13, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.7))
                        contentLbl.font = UIFont.init(name: "Helvetica", size:14)
                        contentLbl.textAlignment = .Center
                        contentLbl.textColor = UIColor.grayColor()
                        contentLbl.text = "You will now be logged out of Health4life. Once your account has been verified by your doctor, you will be notified via a pop-up notification and/or an email. At that time, you will be able to log back into Health4life and self-schedule video visits with your doctor.\n\nThank you for joining Health4life!"
                        contentLbl.numberOfLines = 13
                        alerVw.addSubview(contentLbl)
                        
                        let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.14),alerVw.frame.size.width, alerVw.frame.size.height*0.14))
                        okLbl.setTitle("OK", forState: .Normal)
                        okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                        okLbl.titleLabel?.textAlignment = .Center
                        okLbl.addTarget(self, action: #selector(PharmacySelectionViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                        okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(okLbl)
                    }
                    else if(self.view.frame.size.height > 600 && self.view.frame.size.height < 700){
                        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                        backVw.tag = 2369
                        self.view.addSubview(backVw)
                        
                        let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.48)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.4))
                        alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                        alerVw.backgroundColor = UIColor.whiteColor()
                        backVw.addSubview(alerVw)
                        
                        let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                        rightImg.image = UIImage(named: "confirm")
                        alerVw.addSubview(rightImg)
                        
                        let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.5, self.view.frame.size.width*0.05))
                        verificationLbl.text = " Pharmacy Selected"
                        verificationLbl.font = UIFont.init(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                        verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(verificationLbl)
                        
                        let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                        lineVw.backgroundColor = UIColor.grayColor()
                        alerVw.addSubview(lineVw)
                        
                        let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+13, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.7))
                        contentLbl.font = UIFont.init(name: "Helvetica", size:14)
                        contentLbl.textAlignment = .Center
                        contentLbl.textColor = UIColor.grayColor()
                        contentLbl.text = "You will now be logged out of Health4life. Once your account has been verified by your doctor, you will be notified via a pop-up notification and/or an email. At that time, you will be able to log back into Health4life and self-schedule video visits with your doctor.\n\nThank you for joining Health4life!!"
                        contentLbl.numberOfLines = 13
                        alerVw.addSubview(contentLbl)
                        
                        let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.14),alerVw.frame.size.width, alerVw.frame.size.height*0.14))
                        okLbl.setTitle("OK", forState: .Normal)
                        okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                        okLbl.titleLabel?.textAlignment = .Center
                        okLbl.addTarget(self, action: #selector(PharmacySelectionViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                        okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(okLbl)
                    }
                    else{
                        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                        backVw.tag = 2369
                        self.view.addSubview(backVw)
                        let alerVw = UIView()
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.48)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.36)
                        }else{
                            alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.2)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.3)
                        }
                        alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                        alerVw.backgroundColor = UIColor.whiteColor()
                        backVw.addSubview(alerVw)
                        
                        let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                        rightImg.image = UIImage(named: "confirm")
                        alerVw.addSubview(rightImg)
                        
                        let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.5, self.view.frame.size.width*0.05))
                        verificationLbl.text = " Pharmacy Selected"
                        verificationLbl.font = UIFont.init(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                        verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(verificationLbl)
                        
                        let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                        lineVw.backgroundColor = UIColor.grayColor()
                        alerVw.addSubview(lineVw)
                        let contentLbl = UILabel()
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            contentLbl.font  = UIFont.init(name: "Helvetica", size:14)
                            contentLbl.frame =  CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+13, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.7)
                        }else{
                            contentLbl.font  = UIFont.init(name: "Helvetica", size:19)
                            contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.05,verificationLbl.frame.size.height+25, alerVw.frame.size.width-(alerVw.frame.size.width*0.1),alerVw.frame.size.height*0.6)
                        }
                        
                        contentLbl.textAlignment = .Center
                        contentLbl.textColor = UIColor.grayColor()
                        contentLbl.text = "You will now be logged out of Health4life. Once your account has been verified by your doctor, you will be notified via a pop-up notification and/or an email. At that time, you will be able to log back into Health4life and self-schedule video visits with your doctor.\n\nThank you for joining Health4life!!"
                        contentLbl.numberOfLines = 13
                        alerVw.addSubview(contentLbl)
                        
                        let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.14),alerVw.frame.size.width, alerVw.frame.size.height*0.14))
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                            okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                        }else{
                            okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                        }
                        okLbl.setTitle("OK", forState: .Normal)
                        okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                        okLbl.titleLabel?.textAlignment = .Center
                        okLbl.addTarget(self, action: #selector(PharmacySelectionViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                        okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(okLbl)
                    }
                    
                }else{
                  NSUserDefaults.standardUserDefaults().setValue("0", forKey: "usertype")
                    var storyboard = UIStoryboard()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                    }else{
                        storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    }
                    let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    let vc = storyboard.instantiateViewControllerWithIdentifier("PatientHomeViewController") as! PatientHomeViewController
                    let nav = UINavigationController(rootViewController: vc)
                    appDelegate.window?.rootViewController = nav
                    appDelegate.window?.makeKeyAndVisible()
                }
                }
              }
            }
    
    func okBtnAc2(){
        let view = self.view.viewWithTag(2369)! as UIView
        view.removeFromSuperview()
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.removeObjectForKey("userLoc")
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if(aViewController is ViewController){
                self.navigationController!.popToViewController(aViewController, animated: true);
            }
        }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("Alert", alertmessage: failureError.localizedDescription)
    }

    @IBAction func backBtnAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
extension PharmacySelectionViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
