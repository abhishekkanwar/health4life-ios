

import UIKit
import SlideMenuControllerSwift
import MapKit
import GoogleMaps
import CoreLocation

class SelectPharmacyViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,MKMapViewDelegate,WebServiceDelegate,CLLocationManagerDelegate,GMSMapViewDelegate{

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var selectPharmacyTableVw: UITableView!
    var indexInt = NSInteger()
    var responseData = NSArray()
    var nextBool = Bool()
    let mapView = MKMapView()
    let addData = NSMutableArray()
    let addressdata = NSMutableArray()
    let cityData = NSMutableArray()
    let stateData = NSMutableArray()
    let zipcodeData = NSMutableArray()
    let phonePrimaryData = NSMutableArray()
    let faxData = NSMutableArray()
    let timingData = NSMutableArray()
    let certifiedData = NSMutableArray()
    let pharmIdData = NSMutableArray()
    let storeNoData = NSMutableArray()
    let pharma_idData = NSMutableArray()
    var newAnotation = MKPointAnnotation()
    let locationManager = CLLocationManager()
    var pinAnnotationView:MKPinAnnotationView!
    let regionRadius: CLLocationDistance = 1000
    let annotation = MKPointAnnotation()
    var googleMapsView: GMSMapView!
    var latLng = Bool()
    var markerTittle = NSString()
    var pharmacySelectName = NSString()
    var serverInt = NSInteger()
      override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = NSUserDefaults.standardUserDefaults()
        if (defaults.boolForKey("selectPharma") == false) {
            if(self.view.frame.size.height < 600){
        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        backVw.tag = 2356
        self.view.addSubview(backVw)
        
        let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.07,self.view.frame.size.height/2-((self.view.frame.size.height*0.35)/2), self.view.frame.size.width-(self.view.frame.size.width*0.14), self.view.frame.size.height*0.35))
        alerVw.backgroundColor = UIColor.whiteColor()
        backVw.addSubview(alerVw)
        
        let rightImg = UIImageView.init(frame: CGRectMake(12,12, self.view.frame.size.width*0.06, self.view.frame.size.width*0.06))
        rightImg.image = UIImage(named: "confirm")
        alerVw.addSubview(rightImg)
        
        let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+25,12, self.view.frame.size.width*0.45, self.view.frame.size.width*0.06))
        verificationLbl.text = " Message"
        verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
        verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(verificationLbl)
        
        let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+20,alerVw.frame.size.width,1))
        lineVw.backgroundColor = UIColor.grayColor()
        alerVw.addSubview(lineVw)
        
        let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+20, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.6))
        contentLbl.font = UIFont(name: "Helvetica", size:14)
        contentLbl.textAlignment = .Center
        contentLbl.textColor = UIColor.grayColor()
        contentLbl.text = " You will now be prompted to select a Pharmacy. If your doctor writes you a prescription, it will be filled at this location. You can change your pharmacy location at any time in your patient profile in the ‘My Information’ tab."
        contentLbl.numberOfLines = 7
        alerVw.addSubview(contentLbl)
        
        let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.18),alerVw.frame.size.width, alerVw.frame.size.height*0.18))
        okLbl.setTitle("OK", forState: .Normal)
        okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        okLbl.titleLabel?.textAlignment = .Center
        okLbl.addTarget(self, action: #selector(SelectPharmacyViewController.okBtnAc1), forControlEvents: .TouchUpInside)
        okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(okLbl)
            }
         else if(self.view.frame.size.height > 600 && self.view.frame.size.height < 700){
                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                backVw.tag = 2356
                self.view.addSubview(backVw)
                
                let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.07,self.view.frame.size.height/2-((self.view.frame.size.height*0.35)/2), self.view.frame.size.width-(self.view.frame.size.width*0.14), self.view.frame.size.height*0.28))
                alerVw.backgroundColor = UIColor.whiteColor()
                backVw.addSubview(alerVw)
                
                let rightImg = UIImageView.init(frame: CGRectMake(12,12, self.view.frame.size.width*0.06, self.view.frame.size.width*0.06))
                rightImg.image = UIImage(named: "confirm")
                alerVw.addSubview(rightImg)
                
                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,12, self.view.frame.size.width*0.45, self.view.frame.size.width*0.06))
                verificationLbl.text = " Message"
                verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(verificationLbl)
                
                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+20,alerVw.frame.size.width,1))
                lineVw.backgroundColor = UIColor.grayColor()
                alerVw.addSubview(lineVw)
                
                let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+20, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.6))
                contentLbl.font = UIFont(name: "Helvetica", size:14)
                contentLbl.textAlignment = .Center
                contentLbl.textColor = UIColor.grayColor()
                contentLbl.text = " You will now be prompted to select a Pharmacy. If your doctor writes you a prescription, it will be filled at this location. You can change your pharmacy location at any time in your patient profile in the ‘My Information’ tab."
                contentLbl.numberOfLines = 7
                alerVw.addSubview(contentLbl)
                
                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.18),alerVw.frame.size.width, alerVw.frame.size.height*0.18))
                okLbl.setTitle("OK", forState: .Normal)
                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                okLbl.titleLabel?.textAlignment = .Center
                okLbl.addTarget(self, action: #selector(SelectPharmacyViewController.okBtnAc1), forControlEvents: .TouchUpInside)
                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(okLbl)
         }else{
                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                backVw.tag = 2356
                self.view.addSubview(backVw)
                let alerVw = UIView()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.07,self.view.frame.size.height/2-((self.view.frame.size.height*0.35)/2), self.view.frame.size.width-(self.view.frame.size.width*0.14), self.view.frame.size.height*0.26)
                }else{
                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.2)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.25)
                }
                alerVw.backgroundColor = UIColor.whiteColor()
                backVw.addSubview(alerVw)
                
                let rightImg = UIImageView.init(frame: CGRectMake(12,12, self.view.frame.size.width*0.06, self.view.frame.size.width*0.06))
                rightImg.image = UIImage(named: "confirm")
                alerVw.addSubview(rightImg)
                
                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,12, self.view.frame.size.width*0.45, self.view.frame.size.width*0.06))
                verificationLbl.text = " Message"
                verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(verificationLbl)
                
                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+20,alerVw.frame.size.width,1))
                lineVw.backgroundColor = UIColor.grayColor()
                alerVw.addSubview(lineVw)
                
                let contentLbl = UILabel()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    contentLbl.font  = UIFont(name: "Helvetica", size:14)
                    contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+20, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.6)
                }else{
                    contentLbl.font  = UIFont(name: "Helvetica", size:19)
                    contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.05,verificationLbl.frame.size.height+21, alerVw.frame.size.width-(alerVw.frame.size.width*0.1),alerVw.frame.size.height*0.5)
                }
                contentLbl.textAlignment = .Center
                contentLbl.textColor = UIColor.grayColor()
                contentLbl.text = " You will now be prompted to select a Pharmacy. If your doctor writes you a prescription, it will be filled at this location. You can change your pharmacy location at any time in your patient profile in the ‘My Information’ tab."
                contentLbl.numberOfLines = 7
                alerVw.addSubview(contentLbl)
                
                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.18),alerVw.frame.size.width, alerVw.frame.size.height*0.18))
                okLbl.setTitle("OK", forState: .Normal)
                if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                }else{
                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                }
                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                okLbl.titleLabel?.textAlignment = .Center
                okLbl.addTarget(self, action: #selector(SelectPharmacyViewController.okBtnAc1), forControlEvents: .TouchUpInside)
                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(okLbl)
         }
        }
        self.navigationController?.navigationBarHidden = true
        selectPharmacyTableVw.delegate = self
        selectPharmacyTableVw.dataSource = self
       
        if(nextBool == true){
          backBtn.hidden = true
        }else{
            backBtn.hidden = false
        }
        let headerView: UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height*0.4))
        selectPharmacyTableVw.tableHeaderView = headerView
        self.googleMapsView = GMSMapView(frame: CGRectMake(self.view.frame.size.width*0.05, self.view.frame.size.height*0.02, self.view.frame.size.width-(2*(self.view.frame.size.width*0.05)), self.view.frame.size.height*0.3))
        self.googleMapsView.delegate =  self
        headerView.addSubview(self.googleMapsView)
        let camera = GMSCameraPosition.cameraWithLatitude(36.1699, longitude: -115.1398, zoom: 10)
        googleMapsView.camera = camera
        googleMapsView.setMinZoom(10, maxZoom: 100.0)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        let imageView: UIImageView = UIImageView(frame: CGRectMake(self.view.frame.size.width*0.05, self.view.frame.size.height*0.34, self.view.frame.size.width-(2*(self.view.frame.size.width*0.05)), self.view.frame.size.height*0.06))
        imageView.userInteractionEnabled = true
        imageView.image = UIImage(named: "box2")
        headerView.addSubview(imageView)
        
        let searchImageView: UIImageView = UIImageView(frame: CGRectMake(10,self.view.frame.size.height*0.01, self.view.frame.size.height*0.04, self.view.frame.size.height*0.04))
        searchImageView.image = UIImage(named: "searchIcon")
        imageView.addSubview(searchImageView)
        
        let searchVw : UIView = UIView(frame: CGRectMake(18+self.view.frame.size.height*0.04,self.view.frame.size.height*0.002,1.3, self.view.frame.size.height*0.056))
        searchVw.backgroundColor = UIColor.lightGrayColor()
        imageView.addSubview(searchVw)
        
        let searchField : UITextField = UITextField(frame: CGRectMake(20+self.view.frame.size.height*0.04,self.view.frame.size.height*0.005,imageView.frame.size.width-(25+self.view.frame.size.height*0.04), self.view.frame.size.height*0.05))
        searchField.userInteractionEnabled = true
        searchField.delegate = self
        searchField.tag = 360
        searchField.tintColor = UIColor.blackColor()
        searchField.font = UIFont.systemFontOfSize(self.view.frame.size.width*0.04)
        searchField.placeholder = " enter your zipcode"
        imageView.addSubview(searchField)
        
        //Note: Searching for your pharmacy may take a few seconds. We appreciate your patience!
        let footerView: UIView = UIView(frame: CGRectMake(0, self.view.frame.size.height - (self.view.frame.size.height*0.18), self.view.frame.size.width, self.view.frame.size.height*0.16))
        footerView.tag = 99
        let noteLbl: UILabel = UILabel()
        noteLbl.frame = CGRectMake(self.view.frame.size.width*0.1,footerView.frame.size.height*0.12, self.view.frame.size.width-(2*(self.view.frame.size.width*0.1)),footerView.frame.size.height-footerView.frame.size.height*0.1)
        noteLbl.text = "Note:- We are searching for pharmacies in your zip code and this may take 15-20 seconds as we are searching our database of over 70,000 pharmacies. We appreciate your pa ence!"
         noteLbl.font  = UIFont(name: "Helvetica", size:14)
        noteLbl.textAlignment = .Center
        noteLbl.numberOfLines = 4
        noteLbl.textColor = UIColor.blackColor()
        footerView.addSubview(noteLbl)
        self.view.addSubview(footerView)
        footerView.hidden = true
    }
   
    func getLocationFromAddressString(addressStr: String) -> CLLocationCoordinate2D {
        var latitude :Double = 0
        var longitude : Double = 0
        //let esc_addr = addressStr.stringByAppendingFormat("%@","")
        
        let esc_addr:String = addressStr.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLUserAllowedCharacterSet())!
        let req: String = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=\(esc_addr)"
        let url = NSURL(string: req)
        let result:String? = try? String(contentsOfURL:url!)
        if result != "" {
            let scanner: NSScanner = NSScanner(string: result!)
            if(scanner.scanUpToString("\"lat\" :", intoString: nil) && scanner.scanString("\"lat\" :", intoString: nil)){
                scanner.scanDouble(&latitude)
                if(scanner.scanUpToString("\"lng\" :", intoString: nil) && scanner.scanString("\"lng\" :", intoString: nil)){
                    scanner.scanDouble(&longitude)
                }
            }
        }
        var center:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0);
        center.latitude = latitude;
        center.longitude = longitude;
        if(latitude != 0.0 && longitude != 0.0){
        let position = CLLocationCoordinate2D(latitude: center.latitude, longitude: center.longitude)
        let marker = GMSMarker(position: position)
       // marker.snippet = markerTittle as String
           marker.title = markerTittle as String
           googleMapsView.setMinZoom(10, maxZoom: 100.0)
           marker.map = googleMapsView
            let camera = GMSCameraPosition.cameraWithLatitude(center.latitude, longitude: center.longitude, zoom: 10)
            googleMapsView.camera = camera
            let locationDict = ["lat": center.latitude, "lng": center.longitude]
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(locationDict, forKey: "userLoc")
        }
        //googleMapsView.selectedMarker = marker
        print("lat:\(center.latitude)")
        print("long:\(center.longitude)")
        if(center.latitude == 0.0 && center.longitude == 0.0){
          latLng = false
        }
        else{
           latLng = true
        }
        return center;
    }
    func mapView(mapView: GMSMapView, didTapInfoWindowOfMarker marker: GMSMarker) {
        let title = marker.title
        print(title!)
        print(addData)
        print(addData.indexOfObject(title!))
        indexInt = addData.indexOfObject(title!)
        let vc = storyboard!.instantiateViewControllerWithIdentifier("PharmacySelectionViewController") as! PharmacySelectionViewController
        vc.btnExist = true
        vc.pharmacyName = addData[indexInt] as! NSString
        vc.pharmaAdd = addressdata[indexInt] as! NSString
        vc.pharmaFax = faxData[indexInt] as! NSString
        vc.pharmaCertified = certifiedData[indexInt] as! NSString
        vc.pharmaCity = cityData[indexInt] as! NSString
        vc.pharmaHours = timingData[indexInt] as! NSString
        vc.pharmaState = stateData[indexInt] as! NSString
        vc.pharmaPhone = phonePrimaryData[indexInt] as! NSString
        vc.pharmaZipcode = zipcodeData[indexInt] as! NSString
        vc.pharmacyId = pharmIdData[indexInt] as! NSString
        vc.nxt = nextBool
        vc.pharma_id = pharma_idData[indexInt] as! NSString
        vc.storeNumber = storeNoData[indexInt] as! NSString
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func viewWillAppear(animated: Bool) {
        let defaults = NSUserDefaults.standardUserDefaults()
        if let loc = defaults.valueForKey("userLoc"){
        let lat = loc.valueForKey("lat") as! Double
        let lng = loc.valueForKey("lng") as! Double
        let camera = GMSCameraPosition.cameraWithLatitude(lat, longitude: lng, zoom: 10)
        googleMapsView.camera = camera
        googleMapsView.setMinZoom(10, maxZoom: 100.0)
     }
    }
    func mapView(mapView: GMSMapView, idleAtCameraPosition position: GMSCameraPosition) {
        let lat:CLLocationDegrees =  mapView.camera.target.latitude
        let long:CLLocationDegrees =  mapView.camera.target.longitude
        let location = CLLocation(latitude: lat, longitude: long)
        print(location)
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            print(location)
            if error != nil {
                let defaults = NSUserDefaults.standardUserDefaults()
                if let loc = defaults.valueForKey("userLoc"){
                    let lat = loc.valueForKey("lat") as! Double
                    let lng = loc.valueForKey("lng") as! Double
                    let camera = GMSCameraPosition.cameraWithLatitude(lat, longitude: lng, zoom: 10)
                    self.googleMapsView.camera = camera
                    self.googleMapsView.setMinZoom(10, maxZoom: 100.0)
                }
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            if placemarks!.count > 0 {
                let pm = placemarks![0]
                if(pm.country != nil){
                print(pm.country! as String)
                let country = pm.country!
                if(country != "United States"){
                    let defaults = NSUserDefaults.standardUserDefaults()
                    if let loc = defaults.valueForKey("userLoc"){
                        let lat = loc.valueForKey("lat") as! Double
                        let lng = loc.valueForKey("lng") as! Double
                        let camera = GMSCameraPosition.cameraWithLatitude(lat, longitude: lng, zoom: 10)
                        self.googleMapsView.camera = camera
                        self.googleMapsView.setMinZoom(10, maxZoom: 100.0)
                    }
                }
                }else{
                    let defaults = NSUserDefaults.standardUserDefaults()
                    if let loc = defaults.valueForKey("userLoc"){
                        let lat = loc.valueForKey("lat") as! Double
                        let lng = loc.valueForKey("lng") as! Double
                        let camera = GMSCameraPosition.cameraWithLatitude(lat, longitude: lng, zoom: 10)
                        self.googleMapsView.camera = camera
                        self.googleMapsView.setMinZoom(10, maxZoom: 100.0)
                    }
                }
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
  
    }
    
    func okBtnAc1(){
        let view = self.view.viewWithTag(2356)! as UIView
        view.removeFromSuperview()
    }
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
          self.mapView.removeAnnotations(self.mapView.annotations)
        responseData = []
        let trimNameString = textField.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        if(trimNameString == ""){
          AppManager.sharedManager.Showalert("Alert", alertmessage: "Please enter Zipcode")
           textField.text = ""
            addData.removeAllObjects()
            self.selectPharmacyTableVw.reloadData()
        }
        else{
            self.addData.removeAllObjects()
            self.addressdata.removeAllObjects()
            self.cityData.removeAllObjects()
            self.stateData.removeAllObjects()
            self.zipcodeData.removeAllObjects()
            self.faxData.removeAllObjects()
            self.timingData.removeAllObjects()
            self.certifiedData.removeAllObjects()
            self.phonePrimaryData.removeAllObjects()
            self.pharmIdData.removeAllObjects()
            self.storeNoData.removeAllObjects()
            googleMapsView.clear()
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
            let vw = self.view.viewWithTag(99)!
            vw.hidden = false
        let params = "Value=\(textField.text!)"
            serverInt = 1
        AppManager.sharedManager.postDataOnserver(params, postUrl: "cron/PharmacyCheck")
        }
        return true
    }
   
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        dispatch_async(dispatch_get_main_queue()) {
            let vw = self.view.viewWithTag(99)!
            vw.hidden = true
            
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            if(self.serverInt == 1){
        if(responseDict.valueForKey("data") != nil){
            self.mapView.removeAnnotations(self.mapView.annotations)
        self.responseData = responseDict.valueForKey("data") as! NSArray
        for q in 0...self.responseData.count-1 {
            let location: String = self.responseData[q].valueForKey("fullAddress") as! String
            self.markerTittle = "\(self.responseData[q].valueForKeyPath("data.StoreName") as! String)"
            self.getLocationFromAddressString(location)
            if(self.latLng == true){
          let stoteName = self.responseData[q].valueForKeyPath("data.StoreName") as! NSString
          let address = self.responseData[q].valueForKeyPath("data.Address") as! NSString
            let city = self.responseData[q].valueForKeyPath("data.City") as! NSString
            let state = self.responseData[q].valueForKeyPath("data.State") as! NSString
            let zipcode = self.responseData[q].valueForKeyPath("data.Zip") as! NSString
            let phone = self.responseData[q].valueForKeyPath("data.PhonePrimary") as! NSString
            let Fax = self.responseData[q].valueForKeyPath("data.Fax") as! NSString
            let timing = self.responseData[q].valueForKeyPath("data.Hours24") as! NSString
            let certified = self.responseData[q].valueForKeyPath("data.IsFromSurescripts") as! NSString
            let phrmId = self.responseData[q].valueForKeyPath("data._id") as! NSString
            let storeNumber = self.responseData[q].valueForKeyPath("data.DoseSpotPharmacyID") as! NSString
            let phrma_Id = self.responseData[q].valueForKeyPath("data._id") as! NSString
            self.addData.addObject(stoteName)
            self.addressdata.addObject(address)
            self.cityData.addObject(city)
            self.stateData.addObject(state)
            self.zipcodeData.addObject(zipcode)
            self.faxData.addObject(Fax)
            self.timingData.addObject(timing)
            self.certifiedData.addObject(certified)
            self.phonePrimaryData.addObject(phone)
            self.pharmIdData.addObject(phrmId)
            self.storeNoData.addObject(storeNumber)
            self.pharma_idData.addObject(phrma_Id)
            }
        }
         self.selectPharmacyTableVw.reloadData()
        }else{
            self.mapView.removeAnnotations(self.mapView.annotations)
             let txtFld = self.view.viewWithTag(360) as! UITextField
            txtFld.text = ""
            AppManager.sharedManager.Showalert("Alert", alertmessage:"No Pharmacy Found.")
            self.addData.removeAllObjects()
            
            dispatch_async(dispatch_get_main_queue()) {
                self.selectPharmacyTableVw.reloadData()
            }
         }
       }
      }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addData.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        let imageView: UIView = (cell?.viewWithTag(1650))!
        let address: UILabel = (cell!.viewWithTag(366) as! UILabel)
        address.text = addressdata[indexPath.row]  as? String
        let cityState: UILabel = (cell!.viewWithTag(367) as! UILabel)
        cityState.text = "\(cityData[indexPath.row] as! NSString),\(stateData[indexPath.row] as! NSString)"
        let addLbl: UILabel = (cell!.viewWithTag(365) as! UILabel)
        if(addData.count == 0){
            addLbl.text = ""
            imageView.hidden = true
            tableView.scrollEnabled = false
        }else{
            cell!.layer.cornerRadius = 7.0
            cell!.layer.masksToBounds = true
            imageView.hidden = false
            tableView.scrollEnabled = true
            imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
            imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
            imageView.layer.shadowOffset = CGSizeMake(10, 5);
            imageView.layer.shadowRadius = 1;
            imageView.layer.shadowOpacity = 0.06;
            imageView.backgroundColor = UIColor.whiteColor()
            imageView.layer.masksToBounds = false;
            imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
            imageView.layer.borderWidth = 0.5
            if(indexPath.row%2 == 0){
                imageView.backgroundColor = UIColor.whiteColor()
            }else{
                imageView.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            }
           addLbl.text = addData[indexPath.row] as? String
        }
            return cell!
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       // let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard!.instantiateViewControllerWithIdentifier("PharmacySelectionViewController") as! PharmacySelectionViewController
        vc.btnExist = true
        vc.pharmacyName = addData[indexPath.row] as! NSString
        vc.pharmaAdd = addressdata[indexPath.row] as! NSString
        vc.pharmaFax = faxData[indexPath.row] as! NSString
        vc.pharmaCertified = certifiedData[indexPath.row] as! NSString
        vc.pharmaCity = cityData[indexPath.row] as! NSString
        vc.pharmaHours = timingData[indexPath.row] as! NSString
        vc.pharmaState = stateData[indexPath.row] as! NSString
        vc.pharmaPhone = phonePrimaryData[indexPath.row] as! NSString
        vc.pharmaZipcode = zipcodeData[indexPath.row] as! NSString
        vc.pharmacyId = pharmIdData[indexPath.row] as! NSString
        vc.nxt = nextBool
        vc.pharma_id = pharma_idData[indexPath.row] as! NSString
        vc.storeNumber = storeNoData[indexPath.row] as! NSString
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        if(nextBool == true){
        }else{
          let serchFld = self.view.viewWithTag(360) as! UITextField
            serchFld.delegate = nil
          self.navigationController?.popViewControllerAnimated(true)
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.removeObjectForKey("userLoc")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
//            let address1 = location
//            let geocoder = CLGeocoder()
//
//            geocoder.geocodeAddressString(address1, completionHandler: {(placemarks, error) -> Void in
//                if((error) != nil){
//                    print("Error", error)
//                }
//                if let placemark = placemarks?.first {
//                    let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
//                    coordinates.latitude
//                    coordinates.longitude
//                    print("lat", coordinates.latitude)
//                    print("long", coordinates.longitude)
//                    let position = CLLocationCoordinate2D(latitude: coordinates.latitude, longitude: coordinates.longitude)
//                    let marker = GMSMarker(position: position)
//                    marker.title = self.markerTittle as String
//                    marker.map = self.googleMapsView
//                    let camera = GMSCameraPosition.cameraWithLatitude(coordinates.latitude, longitude: coordinates.longitude, zoom: 10)
//                    self.googleMapsView.camera = camera
//                    let locationDict = ["lat": coordinates.latitude, "lng": coordinates.longitude]
//                    let defaults = NSUserDefaults.standardUserDefaults()
//                    defaults.setObject(locationDict, forKey: "userLoc")
//                }
//            })


//            let geocoder: CLGeocoder = CLGeocoder()
//            geocoder.geocodeAddressString(location,completionHandler: {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
//                if (placemarks?.count > 0) {
//                    let topResult: CLPlacemark = (placemarks?[0])!
//                    let placemark: MKPlacemark = MKPlacemark(placemark: topResult)
//                    var region: MKCoordinateRegion = self.mapView.region
//
//                    region.center.latitude = (placemark.location?.coordinate.latitude)!
//                    region.center.longitude = (placemark.location?.coordinate.longitude)!
//
//                    let locationDict = ["lat": region.center.latitude, "lng": region.center.longitude]
//                    let defaults = NSUserDefaults.standardUserDefaults()
//                    defaults.setObject(locationDict, forKey: "userLoc")
//                    region.span = MKCoordinateSpanMake(0.5, 0.5)
//                    let annotation = MKPointAnnotation()
//                    annotation.coordinate = (placemark.location?.coordinate)!
//                    annotation.title = "\(self.responseData[q].valueForKeyPath("data.StoreName") as! String)"
//                    self.mapView.addAnnotation(annotation)
//
//                    self.mapView.setRegion(region, animated: true)
//                    //self.mapView.addAnnotation(placemark)
//                }
//            })
//
//
//            let lat = self.responseData[q].valueForKey("Lat") as! Double
//            let long = self.responseData[q].valueForKey("Long") as! Double
//            let location = CLLocationCoordinate2D(
//                latitude:  lat,
//                longitude: long
//            )
//            // 2
//            let locationDict = ["lat": lat, "lng": long]
//            let defaults = NSUserDefaults.standardUserDefaults()
//            defaults.setObject(locationDict, forKey: "userLoc")
//            let span = MKCoordinateSpanMake(0.07, 0.07)
//            let region = MKCoordinateRegion(center: location, span: span)
//            self.mapView.setRegion(region, animated: true)
//
//            let annotation = MKPointAnnotation()
//            annotation.coordinate = location
//            annotation.title = "\(self.responseData[q].valueForKeyPath("data.StoreName") as! String)"
//            self.mapView.addAnnotation(annotation)
//            self.mapView.addAnnotation(self.annotation)
