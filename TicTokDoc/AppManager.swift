



import UIKit
import SystemConfiguration


public protocol WebServiceDelegate : NSObjectProtocol
{
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString)
    
    func failureRsponseError(failureError:NSError)
}

public class AppManager: NSObject {
    
    var arr_passCountryList : NSMutableArray = NSMutableArray()
    
    public var delegate: WebServiceDelegate?
    
    //********* Make Instance Of class ***********//
    
    private struct Constants {
        static let sharedManager = AppManager()
    }
    public class var sharedManager: AppManager {
        return Constants.sharedManager
    }
    
    
    //************ Check Internet Connectivity **********//
    
    public class NetWorkReachability
    {
        class func isConnectedToNetwork() -> Bool
        {
            var zeroAddress = sockaddr_in()
            zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
            zeroAddress.sin_family = sa_family_t(AF_INET)
            let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
                SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
            }
            var flags = SCNetworkReachabilityFlags()
            if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags)
            {
                return false
            }
            let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
            let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
            return (isReachable && !needsConnection)
        }
    }
    
    //********** Get Device UDID **********//
    public class DeviceUDID
    {
        class func GETUDID() -> String{
            let uuid = NSUUID().UUIDString
            return uuid
        }
    }
    
    
    //******** Check Content is not or not ***********//
    
    public class  getCurrectValue
    {
        class func CheckContentNullORNot(content:(NSString)) -> Bool
        {
            
            if content .isEqual("null") || content.isEqual("(null)") || content.isEqual("<null>") || content.isEqual("nil") || content.isEqual("") || content.isEqual("<nil>") || content.length == 0
            {
                return false
            }
            else
            {
                return true
            }
        }
    }
    
    
    public func isValidEmail(emailText:NSString)->Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
//        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
//        let result = emailTest.evaluateWithObject(testStr)
//        return result
        
        
//    let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
//    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluateWithObject(emailText)
      }
    
    public   func validate(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluateWithObject(value)
        return result
    }
    
    //************ Append Base Url & Api URl ***********//
    
    func createServerPath(requestPath: String) -> String {
        print("\(Header.BASE_URL)\(requestPath)")
        return "\(Header.BASE_URL)\(requestPath)"
    }
    
    //************ Web Service method ***********//
   public func setProfileImage(imageToResize: UIImage, onImageView: UIImageView) -> UIImage
    {
        let width = imageToResize.size.width
        let height = imageToResize.size.height
        
        var scaleFactor: CGFloat
        
        if(width > height)
        {
            scaleFactor = onImageView.frame.size.height / height;
        }
        else
        {
            scaleFactor = onImageView.frame.size.width / width;
        }
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(width * scaleFactor, height * scaleFactor), false, 0.0)
        imageToResize.drawInRect(CGRectMake(0, 0, width * scaleFactor, height * scaleFactor))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resizedImage!;
    }
   public func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    public func postDataOnserver(params:AnyObject,postUrl:NSString)
    {
        let serverpath: String = self.createServerPath(postUrl as String)
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: NSURL(string: serverpath.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)!)
        request.HTTPMethod        = "POST"
        request.HTTPBody          = params.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        request.timeoutInterval   = 90.0
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            do{
                if(error == nil){
                let responseDictionary : NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    print(responseDictionary)
                self.delegate?.serverReponse(responseDictionary.mutableCopy() as! NSMutableDictionary, serviceurl: postUrl)
                    
                }else{
                    print(error)
                    dispatch_async(dispatch_get_main_queue()) {
                        let errorDescription:String = (error?.localizedDescription)!
                        print(errorDescription)
                        var alert = UIAlertController()
                        if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                            alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                            
                        }else{
                            alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                        }
                        // let alert = UIAlertController(title: "Alert", message: "\(errorDescription)", preferredStyle: UIAlertControllerStyle.Alert)
                        let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                            UIAlertAction in
                            self.postDataOnserver(params, postUrl: postUrl)
                        }
                        alert.addAction(cancelAction)
                        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
                    }
                }
            }
            catch {
                 dispatch_async(dispatch_get_main_queue()) {
                 self.delegate?.failureRsponseError((error as NSError))
                }
            }
        }
        task.resume()
        return
    }
    
    //MARK : - Chat Function APIs
    
    //MARK : - Generate Chat Group Id Function
    
    func generateGroupIdForUser(postUrl:NSString, params:AnyObject, completion: (result: NSDictionary)->()) -> Void {
        
        let serverpath: String = self.createServerPath(postUrl as String)
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: NSURL(string: serverpath.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)!)
        request.HTTPMethod        = "POST"
        request.HTTPBody          = params.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        request.timeoutInterval   = 90.0
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            do{
                if(error == nil){
                    let responseDictionary : NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        //self.delegate?.failureRsponseError((error as NSError))
                        completion(result: responseDictionary)
                    }
                    
                    //self.delegate?.serverReponse(responseDictionary.mutableCopy() as! NSMutableDictionary, serviceurl: postUrl)
                    
                }else{
                    print(error)
                    dispatch_async(dispatch_get_main_queue()) {
                        let errorDescription:String = (error?.localizedDescription)!
                        let alert = UIAlertController(title: "Alert", message: "\(errorDescription)", preferredStyle: UIAlertControllerStyle.Alert)
                        let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                            UIAlertAction in
                            //self.postDataOnserver(params, postUrl: postUrl)
                        }
                        alert.addAction(cancelAction)
                        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
                    }
                }
            }
            catch {
                dispatch_async(dispatch_get_main_queue()) {
                    //self.delegate?.failureRsponseError((error as NSError))
                }
            }
        }
        task.resume()
        return
        
        
    }
    
    func textField1(textField: UITextField,shouldChangeCharactersInRange range: NSRange,replacementString string: String)-> Bool
    {
        if textField.text! == ""
        {
            return true
        }
        return false
    }
    // Activity Indicator
    func showActivityIndicatorInView(inView: UIView, withLabel message: String)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0)) {
            // ...Run some task in the background here...
            dispatch_async(dispatch_get_main_queue()) {
        let loadingNotification = MBProgressHUD.showHUDAddedTo(inView, animated: true)
        loadingNotification.mode = MBProgressHUDMode.Indeterminate
        loadingNotification.label.text = message
        loadingNotification.showAnimated(true)
            }
        }
    }
    
    func hideActivityIndicatorInView(inView: UIView) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0)) {
            // ...Run some task in the background here...
            dispatch_async(dispatch_get_main_queue()) {
                MBProgressHUD.hideAllHUDsForView(inView, animated: true)
                // ...Run something once we're done with the background task...
            }
        }
    }
    public func FetchDatafromServer(params:AnyObject,postUrl:NSString)
    {
        
    }
    func languageSelectedStringForKey(key: String) -> String {
        
        var path = NSString()
        if NSUserDefaults.standardUserDefaults().valueForKey("SelectedLanguage")!.isEqualToString("en") {
            path = NSBundle.mainBundle().pathForResource("en", ofType: "lproj")!
        }
        else if NSUserDefaults.standardUserDefaults().valueForKey("SelectedLanguage")!.isEqualToString("ar") {
            path = NSBundle.mainBundle().pathForResource("ar", ofType: "lproj")!
        }
        else {
            path = NSBundle.mainBundle().pathForResource("en", ofType: "lproj")!
        }
        
        let languageBundle: NSBundle =  NSBundle(path: path as String)!
        let str: String = languageBundle.localizedStringForKey(key, value: "", table: nil)
        return str
    }
    
    //************ Set AlertView *********//
    
    public func Showalert(alerttitle:NSString,alertmessage:NSString)
    {
        let alert = UIAlertController(title: alerttitle as String, message: alertmessage as String, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
    }
    
    
}
