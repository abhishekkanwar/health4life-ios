

import UIKit

class TransmissionErrorViewController: UIViewController,UIWebViewDelegate{

    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var webVw: UIWebView!
    var webUrl = NSString()
    override func viewDidLoad() {
        super.viewDidLoad()
       
       let checkURL : NSURL = NSURL(string: webUrl as String)!
       let requestObj = NSURLRequest(URL: checkURL)
        
        webVw.loadRequest(requestObj)
        webVw.delegate = self
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
    }
    func webViewDidFinishLoad(webView: UIWebView) {
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
    }
    
    @IBAction func doneBtnAc(sender: AnyObject) {
        let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To Exit?", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            NSLog("OK Pressed")
            var storyboard = UIStoryboard()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                storyboard = UIStoryboard(name: "Main", bundle: nil)
            }
            else{
                storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
            }
            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window = UIWindow(frame: UIScreen.mainScreen().bounds)
            let vc = storyboard.instantiateViewControllerWithIdentifier("DoctorHomeViewController") as! DoctorHomeViewController
            let nav = UINavigationController(rootViewController: vc)
            appDelegate.window?.rootViewController = nav
            appDelegate.window?.makeKeyAndVisible()
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

  }
