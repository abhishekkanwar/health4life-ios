

import UIKit
import RealmSwift
protocol  PateintProfileViewControllerDelegate{
    func refreshMyPatientsList( patientId : String )
}


class PateintProfileViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,WebServiceDelegate {
    var imgAr = ["SignUpUser","gnder","yearIcon","phone","Address"]
    var itemAr = ["Name","Gender","Age","Phone number","Address"]
    var staffSelect = Bool()
    var groupIdAr = NSString()
    var tokenAr = NSString()
    var apiKeyAr = NSString()
    var sessionAr = NSString()
    var name = NSString()
    var gnder = NSString()
    var age = NSString()
    var phone = NSString()
    var address = NSString()
    var pending = Bool()
    var acceptRejId = NSString()
    var email = NSString()
    var switchserver = Bool()
    var subscriptionTill = NSString()
    var urlPrescribe = NSString()
    var indexPath = NSInteger()
    var acceptRejectIdAr = NSMutableArray()
    var deviceTkn = NSString()
    var serverInt = NSInteger()
    var ptId = NSString()
    var btnInt = NSInteger()
    var book_id = NSString()
    var schedule_Id = NSString()
    var deviceTokn = NSString()
    var profilepicUrl = NSString()
    var patientNotSub = Bool()
    var doRequest = NSInteger()
    var realm = try! Realm()
    @IBOutlet weak var profileTableVw: UITableView!
    @IBOutlet weak var imgVwPateintProfile: UIImageView!
    @IBOutlet weak var cancelSbBtn: UIButton!
    var valueAr = NSMutableArray()
    
    var delegate : PateintProfileViewControllerDelegate! = nil
    
    override func viewDidLoad(){
        super.viewDidLoad()
        cancelSbBtn.titleLabel?.numberOfLines = 2
        cancelSbBtn.titleLabel?.textAlignment = .Center
        valueAr = [name,gnder,"\(age)","\(phone)",address]
        self.navigationController?.navigationBarHidden = true
        imgVwPateintProfile.layer.cornerRadius = self.imgVwPateintProfile.frame.size.height/2
        imgVwPateintProfile.layer.borderWidth = self.view.frame.size.height*0.012
        imgVwPateintProfile.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        imgVwPateintProfile.clipsToBounds = true
        if let url = NSURL(string: "http://\(profilepicUrl)") {
            self.imgVwPateintProfile.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        if(pending == true){
            cancelSbBtn.hidden = false
            if(btnInt == 2){
                let callAgainSubVw = UIView(frame: CGRectMake(0,0,profileTableVw.frame.size.width,self.view.frame.size.width*0.2))
                let callAgn = UIButton(frame: CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.width*0.04,self.view.frame.size.width-(self.view.frame.size.width*0.4),self.view.frame.size.width*0.11))
                callAgn.setBackgroundImage(UIImage(named: "CallAgain"), forState: .Normal)
                callAgn.addTarget(self, action: #selector(PateintProfileViewController.callAgain), forControlEvents: .TouchUpInside)
                callAgainSubVw.addSubview(callAgn)
                profileTableVw.tableFooterView = callAgainSubVw
            }
            else{
                let cancelSubVw = UIView(frame: CGRectMake(0,0,profileTableVw.frame.size.width,self.view.frame.size.width*0.2))
                let priscriptionBtn = UIButton()
                
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    priscriptionBtn.frame = CGRectMake(self.view.frame.size.width*0.15,self.view.frame.size.width*0.04,self.view.frame.size.width*0.7,self.view.frame.size.width*0.12)
                }else{
                    priscriptionBtn.frame = CGRectMake(self.view.frame.size.width*0.23,self.view.frame.size.height*0.04,self.view.frame.size.width-(self.view.frame.size.width*0.46),self.view.frame.size.width*0.1)
                }
                if(staffSelect == false){
                    priscriptionBtn.setBackgroundImage(UIImage(named: "cancelSub"), forState: .Normal)
                    priscriptionBtn.addTarget(self, action: #selector(PateintProfileViewController.priscriptionAc), forControlEvents: .TouchUpInside)
                }else{
                    priscriptionBtn.setBackgroundImage(UIImage(named: "chatNwBtn"), forState: .Normal)
                    priscriptionBtn.addTarget(self, action: #selector(PateintProfileViewController.chatBtnAc), forControlEvents: .TouchUpInside)
                }
                cancelSubVw.addSubview(priscriptionBtn)
                profileTableVw.tableFooterView = cancelSubVw
            }
        }
        else{
            cancelSbBtn.hidden = true
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                let reqStatusVw = UIView(frame: CGRectMake(0,0,profileTableVw.frame.size.width,self.view.frame.size.width*0.4))
                let confirmReqBtn = UIButton(frame: CGRectMake(20,self.view.frame.size.width*0.04,self.view.frame.size.width*0.8,self.view.frame.size.width*0.13))
                confirmReqBtn.setBackgroundImage(UIImage(named: "bigCnfrmBtn"), forState: .Normal)
                confirmReqBtn.addTarget(self, action: #selector(PateintProfileViewController.confirmBtnAc), forControlEvents: .TouchUpInside)
                reqStatusVw.addSubview(confirmReqBtn)
                
                let deleteReqBtn = UIButton(frame: CGRectMake(20,self.view.frame.size.width*0.21,self.view.frame.size.width*0.8,self.view.frame.size.width*0.13))
                deleteReqBtn.setBackgroundImage(UIImage(named: "bigDelBtn"), forState: .Normal)
                deleteReqBtn.addTarget(self, action: #selector(PateintProfileViewController.deleBtnAc), forControlEvents: .TouchUpInside)
                reqStatusVw.addSubview(deleteReqBtn)
                profileTableVw.tableFooterView = reqStatusVw
            }else{
                let reqStatusVw = UIView(frame: CGRectMake(0,0,profileTableVw.frame.size.width,self.view.frame.size.width*0.2))
                
                let confirmReqBtn = UIButton(frame: CGRectMake(self.view.frame.size.width*0.22,self.view.frame.size.height*0.04,self.view.frame.size.width*0.25,self.view.frame.size.width*0.08))
                confirmReqBtn.setBackgroundImage(UIImage(named: "bigCnfrmBtn"), forState: .Normal)
                confirmReqBtn.addTarget(self, action: #selector(PateintProfileViewController.confirmBtnAc), forControlEvents: .TouchUpInside)
                reqStatusVw.addSubview(confirmReqBtn)
                
                let deleteReqBtn = UIButton(frame: CGRectMake(self.view.frame.size.width*0.53,self.view.frame.size.height*0.04,self.view.frame.size.width*0.25,self.view.frame.size.width*0.08))
                deleteReqBtn.setBackgroundImage(UIImage(named: "bigDelBtn"), forState: .Normal)
                deleteReqBtn.addTarget(self, action: #selector(PateintProfileViewController.deleBtnAc), forControlEvents: .TouchUpInside)
                reqStatusVw.addSubview(deleteReqBtn)
                profileTableVw.tableFooterView = reqStatusVw
            }
        }
        profileTableVw.delegate = self
        profileTableVw.dataSource = self
        
        if let userId = NSUserDefaults.standardUserDefaults().valueForKey("id"){
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
            doRequest = 2
            self.doRequestPost("\(Header.BASE_URL)docter/DRPatientCheck", data: ["DrId":"\(userId)","PatientId":"\(ptId)"])
        }
    }
    func chatBtnAc(){
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("staffId") {
            self.doRequest = 1
            self.doRequestPost("\(Header.BASE_URL)docter/DRdrcreatetoken", data: ["From":"\(userId)","To":"\(ptId)"])
        }
    }
    func doRequestPost(url:String,data:[String: NSObject]){
        let requestDic = data
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                print("Succes:")
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                dispatch_async(dispatch_get_main_queue())
                {
                    if(self.doRequest == 1){
                    self.tokenAr = responseDict.valueForKeyPath("data.TokenData")! as! NSString
                    self.sessionAr = responseDict.valueForKeyPath("data.sessionData")! as! NSString
                    self.apiKeyAr = responseDict.valueForKeyPath("data.apiKey")! as! NSString
                    self.groupIdAr = responseDict.valueForKeyPath("data.groupId")! as! NSString
                    
                    let user = NSMutableDictionary()
                    user.setValue(self.tokenAr, forKey: "TokenData")
                    user.setValue(self.apiKeyAr, forKey: "ApiKey")
                    user.setValue(self.groupIdAr, forKey: "GroupId")
                    user.setValue(self.sessionAr, forKey: "sessionData")
                    user.setValue(self.name, forKey: "Name")
                    let groupId = self.groupIdAr
                    self.msgCountList(groupId as String) { (result) in
                        let chatCount = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", groupId ))
                        if(chatCount.count < result.valueForKey("count") as! Int){
                            let userId = NSUserDefaults.standardUserDefaults().valueForKey("id") as! String
                            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                            RealmInteractor.shared.getMsgsFromServer(userId, groupId:groupId as String) { (result) in
                                AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                                
                                if(result.valueForKey("status") as! Int == 200)
                                {
                                    let chatResults = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", groupId))
                                    try! self.realm.write({
                                        self.realm.delete(chatResults)
                                    })
                                    
                                    let chatArray = result.objectForKey("data") as! NSArray
                                    
                                    for dict in chatArray
                                    {
                                        try! self.realm.write({
                                            
                                            let group = Group()
                                            group.displayName = dict.valueForKey("DisplayName") as! String
                                            group.message = dict.valueForKey("message") as! String
                                            group.messageType = dict.valueForKey("messageType") as! String
                                            group.fromId = dict.valueForKey("From") as! String
                                            group.groupId = dict.valueForKey("GroupId") as! String
                                            self.realm.add(group)
                                            
                                        })
                                        
                                    }
                                    self.navigateToChatView(user)
                                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                                }
                            }
                        }
                        else
                        {
                            self.navigateToChatView(user)
                            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                        }
                    }
                }
                    else{
                        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                        if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 200){
                            
                        }else{
                            self.profileTableVw.tableFooterView = nil
                            self.cancelSbBtn.hidden = true
                        }
                    }
                }
            }
        }
        dataTask.resume()
    }
    func navigateToChatView(user:NSDictionary) -> Void {
        
        let chatResults = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", user.valueForKey("GroupId") as! String))
        print(chatResults.count)
        
        let msgArray = NSMutableArray()
        
        for group in chatResults
        {
            var mySelf = false
            let msgDict = group
            
            let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
            if(userType == 1 || userType == 0 || userType == 2){
                if(msgDict.fromId == (NSUserDefaults.standardUserDefaults().valueForKey("id") as! String))
                {
                    mySelf = true
                }
            }else if(userType == 3){
                if(msgDict.fromId == (NSUserDefaults.standardUserDefaults().valueForKey("staffId") as! String))
                {
                    mySelf = true
                }
            }
            
            let mdict = ["msg":msgDict.message,"bool":mySelf, "msgType":msgDict.messageType,"name":msgDict.displayName]
            msgArray.addObject(mdict)
        }
        
        let firstname:String = NSUserDefaults.standardUserDefaults().valueForKey("firstName") as! String
        
        let vc = storyboard!.instantiateViewControllerWithIdentifier("ChatViewController") as! ChatViewController
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let usertype = defaults.valueForKey("usertype") as! NSInteger
        if(usertype == 3){
            vc.userId = defaults.valueForKey("staffId") as! String
        }else{
            vc.userId = defaults.valueForKey("id") as! String
        }
        
        vc.chatUserId = user.valueForKey("GroupId") as! String
        
        vc.userName = user.valueForKey("Name") as! String
        
        
        vc.Token = user.valueForKey("TokenData") as! String
        vc.SessionID = user.valueForKey("sessionData") as! String
        vc.apiKey = user.valueForKey("ApiKey") as! String
        vc.msgAr = msgArray
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func msgCountList(groupId:String,completion: (result: NSDictionary)->()) -> Void {
        let paramString = "GroupId=\(groupId)"
        AppManager.sharedManager.generateGroupIdForUser("messageChat/listChat", params: paramString) { (result) in
            print("\(result)")
            
            completion(result: result)
        }
    }
    override func viewWillAppear(animated: Bool) {
        let defaults = NSUserDefaults.standardUserDefaults()
        let userType = defaults.valueForKey("usertype") as! NSInteger
        if(userType == 1){
            let drType = defaults.valueForKey("drType") as! String
            if(drType == "MedicinePrescribers"){
            }else{
                if(pending == true && btnInt == 1){
                profileTableVw.tableFooterView = nil
                }
            }
        }//else if(userType == 3 || userType == 0){
        else if(userType == 0){
            cancelSbBtn.hidden = true
        }
        
        
//        if(patientNotSub == true){
//            profileTableVw.tableFooterView = nil
//            cancelSbBtn.hidden = true
//        }
    }
    @IBAction func cancelSubscriptionAction(sender: AnyObject) {
        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        backVw.tag = 2369
        self.view.addSubview(backVw)
        
        let alerVw = UIView()
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
        }else{
            alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.2)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.2)
        }
        alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        alerVw.backgroundColor = UIColor.whiteColor()
        backVw.addSubview(alerVw)
        
        let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
        rightImg.image = UIImage(named: "verifyImg")
        alerVw.addSubview(rightImg)
        
        let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
        verificationLbl.text = " Alert"
        verificationLbl.font = UIFont.init(name: "Helvetica", size: self.view.frame.size.height * 0.03)
        verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(verificationLbl)
        
        let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
        lineVw.backgroundColor = UIColor.grayColor()
        alerVw.addSubview(lineVw)
        
        let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            contentLbl.font = UIFont.init(name: "Helvetica", size:14)
        }else{
            contentLbl.font = UIFont.init(name: "Helvetica", size:19)
        }
        contentLbl.textAlignment = .Center
        contentLbl.textColor = UIColor.grayColor()
        contentLbl.text = "Do You Really Want To cancel Subscription With this Patient?"
        contentLbl.numberOfLines = 2
        alerVw.addSubview(contentLbl)
        
        let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width/2, alerVw.frame.size.height*0.22))
        okLbl.setTitle("Yes", forState: .Normal)
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
        }else{
            okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
        }
        okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        okLbl.titleLabel?.textAlignment = .Center
        okLbl.addTarget(self, action: #selector(PateintProfileViewController.okBtnAc), forControlEvents: .TouchUpInside)
        okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(okLbl)
        let lineLbl = UILabel.init(frame: CGRectMake(okLbl.frame.size.width-2,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),1.2, alerVw.frame.size.height*0.22))
        lineLbl.backgroundColor = UIColor.lightGrayColor()
        alerVw.addSubview(lineLbl)
        
        let noLbl = UIButton.init(frame: CGRectMake(okLbl.frame.size.width,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width/2, alerVw.frame.size.height*0.22))
        noLbl.setTitle("No", forState: .Normal)
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            noLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
        }else{
            noLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
        }
        noLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        noLbl.titleLabel?.textAlignment = .Center
        noLbl.addTarget(self, action: #selector(PateintProfileViewController.noAc), forControlEvents: .TouchUpInside)
        noLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(noLbl)
    }
    func priscriptionAc(){
        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        backVw.tag = 2369
        self.view.addSubview(backVw)
        
        let alerVw = UIView()
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
        }else{
            alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.2)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.2)
        }
        alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        alerVw.backgroundColor = UIColor.whiteColor()
        backVw.addSubview(alerVw)
        
        let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
        rightImg.image = UIImage(named: "verifyImg")
        alerVw.addSubview(rightImg)
        
        let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
        verificationLbl.text = " Alert"
        verificationLbl.font = UIFont.init(name: "Helvetica", size: self.view.frame.size.height * 0.03)
        verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(verificationLbl)
        
        let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
        lineVw.backgroundColor = UIColor.grayColor()
        alerVw.addSubview(lineVw)
        
        let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            contentLbl.font = UIFont.init(name: "Helvetica", size:14)
        }else{
            contentLbl.font = UIFont.init(name: "Helvetica", size:19)
        }
        contentLbl.textAlignment = .Center
        contentLbl.textColor = UIColor.grayColor()
        contentLbl.text = "Do you need to write a prescription for your Patient?"
        contentLbl.numberOfLines = 2
        alerVw.addSubview(contentLbl)
        
        let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width/2, alerVw.frame.size.height*0.22))
        okLbl.setTitle("Yes", forState: .Normal)
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
        }else{
            okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
        }
        okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        okLbl.titleLabel?.textAlignment = .Center
        okLbl.addTarget(self, action: #selector(PateintProfileViewController.okBtnAction), forControlEvents: .TouchUpInside)
        okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(okLbl)
        let lineLbl = UILabel.init(frame: CGRectMake(okLbl.frame.size.width-2,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),1.2, alerVw.frame.size.height*0.22))
        lineLbl.backgroundColor = UIColor.lightGrayColor()
        alerVw.addSubview(lineLbl)
        
        let noLbl = UIButton.init(frame: CGRectMake(okLbl.frame.size.width,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width/2, alerVw.frame.size.height*0.22))
        noLbl.setTitle("No", forState: .Normal)
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            noLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
        }else{
            noLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
        }
        noLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        noLbl.titleLabel?.textAlignment = .Center
        noLbl.addTarget(self, action: #selector(PateintProfileViewController.noAc), forControlEvents: .TouchUpInside)
        noLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(noLbl)
    }
    func okBtnAction(){
        let vc = storyboard!.instantiateViewControllerWithIdentifier("WebViewController") as! WebViewController
        vc.prescribeBool = true
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setValue(urlPrescribe, forKey: "url")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func callAgain(){
        let defaults = NSUserDefaults.standardUserDefaults()
        let userId = defaults.valueForKey("id") as! String
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        let params = "DeviceToken=\(deviceTokn)&PatientId=\(ptId)&DrId=\(userId)&drschedulesetsId=\(schedule_Id)&BookingId=\(book_id)"
        serverInt = 3
        AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrVideo")
    }
    func okBtnAc(){
        
        
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let usertype = defaults.valueForKey("usertype"){
            if(usertype as! NSObject == 1){
                if let userId = defaults.valueForKey("id") {
                    let firstName = defaults.valueForKey("firstName") as! String
                    let lastName = defaults.valueForKey("lastName") as! String
                    let fullName = "\(firstName) \(lastName)"
                    AppManager.sharedManager.delegate=self
                    AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                    //let params = "Dr_id=\(userId)&PatientId=\(ptId)"
                    let params = "Dr_id=\(userId)&PatientId=\(ptId)&CancellationId=\(userId)&CancellationName=\(fullName)&CancellationUserType=\(usertype)&PatientName=\(name)"
                    serverInt = 2
                    AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrCencellationPatient")
                }
            }
            else if(usertype as! NSObject == 3){
                if let userId = defaults.valueForKey("staffId") {
                    let firstName = defaults.valueForKey("firstName") as! String
                    let lastName = defaults.valueForKey("lastName") as! String
                    let fullName = "\(firstName) \(lastName)"
                    let drId = defaults.valueForKey("id") as! NSString
                    AppManager.sharedManager.delegate=self
                    AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                    //let params = "Dr_id=\(drId)&PatientId=\(ptId)"
                    let params = "Dr_id=\(drId)&PatientId=\(ptId)&CancellationId=\(userId)&CancellationName=\(fullName)&CancellationUserType=\(usertype)&PatientName=\(name)"
                    serverInt = 2
                    AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrCencellationPatient")
                }
            }
        }

        
        
        
//        let defaults = NSUserDefaults.standardUserDefaults()
//        if let userId = defaults.valueForKey("id") {
//            
//            let firstName = defaults.valueForKey("firstName") as! String
//            let lastName = defaults.valueForKey("lastName") as! String
//            let fullName = "\(firstName) \(lastName)"
//            
//            
//            AppManager.sharedManager.delegate=self
//            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
//            //let params = "Dr_id=\(userId)&PatientId=\(ptId)"
//            
//            let params = "Dr_id=\(userId)&PatientId=\(ptId)&CancellationId=\(userId)&CancellationName=\(fullName)&CancellationUserType=\(usertype)&PatientName=\(name)"
//            
//            serverInt = 2
//            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrCencellationPatient")
//        }
    }
    func noAc(){
        let view = self.view.viewWithTag(2369)! as UIView
        view.removeFromSuperview()
    }
    func confirmBtnAc(){
        let defaults = NSUserDefaults.standardUserDefaults()
        let userType = defaults.valueForKey("usertype") as! NSObject
        let firstName = defaults.valueForKey("firstName") as! NSString
        let lastName = defaults.valueForKey("lastName") as! NSString
        let drId = defaults.valueForKey("id") as! String
        var createdId = NSString()
        if(userType == 1){
            let userId = defaults.valueForKey("id") as! String
            createdId = userId
        }else{
            let userId = defaults.valueForKey("staffId") as! String
            createdId = userId
        }
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
        let params = "AcceptReject_Id=\(acceptRejId)&Status=1&DeviceToken=\(deviceTkn)&Email=\(email)&CreateId=\(createdId)&CreateName=\(firstName) \(lastName)&CreateUserType=\(userType)&Dr_id=\(drId)&PatientName=\(self.name)"
        serverInt = 1
        AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRAcceptReject")
    }
    func deleBtnAc(){
        let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To Decline The Request??", preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            NSLog("OK Pressed")
            let defaults = NSUserDefaults.standardUserDefaults()
            let userType = defaults.valueForKey("usertype") as! NSObject
            let firstName = defaults.valueForKey("firstName") as! NSString
            let lastName = defaults.valueForKey("lastName") as! NSString
            let drId = defaults.valueForKey("id") as! String
            var createdId = NSString()
            if(userType == 1){
                let userId = defaults.valueForKey("id") as! String
                createdId = userId
            }else{
                let userId = defaults.valueForKey("staffId") as! String
                createdId = userId
            }
            AppManager.sharedManager.delegate=self
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
            let params = "AcceptReject_Id=\(self.acceptRejId)&Status=2&DeviceToken=\(self.deviceTkn)&Email=\(self.email)&CreateId=\(createdId)&CreateName=\(firstName) \(lastName)&CreateUserType=\(userType)&Dr_id=\(drId)&PatientName=\(self.name)"
            self.serverInt = 1
            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRAcceptReject")
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        dispatch_async(dispatch_get_main_queue()){
            if(self.serverInt == 1){
                let status = responseDict.valueForKey("data") as! NSString
                if(status == "Accepted"){
                    let alert = UIAlertController(title: "Alert", message: "Patient Confirmed Successfully.", preferredStyle: UIAlertControllerStyle.Alert)
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                        self.delegate?.refreshMyPatientsList(self.acceptRejId as String)
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                    alert.addAction(okAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }else{
                    let alert = UIAlertController(title: "Alert", message: "Patient Declined Successfully.", preferredStyle: UIAlertControllerStyle.Alert)
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                        self.delegate?.refreshMyPatientsList(self.acceptRejId as String)
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                    alert.addAction(okAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
            else if(self.serverInt == 3){
                dispatch_async(dispatch_get_main_queue()) {
                    let apiKey = responseDict.valueForKey("apikey") as! NSString
                    let token = responseDict.valueForKey("token") as! NSString
                    let session_id = responseDict.valueForKey("session") as! NSString
                    let status = responseDict.valueForKey("Status") as! NSString
                    let url = responseDict.valueForKey("Url") as! NSString
                    let inviteUrl = responseDict.valueForKey("inviteurl") as! NSString
                    let redirectUrl = responseDict.valueForKey("Redirecturl") as! NSString
                    let defaults = NSUserDefaults.standardUserDefaults()
                    defaults.setObject(status, forKey: "status")
                    defaults.setObject(url, forKey: "url")
                    var storyboard = UIStoryboard()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                    }else{
                        storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    }
                    let vc = storyboard.instantiateViewControllerWithIdentifier("ConferenceVideoViewController") as! ConferenceVideoViewController
                    vc.redirectUrl = redirectUrl
                    vc.patientId = self.ptId
                    vc.kApiKey = apiKey
                    vc.kSessionId = session_id
                    vc.kToken = token
                    vc.patName = self.name
                    vc.ptAdd = self.address
                    vc.schId =  self.schedule_Id
                    vc.bokId = self.book_id
                    vc.deviceToken = self.deviceTokn
                    vc.inviteUrl = inviteUrl
                    vc.groupId = responseDict.valueForKey("GroupId") as! String
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else{
                self.slideMenuController()!.navigationController?.popViewControllerAnimated(true)
            }
        }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        cell!.layer.cornerRadius = 7.0
        cell!.layer.masksToBounds = true
        
        let imageView: UIView = (cell?.viewWithTag(11))!
        imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
        imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
        imageView.layer.shadowOffset = CGSizeMake(10, 5);
        imageView.layer.shadowRadius = 1;
        imageView.layer.shadowOpacity = 0.06;
        imageView.layer.masksToBounds = false;
        imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        imageView.layer.borderWidth = 0.5
        if(indexPath.row == 3){
            let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(PateintProfileViewController.imageTap(_:)))
            imageView.userInteractionEnabled = true
            imageView.addGestureRecognizer(tapGestureRecognizer)
            
        }
        if(indexPath.row%2 == 0){
            imageView.backgroundColor = UIColor.whiteColor()
        }else{
            imageView.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
        }
        
        let itemImg: UIImageView = (cell!.viewWithTag(12) as! UIImageView)
        itemImg.image = UIImage(named: imgAr[indexPath.row])!
        let valueLbl: UILabel = (cell!.viewWithTag(14) as! UILabel)
        valueLbl.text = valueAr[indexPath.row] as? String
        
        let itemLbl: UILabel = (cell!.viewWithTag(13) as! UILabel)
        itemLbl.text = itemAr[indexPath.row]
        return cell!
        
    }
    
    func imageTap(img: AnyObject){
        let alert = UIAlertController(title: "Please confirm your call", message: "You’re about to call the following:\n\(phone)", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            if let url = NSURL(string: "tel://\(self.phone)") {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            return self.view.frame.size.width*0.2
        }else{
            return self.view.frame.size.width*0.13
        }
    }
    
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
