

import UIKit
import SlideMenuControllerSwift
import KMPlaceholderTextView
import IQKeyboardManagerSwift
class DoctorRatingViewController: UIViewController,UITextViewDelegate,WebServiceDelegate {

    @IBOutlet weak var starRatingVw: FloatRatingView!
    @IBOutlet weak var feedBackVw: UIView!
    @IBOutlet weak var starVw: UIView!
    @IBOutlet weak var docRatingImgVw: UIImageView!
    @IBOutlet weak var feedbackTxtVw: KMPlaceholderTextView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var docNameLbl: UILabel!
    @IBOutlet weak var docSpecialityLbl: UILabel!
    var drDetailDict = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        feedbackTxtVw.tintColor = UIColor.blackColor()
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().enable = true
        let firstname:String = drDetailDict.valueForKey("FirstName") as! String
        let lastname:String = drDetailDict.valueForKey("LastName") as! String
        let speciality:String = drDetailDict.valueForKey("Speciality") as! String
        let picUrl:String = drDetailDict.valueForKey("ProfilePic") as! String
        if let url = NSURL(string: "http://\(picUrl)") {
            docRatingImgVw.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: "DocIcon")
        let offsetY: CGFloat = -6.0
        attachment.bounds = CGRectMake(-10, offsetY, attachment.image!.size.width, attachment.image!.size.height)
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string:"")
        myString.appendAttributedString(attachmentString)
        let myString1 = NSMutableAttributedString(string:"\(firstname) \(lastname)" as String)
        myString.appendAttributedString(myString1)
        docNameLbl.textAlignment = .Center
        docNameLbl.attributedText = myString
        docSpecialityLbl.text = speciality
        docRatingImgVw.layer.cornerRadius = docRatingImgVw.frame.size.height/2
        docRatingImgVw.layer.borderWidth = docRatingImgVw.frame.size.height*0.07
        docRatingImgVw.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.9).CGColor
        docRatingImgVw.clipsToBounds = true
        self.starRatingVw.emptyImage = UIImage(named: "empty")
        self.starRatingVw.fullImage   = UIImage(named: "full")
        // Optional params
        self.starRatingVw.contentMode  = UIViewContentMode.ScaleAspectFit
        self.starRatingVw.maxRating    = 5
        self.starRatingVw.minRating    = 1
        self.starRatingVw.editable     = true
        self.starRatingVw.halfRatings  = false
        self.starRatingVw.floatRatings = false
        
        submitBtn.enabled = false
        
        feedbackTxtVw.delegate = self
        
        starVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        starVw.layer.borderColor   = UIColor.grayColor().CGColor
        starVw.layer.shadowOffset  = CGSizeMake(5, 5);
        starVw.layer.shadowRadius  = 0.7;
        starVw.layer.shadowOpacity = 0.06;
        starVw.layer.masksToBounds = false;
        starVw.layer.shadowColor   = UIColor.lightGrayColor().CGColor
        starVw.layer.borderWidth   = 0.5
        starVw.backgroundColor     = UIColor.whiteColor()
        
        feedBackVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        feedBackVw.layer.borderColor   = UIColor.grayColor().CGColor
        feedBackVw.layer.shadowOffset  = CGSizeMake(5, 5);
        feedBackVw.layer.shadowRadius  = 0.7;
        feedBackVw.layer.shadowOpacity = 0.06;
        feedBackVw.layer.masksToBounds = false;
        feedBackVw.layer.shadowColor   = UIColor.lightGrayColor().CGColor
        feedBackVw.layer.borderWidth   = 0.5
        feedBackVw.backgroundColor     = UIColor.clearColor()
    }
    func textViewDidEndEditing(textView: UITextView) {
        let trimNameString = feedbackTxtVw.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if(trimNameString != ""){
            submitBtn.enabled = true
        }
        else{
            submitBtn.enabled = false
        }
    }
    @IBAction func sbmitBtnAc(sender: AnyObject) {
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loadng...")
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id"){
         let drId = drDetailDict.valueForKey("_id") as! NSString
        let params = "From=\(userId)&To=\(drId)&Rating=\(self.starRatingVw.rating)&Feedback=\(feedbackTxtVw.text!)"
        AppManager.sharedManager.postDataOnserver(params, postUrl: "users/Rating")
        }
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        dispatch_async(dispatch_get_main_queue()) {
        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        
        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        backVw.tag = 1801
        self.view.addSubview(backVw)
         let alerVw = UIView()
         if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
             alerVw.frame = CGRectMake(self.view.frame.size.width*0.07,self.view.frame.size.height/2-((self.view.frame.size.height*0.28)/2), self.view.frame.size.width-(self.view.frame.size.width*0.14), self.view.frame.size.height*0.28)
         }else{
             alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.2)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.2)
         }
        alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        alerVw.backgroundColor = UIColor.whiteColor()
        backVw.addSubview(alerVw)
        
        let ThankyouLbl = UILabel.init(frame: CGRectMake(0,0, alerVw.frame.size.width, alerVw.frame.size.height*0.2))
        ThankyouLbl.text = "Thank you!"
        ThankyouLbl.font = UIFont(name: "Helvetica Bold", size: 20)
        ThankyouLbl.textColor = UIColor.whiteColor()
        ThankyouLbl.textAlignment = .Center
        ThankyouLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(ThankyouLbl)
        
        let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,ThankyouLbl.frame.size.height+15, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.38))
        contentLbl.font = UIFont(name: "Helvetica", size:14)
        contentLbl.textAlignment = .Center
        contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "For scheduling a Concierge Telemedicine Visit with your Doctor."
        contentLbl.numberOfLines = 3
        alerVw.addSubview(contentLbl)
        
        let contentLbl2 = UILabel.init(frame: CGRectMake(0,ThankyouLbl.frame.size.height+15+contentLbl.frame.size.height, alerVw.frame.size.width,alerVw.frame.size.height*0.1))
        contentLbl2.font = UIFont(name: "Helvetica", size:15)
        contentLbl2.textAlignment = .Center
        contentLbl2.textColor = UIColor.init(red: 35/255, green: 208/255, blue: 179/255, alpha: 1)
        contentLbl2.text = "See you next time!"
        alerVw.addSubview(contentLbl2)
        
        let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.2))
        okLbl.setTitle("OK", forState: .Normal)
        okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        okLbl.titleLabel?.textAlignment = .Center
        okLbl.addTarget(self, action: #selector(DoctorRatingViewController.okBtnAc), forControlEvents: .TouchUpInside)
        okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(okLbl)
     }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    func okBtnAc(){
       // let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let vc = storyboard!.instantiateViewControllerWithIdentifier("PatientHomeViewController") as! PatientHomeViewController 
        let nav = UINavigationController(rootViewController: vc)
        appDelegate.window?.rootViewController = nav
        appDelegate.window?.makeKeyAndVisible()
    }
      override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
}
