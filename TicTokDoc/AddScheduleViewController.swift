
import UIKit
import SlideMenuControllerSwift
import IQKeyboardManagerSwift
class AddScheduleViewController: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,WebServiceDelegate{
    @IBOutlet weak var toVw: UIView!
    @IBOutlet weak var dateVw: UIView!
    @IBOutlet weak var fromVw: UIView!
    @IBOutlet weak var txtFldTo: UITextField!
    @IBOutlet weak var txtFldDate: UITextField!
    @IBOutlet weak var txtFldFrom: UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var fromLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var toLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    var doneTrue   = Bool()
    var fifteenMinS = NSString()
    var thirtyMinS = NSString()
    var sixtyMinS = NSString()
    var NinetyMinS = NSString()
    var timeGap = NSInteger()
    var appendString1 = NSString()
    var dateY      = String()
    var dateM      = String()
    var dateD      = String()
    var timeF      = String()
    var timeF12Hr  = String()
    var timeT      = String()
    var timeT12Hr  = String()
    var timeFrom   = NSDate()
    var timeTo     = NSDate()
    var checkBox   = Bool()
    var int        = NSInteger()
    var toS        = [String]()
    var fromS      = [String]()
    var dateS      = [String]()
    var toInt      = NSInteger()
    var fromInt    = NSInteger()
    var date = NSDate()
    var meetingAr = NSMutableArray()
    var schedule = Bool()
    var currentTime = NSDate()
    var timeAr =
 ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
    var toTimeAr = NSMutableArray()
    var toTimeAr12Hr = NSMutableArray()
    var toTimeArr = ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
    
    var toTimeArr12Hr=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"]
    
    var timeAr12Hr=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"]
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        timeGap = 15
        txtFldTo.delegate    = self
        txtFldDate.delegate  = self
        txtFldFrom.delegate  = self
        
        txtFldTo.tintColor     = UIColor.clearColor()
        txtFldDate.tintColor   = UIColor.clearColor()
        txtFldFrom.tintColor   = UIColor.clearColor()
        
        txtFldTo.userInteractionEnabled    = true
        txtFldDate.userInteractionEnabled  = true
        txtFldFrom.userInteractionEnabled  = true
        
        if(txtFldTo.text == ""  && txtFldDate.text == "" && txtFldFrom.text == ""){
            saveBtn.enabled = false
        }
        
        if(doneTrue == true){
            backBtn.hidden = true
        }
        dateVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        dateVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        dateVw.layer.shadowOffset = CGSizeMake(10, 5);
        dateVw.layer.shadowRadius = 1;
        dateVw.layer.shadowOpacity = 0.02;
        dateVw.layer.masksToBounds = false;
        dateVw.layer.shadowColor = UIColor.lightGrayColor().CGColor
        dateVw.layer.borderWidth = 0.5
        
        fromVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        fromVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        fromVw.layer.shadowOffset = CGSizeMake(10, 5);
        fromVw.layer.shadowRadius = 1;
        fromVw.layer.shadowOpacity = 0.02;
        fromVw.layer.masksToBounds = false
        fromVw.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
        fromVw.layer.shadowColor = UIColor.lightGrayColor().CGColor
        fromVw.layer.borderWidth = 0.5
        
        toVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        toVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        toVw.layer.shadowOffset = CGSizeMake(10, 5);
        toVw.layer.shadowRadius = 1;
        toVw.layer.shadowOpacity = 0.02;
        toVw.layer.masksToBounds = false;
        toVw.layer.shadowColor = UIColor.lightGrayColor().CGColor
        toVw.layer.borderWidth = 0.5
       
    }
    override func viewWillAppear(animated: Bool) {
        self.tabBarController!.tabBar.userInteractionEnabled = false;
        let dateInputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0.4))
        let datePickerView  : UIDatePicker = UIDatePicker(frame: CGRectMake(0, 40,UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height*0.36))
        datePickerView.datePickerMode = UIDatePickerMode.Date
        dateInputView.addSubview(datePickerView)
        txtFldDate.inputView = dateInputView
        let yesterday = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 0, toDate: NSDate(), options: [])!
        datePickerView.minimumDate = yesterday
        datePickerView.addTarget(self, action: #selector(AddScheduleViewController.handlePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        let doneButton = UIButton(frame: CGRectMake((self.view.frame.size.width-100), 0, 100, 50))
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        doneButton.setTitle("Done", forState: UIControlState.Highlighted)
        doneButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        doneButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        dateInputView.addSubview(doneButton)
        doneButton.addTarget(self, action: #selector(AddScheduleViewController.doneButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let cancelBtn = UIButton(frame: CGRectMake(0, 0, 100, 50))
        cancelBtn.setTitle("Cancel", forState: UIControlState.Normal)
        cancelBtn.setTitle("Cancel", forState: UIControlState.Highlighted)
        cancelBtn.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        cancelBtn.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        dateInputView.addSubview(cancelBtn)
        cancelBtn.addTarget(self, action: #selector(AddScheduleViewController.cancelBtn(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        handlePicker(datePickerView)
        txtFldTo.text = ""
        txtFldFrom.text = ""
        for i in 1500...1503 {
            let box = self.view.viewWithTag(i) as! UIButton
            box.setImage(UIImage(named: "emptyCheckBox"), forState: .Normal)
        }
        saveBtn.enabled = false
        fromLbl.text = ""
        toLbl.text = ""
        timeAr = ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
        toTimeArr = ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
        toTimeArr12Hr=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"]
        
         timeAr12Hr=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"]
        
    }
    func handlePicker(sender: UIDatePicker){
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd,yyyy"
        dateD = dateFormatter.stringFromDate(sender.date)
        let todayDate = NSDate()
        timeAr = ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
        toTimeArr = ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
        toTimeArr12Hr=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"]
        
         timeAr12Hr=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"]
        txtFldDate.text = dateD as String
        appendString1 = dateD
        date = sender.date
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: date)
        let year =  components.year
        let month = components.month
        let day = components.day
        dateM = "\(month)"
        dateY = "\(year)"
        dateD = "\(day)"
        if(dateM == "1"){
            dateM = "January"
        }else if(dateM == "2"){
            dateM = "February"
        }else if(dateM == "3"){
            dateM = "March"
        }else if(dateM == "4"){
            dateM = "April"
        }else if(dateM == "5"){
            dateM = "May"
        }else if(dateM == "6"){
            dateM = "June"
        }else if(dateM == "7"){
            dateM = "July"
        }else if(dateM == "8"){
            dateM = "August"
        }else if(dateM == "9"){
            dateM = "September"
        }else if(dateM == "10"){
            dateM = "October"
        }else if(dateM == "11"){
            dateM = "November"
        }else if(dateM == "12"){
            dateM = "December"
        }
        if(dateM != "" && dateD != "" && dateY != ""){
            dateLbl.text = appendString1 as String
            let selectDate = "\(dateD)-\(dateM)-\(dateY)"
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("id") {
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                let params = "ScheduleDate=\(selectDate)&Dr_Id=\(userId)"
                schedule = false
                AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrDate")
            }
        }
    }
    @IBAction func backBtnAc(sender: AnyObject){
        if(doneTrue == false){
            self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
       }
      }
    func textFieldDidBeginEditing(textField: UITextField) {
            if(textField.tag == 2304){
                int = 1
                if(txtFldDate.text == ""){
                    let InputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0))
                    InputView.backgroundColor = UIColor.clearColor()
                    textField.inputView = InputView
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select Date.")
                }else{
                 txtFldTo.text = ""
                 toTimeAr.removeAllObjects()
                toTimeAr12Hr.removeAllObjects()
                let InputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0.4))
                let PickerVw  : UIPickerView = UIPickerView(frame: CGRectMake(0, 40,UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height*0.32))
                PickerVw.delegate            = self
                PickerVw.dataSource          = self
                InputView.addSubview(PickerVw)
                
                let doneButton = UIButton(frame: CGRectMake((self.view.frame.size.width-100), 0, 100, 50))
                doneButton.setTitle("Done", forState: UIControlState.Normal)
                doneButton.setTitle("Done", forState: UIControlState.Highlighted)
                doneButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
                doneButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
                InputView.addSubview(doneButton)
                doneButton.addTarget(self, action: #selector(AddScheduleViewController.doneButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                
                let cancelBtn = UIButton(frame: CGRectMake(0, 0, 100, 50))
                cancelBtn.setTitle("Cancel", forState: UIControlState.Normal)
                cancelBtn.setTitle("Cancel", forState: UIControlState.Highlighted)
                cancelBtn.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
                cancelBtn.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
                InputView.addSubview(cancelBtn)
                cancelBtn.addTarget(self, action: #selector(AddScheduleViewController.cancelBtn(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                textField.inputView = InputView
                }
            }else if(textField.tag == 2305){
                 if(txtFldDate.text == ""){
                    let InputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0))
                    InputView.backgroundColor = UIColor.clearColor()
                    textField.inputView = InputView
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select Date.")
                }
                 else if(txtFldFrom.text == ""){
                    let InputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0))
                    InputView.backgroundColor = UIColor.clearColor()
                    textField.inputView = InputView
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Please fill the Start time.")
                 }
                else{
                 int = 2
                    if(toTimeAr12Hr.count != 0){
                    let InputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0.4))
                    let PickerVw  : UIPickerView = UIPickerView(frame: CGRectMake(0, 40,UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height*0.32))

                    PickerVw.delegate            = self
                    PickerVw.dataSource          = self
                    InputView.addSubview(PickerVw)
                    
                    let doneButton = UIButton(frame: CGRectMake((self.view.frame.size.width-100), 0, 100, 50))
                    doneButton.setTitle("Done", forState: UIControlState.Normal)
                    doneButton.setTitle("Done", forState: UIControlState.Highlighted)
                    doneButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
                    doneButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
                    InputView.addSubview(doneButton)
                    doneButton.addTarget(self, action: #selector(AddScheduleViewController.doneButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                    
                    let cancelBtn = UIButton(frame: CGRectMake(0, 0, 100, 50))
                    cancelBtn.setTitle("Cancel", forState: UIControlState.Normal)
                    cancelBtn.setTitle("Cancel", forState: UIControlState.Highlighted)
                    cancelBtn.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
                    cancelBtn.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
                    InputView.addSubview(cancelBtn)
                    cancelBtn.addTarget(self, action: #selector(AddScheduleViewController.cancelBtn(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                    textField.inputView = InputView
                        let getNewTimeAr = NSMutableArray()
                        var getTime24 = timeF
                        if(getTime24 != "23:45"){
                            for a in 0...toTimeArr.count-1 {
                                let dateFormatter = NSDateFormatter()
                                dateFormatter.dateFormat = "HH:mm"
                                let timeZone = NSTimeZone(name: "UTC")
                                dateFormatter.timeZone=timeZone
                                let timestring = dateFormatter.dateFromString(getTime24)
                                let timeWant = timestring!.dateByAddingTimeInterval(15*(60))
                                let arTime:String = dateFormatter.stringFromDate(timeWant)
                                getTime24 = arTime
                                if(!(toTimeArr.contains(arTime))){
                                    toTimeAr = getNewTimeAr
                                    return
                                }
                                else{
                                    getNewTimeAr.addObject(arTime)
                                    if(arTime == "23:45"){
                                        toTimeAr = getNewTimeAr
                                        return
                                    }
                                }
                            }
                        }
                 }
                    else{
                        let InputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0))
                        InputView.backgroundColor = UIColor.clearColor()
                        textField.inputView = InputView
                        AppManager.sharedManager.Showalert("Alert", alertmessage: "There is No Further Slot, Please Select Some Other from time.")
                    }
                }
            }
            else{
                int = 0
                self.timeF = ""
                self.timeT = ""
                txtFldFrom.text = ""
                txtFldTo.text = ""
                timeAr = ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
                toTimeArr = ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
                 toTimeArr12Hr=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"]
                
                 timeAr12Hr=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"]
                let dateInputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0.4))
                let datePickerView  : UIDatePicker = UIDatePicker(frame: CGRectMake(0, 40,UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height*0.36))
                datePickerView.datePickerMode = UIDatePickerMode.Date
                dateInputView.addSubview(datePickerView)
                txtFldDate.inputView = dateInputView
                let yesterday = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 0, toDate: NSDate(), options: [])!
                datePickerView.minimumDate = yesterday
                datePickerView.addTarget(self, action: #selector(AddScheduleViewController.handlePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
                let doneButton = UIButton(frame: CGRectMake((self.view.frame.size.width-100), 0, 100, 50))
                doneButton.setTitle("Done", forState: UIControlState.Normal)
                doneButton.setTitle("Done", forState: UIControlState.Highlighted)
                doneButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
                doneButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
                dateInputView.addSubview(doneButton)
                doneButton.addTarget(self, action: #selector(AddScheduleViewController.doneButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                
                let cancelBtn = UIButton(frame: CGRectMake(0, 0, 100, 50))
                cancelBtn.setTitle("Cancel", forState: UIControlState.Normal)
                cancelBtn.setTitle("Cancel", forState: UIControlState.Highlighted)
                cancelBtn.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
                cancelBtn.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
                dateInputView.addSubview(cancelBtn)
                cancelBtn.addTarget(self, action: #selector(AddScheduleViewController.cancelBtn(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                handlePicker(datePickerView)
        }
        
        if(txtFldTo.text != ""  && txtFldDate.text != ""  && txtFldFrom.text != "" && (fifteenMinS != "" || thirtyMinS != "" || sixtyMinS != "" || NinetyMinS != "")){
            saveBtn.enabled = true
        }
        else{
            saveBtn.enabled = false
        }
    }
    func handleTimePicker(sender: UIDatePicker){
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        if (int == 1) {
            timeF = dateFormatter.stringFromDate(sender.date)
            timeFrom = sender.date
        }else if(int == 2){
            timeT = dateFormatter.stringFromDate(sender.date)
            timeTo = sender.date
        }
        
    }
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(txtFldFrom.isFirstResponder()){
            return timeAr12Hr.count
        }
        else if(txtFldTo.isFirstResponder()){
            return toTimeAr12Hr.count
        }
        return 0
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(txtFldFrom.isFirstResponder()){
            return timeAr12Hr[row]
        }
        else if(txtFldTo.isFirstResponder()){
            return toTimeAr12Hr[row] as? String
        }
        else{
            return nil
        }
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if(txtFldFrom.isFirstResponder()){
           timeF12Hr = timeAr12Hr[row]
            timeF = timeAr[row]
        }
        else if(txtFldTo.isFirstResponder()){
            timeT12Hr = toTimeAr12Hr[row] as! String
            timeT = toTimeAr[row] as! String
        }
    }
    func alert(){
        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        backVw.tag = 2359
        self.view.addSubview(backVw)
        let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.19))
        alerVw.backgroundColor = UIColor.whiteColor()
        backVw.addSubview(alerVw)
        
        let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
        rightImg.image = UIImage(named: "verifyImg")
        alerVw.addSubview(rightImg)
        
        let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
        verificationLbl.text = " Alert"
        verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.02)
        verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(verificationLbl)
        
        let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
        lineVw.backgroundColor = UIColor.grayColor()
        alerVw.addSubview(lineVw)
        
        let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
        contentLbl.font = UIFont(name: "Helvetica", size:14)
        contentLbl.textAlignment = .Center
        contentLbl.textColor = UIColor.grayColor()
        contentLbl.text = "'From Time' can't be earlier than 'To Time'."
        contentLbl.numberOfLines = 2
        alerVw.addSubview(contentLbl)
        
        let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.21))
        okLbl.setTitle("OK", forState: .Normal)
        okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        okLbl.titleLabel?.textAlignment = .Center
        okLbl.addTarget(self, action: #selector(AddScheduleViewController.okBtnAc2), forControlEvents: .TouchUpInside)
        okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(okLbl)
        
    }
    func okBtnAc2(){
        if(txtFldTo.text != "" && txtFldDate.text != ""  && txtFldFrom.text != "" && (fifteenMinS != "" || thirtyMinS != "" || sixtyMinS != "" || NinetyMinS != "")){
            saveBtn.enabled = true
        }
        else{
            saveBtn.enabled = false
        }
        if(txtFldFrom.isFirstResponder()){
            timeF = ""
        }else if(txtFldTo.isFirstResponder()){
            timeT = ""
        }
        let view = self.view.viewWithTag(2359)! as UIView
        view.removeFromSuperview()
    }
    func doneButton(sender:UIButton)
    {
        if(txtFldFrom.isFirstResponder()){
            if(timeF == ""){
            timeF12Hr = timeAr12Hr[0]
            timeF = timeAr[0]
            }
        }else if(txtFldTo.isFirstResponder()){
            if(timeT == ""){
                timeT12Hr = toTimeAr12Hr[0] as! String
                timeT = toTimeAr[0] as! String
            }
        }
        
        if(appendString1 != ""){
        dateLbl.text = appendString1 as String
        }
        if(timeF != "" && timeT != ""){
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .ShortStyle
        dateFormatter.dateFormat = "HH:mm" // k = Hour in 1~24, mm = Minute
        let timeZone = NSTimeZone(name: "UTC")
        dateFormatter.timeZone=timeZone
         timeTo = dateFormatter.dateFromString("\(timeT)")!
         timeFrom = dateFormatter.dateFromString("\(timeF)")!
        }
        txtFldDate.resignFirstResponder()
        txtFldTo.resignFirstResponder()
        txtFldFrom.resignFirstResponder()
        if(int == 1){
                txtFldFrom.text = timeF12Hr
                fromLbl.text = timeF12Hr
                txtFldFrom.resignFirstResponder()
             let getNewTimeAr = NSMutableArray()
             var getTime24 = timeF
            if(getTime24 != "23:45"){
                for a in 0...toTimeArr.count-1 {
                    let dateFormatter1 = NSDateFormatter()
                    dateFormatter1.dateFormat = "HH:mm"
                    let timeZone = NSTimeZone(name: "UTC")
                    dateFormatter1.timeZone=timeZone
                    let timestring = dateFormatter1.dateFromString(getTime24)
                    let timeWant = timestring!.dateByAddingTimeInterval(15*(60))
                    let arTime:String = dateFormatter1.stringFromDate(timeWant)
                    getTime24 = arTime
                    if(!(toTimeArr.contains(arTime))){
                        toTimeAr = getNewTimeAr
                        return
                    }
                    else{
                        getNewTimeAr.addObject(arTime)
                        if(arTime == "23:45"){
                            toTimeAr = getNewTimeAr
                            return
                        }
                    }
                }
            }
        }
        else if(int == 2){
            if(timeT != ""){
            if(txtFldFrom.text != ""){
                switch timeTo.compare(timeFrom) {
                case .OrderedAscending     :
                    print("TimeTo is small than TimeFrom")
                    self.alert()
                    txtFldTo.resignFirstResponder()
                    
                case .OrderedDescending
                    :   print("timeto is greater than date timefrom")
                    let calendar = NSCalendar.currentCalendar()
                    let datecomponenets = calendar.components(NSCalendarUnit.Minute, fromDate: timeFrom, toDate: timeTo, options: NSCalendarOptions.MatchLast)
                    let min = datecomponenets.minute
                    if(min >= timeGap){
                        txtFldTo.text = timeT12Hr
                        toLbl.text = timeT12Hr
                        txtFldTo.resignFirstResponder()
                    }else{
                        txtFldTo.text = ""
                        toLbl.text  = ""
                        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                        backVw.tag = 2359
                        self.view.addSubview(backVw)
                        let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.22))
                        alerVw.backgroundColor = UIColor.whiteColor()
                        backVw.addSubview(alerVw)
                        
                        let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                        rightImg.image = UIImage(named: "verifyImg")
                        alerVw.addSubview(rightImg)
                        
                        let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                        verificationLbl.text = " Alert"
                        verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.02)
                        verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(verificationLbl)
                        
                        let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                        lineVw.backgroundColor = UIColor.grayColor()
                        alerVw.addSubview(lineVw)
                        
                        let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+20, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.5))
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            contentLbl.font  = UIFont(name: "Helvetica", size:14)
                            
                        }else{
                            contentLbl.font  = UIFont(name: "Helvetica", size:19)
                        }
                        contentLbl.font = UIFont(name: "Helvetica", size:14)
                        contentLbl.textAlignment = .Center
                        contentLbl.textColor = UIColor.grayColor()
                        contentLbl.text = "The gap btween 'From time' and 'To time' should be greater than or equal to \(timeGap) minutes "
                        timeT = ""
                        contentLbl.numberOfLines = 3
                        alerVw.addSubview(contentLbl)
                        
                        let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.21))
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                            okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                        }else{
                            okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                        }
                        okLbl.setTitle("OK", forState: .Normal)
                        okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                        okLbl.titleLabel?.textAlignment = .Center
                        okLbl.addTarget(self, action: #selector(AddScheduleViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                        okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(okLbl)
                        txtFldTo.resignFirstResponder()
                    }
                    
                case .OrderedSame          :
                    print("The two dates are the same")
                }
            }else{
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please fill the Start time.")
            }
        }
        }
    }
   func cancelBtn(sender:UIButton){
        txtFldDate.resignFirstResponder()
        txtFldFrom.resignFirstResponder()
        txtFldTo.resignFirstResponder()
    }
    @IBAction func checkBoxBtnAction(sender: AnyObject){
        if(sender.currentImage == UIImage(named: "emptyCheckBox")){
            sender.setImage(UIImage(named: "checked"), forState: .Normal)
            if(sender.tag == 1500){
                fifteenMinS = "15 Min"
                meetingAr.addObject(fifteenMinS)
            }
            if(sender.tag == 1501){
                thirtyMinS = "30 Min"
                meetingAr.addObject(thirtyMinS)
            }
            if(sender.tag == 1502){
                sixtyMinS = "60 Min"
                meetingAr.addObject(sixtyMinS)
            }
            if(sender.tag == 1503){
                NinetyMinS = "90 Min"
                meetingAr.addObject(NinetyMinS)
            }
        }
        else if(sender.currentImage == UIImage(named: "checked")){
            sender.setImage(UIImage(named: "emptyCheckBox"), forState: .Normal)
            if(sender.tag == 1500){
                meetingAr.removeObject(fifteenMinS)
                fifteenMinS = ""
            }
            if(sender.tag == 1501){
                meetingAr.removeObject(thirtyMinS)
                thirtyMinS = ""
            }
            if(sender.tag == 1502){
                meetingAr.removeObject(sixtyMinS)
                sixtyMinS = ""
            }
            if(sender.tag == 1503){
                meetingAr.removeObject(NinetyMinS)
                NinetyMinS = ""
            }
        }
        if(fifteenMinS != ""){
            timeGap = 15
        }
        if(thirtyMinS != ""){
            timeGap = 30
        }
        if(sixtyMinS != ""){
            timeGap = 60
        }
        if(NinetyMinS != ""){
            timeGap = 90
        }
        if(fifteenMinS == "" && thirtyMinS == "" && sixtyMinS == "" && NinetyMinS == ""){
            timeGap = 15
        }
        
      if(timeT != ""){
        if(int == 1){
            if(txtFldTo.text != ""){

            }
            else{
                txtFldFrom.text = timeF12Hr
                fromLbl.text = timeF12Hr
                txtFldFrom.resignFirstResponder()
            }
        }
        else if(int == 2){
            if(txtFldFrom.text != ""){
                switch timeTo.compare(timeFrom) {
                case .OrderedAscending     :
                    print("TimeTo is small than TimeFrom")
                    self.alert()
                    txtFldTo.resignFirstResponder()
                    
                case .OrderedDescending
                    :   print("timeto is greater than date timefrom")
                    let calendar = NSCalendar.currentCalendar()
                    let datecomponenets = calendar.components(NSCalendarUnit.Minute, fromDate: timeFrom, toDate: timeTo, options: NSCalendarOptions.MatchLast)
                    let min = datecomponenets.minute
                    if(min >= timeGap){
                        txtFldTo.text = timeT12Hr
                        toLbl.text = timeT12Hr
                        txtFldTo.resignFirstResponder()
                    }else{
                        txtFldTo.text = ""
                         toLbl.text  = ""
                        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                        backVw.tag = 2359
                        self.view.addSubview(backVw)
                        let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.22))
                        alerVw.backgroundColor = UIColor.whiteColor()
                        backVw.addSubview(alerVw)
                        
                        let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                        rightImg.image = UIImage(named: "verifyImg")
                        alerVw.addSubview(rightImg)
                        
                        let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                        verificationLbl.text = " Alert"
                        verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.02)
                        verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(verificationLbl)
                        
                        let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                        lineVw.backgroundColor = UIColor.grayColor()
                        alerVw.addSubview(lineVw)
                        
                        let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+20, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.5))
                        contentLbl.font = UIFont(name: "Helvetica", size:14)
                        contentLbl.textAlignment = .Center
                        contentLbl.textColor = UIColor.grayColor()
                        contentLbl.text = "The gap between 'From time' and 'To time' should be greater than or equal to \(timeGap) minutes "
                        timeT = ""
                        contentLbl.numberOfLines = 3
                        alerVw.addSubview(contentLbl)
                        
                        let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.21))
                        okLbl.setTitle("OK", forState: .Normal)
                        okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                        okLbl.titleLabel?.textAlignment = .Center
                        okLbl.addTarget(self, action: #selector(AddScheduleViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                        okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(okLbl)
                        txtFldTo.resignFirstResponder()
                    }
                    
                case .OrderedSame          :
                    print("The two dates are the same")
                    txtFldTo.resignFirstResponder()
                }
            }else{
                txtFldTo.text = timeT12Hr
                toLbl.text = timeT12Hr
                txtFldTo.resignFirstResponder()
            }
         }
        }
        if(txtFldTo.text != ""  && txtFldDate.text != ""  && txtFldFrom.text != "" && (fifteenMinS != "" || thirtyMinS != "" || sixtyMinS != "" || NinetyMinS != "")){
            saveBtn.enabled = true
        }
        else{
            saveBtn.enabled = false
        }

      }
    func textFieldDidEndEditing(textField: UITextField) {
         if(textField == txtFldTo){
            if(timeT != ""  && txtFldDate.text != "" && txtFldFrom.text != "" && (fifteenMinS != "" || thirtyMinS != "" || sixtyMinS != "" || NinetyMinS != "" )){
                saveBtn.enabled = true
            }
           }
         else if(textField == txtFldFrom){
                saveBtn.enabled = false
            
            let getNewTimeAr12Hr = NSMutableArray()
            if(timeF12Hr != ""){
            var getTime = timeF12Hr
           
            if(getTime != "11:45 PM"){
                for a in 0...toTimeArr12Hr.count-1 {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    let timeZone = NSTimeZone(name: "UTC")
                    dateFormatter.timeZone=timeZone
                    let timestring = dateFormatter.dateFromString(getTime)
                    let timeWant = timestring!.dateByAddingTimeInterval(15*(60))
                    let arTime:String = dateFormatter.stringFromDate(timeWant)
                    getTime = arTime
                    if(!(toTimeArr12Hr.contains(arTime))){
                       toTimeAr12Hr = getNewTimeAr12Hr
                        return
                    }
                    else{
                        getNewTimeAr12Hr.addObject(arTime)
                        if(arTime == "11:45 PM"){
                            toTimeAr12Hr = getNewTimeAr12Hr
                            return
                      }
                    }
                  }
                }
              }
            }
           else{
           saveBtn.enabled = false
           }
        }
    @IBAction func saveBtnAction(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd,yyyy"
        let timeZone = NSTimeZone(name: "UTC")
        dateFormatter.timeZone=timeZone
        let schDate = dateFormatter.dateFromString("\(appendString1)")!
        let dateC = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "MMMM dd,yyyy"
        let defaultTimeZoneStr = formatter.stringFromDate(dateC)
        let currentD1 = dateFormatter.dateFromString("\(defaultTimeZoneStr)")!
        switch schDate.compare(currentD1) {
        case .OrderedAscending     :
            print("schDate is small than currentD1")
        case .OrderedDescending
            :   print("schDate is greater than date currentD1")
        case .OrderedSame          :
            print("The two dates are the same")
             let timestamp1 = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .NoStyle, timeStyle: .ShortStyle)
            dateFormatter.timeStyle = .ShortStyle
           
            let fmt = NSDateFormatter.dateFormatFromTemplate("jm", options: 0, locale: NSLocale.currentLocale())!
             dateFormatter.dateFormat = "\(fmt)"
            let currentT:NSDate = dateFormatter.dateFromString("\(timestamp1)")!
            dateFormatter.dateFormat = "HH:mm"
            let date12 = dateFormatter.dateFromString(timeF as String)!
            dateFormatter.dateFormat = "hh:mm a"
            let date22 = dateFormatter.stringFromDate(date12)
            let schT:NSDate = dateFormatter.dateFromString("\(date22)")!
            switch schT.compare(currentT) {
            case .OrderedAscending     :
                print("schT is small than currentT")
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select a time greater than the Current time.")
                return
            case .OrderedDescending
                :   print("schT is greater than currentT")
            case .OrderedSame          :
                print("The two dates are the same")
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Please select a time greater than the Current time.")
                return
            }
            
        }
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
        if(dateM == "1"){
           dateM = "January"
        }else if(dateM == "2"){
          dateM = "February"
        }else if(dateM == "3"){
           dateM = "March"
        }else if(dateM == "4"){
           dateM = "April"
        }else if(dateM == "5"){
           dateM = "May"
        }else if(dateM == "6"){
          dateM = "June"
        }else if(dateM == "7"){
          dateM = "July"
        }else if(dateM == "8"){
           dateM = "August"
        }else if(dateM == "9"){
            dateM = "September"
        }else if(dateM == "10"){
            dateM = "October"
        }else if(dateM == "11"){
            dateM = "November"
        }else if(dateM == "12"){
          dateM = "December"
        }
         let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            schedule = true
            self.tabBarController!.tabBar.userInteractionEnabled = false
            let params = "Dr_id=\(userId)&Year=\(dateY)&Month=\(dateM)&Date=\(dateD)&From=\(timeF)&To=\(timeT)&MeetingSlot=\(meetingAr.componentsJoinedByString(","))"
            print(params)
        AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrScheduleSet")
        }
    }
    
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString)
    {
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        if(schedule == true){
        dispatch_async(dispatch_get_main_queue()) {
           
            self.dateY = ""
            self.dateM = ""
            self.dateD = ""
            self.timeF = ""
            self.timeT = ""
            self.timeGap = 15
            self.fifteenMinS = ""
            self.thirtyMinS = ""
            self.sixtyMinS = ""
            self.NinetyMinS = ""
            self.int = 0
            self.timeAr = ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
             self.toTimeArr = ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        backVw.tag = 2358
        self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.19)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.15)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.15)
            }
        
        alerVw.backgroundColor = UIColor.whiteColor()
        backVw.addSubview(alerVw)
        
        let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
        rightImg.image = UIImage(named: "confirm")
        alerVw.addSubview(rightImg)
        
        let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
        verificationLbl.text = " Confirm"
        verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.02)
        verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(verificationLbl)
        
        let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
        lineVw.backgroundColor = UIColor.grayColor()
        alerVw.addSubview(lineVw)
        
        let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font          = UIFont(name: "Helvetica", size:14)
            }else{
                contentLbl.font          = UIFont(name: "Helvetica", size:19)
            }
        contentLbl.textAlignment = .Center
        contentLbl.textColor = UIColor.grayColor()
        contentLbl.text = "Your schedule has been saved successfully."
        contentLbl.numberOfLines = 2
        alerVw.addSubview(contentLbl)
        
        let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.21))
        okLbl.setTitle("OK", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
        okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        okLbl.titleLabel?.textAlignment = .Center
        okLbl.addTarget(self, action: #selector(AddScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
        okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(okLbl)
         }
        }else{
            dispatch_async(dispatch_get_main_queue()) {
                self.tabBarController!.tabBar.userInteractionEnabled = true
                if let dic:String = responseDict.valueForKey("error") as? String {
                    print(dic)
                }else{
                  let data = responseDict.objectForKey("data") as! NSArray
                    if(data.count != 0){
                    let time = data.valueForKey("Time") as! NSArray
                    for i in 0...time.count-1{
                        let fromTime = time[i].valueForKey("From") as! NSString
                        let toTime = time[i].valueForKey("To") as! NSString
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.timeStyle = .ShortStyle
                        dateFormatter.dateFormat = "k:mm" // k = Hour in 1~24, mm = Minute
                        let timeZone = NSTimeZone(name: "UTC")
                        dateFormatter.timeZone=timeZone
                        let timeT = dateFormatter.dateFromString("\(toTime)")!
                        let timeF = dateFormatter.dateFromString("\(fromTime)")!
                        let timeI =  (timeT.timeIntervalSinceDate(timeF))
                        let i = Int(timeI)
                         var q = timeF
                        var toArray = timeF.dateByAddingTimeInterval(15*60)
                       let minutesSlot = i/60
                        let arCount = (minutesSlot)/15
                        for j in 0...arCount-1 {
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "HH:mm"
                            let timeZone = NSTimeZone(name: "UTC")
                            dateFormatter.timeZone=timeZone
                            let timestring = dateFormatter.stringFromDate(q)
                            self.timeAr12Hr.removeAtIndex(self.timeAr.indexOf(timestring)!)
                            self.timeAr.removeAtIndex(self.timeAr.indexOf(timestring)!)
                            q = q.dateByAddingTimeInterval(15*60)
                        }
                        for k in 0...arCount-1 {
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "HH:mm"
                            let timeZone = NSTimeZone(name: "UTC")
                            dateFormatter.timeZone=timeZone
                            let timestring = dateFormatter.stringFromDate(toArray)
                            self.toTimeArr12Hr.removeAtIndex(self.toTimeArr.indexOf(timestring)!)
                            self.toTimeArr.removeAtIndex(self.toTimeArr.indexOf(timestring)!)
                            toArray = toArray.dateByAddingTimeInterval(15*60)
                        }
                     }
                   }
                }
            }
        }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }

    func okBtnAc(){
        if(doneTrue == true){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let mainViewController = storyboard.instantiateViewControllerWithIdentifier("DoctorMyAppointmentsViewController") as! DoctorMyAppointmentsViewController
            let leftViewController = storyboard.instantiateViewControllerWithIdentifier("DoctorSideBarViewController") as! DoctorSideBarViewController
            
            let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
            leftViewController.mainViewController = nvc
            let slideMenuController = SlideMenuController(mainViewController: nvc, rightMenuViewController:leftViewController)
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            slideMenuController.delegate = mainViewController
            appDelegate.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
            appDelegate.window?.rootViewController = slideMenuController
            appDelegate.window?.makeKeyAndVisible()

        }else{
             self.tabBarController!.tabBar.userInteractionEnabled = true
            let view = self.view.viewWithTag(2358)! as UIView
            view.removeFromSuperview()
            txtFldDate.text = ""
            txtFldTo.text = ""
            txtFldFrom.text = ""
            dateY = ""
            dateM = ""
            dateD = ""
            timeF = ""
            timeT = ""
            timeGap = 15
            fromLbl.text = ""
            toLbl.text = ""
            dateLbl.text = ""
            meetingAr.removeAllObjects()
             timeAr = ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
             toTimeArr = ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
             toTimeArr12Hr=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"]
            
             timeAr12Hr=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"]
            int = 0
            for i in 1500...1503 {
            let box = self.view.viewWithTag(i) as! UIButton
             box.setImage(UIImage(named: "emptyCheckBox"), forState: .Normal)
            }
            saveBtn.enabled = false

        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
}
}
extension AddScheduleViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
