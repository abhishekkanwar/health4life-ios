 

import UIKit
import RealmSwift
class StaffListingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,WebServiceDelegate,StaffListingViewControllerDelegate {
    
    @IBOutlet weak var staffListingTableVw: UITableView!
    @IBOutlet weak var addStaffBtn: UIButton!
    @IBOutlet weak var noStaffImg: UIImageView!
    @IBOutlet weak var drStaffLbl: UILabel!
    var limitStop = NSInteger()
    var showStaff = Bool()
    var stffListWho = Bool()
    var gnderAr = NSMutableArray()
    var nameAr = NSMutableArray()
    var profilePicAr = NSMutableArray()
    var phoneAr = NSMutableArray()
    var designationAr = NSMutableArray()
    var apiKeyAr = NSString()
    var sessionAr = NSString()
    var tokenAr = NSString()
    var groupIdAr = NSString()
    var emailAr = NSMutableArray()
    var realm = try! Realm()
    var searchDocUserId = NSString()
    var staffStatusAr = NSMutableArray()
    var staffIdAr = NSMutableArray()
    var drName = NSString()
    var server = NSInteger()
    var happened = NSString()
    var doctorItself = Bool()
    var indexPathName = NSIndexPath()
    func refreshMyStaffList( staffBool : Bool ){
        showStaff = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        drStaffLbl.text = "\(drName) Staff"
        self.navigationController?.navigationBarHidden = true
        staffListingTableVw.delegate = self
        staffListingTableVw.dataSource = self
        staffListingTableVw.separatorStyle = .None
        noStaffImg.hidden = true
        let usertype = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
       
        
        staffListingTableVw.hidden = true
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        if(usertype == 1){
            let defaults = NSUserDefaults.standardUserDefaults()
            if(doctorItself == false){
                let params = "DrId=\(searchDocUserId)&StaffId=abc"
                AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRStaffList")
                addStaffBtn.hidden = true
            }else{
                if let userId = defaults.valueForKey("id"){
                    let params = "DrId=\(userId)&StaffId=abc"
                    AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRStaffList")
                    addStaffBtn.hidden = false
                }
            }
        }else if(usertype == 0){
            addStaffBtn.hidden = true
            let params = "DrId=\(searchDocUserId)&StaffId=abc"
            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRStaffList")
        }else{
            addStaffBtn.hidden = true
            let defaults = NSUserDefaults.standardUserDefaults()
            let staffId = defaults.valueForKey("staffId") as! String
            let params = "DrId=\(searchDocUserId)&StaffId=\(staffId)"
            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRStaffList")
        }
        
        
       
    }
    override func viewWillAppear(animated: Bool) {
        let usertype = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
        staffListingTableVw.hidden = true
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        if(usertype == 1){
            let defaults = NSUserDefaults.standardUserDefaults()
            if(doctorItself == false){
                let params = "DrId=\(searchDocUserId)&StaffId=abc"
                AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRStaffList")
                addStaffBtn.hidden = true
            }else{
                if let userId = defaults.valueForKey("id"){
                    let params = "DrId=\(userId)&StaffId=abc"
                    AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRStaffList")
                    addStaffBtn.hidden = false
                }
            }
        }else if(usertype == 0){
            addStaffBtn.hidden = true
            let params = "DrId=\(searchDocUserId)&StaffId=abc"
            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRStaffList")
        }else{
            addStaffBtn.hidden = true
            let defaults = NSUserDefaults.standardUserDefaults()
            let staffId = defaults.valueForKey("staffId") as! String
            let params = "DrId=\(searchDocUserId)&StaffId=\(staffId)"
            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DRStaffList")
        }
    }
    
    @IBAction func addStaffAc(sender: AnyObject) {
        if(limitStop > 0){
            var storyboard = UIStoryboard()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                storyboard = UIStoryboard(name: "Main", bundle: nil)
            }else{
                storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
            }
            let vc = storyboard.instantiateViewControllerWithIdentifier("AddStaffViewController") as! AddStaffViewController
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            AppManager.sharedManager.Showalert("Alert", alertmessage: "You Reached the limit of adding staff.")
        }
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        
        if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 200){
            dispatch_async(dispatch_get_main_queue()) {
                let dataAr = responseDict.valueForKey("data") as! NSArray
                self.staffStatusAr.removeAllObjects()
                self.nameAr.removeAllObjects()
                self.gnderAr.removeAllObjects()
                self.profilePicAr.removeAllObjects()
                self.phoneAr.removeAllObjects()
                self.designationAr.removeAllObjects()
                self.staffIdAr.removeAllObjects()
                self.emailAr.removeAllObjects()
                if(dataAr.count != 0){
                    self.staffListingTableVw.hidden = false
                    self.noStaffImg.hidden = true
                    let limit = responseDict.valueForKey("limit") as! NSInteger
                    self.limitStop = limit
                    let data = responseDict.valueForKey("data") as! NSArray
                    for i in 0...data.count-1 {
                        let dict = data[i]
                        let staffStatus = dict.valueForKey("StaffStatus") as! NSInteger
                        let usertype = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
                        if(usertype == 1 && self.doctorItself == true){
                            self.nameAr.addObject(dict.valueForKey("Name")!)
                            self.emailAr.addObject(dict.valueForKey("Email")!)
                            self.gnderAr.addObject(dict.valueForKey("Gender")!)
                            self.profilePicAr.addObject(dict.valueForKey("ProfilePic")!)
                            let phoneNumber = "(\(dict.valueForKey("Code")!))\(dict.valueForKey("Mobile")!)"
                            self.phoneAr.addObject(phoneNumber)
                            self.staffStatusAr.addObject(dict.valueForKey("StaffStatus")!)
                            self.designationAr.addObject(dict.valueForKey("Designation")!)
                            self.staffIdAr.addObject(dict.valueForKey("StaffId")!)
                        }
                        else{
                            if(staffStatus == 1){
                                self.staffStatusAr.addObject(dict.valueForKey("StaffStatus")!)
                                self.nameAr.addObject(dict.valueForKey("Name")!)
                                self.emailAr.addObject(dict.valueForKey("Email")!)
                                self.gnderAr.addObject(dict.valueForKey("Gender")!)
                                self.profilePicAr.addObject(dict.valueForKey("ProfilePic")!)
                                let phoneNumber = "(\(dict.valueForKey("Code")!))\(dict.valueForKey("Mobile")!)"
                                self.phoneAr.addObject(phoneNumber)
                                self.designationAr.addObject(dict.valueForKey("Designation")!)
                                self.staffIdAr.addObject(dict.valueForKey("StaffId")!)
                            }
                        }
                    }
                    if(self.nameAr.count != 0){
                        self.staffListingTableVw.reloadData()
                    }else{
                        self.staffListingTableVw.reloadData()
                        self.staffListingTableVw.hidden = true
                    }
                }else{
                    let limit = responseDict.valueForKey("limit") as! NSInteger
                    self.limitStop = limit
                    self.staffListingTableVw.hidden = true
                    self.noStaffImg.hidden = false
                }
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue()) {
                let limit = responseDict.valueForKey("limit") as! NSInteger
                self.limitStop = limit
                self.staffListingTableVw.hidden = true
                self.noStaffImg.hidden = false
            }
        }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        if(stffListWho == false){
            self.navigationController?.popViewControllerAnimated(true)
        }else{
            self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
        }
    }
    @IBAction func deleteBtnAc(sender: AnyObject) {
        print("delete")
        let btnPos = sender.convertPoint(CGPointZero, toView: self.staffListingTableVw)
        let indexPath:NSIndexPath = self.staffListingTableVw.indexPathForRowAtPoint(btnPos)!
        let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To Delete \(nameAr[indexPath.row])?", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            NSLog("OK Pressed")
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("id") {
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                self.server = 1
                self.happened = "Deleted"
                self.doRequestPost("\(Header.BASE_URL)docter/DRStaffActiveDeactiveDelete", data: ["DrId":"\(userId)","StaffUserId":"\(self.staffIdAr[indexPath.row])","Status":"5"])
            }
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func deactivateAc(sender: AnyObject) {
        print("deactivate")
        let btnPos = sender.convertPoint(CGPointZero, toView: self.staffListingTableVw)
        let indexPath:NSIndexPath = self.staffListingTableVw.indexPathForRowAtPoint(btnPos)!
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
            server = 1
            happened = "Deactivated"
            self.doRequestPost("\(Header.BASE_URL)docter/DRStaffActiveDeactiveDelete", data: ["DrId":"\(userId)","StaffUserId":"\(staffIdAr[indexPath.row])","Status":"0"])
        }
    }
    @IBAction func activateAc(sender: AnyObject) {
        print("activate")
        let btnPos = sender.convertPoint(CGPointZero, toView: self.staffListingTableVw)
        let indexPath:NSIndexPath = self.staffListingTableVw.indexPathForRowAtPoint(btnPos)!
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
            server = 1
            happened = "Activate"
            self.doRequestPost("\(Header.BASE_URL)docter/DRStaffActiveDeactiveDelete", data: ["DrId":"\(userId)","StaffUserId":"\(staffIdAr[indexPath.row])","Status":"1"])
        }
    }
    func doRequestPost(url:String,data:[String: NSObject]){
        let requestDic = data
        print(requestDic)
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                print("Succes:")
                let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                dispatch_async(dispatch_get_main_queue())
                {
                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                    if(self.server == 1){
                        let alert = UIAlertController(title: "Alert", message: "Staff Member \(self.happened) successfully.", preferredStyle: UIAlertControllerStyle.Alert)
                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                            if(self.stffListWho == false){
                                self.navigationController?.popViewControllerAnimated(true)
                            }else{
                                self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
                            }
                        }
                        alert.addAction(okAction)
                        self.presentViewController(alert, animated: true, completion: nil)
                        
                    }else{
                        if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 200){
                            self.apiKeyAr = responseDict.valueForKeyPath("data.apiKey") as! String
                            self.sessionAr = responseDict.valueForKeyPath("data.sessionData") as! String
                            self.tokenAr = responseDict.valueForKeyPath("data.TokenData") as! String
                            self.groupIdAr = responseDict.valueForKeyPath("data.groupId") as! String
                        }
                        
                        let user = NSMutableDictionary()
                        user.setValue(self.tokenAr, forKey: "TokenData")
                        user.setValue(self.apiKeyAr, forKey: "ApiKey")
                        user.setValue(self.groupIdAr, forKey: "GroupId")
                        user.setValue(self.sessionAr, forKey: "sessionData")
                        user.setValue(self.nameAr[self.indexPathName.row],forKey: "Name")
                        let groupId = self.groupIdAr as String
                        self.msgCountList(groupId) { (result) in
                            let chatCount = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", groupId ))
                            if(chatCount.count < result.valueForKey("count") as! Int){
                                var userId = NSString()
                                let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
                                if(userType == 3){
                                    userId = NSUserDefaults.standardUserDefaults().valueForKey("staffId") as! String
                                }else{
                                    userId = NSUserDefaults.standardUserDefaults().valueForKey("id") as! String
                                }
                              
                                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                                RealmInteractor.shared.getMsgsFromServer(userId as String, groupId:groupId) { (result) in
                                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                                    
                                    if(result.valueForKey("status") as! Int == 200)
                                    {
                                        let chatResults = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", groupId))
                                        try! self.realm.write({
                                            self.realm.delete(chatResults)
                                        })
                                        
                                        let chatArray = result.objectForKey("data") as! NSArray
                                        
                                        for dict in chatArray
                                        {
                                            try! self.realm.write({
                                                
                                                let group = Group()
                                                group.displayName = dict.valueForKey("DisplayName") as! String
                                                group.message = dict.valueForKey("message") as! String
                                                group.messageType = dict.valueForKey("messageType") as! String
                                                group.fromId = dict.valueForKey("From") as! String
                                                group.groupId = dict.valueForKey("GroupId") as! String
                                                self.realm.add(group)
                                                
                                            })
                                            
                                        }
                                        self.navigateToChatView(user)
                                    }
                                }
                            }
                            else
                            {
                                self.navigateToChatView(user)
                            }
                        }
                        
                        
                    }
                }
            }
        }
        dataTask.resume()
    }
    @IBAction func chatBtnAc(sender: AnyObject) {
        print("chatstart")
        
        let btnPos = sender.convertPoint(CGPointZero, toView: self.staffListingTableVw)
        let indexPath:NSIndexPath = self.staffListingTableVw.indexPathForRowAtPoint(btnPos)!
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        indexPathName = indexPath
        let staffIdSelect = self.staffIdAr[indexPath.row] as! String
        let defaults = NSUserDefaults.standardUserDefaults()
        server = 2
        let usertype = defaults.valueForKey("usertype") as! NSInteger
        if(usertype == 1 || usertype == 0){
            let userId = defaults.valueForKey("id") as! String
            self.doRequestPost("\(Header.BASE_URL)docter/DRdrcreatetoken", data: ["From":"\(userId)","To":"\(staffIdSelect)"])
        }
        else if(usertype == 3){
            let userId = defaults.valueForKey("staffId") as! String
            self.doRequestPost("\(Header.BASE_URL)docter/DRdrcreatetoken", data: ["From":"\(userId)","To":"\(staffIdSelect)"])
        }
        
        
        
        
    }
    func navigateToChatView(user:NSDictionary) -> Void {
        
        let chatResults = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", user.valueForKey("GroupId") as! String))
        print(chatResults.count)
        
        let msgArray = NSMutableArray()
        
        for group in chatResults
        {
            var mySelf = false
            let msgDict = group
            
            let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
            if(userType == 1 || userType == 0 || userType == 2){
                if(msgDict.fromId == (NSUserDefaults.standardUserDefaults().valueForKey("id") as! String))
                {
                    mySelf = true
                }
            }else if(userType == 3){
                if(msgDict.fromId == (NSUserDefaults.standardUserDefaults().valueForKey("staffId") as! String))
                {
                    mySelf = true
                }
            }
            
            let mdict = ["msg":msgDict.message,"bool":mySelf, "msgType":msgDict.messageType,"name":msgDict.displayName]
            msgArray.addObject(mdict)
        }
        
        let firstname:String = NSUserDefaults.standardUserDefaults().valueForKey("firstName") as! String
        
        let vc = storyboard!.instantiateViewControllerWithIdentifier("ChatViewController") as! ChatViewController
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let usertype = defaults.valueForKey("usertype") as! NSInteger
        if(usertype == 3){
            vc.userId = defaults.valueForKey("staffId") as! String
        }else{
            vc.userId = defaults.valueForKey("id") as! String
        }
        
        vc.chatUserId = user.valueForKey("GroupId") as! String
        
        vc.userName = user.valueForKey("Name") as! String
        
        
        vc.Token = user.valueForKey("TokenData") as! String
        vc.SessionID = user.valueForKey("sessionData") as! String
        vc.apiKey = user.valueForKey("ApiKey") as! String
        vc.msgAr = msgArray
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func msgCountList(groupId:String,completion: (result: NSDictionary)->()) -> Void {
        let paramString = "GroupId=\(groupId)"
        AppManager.sharedManager.generateGroupIdForUser("messageChat/listChat", params: paramString) { (result) in
            print("\(result)")
            
            completion(result: result)
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameAr.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        cell!.layer.cornerRadius = 7.0
        cell!.layer.masksToBounds = true
        
        let itemImg: UIImageView = (cell!.viewWithTag(701) as! UIImageView)
        itemImg.layer.cornerRadius = itemImg.frame.size.height/2
        itemImg.layer.borderWidth = self.view.frame.size.height*0.008
        itemImg.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        if let url = NSURL(string: "http://\(profilePicAr[indexPath.row])") {
            itemImg.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        let delBtn: UIButton = (cell!.viewWithTag(11) as! UIButton)
        delBtn.layer.cornerRadius  =  self.view.frame.size.height*0.005
        delBtn.layer.masksToBounds = false;
        
        let deactivBtn: UIButton = (cell!.viewWithTag(12) as! UIButton)
        deactivBtn.layer.cornerRadius  =  self.view.frame.size.height*0.005
        deactivBtn.layer.masksToBounds = false;
        
        let chatBtn: UIButton = (cell!.viewWithTag(50) as! UIButton)
        
        let activBtn: UIButton = (cell!.viewWithTag(20) as! UIButton)
        activBtn.layer.cornerRadius  =  self.view.frame.size.height*0.005
        activBtn.layer.masksToBounds = false;
        let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
        if(userType == 1){
            if(staffStatusAr[indexPath.row] as! NSObject  == 1){
                activBtn.hidden = true
                activBtn.userInteractionEnabled = false
                deactivBtn.userInteractionEnabled = true
                deactivBtn.hidden = false
            }else if(staffStatusAr[indexPath.row] as! NSObject  == 0){
                activBtn.hidden = false
                chatBtn.hidden = true
                activBtn.userInteractionEnabled = true
                deactivBtn.userInteractionEnabled = false
                deactivBtn.hidden = true
            }
        }else{
            chatBtn.hidden = false
        }
        
        
        let imageView: UIView = (cell?.viewWithTag(700))!
        imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
        imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
        imageView.layer.shadowOffset = CGSizeMake(10, 5);
        imageView.layer.shadowRadius = 1;
        imageView.layer.shadowOpacity = 0.06;
        imageView.layer.masksToBounds = false;
        imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        imageView.layer.borderWidth = 0.5
        if(indexPath.row%2 == 0){
            imageView.backgroundColor = UIColor.whiteColor()
            itemImg.backgroundColor = UIColor.lightGrayColor()
        }else{
            imageView.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            itemImg.backgroundColor = UIColor.whiteColor()
        }
        
        let nameLbl: UILabel = (cell!.viewWithTag(702) as! UILabel)
        nameLbl.text = nameAr[indexPath.row] as? String
        let genderLbl: UILabel = (cell!.viewWithTag(703) as! UILabel)
        genderLbl.text = gnderAr[indexPath.row] as? String
        let designationLbl: UILabel = (cell!.viewWithTag(704) as! UILabel)
        designationLbl.text = designationAr[indexPath.row] as? String
        
        
        let usertype = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
        if(usertype == 0 || usertype == 3){
            delBtn.hidden = true
            activBtn.hidden = true
            deactivBtn.hidden = true
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                let botmConst = NSLayoutConstraint(
                    item:designationLbl,
                    attribute: NSLayoutAttribute.BottomMargin,
                    relatedBy: NSLayoutRelation.Equal,
                    toItem: imageView,
                    attribute: NSLayoutAttribute.BottomMargin,
                    multiplier: 1,
                    constant: 0)
                cell!.addConstraints([botmConst])
            }else{
                let botmConst = NSLayoutConstraint(
                    item:designationLbl,
                    attribute: NSLayoutAttribute.BottomMargin,
                    relatedBy: NSLayoutRelation.Equal,
                    toItem: imageView,
                    attribute: NSLayoutAttribute.BottomMargin,
                    multiplier:0.7,
                    constant: 0)
                cell!.addConstraints([botmConst])
            }
        }
        else if(usertype == 1){
            if(doctorItself == false){
                delBtn.hidden = true
                activBtn.hidden = true
                deactivBtn.hidden = true
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    let botmConst = NSLayoutConstraint(
                        item:designationLbl,
                        attribute: NSLayoutAttribute.BottomMargin,
                        relatedBy: NSLayoutRelation.Equal,
                        toItem: imageView,
                        attribute: NSLayoutAttribute.BottomMargin,
                        multiplier: 1,
                        constant: 0)
                    cell!.addConstraints([botmConst])
                }else{
                    let botmConst = NSLayoutConstraint(
                        item:designationLbl,
                        attribute: NSLayoutAttribute.BottomMargin,
                        relatedBy: NSLayoutRelation.Equal,
                        toItem: imageView,
                        attribute: NSLayoutAttribute.BottomMargin,
                        multiplier: 0.7,
                        constant: 0)
                    cell!.addConstraints([botmConst])
                }
            }else{
                delBtn.hidden = false
                if(staffStatusAr[indexPath.row] as! NSObject  == 1){
                    activBtn.hidden = true
                    chatBtn.hidden = false
                    activBtn.userInteractionEnabled = false
                    deactivBtn.userInteractionEnabled = true
                    deactivBtn.hidden = false
                }else if(staffStatusAr[indexPath.row] as! NSObject  == 0){
                    activBtn.hidden = false
                    chatBtn.hidden = true
                    activBtn.userInteractionEnabled = true
                    deactivBtn.userInteractionEnabled = false
                    deactivBtn.hidden = true
                }
            }
        }
        
        
        return cell!
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(showStaff == true){
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            return self.view.frame.size.width*0.3
            }else{
             return self.view.frame.size.width*0.2
            }
        }else{
            return UITableViewAutomaticDimension
        }
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return self.view.frame.size.width*0.1
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var storyboard = UIStoryboard()
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        }else{
            storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
        }
        let vc = storyboard.instantiateViewControllerWithIdentifier("DocStaffDetailViewController") as! DocStaffDetailViewController
        var compPh = "+1\(phoneAr[indexPath.row])"
        compPh = compPh.insert("-", ind: 10)
        vc.valueAr.addObject(nameAr[indexPath.row])
        vc.valueAr.addObject(gnderAr[indexPath.row])
        vc.valueAr.addObject(compPh)
        vc.valueAr.addObject(emailAr[indexPath.row])
        vc.valueAr.addObject(designationAr[indexPath.row])
        vc.staffStatus = Int(staffStatusAr[indexPath.row] as! NSNumber)
        vc.apiKey = apiKeyAr 
        vc.Token = tokenAr
        vc.sessionId = sessionAr
        vc.groupId = groupIdAr
        vc.staffIdAr = staffIdAr[indexPath.row] as! NSString
        vc.imgUrl = profilePicAr[indexPath.row] as! NSString
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
 
    
}
