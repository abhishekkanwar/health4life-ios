

import UIKit
import MapKit
import SlideMenuControllerSwift
import MessageUI
class ContactUsViewController: UIViewController,WebServiceDelegate,MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var mapViewController: MKMapView!
    @IBOutlet weak var BoxView: UIView!
    var contactSwich = Bool()
    var phone = NSString()
    var status = NSString()
    var statusBool = Bool()
    var emailId = [String]()
    var err = NSString()
    override func viewDidLoad(){
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        AppManager.sharedManager.delegate=self
        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.85)
        backVw.tag = 347
        self.view.addSubview(backVw)
        AppManager.sharedManager.showActivityIndicatorInView(backVw, withLabel: "Loading..")
        AppManager.sharedManager.postDataOnserver("", postUrl: "users/Contact")
        mapViewController.layer.cornerRadius = self.view.frame.size.height*0.009
        let location = CLLocationCoordinate2D(
            latitude:  37.0902,
            longitude: -95.7129
        )
        let span = MKCoordinateSpanMake(20.00, 20.00)
        let region = MKCoordinateRegion(center: location, span: span)
        mapViewController.setRegion(region, animated: true)
        BoxView.layer.cornerRadius = self.view.frame.size.height*0.009
        BoxView.layer.borderColor = UIColor.darkGrayColor().CGColor
        BoxView.layer.shadowOffset = CGSizeMake(10, 5);
        BoxView.layer.shadowRadius = 1;
        BoxView.layer.shadowOpacity = 0.06;
        BoxView.layer.masksToBounds = false;
        BoxView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        BoxView.layer.borderWidth = 0.5
        
        let addressLbl = BoxView.viewWithTag(341) as! UILabel
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: "cityState")
        let offsetY: CGFloat = -5.0
        attachment.bounds = CGRectMake(0, offsetY, attachment.image!.size.width, attachment.image!.size.height)
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string:"")
        myString.appendAttributedString(attachmentString)
        let myString1 = NSMutableAttributedString(string:"  Address")
        myString.appendAttributedString(myString1)
        addressLbl.textAlignment = .Center
        addressLbl.attributedText = myString
        
        let phoneLbl = BoxView.viewWithTag(343) as! UILabel
        let attachment1 = NSTextAttachment()
        attachment1.image = UIImage(named: "phone")
        let offsetY1: CGFloat = -5.0
        attachment1.bounds = CGRectMake(0, offsetY1, attachment1.image!.size.width, attachment1.image!.size.height)
        let attachmentString1 = NSAttributedString(attachment: attachment1)
        let myString2 = NSMutableAttributedString(string:"")
        myString2.appendAttributedString(attachmentString1)
        let myString3 = NSMutableAttributedString(string:"  Phone")
        myString2.appendAttributedString(myString3)
        phoneLbl.textAlignment = .Center
        phoneLbl.attributedText = myString2
        
        let emailLbl = BoxView.viewWithTag(345) as! UILabel
        let attachment2 = NSTextAttachment()
        attachment2.image = UIImage(named: "ForgotPaswrdEmail")
        let offsetY2: CGFloat = -5.0
        attachment2.bounds = CGRectMake(0, offsetY2, attachment2.image!.size.width, attachment2.image!.size.height)
        let attachmentString2 = NSAttributedString(attachment: attachment2)
        let myString4 = NSMutableAttributedString(string:"")
        myString4.appendAttributedString(attachmentString2)
        let myString5 = NSMutableAttributedString(string:"  Email Address")
        myString4.appendAttributedString(myString5)
        emailLbl.textAlignment = .Center
        emailLbl.attributedText = myString4
    }
    override func viewWillAppear(animated: Bool) {
        emailId = []
        if(statusBool == true){
            if(status == "cancel"){
                statusBool = false
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Mail Cancelled")
            }
            else if(status == "saved"){
                statusBool = false
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Mail Saved")
            }
            else if(status == "sent"){
                statusBool = false
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Mail Sent")
            }
            else if(status == err){
                statusBool = false
                AppManager.sharedManager.Showalert("Alert", alertmessage: err)
            }
        }
    }
    //MARK:- ServerResponce
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        if((responseDict.valueForKey("data")) != nil){
            let data = responseDict.valueForKey("data") as! NSDictionary
            dispatch_async(dispatch_get_main_queue()) {
                let view = self.view.viewWithTag(347)! as UIView
                AppManager.sharedManager.hideActivityIndicatorInView(view)
                view.removeFromSuperview()
                let address = data.valueForKey("Address") as! NSString
                let add = self.BoxView.viewWithTag(342) as! UILabel
                add.numberOfLines = 2
                add.text = address as String
                let email = data.valueForKey("Email") as! NSString
                let eml = self.BoxView.viewWithTag(346) as! UILabel
                eml.text = email as String
                self.emailId.append(email as String)
                let tapGestureRecognizer2 = UITapGestureRecognizer(target:self, action:#selector(ContactUsViewController.imageTapped2(_:)))
                eml.userInteractionEnabled = true
                eml.addGestureRecognizer(tapGestureRecognizer2)
                
                let phn = data.valueForKey("Phone") as! NSString
                let phnlbl = self.BoxView.viewWithTag(344) as! UILabel
                self.phone = phn
                phnlbl.text = phn as String
                let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(ContactUsViewController.imageTapped(_:)))
                phnlbl.userInteractionEnabled = true
                phnlbl.addGestureRecognizer(tapGestureRecognizer)
                let lat = data.valueForKey("Lat") as! NSString
                let long = data.valueForKey("Long") as! NSString
                let myDoubleLat = Double(lat as String)
                let myDoubleLong = Double(long as String)
                let location = CLLocationCoordinate2D(
                    latitude:  myDoubleLat!,
                    longitude: myDoubleLong!
                )
                let span = MKCoordinateSpanMake(0.10, 0.10)
                let region = MKCoordinateRegion(center: location, span: span)
                self.mapViewController.setRegion(region, animated: true)
                let annotation = MKPointAnnotation()
                annotation.coordinate = location
                annotation.title = "\(data.valueForKey("Address") as! String)"
                self.mapViewController.addAnnotation(annotation)
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue()) {
                let view = self.view.viewWithTag(347)! as UIView
                AppManager.sharedManager.hideActivityIndicatorInView(view)
                view.removeFromSuperview()
            }
        }
    }
    
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    
    
    //MARK:- Tap&ButtonAction
    func imageTapped(img: AnyObject){
        let alert = UIAlertController(title: "Please confirm your call", message: "You’re about to call the following:\n\(phone)", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            if let url = NSURL(string: "tel://\(self.phone)") {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    func imageTapped2(img: AnyObject){
        if(MFMailComposeViewController.canSendMail()){
            let emailTitle = ""
            let messageBody = ""
            let toRecipents = emailId
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setMessageBody(messageBody, isHTML: false)
            mc.setToRecipients(toRecipents)
            self.presentViewController(mc, animated: true, completion: nil)
        }else{
            AppManager.sharedManager.Showalert("Alert", alertmessage: "No email account is configured with your device.")
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?){
        statusBool = true
        switch result {
        case .Cancelled:
            status = "cancel"
            break
        case .Saved:
            status = "saved"
            break
        case .Sent:
            status = "sent"
            break
        case .Failed:
            err = "\(error?.localizedDescription)"
            status = err
            break
        default:
            break
            
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
        emailId = []
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        if(contactSwich == true){
            self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
        }else{
            self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension ContactUsViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
