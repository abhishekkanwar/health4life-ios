

import UIKit
import SlideMenuControllerSwift
import MessageUI
class MyDoctorsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,WebServiceDelegate,MFMailComposeViewControllerDelegate {
    var dataCount = NSInteger()
    var specialitydataAr = NSMutableArray()
    var nameDataAr = NSMutableArray()
    var addDataAr = NSMutableArray()
    var qualiDataAr = NSMutableArray()
    var languageDataAr = NSMutableArray()
    var phDataAr = NSMutableArray()
    var bioDataAr = NSMutableArray()
    var statusArr = NSMutableArray()
    var dr_idAr = NSMutableArray()
    var phCodeDataAr = NSMutableArray()
    var profilepicAr = NSMutableArray()
    var emailAr = NSMutableArray()
    var ratingAr = NSMutableArray()
    var devicetoknAr = NSMutableArray()
    var devicetypeAr = NSMutableArray()
    var ph = NSInteger()
    var status = NSString()
    var statusBool = Bool()
    var err = NSString()
    var emailAdd = [String]()
    var serverInt = NSInteger()
    var drEmailId = NSString()
    var drName = NSString()
    var dr_id = NSString()
    var drDeviceToken = NSString()
    var drDeviceType = NSString()
    
    @IBOutlet weak var myDocTableVw: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        myDocTableVw.delegate = self
        myDocTableVw.dataSource = self
        self.navigationController?.navigationBarHidden = true
        self.specialitydataAr.removeAllObjects()
        self.addDataAr.removeAllObjects()
        self.nameDataAr.removeAllObjects()
        self.bioDataAr.removeAllObjects()
        self.phDataAr.removeAllObjects()
        self.phCodeDataAr.removeAllObjects()
        self.qualiDataAr.removeAllObjects()
        self.languageDataAr.removeAllObjects()
        self.statusArr.removeAllObjects()
        profilepicAr.removeAllObjects()
        self.emailAr.removeAllObjects()
        self.ratingAr.removeAllObjects()
        //let footerView: UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height*0.12))
        //        let addDocBtn: UIButton = UIButton()
        //        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
        //            addDocBtn.frame = CGRectMake(self.view.frame.size.width*0.1,footerView.frame.size.height*0.17, self.view.frame.size.width-(2*(self.view.frame.size.width*0.1)),footerView.frame.size.height-footerView.frame.size.height*0.34)
        //        }else{
        //            if(self.view.frame.size.height < 1366){
        //                addDocBtn.frame = CGRectMake(self.view.frame.size.width*0.2,footerView.frame.size.height*0.17, self.view.frame.size.width-(2*(self.view.frame.size.width*0.2)),footerView.frame.size.height-footerView.frame.size.height*0.32)
        //            }
        //            else{
        //                addDocBtn.frame = CGRectMake(self.view.frame.size.width*0.25,footerView.frame.size.height*0.17, self.view.frame.size.width-(2*(self.view.frame.size.width*0.25)),footerView.frame.size.height-footerView.frame.size.height*0.5)
        //            }
        //        }
        //        addDocBtn.setBackgroundImage(UIImage(named: "addDocBtn"), forState: .Normal)
        //        addDocBtn.addTarget(self, action: #selector(MyDoctorsViewController.addDocAc), forControlEvents: .TouchUpInside)
        //        footerView.addSubview(addDocBtn)
        //        myDocTableVw.tableFooterView = footerView
    }
    override func viewWillAppear(animated: Bool) {
        emailAdd = []
        self.specialitydataAr.removeAllObjects()
        self.addDataAr.removeAllObjects()
        self.nameDataAr.removeAllObjects()
        self.bioDataAr.removeAllObjects()
        self.phDataAr.removeAllObjects()
        self.phCodeDataAr.removeAllObjects()
        self.qualiDataAr.removeAllObjects()
        self.languageDataAr.removeAllObjects()
        self.statusArr.removeAllObjects()
        profilepicAr.removeAllObjects()
        self.ratingAr.removeAllObjects()
        emailAr.removeAllObjects()
        devicetypeAr.removeAllObjects()
        devicetoknAr.removeAllObjects()
        self.patDrList()
        
        if(statusBool == true){
            if(status == "cancel"){
                statusBool = false
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Mail Cancelled")
            }
            else if(status == "saved"){
                statusBool = false
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Mail Saved")
            }
            else if(status == "sent"){
                statusBool = false
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Mail Sent")
            }
            else if(status == err){
                statusBool = false
                AppManager.sharedManager.Showalert("Alert", alertmessage: err)
            }
        }
    }
    func patDrList(){
        self.specialitydataAr.removeAllObjects()
        self.addDataAr.removeAllObjects()
        self.nameDataAr.removeAllObjects()
        self.bioDataAr.removeAllObjects()
        self.phDataAr.removeAllObjects()
        self.phCodeDataAr.removeAllObjects()
        self.qualiDataAr.removeAllObjects()
        self.languageDataAr.removeAllObjects()
        self.statusArr.removeAllObjects()
        profilepicAr.removeAllObjects()
        self.ratingAr.removeAllObjects()
        emailAr.removeAllObjects()
        devicetypeAr.removeAllObjects()
        devicetoknAr.removeAllObjects()
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            AppManager.sharedManager.delegate=self
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.85)
            backVw.tag = 2359
            self.view.addSubview(backVw)
            AppManager.sharedManager.showActivityIndicatorInView(backVw, withLabel: "Loading..")
            serverInt = 1
            let params = "Patient_id=\(userId)"
            AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/PatientDrList")
        }
    }
    //MARK:- ServerResponce
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString)
    {
        if(serverInt == 1){
            let bckVw = self.view.viewWithTag(2359)
            AppManager.sharedManager.hideActivityIndicatorInView(bckVw!)
            if(responseDict.objectForKey("data") != nil){
                let dataDict = responseDict.objectForKey("data") as! NSArray
                dataCount = dataDict.count
                for i in 0...self.dataCount-1 {
                    let d = dataDict[i]
                    let groupId = d.valueForKey("GroupId") as! String
                    let drId = d.valueForKey("Dr_id") as! String
                    let status = d.valueForKey("status") as! NSInteger
                    if(groupId == ""){
                        if(status == 1){
                        let defaults = NSUserDefaults.standardUserDefaults()
                        if let userId = defaults.valueForKey("id") {
                            self.doRequestPost("\(Header.BASE_URL)messageChat/createsession", data: ["DrId":"\(drId)","PatientId":"\(userId)","Group":"0"])
                         }
                        }
                    }
                    dr_idAr.addObject(d.valueForKey("Dr_id")!)
                    specialitydataAr.addObject(d.valueForKey("Speciality")!)
                    addDataAr.addObject(d.valueForKey("Address")!)
                    nameDataAr.addObject(d.valueForKey("Name")!)
                    bioDataAr.addObject(d.valueForKey("Bio")!)
                    phDataAr.addObject(d.valueForKey("Mobile")!)
                    phCodeDataAr.addObject(d.valueForKey("Code")!)
                    qualiDataAr.addObject(d.valueForKey("Qualification")!)
                    languageDataAr.addObject(d.valueForKey("Languages")!)
                    statusArr.addObject(d.valueForKey("status")!)
                    profilepicAr.addObject(d.valueForKey("ProfilePic")!)
                    emailAr.addObject(d.valueForKey("Email")!)
                    ratingAr.addObject(d.valueForKey("Rating")!)
                    devicetoknAr.addObject(d.valueForKey("DeviceToken")!)
                    devicetypeAr.addObject(d.valueForKey("DeviceType")!)
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.myDocTableVw.reloadData()
                bckVw?.removeFromSuperview()
            }
        }
        else{
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            if((responseDict.valueForKey("error")) != nil){
                
                dispatch_async(dispatch_get_main_queue()) {
                    let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                    backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                    backVw.tag = 2356
                    self.view.addSubview(backVw)
                    
                    let alerVw = UIView()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        alerVw.frame = CGRectMake(self.view.frame.size.width*0.07,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.14), self.view.frame.size.height*0.184)
                    }else{
                        alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.12)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.12)
                    }
                    
                    alerVw.backgroundColor = UIColor.whiteColor()
                    backVw.addSubview(alerVw)
                    
                    let rightImg = UIImageView.init(frame: CGRectMake(12,12, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                    rightImg.image = UIImage(named: "confirm")
                    alerVw.addSubview(rightImg)
                    
                    let verificationLbl  = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,12, self.view.frame.size.width*0.45, self.view.frame.size.width*0.06))
                    verificationLbl.text = "Alert"
                    verificationLbl.font = UIFont(name: "Bold", size: self.view.frame.size.height * 0.02)
                    verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(verificationLbl)
                    
                    let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+20,alerVw.frame.size.width,1))
                    lineVw.backgroundColor = UIColor.grayColor()
                    alerVw.addSubview(lineVw)
                    
                    let contentLbl = UILabel()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        contentLbl.font  = UIFont(name: "Helvetica", size:14)
                        contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+22, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36)
                    }else{
                        contentLbl.font  = UIFont(name: "Helvetica", size:19)
                        contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.21)
                    }
                    contentLbl.textAlignment = .Center
                    contentLbl.textColor = UIColor.grayColor()
                    contentLbl.text = "Request has already been sent."
                    contentLbl.numberOfLines = 2
                    alerVw.addSubview(contentLbl)
                    
                    let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.2))
                    okLbl.setTitle("OK", forState: .Normal)
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                        okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                    }else{
                        okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                    }
                    okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                    okLbl.titleLabel?.textAlignment = .Center
                    okLbl.addTarget(self, action: #selector(MyDoctorsViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                    okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                    alerVw.addSubview(okLbl)
                }
                
            }
            else{
                dispatch_async(dispatch_get_main_queue()) {
                    if(self.view.frame.size.height < 600){
                        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                        backVw.tag = 2356
                        self.view.addSubview(backVw)
                        
                        let alerVw = UIView()
                        alerVw.frame = CGRectMake(self.view.frame.size.width*0.07,self.view.frame.size.height/2-((self.view.frame.size.height*0.38)/2), self.view.frame.size.width-(self.view.frame.size.width*0.14), self.view.frame.size.height*0.38)
                        alerVw.backgroundColor = UIColor.whiteColor()
                        backVw.addSubview(alerVw)
                        
                        let rightImg = UIImageView.init(frame: CGRectMake(12,12, self.view.frame.size.width*0.06, self.view.frame.size.width*0.06))
                        rightImg.image = UIImage(named: "confirm")
                        alerVw.addSubview(rightImg)
                        
                        let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,12, self.view.frame.size.width*0.45, self.view.frame.size.width*0.06))
                        verificationLbl.text = " Request Sent"
                        verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                        verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(verificationLbl)
                        
                        let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+20,alerVw.frame.size.width,1))
                        lineVw.backgroundColor = UIColor.grayColor()
                        alerVw.addSubview(lineVw)
                        
                        let contentLbl = UILabel()
                        contentLbl.font  = UIFont(name: "Helvetica", size:14)
                        contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+20, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.56)
                        contentLbl.textAlignment = .Center
                        contentLbl.textColor = UIColor.grayColor()
                        let trimNameString = self.drName.stringByTrimmingCharactersInSet(
                            NSCharacterSet.whitespaceAndNewlineCharacterSet());
                        contentLbl.text = "You can self-schedule your video visits with Dr. \(trimNameString) once they accept your request. You will be notified via a pop-up notification and/or an email as soon as your account has been verified by your doctor."
                        contentLbl.numberOfLines = 7
                        alerVw.addSubview(contentLbl)
                        
                        let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.2))
                        okLbl.setTitle("OK", forState: .Normal)
                        okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                        okLbl.titleLabel?.textAlignment = .Center
                        okLbl.addTarget(self, action: #selector(MyDoctorsViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                        okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(okLbl)
                    }
                    else if(self.view.frame.size.height > 600 && self.view.frame.size.height < 700){
                        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                        backVw.tag = 2356
                        self.view.addSubview(backVw)
                        
                        let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.07,self.view.frame.size.height/2-((self.view.frame.size.height*0.38)/2), self.view.frame.size.width-(self.view.frame.size.width*0.14), self.view.frame.size.height*0.33))
                        alerVw.backgroundColor = UIColor.whiteColor()
                        backVw.addSubview(alerVw)
                        
                        let rightImg = UIImageView.init(frame: CGRectMake(12,12, self.view.frame.size.width*0.06, self.view.frame.size.width*0.06))
                        rightImg.image = UIImage(named: "confirm")
                        alerVw.addSubview(rightImg)
                        
                        let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,12, self.view.frame.size.width*0.45, self.view.frame.size.width*0.06))
                        verificationLbl.text = " Request Sent"
                        verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                        verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(verificationLbl)
                        
                        let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+20,alerVw.frame.size.width,1))
                        lineVw.backgroundColor = UIColor.grayColor()
                        alerVw.addSubview(lineVw)
                        
                        let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+20, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.56))
                        contentLbl.font = UIFont(name: "Helvetica", size:14)
                        contentLbl.textAlignment = .Center
                        contentLbl.textColor = UIColor.grayColor()
                        let trimNameString = self.drName.stringByTrimmingCharactersInSet(
                            NSCharacterSet.whitespaceAndNewlineCharacterSet());
                        contentLbl.text = "You can self-schedule your video visits with Dr. \(trimNameString) once they accept your request. You will be notified via a pop-up notification and/or an email as soon as your account has been verified by your doctor."
                        contentLbl.numberOfLines = 7
                        alerVw.addSubview(contentLbl)
                        
                        let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.18),alerVw.frame.size.width, alerVw.frame.size.height*0.18))
                        okLbl.setTitle("OK", forState: .Normal)
                        okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                        okLbl.titleLabel?.textAlignment = .Center
                        okLbl.addTarget(self, action: #selector(MyDoctorsViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                        okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(okLbl)
                    }else{
                        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                        backVw.tag = 2356
                        self.view.addSubview(backVw)
                        let alerVw = UIView()
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            alerVw.frame = CGRectMake(self.view.frame.size.width*0.07,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.14), self.view.frame.size.height*0.3)
                        }else{
                            alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.2)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.24)
                        }
                        alerVw.backgroundColor = UIColor.whiteColor()
                        backVw.addSubview(alerVw)
                        
                        let rightImg = UIImageView.init(frame: CGRectMake(12,12, self.view.frame.size.width*0.06, self.view.frame.size.width*0.06))
                        rightImg.image = UIImage(named: "confirm")
                        alerVw.addSubview(rightImg)
                        
                        let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,12, self.view.frame.size.width*0.45, self.view.frame.size.width*0.06))
                        verificationLbl.text = " Request Sent"
                        verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                        verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(verificationLbl)
                        
                        let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+20,alerVw.frame.size.width,1))
                        lineVw.backgroundColor = UIColor.grayColor()
                        alerVw.addSubview(lineVw)
                        
                        
                        let contentLbl = UILabel()
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                            contentLbl.font  = UIFont(name: "Helvetica", size:14)
                            contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+20, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.56)
                        }else{
                            contentLbl.font  = UIFont(name: "Helvetica", size:19)
                            contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+22, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.45)
                        }
                        contentLbl.textAlignment = .Center
                        contentLbl.textColor = UIColor.grayColor()
                        let trimNameString = self.drName.stringByTrimmingCharactersInSet(
                            NSCharacterSet.whitespaceAndNewlineCharacterSet());
                        contentLbl.text = "You can self-schedule your video visits with Dr. \(trimNameString) once they accept your request. You will be notified via a pop-up notification and/or an email as soon as your account has been verified by your doctor."
                        contentLbl.numberOfLines = 7
                        alerVw.addSubview(contentLbl)
                        
                        let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.18),alerVw.frame.size.width, alerVw.frame.size.height*0.18))
                        okLbl.setTitle("OK", forState: .Normal)
                        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                            okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                        }else{
                            okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                        }
                        okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                        okLbl.titleLabel?.textAlignment = .Center
                        okLbl.addTarget(self, action: #selector(MyDoctorsViewController.okBtnAc2), forControlEvents: .TouchUpInside)
                        okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                        alerVw.addSubview(okLbl)
                    }
                }
            }
        }
    }
    func doRequestPost(url:String,data:[String: NSObject]){
        let requestDic = data
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        
        
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                print("Succes:")
                let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                dispatch_async(dispatch_get_main_queue())
                {
                    
                }
            }
        }
        dataTask.resume()
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("Oops", alertmessage: failureError.localizedDescription)
    }
    
    
    
    //MARK:- TableViewMethod
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataCount
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        cell!.layer.cornerRadius = 7.0
        cell!.layer.masksToBounds = true
        let statusLbl = (cell?.viewWithTag(701)) as! UILabel
        statusLbl.layer.cornerRadius =  statusLbl.frame.size.height*0.009
        let drImg: UIImageView = (cell!.viewWithTag(5026) as! UIImageView)
        drImg.layer.cornerRadius = drImg.frame.size.height/2
        drImg.layer.borderWidth = self.view.frame.size.height*0.012
        drImg.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        if let url = NSURL(string: "http://\(profilepicAr[indexPath.row])") {
            drImg.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        let imageView: UIView = (cell?.viewWithTag(700))!
        imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
        imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
        imageView.layer.shadowOffset = CGSizeMake(10, 5);
        imageView.layer.shadowRadius = 1;
        imageView.layer.shadowOpacity = 0.06;
        imageView.backgroundColor = UIColor.whiteColor()
        imageView.layer.masksToBounds = false;
        imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        imageView.layer.borderWidth = 0.5
        if(indexPath.row%2 == 0){
            imageView.backgroundColor = UIColor.whiteColor()
            drImg.backgroundColor = UIColor.lightGrayColor()
        }else{
            imageView.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            drImg.backgroundColor = UIColor.whiteColor()
        }
        let name = cell?.viewWithTag(5022) as! UILabel
        name.text = "Dr. \(nameDataAr[indexPath.row])"
        let speciality = cell?.viewWithTag(5023) as! UILabel
        speciality.text = "\(specialitydataAr[indexPath.row])"
        let qualification = cell?.viewWithTag(5024) as! UILabel
        qualification.text = "\(qualiDataAr[indexPath.row])"
        let starRatingVw = cell?.viewWithTag(5025) as! FloatRatingView
        starRatingVw.emptyImage = UIImage(named: "empty")
        starRatingVw.fullImage   = UIImage(named: "full")
        starRatingVw.contentMode  = UIViewContentMode.ScaleAspectFit
        starRatingVw.rating = ratingAr[indexPath.row] as! Float
        let status = cell?.viewWithTag(701) as! UILabel
        if(statusArr[indexPath.row] as! NSObject == 0){
            status.text = " Profile Status: Pending"
        }else if(statusArr[indexPath.row] as! NSObject == 2){
            status.text = " Profile Status: Rejected"
        }
        else if(statusArr[indexPath.row] as! NSObject == 1){
            status.text = " Profile Status: Verified"
        }
        else if(statusArr[indexPath.row] as! NSObject == 4){
            status.text = " Profile Status: Cancelled"
        }
        else if(statusArr[indexPath.row] as! NSObject == 5){
            status.text = " Profile Status: Doctor Deactivated"
        }
        return cell!
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            return self.view.frame.size.height*0.22
        }else{
            if(self.view.frame.size.height < 1366){
                return self.view.frame.size.height*0.20
            }
            else{
                return self.view.frame.size.height*0.16
            }
        }
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let indexPath = myDocTableVw.indexPathForSelectedRow!
        let currentCell = myDocTableVw.cellForRowAtIndexPath(indexPath)! as UITableViewCell
        let  lbl = currentCell.viewWithTag(701) as! UILabel
        if(lbl.text == " Profile Status: Pending"){
            emailAdd.append(emailAr[indexPath.row] as! String)
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 1800
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.07,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.14), self.view.frame.size.height*0.35)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.16)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.25)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let verificationImg = UIImageView.init(frame: CGRectMake(12,12, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            verificationImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(verificationImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,12, self.view.frame.size.width*0.51, self.view.frame.size.width*0.06))
            verificationLbl.text = " Verification Pending"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+20,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+30, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.51))
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font  = UIFont(name: "Helvetica", size:14)
            }else{
                contentLbl.font  = UIFont(name: "Helvetica", size:19)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            let trimNameString = nameDataAr[indexPath.row].stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet());
            contentLbl.text = "Dr. \(trimNameString) has not accepted your request just yet.You can contact the doctor’s office through the tabs below in order to expedite the verification of your account."
            contentLbl.numberOfLines = 6
            alerVw.addSubview(contentLbl)
            
            let crossBtn = UIButton.init(frame: CGRectMake(alerVw.frame.size.width-(((alerVw.frame.size.width*0.130)/2)),-((alerVw.frame.size.width*0.130)/2), alerVw.frame.size.width*0.1,alerVw.frame.size.width*0.1))
            crossBtn.center = CGPointMake(CGRectGetMaxX(alerVw.frame), CGRectGetMinY(alerVw.frame))
            crossBtn.setBackgroundImage(UIImage(named: "crossBtn"), forState: .Normal)
            crossBtn.addTarget(self, action: #selector(MyDoctorsViewController.crossBtnAc), forControlEvents: .TouchUpInside)
            backVw.addSubview(crossBtn)
            let compPh = "\(phCodeDataAr[indexPath.row])\(phDataAr[indexPath.row])"
            ph = (compPh as NSString).integerValue
            
            let callBtn = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height/5), alerVw.frame.size.width/2,alerVw.frame.size.height/5))
            callBtn.setBackgroundImage(UIImage(named: "callButn"), forState: .Normal)
            callBtn.addTarget(self, action: #selector(MyDoctorsViewController.callBtnAc), forControlEvents: .TouchUpInside)
            alerVw.addSubview(callBtn)
            
            let emailBtn = UIButton.init(frame: CGRectMake(callBtn.frame.size.width,alerVw.frame.size.height-(alerVw.frame.size.height/5), alerVw.frame.size.width/2,alerVw.frame.size.height/5))
            emailBtn.setBackgroundImage(UIImage(named: "EmailBtn"), forState: .Normal)
            emailBtn.addTarget(self, action: #selector(MyDoctorsViewController.emailBtnAc), forControlEvents: .TouchUpInside)
            alerVw.addSubview(emailBtn)
        }else if(lbl.text == " Profile Status: Verified"){
            let vc = storyboard!.instantiateViewControllerWithIdentifier("DocrtorsBioViewController") as! DocrtorsBioViewController
            vc.adress = "\(addDataAr[indexPath.row])"
            vc.bio = "\(bioDataAr[indexPath.row])"
            vc.drname = "Dr. \(nameDataAr[indexPath.row])"
            vc.lang = "\(languageDataAr[indexPath.row])"
            var compPh = "+1(\(phCodeDataAr[indexPath.row]))\(phDataAr[indexPath.row])"
            compPh = compPh.insert("-", ind: 10)
            vc.phone = compPh
            vc.dr_Id = "\(dr_idAr[indexPath.row])"
            vc.quali = "\(qualiDataAr[indexPath.row])"
            vc.profilrPicUrl = profilepicAr[indexPath.row] as! NSString
            vc.speciality = "\(specialitydataAr[indexPath.row])"
            vc.docBio = false
            self.navigationController?.pushViewController(vc, animated: true)
        }else if(lbl.text == " Profile Status: Cancelled" || lbl.text == " Profile Status: Rejected"){
            drName = "\(nameDataAr[indexPath.row])"
            drEmailId = "\(emailAr[indexPath.row])"
            drDeviceToken = "\(devicetoknAr[indexPath.row])"
            drDeviceType = "\(devicetypeAr[indexPath.row])"
            dr_id = "\(dr_idAr[indexPath.row])"
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2369
            self.view.addSubview(backVw)
            
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.2)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.2)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "confirm")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Send Request"
            verificationLbl.font = UIFont.init(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font = UIFont.init(name: "Helvetica", size:14)
            }else{
                contentLbl.font = UIFont.init(name: "Helvetica", size:19)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "Are you sure you want to send request?"
            contentLbl.numberOfLines = 2
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width/2, alerVw.frame.size.height*0.22))
            okLbl.setTitle("Yes", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(MyDoctorsViewController.okBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            let lineLbl = UILabel.init(frame: CGRectMake(okLbl.frame.size.width-2,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),1.2, alerVw.frame.size.height*0.22))
            lineLbl.backgroundColor = UIColor.lightGrayColor()
            alerVw.addSubview(lineLbl)
            
            let noLbl = UIButton.init(frame: CGRectMake(okLbl.frame.size.width,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width/2, alerVw.frame.size.height*0.22))
            noLbl.setTitle("No", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                noLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                noLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            noLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            noLbl.titleLabel?.textAlignment = .Center
            noLbl.addTarget(self, action: #selector(MyDoctorsViewController.noAc), forControlEvents: .TouchUpInside)
            noLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(noLbl)
        }
    }
    
    //MARK:- ButtonAction
    func noAc(){
        let view = self.view.viewWithTag(2369)! as UIView
        view.removeFromSuperview()
    }
    func okBtnAc(){
        let view = self.view.viewWithTag(2369)! as UIView
        view.removeFromSuperview()
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
        let defaults = NSUserDefaults.standardUserDefaults()
        let firstname = defaults.valueForKey("firstName") as! String
        let lastname = defaults.valueForKey("lastName") as! String
        if let userId = defaults.valueForKey("id"){
            serverInt = 2
            let params = "Patient_id=\(userId)&Dr_id=\(dr_id)&DeviceToken=\(drDeviceToken)&Email=\(drEmailId)&DrName=\(drName)&PatientName=\(firstname) \(lastname)&DeviceType=\(drDeviceType)"
            AppManager.sharedManager.postDataOnserver(params, postUrl:"patients/request")
        }
    }
    func okBtnAc2(){
        let view = self.view.viewWithTag(2356)! as UIView
        view.removeFromSuperview()
        self.patDrList()
    }
    @IBAction func addDocAc(sender: AnyObject) {
        let vc = storyboard!.instantiateViewControllerWithIdentifier("SearchDoctorViewController") as! SearchDoctorViewController
        vc.searchDoc = false
        vc.sndRequ = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //    func addDocAc(){
    //        let vc = storyboard!.instantiateViewControllerWithIdentifier("SearchDoctorViewController") as! SearchDoctorViewController
    //        vc.searchDoc = false
    //        vc.sndRequ = true
    //        self.navigationController?.pushViewController(vc, animated: true)
    //    }
    func crossBtnAc(){
        let view = self.view.viewWithTag(1800)! as UIView
        view.removeFromSuperview()
    }
    func callBtnAc(){
        let view = self.view.viewWithTag(1800)! as UIView
        view.removeFromSuperview()
        if let url = NSURL(string: "tel://+1\(ph)") {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    func emailBtnAc(){
        if(MFMailComposeViewController.canSendMail()){
            let emailTitle = ""
            let messageBody = ""
            let toRecipents = emailAdd
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setMessageBody(messageBody, isHTML: false)
            mc.setToRecipients(toRecipents)
            self.presentViewController(mc, animated: true, completion: nil)
        }else{
            AppManager.sharedManager.Showalert("Alert", alertmessage: "No email account is configured with your device.")
        }
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
        
    }
    
    //MARK:- MailComposer
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?){
        statusBool = true
        switch result {
        case .Cancelled:
            status = "cancel"
            break
        case .Saved:
            status = "saved"
            break
        case .Sent:
            status = "sent"
            break
        case .Failed:
            err = "\(error?.localizedDescription)"
            status = err
            break
        default:
            break
            
        }
        let view = self.view.viewWithTag(1800)! as UIView
        view.removeFromSuperview()
        self.dismissViewControllerAnimated(true, completion: nil)
        emailAdd = []
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
//extension String {
//    func insert(string:String,ind:Int) -> String {
//        return  String(self.characters.prefix(ind)) + string + String(self.characters.suffix(self.characters.count-ind))
//    }
//}

extension MyDoctorsViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
