

import UIKit
import AVKit
import AVFoundation
import SlideMenuControllerSwift
class ViewController: UIViewController,WebServiceDelegate{
    @IBOutlet weak var btnWhoAreWe: UIButton!
    @IBOutlet weak var btnTelemedicine: UIButton!
    @IBOutlet weak var btnFAQ: UIButton!
    @IBOutlet weak var btnPtFAQ: UIButton!
    @IBOutlet weak var btnDoctor: UIButton!
    @IBOutlet weak var btnPateint: UIButton!
    @IBOutlet weak var btnClinic: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var clinicBackVw: UIView!
    @IBOutlet weak var btnDocStaff: UIButton!
    var drFaqContentText = NSString()
    var ptFaqContentText = NSString()
    var tele = NSString()
    var whoareWe = NSString()
    var appDelegate = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        clinicBackVw.hidden = true
        clinicBackVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.7)
        self.navigationController?.navigationBarHidden = true
        let backgroundImage = UIImageView(frame: UIScreen.mainScreen().bounds)
        backgroundImage.image = UIImage(named: "bg_image")
        self.view.insertSubview(backgroundImage, atIndex: 0)
        self.screeTextFont()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ViewController.handleTap(_:)))
        clinicBackVw.addGestureRecognizer(tap)
    }
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        clinicBackVw.hidden = true
    }
    override func viewWillAppear(animated: Bool) {
        clinicBackVw.hidden = true
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.postDataOnserver(" ", postUrl: "users/LandingApi")
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        dispatch_async(dispatch_get_main_queue()) {
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            if(responseDict.valueForKey("data") != nil){
                let data = responseDict.valueForKey("data") as! NSDictionary
                self.drFaqContentText = data.valueForKey("FaqDr") as! NSString
                self.ptFaqContentText = data.valueForKey("FaqPatient") as! NSString
                self.tele = data.valueForKey("TeliMedicine") as! NSString
                self.whoareWe = data.valueForKey("WhoAreWe") as! NSString
            }
        }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    
    @IBAction func playBtnAc(sender: AnyObject) {
        let path = NSBundle.mainBundle().pathForResource("Tik Tok Doc - Who We Are", ofType:"mp4")
        let url = NSURL.fileURLWithPath(path!)
        let player = AVPlayer(URL: url)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.presentViewController(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    @IBAction func whatIsTelemedicineAction(sender: AnyObject) {
        let url = NSURL(string: "\(tele)")!
        UIApplication.sharedApplication().openURL(url)
    }
    @IBAction func whoAreWeAction(sender: AnyObject) {
        let url = NSURL(string: "\(whoareWe)")!
        UIApplication.sharedApplication().openURL(url)
    }
    @IBAction func faqAction(sender: AnyObject) {
        let url = NSURL(string: "\(drFaqContentText)")!
        UIApplication.sharedApplication().openURL(url)
    }
    @IBAction func faqPtAction(sender: AnyObject) {
        let url = NSURL(string: "\(ptFaqContentText)")!
        UIApplication.sharedApplication().openURL(url)
    }
    @IBAction func clinicBtnAc(sender: AnyObject) {
        clinicBackVw.hidden = false
    }
    @IBAction func doctorAc(sender: AnyObject) {
        let vc = storyboard!.instantiateViewControllerWithIdentifier("DoctorLogInViewController") as! DoctorLogInViewController
        vc.UserType = 1
        self.navigationController?.pushViewController(vc, animated: true)
        vc.docPatLogin = true
    }
    @IBAction func patientAc(sender: AnyObject) {
        let vc = storyboard!.instantiateViewControllerWithIdentifier("DoctorLogInViewController") as! DoctorLogInViewController
        vc.snd = false
        vc.UserType = 0
        self.navigationController?.pushViewController(vc, animated: true)
        vc.docPatLogin = false
    }
    
    @IBAction func docStaffBtnAc(sender: AnyObject) {
        let vc = storyboard!.instantiateViewControllerWithIdentifier("StaffLoginViewController") as! StaffLoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func screeTextFont(){
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
