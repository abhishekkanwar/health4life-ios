

import UIKit
import RealmSwift
enum StaffRightMenu: Int {
    case Appointmnt = 0
    case scedule
    case Pateints
    case Profile
    case contactUs
    case logout
}
protocol StaffMenuProtocol : class {
    func changeViewController(menu: StaffRightMenu)
}
class StaffSlideViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,WebServiceDelegate{

    @IBOutlet weak var staffMemberName: UILabel!
    @IBOutlet weak var designationStaff: UILabel!
    @IBOutlet weak var staffPic: UIImageView!
    @IBOutlet weak var sideTblVw: UITableView!
    
    var arMenu = ["My Appointments", "My Schedules","My Patients","My Profile","Contact Us","Logout"]
    var arMenuImages = ["myAppointmnt", "dateIcon","myPateint","myProfile","ContactUs", "logout"]
    var mainViewController: UIViewController!
    var myPateints : UIViewController!
    var myAppointment : UIViewController!
    var myProfile : UIViewController!
    var setSchedule : UIViewController!
    var pescription : UIViewController!
    var logoutC : UIViewController!
    var editInfoVw : UIViewController!
    var contactUs : UIViewController!
    var serverInt = NSInteger()
    let realm = try! Realm()
    override func viewDidLoad(){
        super.viewDidLoad()
        sideTblVw.dataSource = self
        sideTblVw.delegate = self
        let defaults = NSUserDefaults.standardUserDefaults()
        let firstname:String = defaults.valueForKey("firstName") as! String
        let lastname:String = defaults.valueForKey("lastName") as! String
        let speciality:String = defaults.valueForKey("designation") as! String
        let imgUrl:String = defaults.valueForKey("profilePic") as! String
        if let url = NSURL(string: "http://\(imgUrl)") {
            staffPic.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        staffMemberName.text = "\(firstname) \(lastname)"
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: "HomeStethoscope")
        let offsetY: CGFloat = -8.0
        attachment.bounds = CGRectMake(0, offsetY, attachment.image!.size.width, attachment.image!.size.height)
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string:"")
        myString.appendAttributedString(attachmentString)
        let myString1 = NSMutableAttributedString(string:"\(speciality)")
        myString.appendAttributedString(myString1)
        designationStaff.textAlignment = .Center
        designationStaff.attributedText = myString
        
        staffPic.layer.cornerRadius = staffPic.frame.size.height/2
        staffPic.layer.borderWidth = staffPic.frame.size.height*0.03
        staffPic.layer.borderColor = UIColor.init(red: 206/255, green: 205/255, blue: 205/255, alpha: 1).CGColor
        staffPic.clipsToBounds = true
        
        let myAppointmentVw = storyboard!.instantiateViewControllerWithIdentifier("DoctorMyAppointmentsViewController") as! DoctorMyAppointmentsViewController
        myAppointmentVw.selectStaff = true
        self.myAppointment = UINavigationController(rootViewController: myAppointmentVw)
        
        let myPateintsVw = storyboard!.instantiateViewControllerWithIdentifier("DocMyPateintViewController") as! DocMyPateintViewController
        myPateintsVw.staff = true
        self.myPateints = UINavigationController(rootViewController: myPateintsVw)
        
        let myProfileVw = storyboard!.instantiateViewControllerWithIdentifier("StaffProfileViewController") as! StaffProfileViewController
        self.myProfile = UINavigationController(rootViewController: myProfileVw)
        
        let mySch = storyboard!.instantiateViewControllerWithIdentifier("TabBar") as! TabBarViewController
        
        let nc = UINavigationController(rootViewController: mySch)
        nc.navigationBarHidden = true
        self.setSchedule = nc
        
        let logoutVw = storyboard!.instantiateViewControllerWithIdentifier("mainViewController") as! ViewController
        self.logoutC = UINavigationController(rootViewController: logoutVw)
        
        let contactus = storyboard!.instantiateViewControllerWithIdentifier("ContactUsViewController") as! ContactUsViewController
        contactus.contactSwich = true
        self.contactUs = UINavigationController(rootViewController: contactus)

    }
    func changeViewController(menu: StaffRightMenu) {
        switch menu {
        case .Appointmnt:
            self.slideMenuController()?.changeMainViewController(self.myAppointment, close: true)
        case .Pateints:
            self.slideMenuController()?.changeMainViewController(self.myPateints, close: true)
        case .Profile:
            self.slideMenuController()?.changeMainViewController(self.myProfile, close: true)
        case .scedule:
            self.slideMenuController()?.changeMainViewController(self.setSchedule, close: true)
        case.contactUs:
            self.slideMenuController()?.changeMainViewController(self.contactUs, close: true)
        case.logout:
            let alert = UIAlertController(title: "Alert", message: "Are You Sure You Want To Logout?", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
                UIAlertAction in
                NSLog("OK Pressed")
                let defaults = NSUserDefaults.standardUserDefaults()
                if let userId = defaults.valueForKey("staffId") {
                    var token =  NSString()
                    if(UIDevice.isSimulator == true){
                        token = "b6c8e3e79f8116b3f0cc439867fa6eaca2a6b90357644b6077ebdca30af517be"
                    }else{
                        token = defaults.valueForKey("deviceToken") as! NSString
                    }
                    AppManager.sharedManager.delegate=self
                    AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Log Out")
                    let params = "UserId=\(userId)&DeviceToken=\(token)"
                    self.serverInt = 1
                    AppManager.sharedManager.postDataOnserver(params, postUrl: "users/logout")
                }
            }
            let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }

    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        dispatch_async(dispatch_get_main_queue()){
            if(self.serverInt == 1){
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("id")
                defaults.removeObjectForKey("usertype")
                defaults.removeObjectForKey("firstName")
                defaults.removeObjectForKey("drType")
                defaults.removeObjectForKey("staffId")
                defaults.removeObjectForKey("selectDrName")
                defaults.removeObjectForKey("selectDrImg")
                defaults.removeObjectForKey("selectDrSpeciality")
                defaults.removeObjectForKey("selectDrId")
                defaults.removeObjectForKey("msgCount")
                try! self.realm.write({
                    self.realm.deleteAll()
                })
                UIApplication.sharedApplication().cancelAllLocalNotifications()
                self.slideMenuController()?.changeMainViewController(self.logoutC, close: true)
            }
        }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    
    @IBAction func editBtnAc(sender: AnyObject) {
        let editVw = storyboard!.instantiateViewControllerWithIdentifier("StaffProfileViewController") as! StaffProfileViewController
        self.editInfoVw = UINavigationController(rootViewController: editVw)
        editVw.editable = true
        self.slideMenuController()?.changeMainViewController(self.editInfoVw, close: true)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arMenu.count
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            let  cellId:String = "cell"+String(indexPath.row)
            var cell  = tableView.dequeueReusableCellWithIdentifier(cellId)
            if (cell == nil){
                cell = UITableViewCell(style: .Default, reuseIdentifier:cellId)
            }
            let lbl_title = UILabel(frame: CGRectMake(self.view.frame.size.width*0.25, view.frame.size.height*0.01, view.frame.size.width*0.55,  view.frame.size.height*0.05))
            lbl_title.text = arMenu[indexPath.row] 
            lbl_title.font = UIFont(name: "Helvetica", size: self.view.frame.height*0.025)
            lbl_title.textColor = UIColor.blackColor()
            let image = UIImage(named:arMenuImages[indexPath.row] )
            let imageVW = UIImageView(frame: CGRectMake(self.view.frame.size.width*0.1,view.frame.size.height*0.012,view.frame.size.height*0.04, view.frame.size.height*0.04))
            imageVW.image = image
            let vwSelect = UIView(frame: CGRectMake(self.view.frame.size.width*0,view.frame.size.height*0,self.view.frame.size.width*0.027, self.view.frame.size.width*0.17))
            vwSelect.backgroundColor = UIColor.clearColor()
            vwSelect.tag = 900+indexPath.row
            cell?.contentView.addSubview(vwSelect)
            cell?.contentView.addSubview(imageVW)
            cell?.contentView.addSubview(lbl_title)
            return cell! as UITableViewCell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("cell")
            let drImg: UIImageView = (cell!.viewWithTag(11) as! UIImageView)
            drImg.image = UIImage(named: arMenuImages[indexPath.row] )
            
            let nameLbl: UILabel = (cell!.viewWithTag(12) as! UILabel)
            nameLbl.text = arMenu[indexPath.row] as? String
            let vwSelect = UIView(frame: CGRectMake(self.view.frame.size.width*0,view.frame.size.height*0,self.view.frame.size.width*0.027, self.view.frame.size.width*0.17))
            vwSelect.backgroundColor = UIColor.clearColor()
            vwSelect.tag = 900+indexPath.row
            cell?.contentView.addSubview(vwSelect)
            return cell! as UITableViewCell
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let  vw = self.view.viewWithTag(900+indexPath.row)
        vw?.backgroundColor = UIColor.clearColor()
        vw?.backgroundColor = UIColor.init(red: 10/255, green: 4/255, blue: 88/255, alpha: 1)
        if let menu = StaffRightMenu(rawValue: indexPath.item) {
        self.changeViewController(menu)
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.view.frame.size.width*0.17
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
