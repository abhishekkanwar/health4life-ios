

import UIKit
import SlideMenuControllerSwift
import IQKeyboardManagerSwift
import CoreLocation
import GooglePlaces
class MyInformationViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,WebServiceDelegate,UITextViewDelegate,UISearchBarDelegate,CLLocationManagerDelegate,UIActionSheetDelegate{

    @IBOutlet weak var nameText: UITextView!
    @IBOutlet weak var lastNameText: UITextView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dateOfBirthTxt: UITextView!
    @IBOutlet weak var emailAddressText: UITextView!
    @IBOutlet weak var phnoText: UITextView!
    @IBOutlet weak var addressText: UITextView!
    @IBOutlet weak var ZipCodeText: UITextView!
    @IBOutlet weak var pharmacyLblText: UITextView!
    @IBOutlet weak var pharmacyLbl: UILabel!
    @IBOutlet weak var arroImgVw: UIImageView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var myInfoimgVw: UIImageView!
    @IBOutlet weak var selfSchSaveBtn: UIButton!
    @IBOutlet weak var chngPswrdBtn: UIButton!
    @IBOutlet weak var phCodeText: UITextView!
    
    @IBOutlet weak var myInfoTittleLbl: UILabel!
    @IBOutlet weak var myInfoScrollVw: UIScrollView!
    
    @IBOutlet weak var firstNameVw: UIView!
    @IBOutlet weak var latNameVw: UIView!
    @IBOutlet weak var dobVw: UIView!
    @IBOutlet weak var emailAddVw: UIView!
    @IBOutlet weak var codeVw: UIView!
    @IBOutlet weak var phoneNumberVw: UIView!
    @IBOutlet weak var addressVw: UIView!
    @IBOutlet weak var zipcodeVw: UIView!
    @IBOutlet weak var pharmaVw: UIView!
    var server = NSInteger()
    var editabl = Bool()
    var date = NSDate()
    var dateS = NSString()
    var response = NSDictionary()
    var searchResultController: SearchResultsController!
    var resultsArray = [String]()
    var lat = String()
    var long = String()
    var pharm = NSString()
    var arCode = []
    var topConstraint = NSLayoutConstraint()
    var firstNm = NSString()
    var lastNm = NSString()
    var fieldName = NSString()
    
    var addData = NSString()
    var addressdata = NSString()
    var cityData = NSString()
    var stateData = NSString()
    var zipcodeData = NSString()
    var phonePrimaryData = NSString()
    var faxData = NSString()
    var timingData = NSString()
    var certifiedData = NSString()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSet()
         arCode = ["201", "202", "203", "204", "205", "206", "207", "208", "209", "210", "212", "213", "214", "215", "216", "217", "218", "219", "224", "225", "226", "228", "229", "231", "234", "239", "240", "242", "246", "248", "250", "251", "252", "253", "254", "256", "260", "262", "264", "267", "268", "269", "270", "276", "281", "284", "289", "301", "302", "303", "304", "305", "306", "307", "308", "309", "310", "312", "313", "314", "315", "316", "317", "318", "319", "320", "321", "323", "325", "330", "331", "334", "336", "337", "339", "340", "343", "345", "347", "351", "352", "360", "361", "385", "386", "401", "402", "403", "404", "405", "406", "407", "408", "409", "410", "412", "413", "414", "415", "416", "417", "418", "419", "423", "424", "425", "430", "432", "434", "435", "438", "440", "441", "442", "443", "450", "456", "458", "469", "470", "473", "475", "478", "479", "480", "484", "500", "501", "502", "503", "504", "505", "506", "507", "508", "509", "510", "512", "513", "514", "515", "516", "517", "518", "519", "520", "530", "533", "534", "539", "540", "541", "551", "559", "561", "562", "563", "567", "570", "571", "573", "574", "575", "579", "580", "581", "585", "586", "587", "600", "601", "602", "603", "604", "605", "606", "607", "608", "609", "610", "612", "613", "614", "615", "616", "617", "618", "619", "620", "623", "626", "630", "631", "636", "641", "646", "647", "649", "650", "651", "657", "660", "661", "662", "664", "670", "671", "678", "681", "682", "684", "669", "700", "701", "702", "703", "704", "705", "706", "707", "708", "709", "710", "712", "713", "714", "715", "716", "717", "718", "719", "720", "724", "727", "731", "732", "734", "740", "747", "754", "757", "758", "760", "762", "763", "765", "767", "769", "770", "772", "773", "774", "775", "778", "779", "780", "781", "784", "785", "786", "787", "800", "801", "802", "803", "804", "805", "806", "807", "808", "809", "810", "812", "813", "814", "815", "816", "817", "818", "819", "828", "829", "830", "831", "832", "843", "845", "847", "848", "849", "850", "855", "856", "857", "858", "859", "860", "862", "863", "864", "865", "866", "867", "868", "869", "870", "872", "876", "877", "878", "888", "900", "901", "902", "903", "904", "905", "906", "907", "908", "909", "910", "912", "913", "914", "915", "916", "917", "918", "919", "920", "925", "928", "931", "936", "937", "938", "939", "940", "941", "947", "949", "951", "952", "954", "956", "970", "971", "972", "973", "978", "979", "980", "985", "989"]
        searchResultController = SearchResultsController()
        let defaults = NSUserDefaults.standardUserDefaults()
        if let userId = defaults.valueForKey("id") {
            server = 1
            addressText.delegate = self
            nameText.delegate = self
            phnoText.delegate = self
            lastNameText.delegate = self
            phCodeText.delegate = self
            ZipCodeText.delegate = self
            
            addressText.autocapitalizationType = .Words
            nameText.autocapitalizationType = .Words
            lastNameText.autocapitalizationType = .Words
            
            addressText.tintColor = UIColor.blackColor()
            lastNameText.tintColor = UIColor.blackColor()
            nameText.tintColor = UIColor.blackColor()
            phnoText.tintColor = UIColor.blackColor()
            lastNameText.tintColor = UIColor.blackColor()
            phCodeText.tintColor = UIColor.blackColor()
            ZipCodeText.tintColor = UIColor.blackColor()
            AppManager.sharedManager.delegate=self
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.85)
            backVw.tag = 666
            self.view.addSubview(backVw)
            AppManager.sharedManager.showActivityIndicatorInView(backVw, withLabel: "Loading..")
            let params = "id=\(userId)"
            AppManager.sharedManager.postDataOnserver(params, postUrl: "users/details")
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updatePharmacyAction), name: "UpdatePharmacy", object: nil)
        }
       
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
        topConstraint = NSLayoutConstraint(item:dobVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: firstNameVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 30)
        self.view.addConstraints([topConstraint])
            latNameVw.hidden = true
        }
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().enable = true
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(MyInformationViewController.profileImageTapped(_:)))
        myInfoimgVw.userInteractionEnabled = true
        myInfoimgVw.addGestureRecognizer(tapGestureRecognizer1)
        self.navigationController?.navigationBarHidden = true
        
        nameText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        dateOfBirthTxt.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        phnoText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        phCodeText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        emailAddressText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        addressText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        ZipCodeText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        pharmacyLblText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        lastNameText.contentInset = UIEdgeInsetsMake(-6.7,0.0,0,0.0);
        self.addressText.textContainerInset = UIEdgeInsetsMake(10, 0,0, 0);

        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(MyInformationViewController.imageTapped(_:)))
        pharmaVw.userInteractionEnabled = true
        pharmaVw.addGestureRecognizer(tapGestureRecognizer)
        
      self.navigationController?.navigationBarHidden = true
        
        myInfoimgVw.layer.cornerRadius = myInfoimgVw.frame.size.height/2
        myInfoimgVw.layer.borderWidth = myInfoimgVw.frame.size.height*0.07
        myInfoimgVw.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        myInfoimgVw.clipsToBounds = true
        arroImgVw.hidden = true
        if(editabl == true){
            latNameVw.hidden = false
            lastNameText.editable = true
            nameLbl.text = "First Name"
            editBtn.hidden = true
            editabl = true
            lastNameText.editable = true
            chngPswrdBtn.hidden = true
            arroImgVw.hidden = false
            pharmacyLbl.text = " Edit pharmacy"
            myInfoTittleLbl.text = "Edit Information"
            phnoText.selectedRange = NSMakeRange(2, 0);
            selfSchSaveBtn.setBackgroundImage(UIImage(named: "saveBtn"), forState: .Normal)
            nameText.editable = true
            dateOfBirthTxt.editable = true
            phnoText.editable = true
            phCodeText.editable = true
            addressText.editable = true
            ZipCodeText.editable = true
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
             firstNameVw.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            }
          if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            let topConstraint1 = NSLayoutConstraint(item:latNameVw,attribute: NSLayoutAttribute.TopMargin,relatedBy: NSLayoutRelation.Equal,toItem: firstNameVw,attribute: NSLayoutAttribute.BottomMargin,multiplier: 1,constant: 30)
            self.view.addConstraints([topConstraint1])
            self.view.removeConstraint(topConstraint)
          }
        }
        else{
         selfSchSaveBtn.setBackgroundImage(UIImage(named: "selfScheduleBtn"), forState: .Normal)
        }
        let dateInputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, UIScreen.mainScreen().bounds.height*0.4))
        
        let datePickerView  : UIDatePicker = UIDatePicker(frame: CGRectMake(0, 40,UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height*0.36))
        datePickerView.datePickerMode = UIDatePickerMode.Date
        dateInputView.addSubview(datePickerView)
        let yesterday = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -1, toDate: NSDate(), options: [])!
        datePickerView.maximumDate = yesterday
        dateOfBirthTxt.inputView = dateInputView
        datePickerView.addTarget(self, action: #selector(MyInformationViewController.handlePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        handlePicker(datePickerView)
    }
    func viewSet(){
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone)
        {
            firstNameVw.backgroundColor = UIColor.whiteColor()
            latNameVw.backgroundColor = UIColor.whiteColor()
            addressVw.backgroundColor = UIColor.whiteColor()
            dobVw.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            codeVw.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            phoneNumberVw.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            emailAddVw.backgroundColor = UIColor.whiteColor()
            zipcodeVw.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
            pharmaVw.backgroundColor = UIColor.whiteColor()
        }else{
            firstNameVw.backgroundColor = UIColor.whiteColor()
            latNameVw.backgroundColor = UIColor.whiteColor()
            addressVw.backgroundColor = UIColor.whiteColor()
            codeVw.backgroundColor = UIColor.whiteColor()
            phoneNumberVw.backgroundColor = UIColor.whiteColor()
            emailAddVw.backgroundColor = UIColor.whiteColor()
            zipcodeVw.backgroundColor = UIColor.whiteColor()
            pharmaVw.backgroundColor = UIColor.whiteColor()
        }
        firstNameVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        firstNameVw.layer.shadowOffset = CGSizeMake(7, 5);
        firstNameVw.layer.shadowRadius = 0.5;
        firstNameVw.layer.shadowOpacity = 0.06;
        firstNameVw.layer.masksToBounds = false;
        firstNameVw.layer.borderWidth = 0.5
        
        latNameVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        latNameVw.layer.shadowOffset = CGSizeMake(7, 5);
        latNameVw.layer.shadowRadius = 0.5;
        latNameVw.layer.shadowOpacity = 0.06;
        latNameVw.layer.masksToBounds = false;
        latNameVw.layer.borderWidth = 0.5
        
        addressVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        addressVw.layer.shadowOffset = CGSizeMake(7, 5);
        addressVw.layer.shadowRadius = 0.5;
        addressVw.layer.shadowOpacity = 0.06;
        addressVw.layer.masksToBounds = false;
        addressVw.layer.borderWidth = 0.5
        
        dobVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        dobVw.layer.shadowOffset = CGSizeMake(7, 5);
        dobVw.layer.shadowRadius = 0.5;
        dobVw.layer.shadowOpacity = 0.06;
        dobVw.layer.masksToBounds = false;
        dobVw.layer.borderWidth = 0.5
        
        codeVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        codeVw.layer.shadowOffset = CGSizeMake(7, 5);
        codeVw.layer.shadowRadius = 0.5;
        codeVw.layer.shadowOpacity = 0.06;
        codeVw.layer.masksToBounds = false;
        codeVw.layer.borderWidth = 0.5
        
        phoneNumberVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        phoneNumberVw.layer.shadowOffset = CGSizeMake(7, 5);
        phoneNumberVw.layer.shadowRadius = 0.5;
        phoneNumberVw.layer.shadowOpacity = 0.06;
        phoneNumberVw.layer.masksToBounds = false;
        phoneNumberVw.layer.borderWidth = 0.5
        
        emailAddVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        emailAddVw.layer.shadowOffset = CGSizeMake(7, 5);
        emailAddVw.layer.shadowRadius = 0.5;
        emailAddVw.layer.shadowOpacity = 0.06;
        emailAddVw.layer.masksToBounds = false;
        emailAddVw.layer.borderWidth = 0.5
        
        zipcodeVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        zipcodeVw.layer.shadowOffset = CGSizeMake(7, 5);
        zipcodeVw.layer.shadowRadius = 0.5;
        zipcodeVw.layer.shadowOpacity = 0.06;
        zipcodeVw.layer.masksToBounds = false;
        zipcodeVw.layer.borderWidth = 0.5
        
        pharmaVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        pharmaVw.layer.shadowOffset = CGSizeMake(7, 5);
        pharmaVw.layer.shadowRadius = 0.5;
        pharmaVw.layer.shadowOpacity = 0.06;
        pharmaVw.layer.masksToBounds = false;
        pharmaVw.layer.borderWidth = 0.5
    }
    func updatePharmacyAction(notification: NSNotification){
        let pharmacy = notification.object as! String
        self.pharmacyLblText.text = pharmacy
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)

    }
    
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        let addr = searchResultController.address
        if(addr != ""){
            addressText.text = addr
            self.getLocationFromAddressString(addressText.text!)
        }
        else{
            let defaults = NSUserDefaults.standardUserDefaults()
            if let add = defaults.valueForKey("address") {
                addressText.text = add as! String
                self.getLocationFromAddressString(add as! String)
            }
        }
        return true;
    }
    func getLocationFromAddressString(loadAddressStr: String)  {
       
        var latitude :Double = 0
        var longitude : Double = 0
        let esc_addr = loadAddressStr.stringByAppendingFormat("%@","")
        let req = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=\(esc_addr)"
        let escapedAddress = req.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        let url: NSURL = NSURL(string: escapedAddress!)!
        let result:String? = try? String(contentsOfURL:url)
        if result != nil {
            let scanner: NSScanner = NSScanner(string: result!)
            if(scanner.scanUpToString("\"lat\" :", intoString: nil) && scanner.scanString("\"lat\" :", intoString: nil)){
                scanner.scanDouble(&latitude)
                if(scanner.scanUpToString("\"lng\" :", intoString: nil) && scanner.scanString("\"lng\" :", intoString: nil)){
                    scanner.scanDouble(&longitude)
                }
            }
        }
        var center :CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0);
        center.latitude = latitude;
        center.longitude = longitude;
        lat = String(format: "%.20f", center.latitude)
        long = String(format: "%.20f", center.longitude)
        
    }
    func textViewDidBeginEditing(textView: UITextView) {
        if(textView == addressText){
            fieldName = "Address"

        }
        else if(textView == phCodeText){

        }
        else if(textView == lastNameText){
            fieldName = "Last Name"
        }
        else if(textView == nameText){
            fieldName = "First Name"
        }
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(textView == addressText || textView == nameText || textView == lastNameText){
            let currentCharacterCount = textView.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + text.characters.count - range.length
            if(newLength == 36){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "This field does not contain more than 35 characters.")
            }
            return newLength <= 35
        }
        if(textView == ZipCodeText){
            let currentCharacterCount = textView.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + text.characters.count - range.length
            return newLength <= 7
        }
        if(textView == phnoText){
            let currentCharacterCount = textView.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + text.characters.count - range.length
            return newLength <= 7
        }
        if(textView == phCodeText){
            let currentCharacterCount = textView.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + text.characters.count - range.length
            return newLength <= 3
        }
        return true
    }
    func profileImageTapped(img: AnyObject){
    if(editabl == true){
        self.view.endEditing(true)
       let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        actionSheet.showInView(self.view)
       }
    }
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex){
            
        case 0:
            print("Cancel")
        case 1:
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .Camera
                self.presentViewController(imagePicker, animated: true, completion:nil)
              }
             })
            }else{
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .Camera
                    presentViewController(imagePicker, animated: true, completion:nil)
                }
            }
        case 2:
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .PhotoLibrary
                    self.presentViewController(imagePicker, animated: true, completion:nil)
                }
            })
            }else{
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .PhotoLibrary
                presentViewController(imagePicker, animated: true, completion:nil)
            }
            }
        default:
            print("Default")
        }
    }

    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        let selectedImage : UIImage = image
        myInfoimgVw.image=self.ResizeImage(selectedImage, targetSize: CGSizeMake(200.0, 200.0))
        myInfoimgVw.clipsToBounds = true
        self.dismissViewControllerAnimated(true, completion: nil)
    }
   func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
    let size = image.size
    
    let widthRatio  = targetSize.width  / image.size.width
    let heightRatio = targetSize.height / image.size.height
    
    var newSize: CGSize
    if(widthRatio > heightRatio) {
        newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
    } else {
        newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
    }
    let rect = CGRectMake(0, 0, newSize.width, newSize.height)
    
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.drawInRect(rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
}

    override func viewDidLayoutSubviews() {
        if((self.storyboard?.valueForKey("name"))! as! String == "Main"){
            if(editabl == true){
            myInfoScrollVw.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*1.07)
            }else{
               myInfoScrollVw.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*0.95)
            }
        }
    }
    func imageTapped(img: AnyObject)
    {
        if(editabl == true){
           let defaults = NSUserDefaults.standardUserDefaults()
            defaults.removeObjectForKey("userLoc")
           let vc = storyboard!.instantiateViewControllerWithIdentifier("SelectPharmacyViewController") as! SelectPharmacyViewController
            vc.nextBool = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let vc = storyboard!.instantiateViewControllerWithIdentifier("PharmacySelectionViewController") as! PharmacySelectionViewController
            vc.pharmacyName = addData 
            vc.pharmaAdd = addressdata
            vc.pharmaFax = faxData
            vc.pharmaCertified = certifiedData
            vc.pharmaCity = cityData 
            vc.pharmaHours = timingData
            vc.pharmaState = stateData
            vc.pharmaPhone = phonePrimaryData
            vc.pharmaZipcode = zipcodeData
            vc.nxt = false
            vc.btnExist = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func selfSchAc(sender: AnyObject) {
        if(editabl == true){
            let firstName = nameText.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let lastName = lastNameText.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let trimNameString1 = phnoText.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            let trimNameString2 = addressText.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let trimNameString3 = ZipCodeText.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let trimNameString4 = phCodeText.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            if(!(arCode.containsObject(trimNameString4))){
                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                backVw.tag = 2369
                self.view.addSubview(backVw)
                
                let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21))
                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                alerVw.backgroundColor = UIColor.whiteColor()
                backVw.addSubview(alerVw)
                
                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                rightImg.image = UIImage(named: "verifyImg")
                alerVw.addSubview(rightImg)
                
                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                verificationLbl.text = " Alert"
                verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(verificationLbl)
                
                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                lineVw.backgroundColor = UIColor.grayColor()
                alerVw.addSubview(lineVw)
                
                let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.39))
                contentLbl.font = UIFont(name: "Helvetica", size:14)
                contentLbl.textAlignment = .Center
                contentLbl.textColor = UIColor.grayColor()
                contentLbl.text = "Please Enter a Valid Area Code."
                contentLbl.numberOfLines = 3
                alerVw.addSubview(contentLbl)
                
                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
                okLbl.setTitle("OK", forState: .Normal)
                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                okLbl.titleLabel?.textAlignment = .Center
                okLbl.addTarget(self, action: #selector(MyInformationViewController.okBtnAc), forControlEvents: .TouchUpInside)
                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(okLbl)
                return
            }
          else if(trimNameString3.characters.count != 5 && trimNameString3.characters.count < 5){
                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                backVw.tag = 2369
                self.view.addSubview(backVw)
                
                let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21))
                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                alerVw.backgroundColor = UIColor.whiteColor()
                backVw.addSubview(alerVw)
                
                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                rightImg.image = UIImage(named: "verifyImg")
                alerVw.addSubview(rightImg)
                
                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                verificationLbl.text = " Alert"
                verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(verificationLbl)
                
                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                lineVw.backgroundColor = UIColor.grayColor()
                alerVw.addSubview(lineVw)
                
                let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
                contentLbl.font = UIFont(name: "Helvetica", size:14)
                contentLbl.textAlignment = .Center
                contentLbl.textColor = UIColor.grayColor()
                contentLbl.text = "Please Enter valid zipcode"
                contentLbl.numberOfLines = 1
                alerVw.addSubview(contentLbl)
                
                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
                okLbl.setTitle("OK", forState: .Normal)
                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                okLbl.titleLabel?.textAlignment = .Center
                okLbl.addTarget(self, action: #selector(MyInformationViewController.okBtnAc), forControlEvents: .TouchUpInside)
                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(okLbl)
                return
            }
            else if(trimNameString3.characters.count != 9 && trimNameString3.characters.count > 5){
                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                backVw.tag = 2369
                self.view.addSubview(backVw)
                
                let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21))
                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                alerVw.backgroundColor = UIColor.whiteColor()
                backVw.addSubview(alerVw)
                
                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                rightImg.image = UIImage(named: "verifyImg")
                alerVw.addSubview(rightImg)
                
                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                verificationLbl.text = " Alert"
                verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(verificationLbl)
                
                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                lineVw.backgroundColor = UIColor.grayColor()
                alerVw.addSubview(lineVw)
                
                let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
                contentLbl.font = UIFont(name: "Helvetica", size:14)
                contentLbl.textAlignment = .Center
                contentLbl.textColor = UIColor.grayColor()
                contentLbl.text = "Please Enter valid zipcode"
                contentLbl.numberOfLines = 1
                alerVw.addSubview(contentLbl)
                
                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
                okLbl.setTitle("OK", forState: .Normal)
                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                okLbl.titleLabel?.textAlignment = .Center
                okLbl.addTarget(self, action: #selector(MyInformationViewController.okBtnAc), forControlEvents: .TouchUpInside)
                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(okLbl)
                return
            }
            else if(trimNameString4.characters.count != 3 || trimNameString1.characters.count != 7){
                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                backVw.tag = 2369
                self.view.addSubview(backVw)
                
                let alerVw = UIView.init(frame: CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.3)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.21))
                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                alerVw.backgroundColor = UIColor.whiteColor()
                backVw.addSubview(alerVw)
                
                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                rightImg.image = UIImage(named: "verifyImg")
                alerVw.addSubview(rightImg)
                
                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                verificationLbl.text = " Alert"
                verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(verificationLbl)
                
                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                lineVw.backgroundColor = UIColor.grayColor()
                alerVw.addSubview(lineVw)
                
                let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
                contentLbl.font = UIFont(name: "Helvetica", size:14)
                contentLbl.textAlignment = .Center
                contentLbl.textColor = UIColor.grayColor()
                contentLbl.text = "Please enter 10 digit phone number."
                contentLbl.numberOfLines = 2
                alerVw.addSubview(contentLbl)
                
                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width, alerVw.frame.size.height*0.22))
                okLbl.setTitle("OK", forState: .Normal)
                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                okLbl.titleLabel?.textAlignment = .Center
                okLbl.addTarget(self, action: #selector(PatientSetScheduleViewController.okBtnAc), forControlEvents: .TouchUpInside)
                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(okLbl)
                return
            }
            
            if(lat == ""){
                self.getLocationFromAddressString(addressText.text!)
            }
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("id") {
                let image : UIImage = myInfoimgVw.image!
                let  mImageData              = UIImagePNGRepresentation(image)! as NSData
                let base64String = mImageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.EncodingEndLineWithLineFeed)
                AppManager.sharedManager.delegate = self
                if(firstName == ""){
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the FirstName")
                    return
                }else if(lastName == ""){
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the LastName")
                    return
                }else if(trimNameString3 == ""){
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the Zipcode")
                    return
                }else if(trimNameString1 == ""){
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the phone")
                    return
                }else if(trimNameString2 == ""){
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill the Address")
                    return
                }
                if(firstName == "" || lastName == "" || trimNameString1 == "" || trimNameString2 == "" || trimNameString3 == ""){
                  AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Fill All The Fields")
                    return
                }
                let params = "FirstName=\(firstName)&LastName=\(lastName)&Mobile=\(trimNameString1)&Code=\(trimNameString4)&Address=\(trimNameString2)&Lat=\(lat)&Long=\(long)&DOB=\(dateOfBirthTxt.text!)&id=\(userId)&Zipcode=\(trimNameString3)&ProfilePic=\(base64String)"
                server = 2
                firstNm = firstName
                lastNm = lastName
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                AppManager.sharedManager.postDataOnserver(params, postUrl: "users/useredit")
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
             myInfoScrollVw.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
                }
            }
        }else{
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("id") {
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading..")
                let params = "Patient_id=\(userId)"
                server = 3
                AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/PatientDrList")
            }

       }
    }
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    func okBtnAc(){
        let view = self.view.viewWithTag(2369)! as UIView
        view.removeFromSuperview()
    }

    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString)
    {
        if(server == 1){
            dispatch_async(dispatch_get_main_queue()){
                self.response = responseDict
                let dict = responseDict["data"]
                let view = self.view.viewWithTag(666)! as UIView
                view.removeFromSuperview()
                AppManager.sharedManager.hideActivityIndicatorInView(view)
                let name:String = dict!["FirstName"]  as! String
                self.firstNm = "\(name)"
                let lastName:String = dict!["LastName"] as! String
                self.lastNm = "\(lastName)"
                self.lastNameText.text = self.lastNm as String
                let birth:String = dict!["DOB"] as! String
                let email:String = dict!["Email"] as! String
                let phone = dict!["Mobile"] as! NSString
                let code = dict!["Code"] as! NSString
                let zipcode:String = dict!["Zipcode"] as! String
                let address:String = dict!["Address"] as! String
                let pharmaName:String = dict!["PharmacyName"] as! String
                let pharmacy = "\(pharmaName)"
                let pic:String = dict!["ProfilePic"] as! String
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(address, forKey: "address")
                if let url = NSURL(string: "http://\(pic)") {
                    self.myInfoimgVw.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
                }
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                if(self.editabl == true){
                    self.nameText.text = self.firstNm as String
                }else{
                   self.nameText.text = "\(name) \(lastName)"
                }
                }else{
                  self.nameText.text = "\(name)"
                   self.lastNameText.text = "\(lastName)"
                }
                self.dateOfBirthTxt.text = birth
                self.emailAddressText.text = email
                self.phnoText.text = "\(phone)"
                self.addressText.text = address
                self.ZipCodeText.text = "\(zipcode)"
                self.pharmacyLblText.text = pharmacy
                self.phCodeText.text = "\(code)"
                self.addData = (dict?.valueForKeyPath("PharmacyDetails.StoreName"))! as! NSString
                self.addressdata = (dict?.valueForKeyPath("PharmacyDetails.Address"))! as! NSString
                self.cityData = (dict?.valueForKeyPath("PharmacyDetails.City"))! as! NSString
                self.stateData = (dict?.valueForKeyPath("PharmacyDetails.State"))! as! NSString
                self.timingData = (dict?.valueForKeyPath("PharmacyDetails.Hours24"))! as! NSString
                self.faxData = (dict?.valueForKeyPath("PharmacyDetails.Fax"))! as! NSString
                self.zipcodeData = (dict?.valueForKeyPath("PharmacyDetails.Zip"))! as! NSString
                self.phonePrimaryData = (dict?.valueForKeyPath("PharmacyDetails.PhonePrimary"))! as! NSString
            }
        }
        else if(server == 2){
           
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
             dispatch_async(dispatch_get_main_queue()) {
                 self.response = responseDict
                let data = responseDict.valueForKey("data") as! NSDictionary
                print(data)
                let imgUrl = data.valueForKey("ProfilePic") as! NSString
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(imgUrl, forKey: "profilePic")
                self.addData = (data.valueForKeyPath("PharmacyId.StoreName"))! as! NSString
                self.addressdata = (data.valueForKeyPath("PharmacyId.Address"))! as! NSString
                self.cityData = (data.valueForKeyPath("PharmacyId.City"))! as! NSString
                self.stateData = (data.valueForKeyPath("PharmacyId.State"))! as! NSString
                self.timingData = (data.valueForKeyPath("PharmacyId.Hours24"))! as! NSString
                self.faxData = (data.valueForKeyPath("PharmacyId.Fax"))! as! NSString
                self.zipcodeData = (data.valueForKeyPath("PharmacyId.Zip"))! as! NSString
                self.phonePrimaryData = (data.valueForKeyPath("PharmacyId.PhonePrimary"))! as! NSString
            self.editBtn.hidden = false
            self.backBtn.hidden = false
            self.arroImgVw.hidden = true
            self.chngPswrdBtn.hidden = false
            self.selfSchSaveBtn.setBackgroundImage(UIImage(named: "selfScheduleBtn"), forState: .Normal)
            self.pharmacyLbl.text = " Pharmacy"
            self.nameText.editable = false
            self.dateOfBirthTxt.editable = false
            self.emailAddressText.editable = false
            self.phnoText.editable = false
            self.phCodeText.editable = false
            self.addressText.editable = false
            self.ZipCodeText.editable = false
            self.lastNameText.editable = false
            
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                self.nameLbl.text = "Name"
                self.latNameVw.hidden = true
                self.nameText.text = "\(self.nameText.text!) \(self.lastNameText.text!)"
                 self.view.addConstraints([self.topConstraint])
            }
            self.lastNameText.editable = false
            self.myInfoTittleLbl.text = "My Information"
           
            self.editabl = false
            AppManager.sharedManager.Showalert("Alert", alertmessage: "Your Profile Information has been updated successfully")
            }
        }else if(server == 3){
            AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            dispatch_async(dispatch_get_main_queue()) {
            if( responseDict.objectForKey("data") != nil){
                let docMutableAr = NSMutableArray()
                let docidAr = NSMutableArray()
                let dataDict = responseDict.objectForKey("data") as! NSArray
                for i in 0...dataDict.count-1 {
                    let d = dataDict[i]
                    let status = d.valueForKey("status") as! NSInteger
                    if(status == 1){
                        docMutableAr.addObject(d.valueForKey("Name")!)
                        docidAr.addObject(d.valueForKey("Dr_id")!)
                    }
                }
                if(docMutableAr.count != 0){
                    var storyboard = UIStoryboard()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                    }else{
                        storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    }
                    let vc = storyboard.instantiateViewControllerWithIdentifier("PatientSetScheduleViewController") as! PatientSetScheduleViewController
                    self.navigationController!.pushViewController(vc, animated: true)
                    
                }else{
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Your doctor has not verified your account yet. Please contact your doctor’s office to expedite this process or send a new request.")
                }
            }
            else{
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Your doctor has not verified your account yet. Please contact your doctor’s office to expedite this process or send a new request.")
            }
        }
        }
    }
    func failureRsponseError(failureError: NSError) {
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    @IBAction func editBtnAc(sender: AnyObject){
        latNameVw.hidden = false
        lastNameText.editable = true
        nameLbl.text = "First Name"
        nameText.text = firstNm as String
        lastNameText.text = lastNm as String
        editBtn.hidden = true
        editabl = true
        lastNameText.editable = true
        chngPswrdBtn.hidden = true
        arroImgVw.hidden = false
        pharmacyLbl.text = " Edit pharmacy"
        myInfoTittleLbl.text = "Edit Information"
        phnoText.selectedRange = NSMakeRange(2, 0);
        selfSchSaveBtn.setBackgroundImage(UIImage(named: "saveBtn"), forState: .Normal)
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
         firstNameVw.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
        }
        nameText.editable = true
        dateOfBirthTxt.editable = true
        phnoText.editable = true
        phCodeText.editable = true
        addressText.editable = true
        ZipCodeText.editable = true
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
        let topConstraint1 = NSLayoutConstraint(
            item:latNameVw,
            attribute: NSLayoutAttribute.TopMargin,
            relatedBy: NSLayoutRelation.Equal,
            toItem: firstNameVw,
            attribute: NSLayoutAttribute.BottomMargin,
            multiplier: 1,
            constant: 30)
        self.view.addConstraints([topConstraint1])
        self.view.removeConstraint(topConstraint)
        }
        nameText.becomeFirstResponder()
    }
    func handlePicker(sender: UIDatePicker){
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd,yyyy"
        dateS = dateFormatter.stringFromDate(sender.date)
        dateOfBirthTxt.text = dateS as String
        date = sender.date
     }
    @IBAction func backBtnAc(sender: AnyObject) {
        let firstName: String = response.valueForKeyPath("data.FirstName") as! String
        let lastName:String   = response.valueForKeyPath("data.LastName") as! String
        let addTxt:String     = response.valueForKeyPath("data.Address") as! String
        let defaults          = NSUserDefaults.standardUserDefaults()
        defaults.setObject(firstName, forKey: "firstName")
        defaults.setObject(lastName, forKey: "lastName")
        defaults.setObject(addTxt, forKey: "address")
    self.slideMenuController()?.navigationController?.popViewControllerAnimated(true)
        
    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        let placeClient = GMSPlacesClient()
        placeClient.autocompleteQuery(searchText, bounds: nil, filter: nil) { (data, error: NSError?) -> Void in
            self.resultsArray.removeAll()
        }
        placeClient.autocompleteQuery(searchText, bounds: nil, filter: nil) { (results, error: NSError?) -> Void in
            self.resultsArray.removeAll()
            if results == nil {
                return
            }
            for result in results! {
                if let result = result as? GMSAutocompletePrediction {
                    self.resultsArray.append(result.attributedFullText.string)
                }
            }
            self.searchResultController.reloadDataWithArray(self.resultsArray)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension MyInformationViewController : SlideMenuControllerDelegate {
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
    extension UIImage {
        func resizeWithPercentage(percentage: CGFloat) -> UIImage? {
            let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
            imageView.contentMode = .ScaleAspectFit
            imageView.image = self
            UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
            guard let context = UIGraphicsGetCurrentContext() else { return nil }
            imageView.layer.renderInContext(context)
            guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
            UIGraphicsEndImageContext()
            return result
        }
        
}
