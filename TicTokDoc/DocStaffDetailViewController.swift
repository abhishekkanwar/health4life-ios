

import UIKit
import RealmSwift
import CZPicker
class DocStaffDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, CZPickerViewDelegate, CZPickerViewDataSource{
    
    @IBOutlet weak var staffPic: UIImageView!
    @IBOutlet weak var staffInfoTableVw: UITableView!
    @IBOutlet weak var chatNwBtn: UIButton!
    
    var imgAr = ["SignUpUser","gnder","phone","ForgotPaswrdEmail","designation","HomeStethoscope"]
    var itemAr = ["Name","Gender","Phone number","Email","Title","Associated Doctors"]
    var valueAr = NSMutableArray()
    var nameAr = NSMutableArray()
    var apiKey = NSString()
    var sessionId = NSString()
    var Token = NSString()
    var groupId = NSString()
    var realm = try! Realm()
    var phone = NSString()
    var imgUrl = NSString()
    var staffIdAr = NSString()
    var staffStatus = NSInteger()
    var serverInt = NSInteger()
    var tblVwNo = NSInteger()
    override func viewDidLoad() {
        super.viewDidLoad()
        phone = valueAr.objectAtIndex(2) as! String
        
        if(staffStatus == 1){
            chatNwBtn.hidden = false
        }else{
            chatNwBtn.hidden = true
        }
        
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        serverInt = 2
        self.doRequestPost("\(Header.BASE_URL)staffapi/staffDrList", data: ["StaffId":"\(staffIdAr)"])
        
        
        staffPic.layer.cornerRadius = staffPic.frame.size.height/2
        staffPic.layer.borderWidth = staffPic.frame.size.height*0.07
        staffPic.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        staffPic.clipsToBounds = true
        if let url = NSURL(string: "http://\(imgUrl)") {
            self.staffPic.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        
    }
    
    //MARK:- Touche&Tap
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func imageTap(img: AnyObject){
        let alert = UIAlertController(title: "Please confirm your call", message: "You’re about to call the following:\n\(phone)", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            if let url = NSURL(string: "tel://\(self.phone)") {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    func imageTap2(img: AnyObject){
        print(nameAr)
        let picker = CZPickerView(headerTitle: "Associated Doctor", cancelButtonTitle: "", confirmButtonTitle: "")
        picker.delegate = self
        picker.dataSource = self
        picker.show()
    }
    func numberOfRowsInPickerView(pickerView: CZPickerView!) -> Int {
        return nameAr.count
    }
    func czpickerView(pickerView: CZPickerView!, titleForRow row: Int) -> String! {
        return nameAr[row] as! String
    }
    
    //MARK:- ButtonAction&ChatNavigate
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func chatNwBtn(sender: AnyObject) {
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        let defaults = NSUserDefaults.standardUserDefaults()
        let usertype = defaults.valueForKey("usertype") as! NSInteger
        if(usertype == 1 || usertype == 0){
            let userId = defaults.valueForKey("id") as! String
            self.doRequestPost("\(Header.BASE_URL)docter/DRdrcreatetoken", data: ["From":"\(userId)","To":"\(staffIdAr)"])
            serverInt = 1
        }
        else if(usertype == 3){
            let userId = defaults.valueForKey("staffId") as! String
            self.doRequestPost("\(Header.BASE_URL)docter/DRdrcreatetoken", data: ["From":"\(userId)","To":"\(staffIdAr)"])
            serverInt = 1
        }
    }
    
    func navigateToChatView(user:NSDictionary) -> Void {
        
        let chatResults = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", user.valueForKey("GroupId") as! String))
        
        let msgArray = NSMutableArray()
        
        for group in chatResults
        {
            var mySelf = false
            let msgDict = group
            
            let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
            if(userType == 1 || userType == 0 || userType == 2){
                if(msgDict.fromId == (NSUserDefaults.standardUserDefaults().valueForKey("id") as! String))
                {
                    mySelf = true
                }
            }else if(userType == 3){
                if(msgDict.fromId == (NSUserDefaults.standardUserDefaults().valueForKey("staffId") as! String))
                {
                    mySelf = true
                }
            }
            
            let mdict = ["msg":msgDict.message,"bool":mySelf, "msgType":msgDict.messageType,"name":msgDict.displayName]
            msgArray.addObject(mdict)
        }
        
        let firstname:String = NSUserDefaults.standardUserDefaults().valueForKey("firstName") as! String
        
        let vc = storyboard!.instantiateViewControllerWithIdentifier("ChatViewController") as! ChatViewController
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let usertype = defaults.valueForKey("usertype") as! NSInteger
        if(usertype == 3){
            vc.userId = defaults.valueForKey("staffId") as! String
        }else{
            vc.userId = defaults.valueForKey("id") as! String
        }
        
        vc.chatUserId = user.valueForKey("GroupId") as! String
        
        vc.userName = user.valueForKey("Name") as! String
        
        
        vc.Token = user.valueForKey("TokenData") as! String
        vc.SessionID = user.valueForKey("sessionData") as! String
        vc.apiKey = user.valueForKey("ApiKey") as! String
        vc.msgAr = msgArray
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func msgCountList(groupId:String,completion: (result: NSDictionary)->()) -> Void {
        let paramString = "GroupId=\(groupId)"
        AppManager.sharedManager.generateGroupIdForUser("messageChat/listChat", params: paramString) { (result) in
            completion(result: result)
        }
    }
    
    //MARK:- ServerResponce
    func doRequestPost(url:String,data:[String: NSObject]){
        let requestDic = data
        let theJSONData = try? NSJSONSerialization.dataWithJSONObject(
            data ,
            options: NSJSONWritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!,
                                  encoding: NSASCIIStringEncoding)
        let session = NSURLSession.sharedSession()
        
        let urlPath = NSURL(string: url)
        let request = NSMutableURLRequest(URL: urlPath!)
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:true)
        
        let dataTask = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if((error) != nil) {
                print(error!.localizedDescription)
                var alert = UIAlertController()
                if(AppManager.NetWorkReachability.isConnectedToNetwork() == false){
                    alert = UIAlertController(title: "Alert", message: "Network Connection Lost.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                }else{
                    alert = UIAlertController(title: "Alert", message: "Could Not Connect To The Server.", preferredStyle: UIAlertControllerStyle.Alert)
                }
                
                let cancelAction = UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default) {
                    UIAlertAction in
                    self.doRequestPost(url, data: requestDic)
                }
                alert.addAction(cancelAction)
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
            }else {
                print("Succes:")
                let responceString = NSString(data: data!, encoding:NSUTF8StringEncoding)
                let _: NSError?
                let responseDict: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                dispatch_async(dispatch_get_main_queue())
                {
                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                    if(self.serverInt == 1){
                        if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 200){
                            self.apiKey = responseDict.valueForKeyPath("data.apiKey") as! String
                            self.sessionId = responseDict.valueForKeyPath("data.sessionData") as! String
                            self.Token = responseDict.valueForKeyPath("data.TokenData") as! String
                            self.groupId = responseDict.valueForKeyPath("data.groupId") as! String
                        }
                        
                        let user = NSMutableDictionary()
                        user.setValue(self.Token, forKey: "TokenData")
                        user.setValue(self.apiKey, forKey: "ApiKey")
                        user.setValue(self.groupId, forKey: "GroupId")
                        user.setValue(self.sessionId, forKey: "sessionData")
                        user.setValue(self.valueAr[0],forKey: "Name")
                        let groupIdA = self.groupId as String
                        self.msgCountList(groupIdA) { (result) in
                            let chatCount = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", groupIdA))
                            if(chatCount.count < result.valueForKey("count") as! Int){
                                var userId = NSString()
                                let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSInteger
                                if(userType == 3){
                                    userId = NSUserDefaults.standardUserDefaults().valueForKey("staffId") as! String
                                }else{
                                    userId = NSUserDefaults.standardUserDefaults().valueForKey("id") as! String
                                }
                                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                                RealmInteractor.shared.getMsgsFromServer(userId as String, groupId:groupIdA) { (result) in
                                    AppManager.sharedManager.hideActivityIndicatorInView(self.view)
                                    
                                    if(result.valueForKey("status") as! Int == 200)
                                    {
                                        let chatResults = self.realm.objects(Group.self).filter(NSPredicate.init(format: "groupId = %@", groupIdA))
                                        try! self.realm.write({
                                            self.realm.delete(chatResults)
                                        })
                                        
                                        let chatArray = result.objectForKey("data") as! NSArray
                                        
                                        for dict in chatArray
                                        {
                                            try! self.realm.write({
                                                
                                                let group = Group()
                                                group.displayName = dict.valueForKey("DisplayName") as! String
                                                group.message = dict.valueForKey("message") as! String
                                                group.messageType = dict.valueForKey("messageType") as! String
                                                group.fromId = dict.valueForKey("From") as! String
                                                group.groupId = dict.valueForKey("GroupId") as! String
                                                self.realm.add(group)
                                                
                                            })
                                            
                                        }
                                        self.navigateToChatView(user)
                                    }
                                }
                            }
                            else
                            {
                                self.navigateToChatView(user)
                            }
                        }
                        
                    }else{
                       
                        if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 200){
                            let data = responseDict.valueForKey("data") as! NSArray
                            for i in 0...data.count-1{
                                let statusD = data[i].valueForKey("StaffStatus") as! NSInteger
                                let name = data[i].valueForKey("Name") as! NSString
                                if(statusD == 1){
                                    self.nameAr.addObject(name)
                                }
                            }
                            self.tblVwNo = 6
                            if(self.nameAr.count > 1){
                                self.valueAr.addObject("\(self.nameAr[0]),\(self.nameAr[1])")
                            }
                            else{
                                self.valueAr.addObject("\(self.nameAr[0])")
                            }
                        }
                        else{
                            self.tblVwNo = 5
                        }
                        self.staffInfoTableVw.delegate = self
                        self.staffInfoTableVw.dataSource = self
                        self.staffInfoTableVw.reloadData()
                    }
                }
            }
            
        }
        dataTask.resume()
    }
    
    //MARK:- TableViewDelegate&Datasource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblVwNo
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        cell!.layer.cornerRadius = 7.0
        cell!.layer.masksToBounds = true
        
        let imageView: UIView = (cell?.viewWithTag(11))!
        imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
        imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
        imageView.layer.shadowOffset = CGSizeMake(10, 5);
        imageView.layer.shadowRadius = 1;
        imageView.layer.shadowOpacity = 0.06;
        imageView.layer.masksToBounds = false;
        imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        imageView.layer.borderWidth = 0.5
        
        if(indexPath.row == 5){
            let image = UIImage(named:"RightArrow")
            let imageVW = UIImageView(frame: CGRectMake(self.view.frame.size.width*0.8,view.frame.size.height*0.045,self.view.frame.size.height*0.018, self.view.frame.size.height*0.023))
            imageVW.image = image
            cell?.contentView.addSubview(imageVW)
        }
        if(indexPath.row == 2){
            let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(DocStaffDetailViewController.imageTap(_:)))
            imageView.userInteractionEnabled = true
            imageView.addGestureRecognizer(tapGestureRecognizer)
        }
        if(indexPath.row == 5){
            let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(DocStaffDetailViewController.imageTap2(_:)))
            imageView.userInteractionEnabled = true
            imageView.addGestureRecognizer(tapGestureRecognizer)
        }
        if(indexPath.row%2 == 0){
            imageView.backgroundColor = UIColor.whiteColor()
        }else{
            imageView.backgroundColor = UIColor.init(red: 238/255, green: 239/255, blue: 240/255, alpha: 1)
        }
        
        let itemImg: UIImageView = (cell!.viewWithTag(12) as! UIImageView)
        itemImg.image = UIImage(named: imgAr[indexPath.row])!
        let valueLbl: UILabel = (cell!.viewWithTag(14) as! UILabel)
        valueLbl.text = valueAr[indexPath.row] as? String
        
        let itemLbl: UILabel = (cell!.viewWithTag(13) as! UILabel)
        itemLbl.text = itemAr[indexPath.row]
        return cell!
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return self.view.frame.size.width*0.1
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
