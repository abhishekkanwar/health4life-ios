  

import UIKit

class ForgotPasswordViewController: UIViewController,WebServiceDelegate {

    @IBOutlet weak var emailVw: UIView!
    @IBOutlet weak var emailFld: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        emailVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        emailVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        emailVw.layer.masksToBounds = false;
        emailVw.layer.borderWidth = 0.5
        
       emailFld.keyboardType = .EmailAddress
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
         dispatch_async(dispatch_get_main_queue()) {
            if let dic:String = responseDict.valueForKey("error") as? String {
               AppManager.sharedManager.Showalert("Alert", alertmessage: "Email id does not exist.")  
            }else if(responseDict.valueForKey("status") as! Int == 401){
               AppManager.sharedManager.Showalert("Alert", alertmessage: "Inactive User.")
            }else if(responseDict.valueForKey("status") as! Int == 601){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "Email id already exist with another account.")
            }
            else{
                self.emailFld.text = ""
                let alert = UIAlertController(title: "Alert", message: "A new password has been sent to your Email address.", preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                    self.navigationController?.popViewControllerAnimated(true)
                }
                alert.addAction(okAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }
         }
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func resetBtnAc(sender: AnyObject) {
        let trimNameString = emailFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        if(trimNameString == ""){
          AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Enter The Email Id.")
            return
        }else if(!(AppManager.sharedManager.isValidEmail(emailFld.text!))){
          AppManager.sharedManager.Showalert("Alert", alertmessage: "please enter correct email format like, 'abc@gmail.com'")
            return
        }
        let emailLowerCaseString:String = (trimNameString.lowercaseString)
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        let params = "Email=\(emailLowerCaseString)"
        AppManager.sharedManager.postDataOnserver(params, postUrl: "users/ForgotPassword")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
