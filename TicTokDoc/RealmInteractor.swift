

import UIKit
import RealmSwift


class RealmInteractor: NSObject {
    //    MARK: sharedInstance
    class var shared: RealmInteractor {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: RealmInteractor? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = RealmInteractor()
        }
        return Static.instance!
    }
    
    
    var realm = try! Realm()
    //  MARK: Add User to Realm
    func addUser(userDict:NSDictionary){
        try! realm.write({
            let user = User()
            user.userId = userDict.valueForKey("userId") as! String
            user.name = userDict.valueForKey("name") as! String
            user.age = userDict.valueForKey("age") as! String
            user.profilePic = userDict.valueForKey("profilePic") as! String
            user.activeStatus = userDict.valueForKey("activeStatus") as! Bool
            user.groupId = userDict.valueForKey("groupId") as! String
            user.openTokSession = userDict.valueForKey("openTokSession") as! String
            user.openTokToken = userDict.valueForKey("openTokToken") as! String
            user.openTokAPIKey = userDict.valueForKey("openTokAPIKey") as! String
            
            if let speciality = userDict.valueForKey("speciality"){
                user.speciality = speciality as! String
            }
            realm.add(user)
        })
    }
    
    func updateUserOpenTokData(userId:String, dictionary:NSDictionary ){
        let user = realm.objectForPrimaryKey(User.self, key: userId)
        try! realm.write({
            user?.groupId = dictionary.valueForKey("groupId") as! String
            user?.openTokSession = dictionary.valueForKey("openTokSession") as! String
            user?.openTokToken = dictionary.valueForKey("openTokToken") as! String
            user?.openTokAPIKey = dictionary.valueForKey("openTokAPIKey") as! String
            realm.add(user!, update: true)
        })
    }
    
    
    //  MARK: Add chatmsg to Realm
    func addChatMsg(msgDic:NSDictionary,userId:String){
        let user = realm.objectForPrimaryKey(User.self, key: userId)
        try! realm.write({
            let group = Group()
            group.msgId = (msgDic.valueForKey("msgId") as! String)
            group.groupId = (msgDic.valueForKey("groupId") as! String)
            group.fromId = (msgDic.valueForKey("fromId") as! String)
            group.message = (msgDic.valueForKey("message") as! String)
            group.messageType = (msgDic.valueForKey("messageType") as! String)
            group.readStatus = (msgDic.valueForKey("readStatus") as! Bool)
            user!.groupMsgs.append(group)
            realm.add(user!, update: true)
        })
    }
    //  MARK: changeUserStatus
    func changeUserStatus(userId:String,active:Bool){
        let user = realm.objectForPrimaryKey(User.self, key: userId)
        try! realm.write({
            user?.activeStatus = active
            realm.add(user!, update: true)
        })
    }
    
    
    func getUserBy(userId:String) -> User {
        
        let user = realm.objectForPrimaryKey(User.self, key: userId)
        return user!
    }
    
    
    //  MARK: getUsers
    func getUsers() ->NSArray{
        let users = realm.objects(User.self)
        let userAr = NSMutableArray()
        for i in 0..<users.count {
            userAr.addObject(users[i])
        }
        return userAr
    }
    //  MARK: updateChatReadStatus
    func updateChatReadStatus(msgId:String,readStatus:Bool){
        let group = realm.objectForPrimaryKey(Group.self, key: msgId)
        try! realm.write({
            group?.readStatus = readStatus
            realm.add(group!, update: true)
        })
        
    }
    
    
    //MARK: - Messages Syncing With Server APIs
    
    //MARK: - Fetch UnRead Messages From Server
    
    func createServerPath(requestPath: String) -> String {
        print("\(Header.BASE_URL)\(requestPath)")
        return "\(Header.BASE_URL)\(requestPath)"
    }
    
    func fetchUnreadMessagesFunc(groupId:String, timeStamp: String) -> Void {
        
        
    }
    func getMsgsFromServer(userId:String, groupId:String,completion: (result: NSDictionary)->()) -> Void {
        
        let paramString = "GroupId=\(groupId)&UserId=\(userId)"
        AppManager.sharedManager.generateGroupIdForUser("messageChat/messagelist", params: paramString) { (result) in
            print("\(result)")
            
            completion(result: result)
        }
    }
    func syncMsgsWithMsg(param: NSDictionary, push: Bool){
        var paramString = ""
        
        for (key, value) in param
        {
            if(paramString.isEmpty)
            {
                paramString = paramString+"\(key)=\(value)"
            }
            else
            {
                paramString = paramString+"&\(key)=\(value)"
            }
        }
        
        //BatchPush","PushUserId
        paramString = "BatchPush=\(param.valueForKey("BatchPush") as! Int)&PushUserId=\(param.valueForKey("PushUserId") as! String)&GroupId=\(param.valueForKey("GroupId") as! String)"
        
        
        let dataArray = param.objectForKey("Data") as! NSArray
        
        for i in 0..<dataArray.count {
            
            let dayDict = dataArray.objectAtIndex(i) as! NSDictionary
            
            paramString = paramString+"&Data[\(i)][GroupId]=\(dayDict.valueForKey("GroupId")!)&Data[\(i)][From]=\(dayDict.valueForKey("From")!)&Data[\(i)][message]=\(dayDict.valueForKey("message")!)&Data[\(i)][messageId]=\(dayDict.valueForKey("messageId")!)&Data[\(i)][messageType]=\(dayDict.valueForKey("messageType")!)&Data[\(i)][DisplayName]=\(dayDict.valueForKey("DisplayName")!)"
        }
        
        AppManager.sharedManager.generateGroupIdForUser("messageChat/messagesync", params: paramString) { (result) in
            print("\(result)")
            if(result.valueForKey("status") as! Int == 200)
            {
                if let msgIdArray = result.valueForKey("MessageId") as? NSArray
                {
                    if let msgId = (msgIdArray.lastObject as? NSDictionary)?.valueForKey("MessageId") as? String
                    {
                        print("\(msgId)")
                        
                    }
                    
                }
                
            }
            
            
        }
    }
    
}
