
import UIKit
protocol  StaffListingViewControllerDelegate{
    func refreshMyStaffList( staffBool : Bool )
}
class AddStaffViewController: UIViewController,SSRadioButtonControllerDelegate,UITextFieldDelegate,WebServiceDelegate {
    @IBOutlet weak var firstnameVw: UIView!
    @IBOutlet weak var lastnameVw: UIView!
    @IBOutlet weak var codeVw: UIView!
    @IBOutlet weak var emailVw: UIView!
    @IBOutlet weak var designationVw: UIView!
    @IBOutlet weak var phoneVw: UIView!
    @IBOutlet weak var scrollVw: UIScrollView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var staffPic: UIImageView!
    
    @IBOutlet weak var femaleBtn: SSRadioButton!
    @IBOutlet weak var maleBtn: SSRadioButton!
    
    @IBOutlet weak var firstNameFld: UITextField!
    @IBOutlet weak var lastNameFld: UITextField!
    @IBOutlet weak var emailAddFld: UITextField!
    @IBOutlet weak var codeFld: UITextField!
    @IBOutlet weak var phoneNoFld: UITextField!
    @IBOutlet weak var designationfld: UITextField!
    
    var gender = NSString()
    var arCode = []
    var radioButtonController: SSRadioButtonsController?
    var delegate : StaffListingViewControllerDelegate! = nil
    override func viewDidLoad(){
        arCode = ["201", "202", "203", "204", "205", "206", "207", "208", "209", "210", "212", "213", "214", "215", "216", "217", "218", "219", "224", "225", "226", "228", "229", "231", "234", "239", "240", "242", "246", "248", "250", "251", "252", "253", "254", "256", "260", "262", "264", "267", "268", "269", "270", "276", "281", "284", "289", "301", "302", "303", "304", "305", "306", "307", "308", "309", "310", "312", "313", "314", "315", "316", "317", "318", "319", "320", "321", "323", "325", "330", "331", "334", "336", "337", "339", "340", "343", "345", "347", "351", "352", "360", "361", "385", "386", "401", "402", "403", "404", "405", "406", "407", "408", "409", "410", "412", "413", "414", "415", "416", "417", "418", "419", "423", "424", "425", "430", "432", "434", "435", "438", "440", "441", "442", "443", "450", "456", "458", "469", "470", "473", "475", "478", "479", "480", "484", "500", "501", "502", "503", "504", "505", "506", "507", "508", "509", "510", "512", "513", "514", "515", "516", "517", "518", "519", "520", "530", "533", "534", "539", "540", "541", "551", "559", "561", "562", "563", "567", "570", "571", "573", "574", "575", "579", "580", "581", "585", "586", "587", "600", "601", "602", "603", "604", "605", "606", "607", "608", "609", "610", "612", "613", "614", "615", "616", "617", "618", "619", "620", "623", "626", "630", "631", "636", "641", "646", "647", "649", "650", "651", "657", "660", "661", "662", "664", "670", "671", "678", "681", "682", "684", "669", "700", "701", "702", "703", "704", "705", "706", "707", "708", "709", "710", "712", "713", "714", "715", "716", "717", "718", "719", "720", "724", "727", "731", "732", "734", "740", "747", "754", "757", "758", "760", "762", "763", "765", "767", "769", "770", "772", "773", "774", "775", "778", "779", "780", "781", "784", "785", "786", "787", "800", "801", "802", "803", "804", "805", "806", "807", "808", "809", "810", "812", "813", "814", "815", "816", "817", "818", "819", "828", "829", "830", "831", "832", "843", "845", "847", "848", "849", "850", "855", "856", "857", "858", "859", "860", "862", "863", "864", "865", "866", "867", "868", "869", "870", "872", "876", "877", "878", "888", "900", "901", "902", "903", "904", "905", "906", "907", "908", "909", "910", "912", "913", "914", "915", "916", "917", "918", "919", "920", "925", "928", "931", "936", "937", "938", "939", "940", "941", "947", "949", "951", "952", "954", "956", "970", "971", "972", "973", "978", "979", "980", "985", "989"]
        super.viewDidLoad()
        self.boxRound()
        scrollVw.userInteractionEnabled = true
        staffPic.layer.cornerRadius = self.staffPic.frame.size.height/2
        staffPic.layer.borderWidth = self.view.frame.size.height*0.012
        staffPic.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
        staffPic.clipsToBounds = true
        
        addBtn.enabled = false
        
        radioButtonController = SSRadioButtonsController(buttons: maleBtn, femaleBtn)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
        
        firstNameFld.delegate = self
        lastNameFld.delegate = self
        codeFld.delegate = self
        phoneNoFld.delegate = self
        emailAddFld.delegate = self
        designationfld.delegate = self
        
       
        
        firstNameFld.autocapitalizationType = .Words
        lastNameFld.autocapitalizationType = .Words
        designationfld.autocapitalizationType = .Words
        
        firstNameFld.tintColor     = UIColor.blackColor()
        lastNameFld.tintColor = UIColor.blackColor()
        designationfld.tintColor   = UIColor.blackColor()
        emailAddFld.tintColor   = UIColor.blackColor()
        phoneNoFld.tintColor   = UIColor.blackColor()
        codeFld.tintColor     = UIColor.blackColor()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 200){
            dispatch_async(dispatch_get_main_queue()) {
                let alert = UIAlertController(title: "Confirm", message: "Staff Member added successfully.", preferredStyle: UIAlertControllerStyle.Alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) {
                    UIAlertAction in
                    self.delegate.refreshMyStaffList(false)
                    self.navigationController?.popViewControllerAnimated(true)
                }
                alert.addAction(okAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
        else if(((responseDict as AnyObject).valueForKey("status"))  as! Int == 401){
            dispatch_async(dispatch_get_main_queue()) {
                let userType = responseDict.valueForKey("UserType") as! NSInteger
                if(userType == 3){
                    var storyboard = UIStoryboard()
                    if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                    }else{
                        storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                    }
                    let vc = storyboard.instantiateViewControllerWithIdentifier("StaffIdViewController") as! StaffIdViewController
                    vc.staffUserId = responseDict.valueForKey("userId") as! String
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    AppManager.sharedManager.Showalert("Alert", alertmessage: "Email id already exist with another account.")
                }
            }
        }
    }
    
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if(textField == firstNameFld || textField == lastNameFld){
            let trimNameString1 = firstNameFld.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet());
            let trimNameString2 = lastNameFld.text!.stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet())
            if(trimNameString1 != "" && trimNameString2 != ""){
                let lblNameInitialize = UILabel()
                lblNameInitialize.frame.size = CGSize(width: 100.0, height: 100.0)
                lblNameInitialize.textColor = UIColor.whiteColor()
                lblNameInitialize.font = UIFont(name: "Helvetica Bold", size: 30)
                lblNameInitialize.text = String(trimNameString1.characters.first!) + String(trimNameString2.characters.first!)
                lblNameInitialize.textAlignment = NSTextAlignment.Center
                lblNameInitialize.backgroundColor = UIColor.init(red: 45/255, green: 78/255, blue: 107/255, alpha: 1)
                lblNameInitialize.layer.cornerRadius = 50.0
                
                UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
                lblNameInitialize.layer.renderInContext(UIGraphicsGetCurrentContext()!)
                staffPic.image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
            }
            else{
                let lblNameInitialize = UILabel()
                lblNameInitialize.frame.size = CGSize(width: 100.0, height: 100.0)
                lblNameInitialize.textColor = UIColor.whiteColor()
                lblNameInitialize.font = UIFont(name: "Helvetica Bold", size: 30)
                lblNameInitialize.text = String("") + String("")
                lblNameInitialize.textAlignment = NSTextAlignment.Center
                lblNameInitialize.backgroundColor = UIColor.init(red: 45/255, green: 78/255, blue: 107/255, alpha: 1)
                lblNameInitialize.layer.cornerRadius = 50.0
                
                UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
                lblNameInitialize.layer.renderInContext(UIGraphicsGetCurrentContext()!)
                staffPic.image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
            }
        }
        let trimNameString1 = firstNameFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        let trimNameString2 = lastNameFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString3 = designationfld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        
        if(trimNameString1 != "" && trimNameString2 != "" && codeFld != "" && phoneNoFld.text != "" && trimNameString3 != "" && emailAddFld.text != "" && gender != ""){
            addBtn.enabled = true
        }
        else{
            addBtn.enabled = false
        }
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if( textField == firstNameFld || textField == designationfld || textField == lastNameFld || textField == emailAddFld ){
            var fldName = NSString()
            if(textField == firstNameFld){
                fldName = "First Name"
            }else if(textField == lastNameFld){
                fldName = "Last Name"
            }else if(textField == designationfld){
                fldName = "Title"
            }else if(textField == emailAddFld){
                fldName = "Email Id"
            }
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            if(newLength == 36){
                AppManager.sharedManager.Showalert("Alert", alertmessage: "\(fldName) Field does not contain more than 35 characters.")
            }
            return newLength <= 35
        }
        
        if(textField == phoneNoFld){
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            return newLength <= 7
        }
        if(textField == codeFld){
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            if(newLength > 3){
                phoneNoFld.becomeFirstResponder()
            }
            return newLength <= 3
        }
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func viewDidAppear(animated: Bool) {
        addBtn.enabled = false
        maleBtn.selected = false
        femaleBtn.selected = false
        gender = ""
        firstNameFld.text = ""
        lastNameFld.text = ""
        codeFld.text = ""
        phoneNoFld.text = ""
        emailAddFld.text = ""
        designationfld.text = ""
        let lblNameInitialize = UILabel()
        lblNameInitialize.frame.size = CGSize(width: 100.0, height: 100.0)
        lblNameInitialize.textColor = UIColor.whiteColor()
        lblNameInitialize.font = UIFont(name: "Helvetica Bold", size: 30)
        lblNameInitialize.text = String("") + String("")
        lblNameInitialize.textAlignment = NSTextAlignment.Center
        lblNameInitialize.backgroundColor = UIColor.init(red: 45/255, green: 78/255, blue: 107/255, alpha: 1)
        lblNameInitialize.layer.cornerRadius = 50.0
        
        UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
        lblNameInitialize.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        staffPic.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    func didSelectButton(aButton: UIButton?){
        firstNameFld.resignFirstResponder()
        lastNameFld.resignFirstResponder()
        codeFld.resignFirstResponder()
        phoneNoFld.resignFirstResponder()
        designationfld.resignFirstResponder()
        emailAddFld.resignFirstResponder()
        
        if(aButton == maleBtn){
            gender = "Male"
        }else if(aButton == femaleBtn){
            gender = "Female"
        }
        if(firstNameFld.text != "" && lastNameFld.text != "" && codeFld != "" && phoneNoFld.text != "" && designationfld.text != "" && emailAddFld.text != "" && gender != ""){
            addBtn.enabled = true
        }
    }
    
    func boxRound(){
        
        codeVw.layer.cornerRadius  =  self.view.frame.size.height*0.009
        codeVw.layer.borderColor   = UIColor.darkGrayColor().CGColor
        codeVw.layer.masksToBounds = false;
        codeVw.layer.borderWidth   = 0.8
        
        emailVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        emailVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        emailVw.layer.masksToBounds = false;
        emailVw.layer.borderWidth = 0.8
        
        firstnameVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        firstnameVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        firstnameVw.layer.masksToBounds = false;
        firstnameVw.layer.borderWidth = 0.8
        
        lastnameVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        lastnameVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        lastnameVw.layer.masksToBounds = false;
        lastnameVw.layer.borderWidth = 0.8
        
        phoneVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        phoneVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        phoneVw.layer.masksToBounds = false;
        phoneVw.layer.borderWidth = 0.8
        
        designationVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        designationVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        designationVw.layer.masksToBounds = false;
        designationVw.layer.borderWidth = 0.8
        
    }
    @IBAction func addStaffAc(sender: AnyObject) {
        let compPh = "\(codeFld.text! as String)\(phoneNoFld.text! as String)"
        if(!(arCode.containsObject(codeFld.text!))){
            AppManager.sharedManager.Showalert("Alert", alertmessage: "Please Enter a Valid Area Code.")
            return
        }
        else if(compPh.characters.count != 10){
            AppManager.sharedManager.Showalert("Alert", alertmessage: "Please enter 10 digit phone number.")
            return
        }
        else if(!(AppManager.sharedManager.isValidEmail(emailAddFld.text!))){
            AppManager.sharedManager.Showalert("Alert", alertmessage: "Please enter valid email address.")
            return
        }
        
        
        let image : UIImage = staffPic.image!
        let  mImageData              = UIImagePNGRepresentation(image)! as NSData
        let base64String = mImageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.EncodingEndLineWithLineFeed)
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        let defaults = NSUserDefaults.standardUserDefaults()
        let trimNameString1 = firstNameFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        let trimNameString2 = lastNameFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let trimNameString3 = designationfld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        let trimNameString4 = emailAddFld.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet());
        
        let emailLowerCaseString:String = (trimNameString4.lowercaseString)
        let firstNameLowerCaseString:String = (trimNameString1.lowercaseString)
        let lastNameLowerCaseString:String = (trimNameString2.lowercaseString)
        let frNm = defaults.valueForKey("firstName") as! String
        let lsNm = defaults.valueForKey("lastName") as! String
        if let userId = defaults.valueForKey("id") {
            let params = "FirstName=\(firstNameLowerCaseString)&LastName=\(lastNameLowerCaseString)&Gender=\(gender)&Mobile=\(phoneNoFld.text!)&Code=\(codeFld.text!)&ProfilePic=\(base64String)&UserType=3&Email=\(emailLowerCaseString)&MediaType=M&DrId=\(userId)&DeviceToken=123456&Designation=\(trimNameString3)&DrName=\(frNm) \(lsNm)"
            AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/drmemberadd")
        }
    }
    override func viewDidLayoutSubviews() {
        if((self.storyboard?.valueForKey("name"))! as! String == "Main"){
            scrollVw.scrollEnabled = true
            scrollVw.contentSize = CGSizeMake(self.view.frame.width,self.view.frame.size.height*0.75)
        }
    }
    
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
   
    
    
}
