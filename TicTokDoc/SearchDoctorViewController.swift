

import UIKit
import IQKeyboardManagerSwift
import MapKit
import CoreLocation

class SearchDoctorViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,WebServiceDelegate{
    @IBOutlet weak var searchDocTableVw: UITableView!
    @IBOutlet weak var backBtnSearch: UIButton!
    @IBOutlet weak var searchVw: UIView!
    @IBOutlet weak var searchTxtFld: UITextField!
    
    var searchDoc = Bool()
    var sndRequ = Bool()
    let mapView = MKMapView()
    let regionRadius: CLLocationDistance = 5000
    var locationManager = CLLocationManager()
    var speciality = NSString()
    var distance = NSInteger()
    var address = NSString()
    var FirstName = NSString()
    var lastname = NSString()
    var bio = NSString()
    var ph = NSInteger()
    var qualification = NSString()
    var language = NSString()
    var tableVwHide = Bool()
    var lat = NSString()
    var long = NSString()
    var butnTap = NSInteger()
    var dataCount = NSInteger()
    var specialitydataAr = NSMutableArray()
    var nameDataAr = NSMutableArray()
    var addDataAr = NSMutableArray()
    var qualiDataAr = NSMutableArray()
    var profilePicAr = NSMutableArray()
    var languageDataAr = NSMutableArray()
    var distanceDataAr = NSMutableArray()
    var phDataAr = NSMutableArray()
    var bioDataAr = NSMutableArray()
    var phCodeDataAr = NSMutableArray()
    var drDeviceToken = NSMutableArray()
    var drEmailid = NSMutableArray()
    var dr_idAr = NSMutableArray()
    var firstSearch = Bool()
    var totalData = NSArray()
    var searchWho = NSString()
    var deviceTypeAr = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        searchVw.layer.cornerRadius =  self.view.frame.size.height*0.007
        searchVw.layer.masksToBounds = false;
        searchVw.layer.borderWidth = 0.5
        searchVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        self.navigationController?.navigationBarHidden = true
        IQKeyboardManager.sharedManager().enable = true
        tableVwHide = false
        searchDocTableVw.reloadData()
        searchDocTableVw.delegate = self
        searchDocTableVw.dataSource = self
        if(firstSearch == true){
         backBtnSearch.hidden = true
        }
        else{
            backBtnSearch.hidden = false
        }

        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        self.checkLocationStatus()
        searchTxtFld.tintColor = UIColor.blackColor()
        searchTxtFld.addTarget(self, action: #selector(SearchDoctorViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
    }
    
    func checkLocationStatus(){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .NotDetermined, .Restricted, .Denied:
                print("No access")
                let defaults = NSUserDefaults.standardUserDefaults()
                lat = defaults.valueForKey("lat")  as! String
                long = defaults.valueForKey("long") as! String
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                if(searchWho == "doctor"){
                    searchTxtFld.resignFirstResponder()
                    let defaults = NSUserDefaults.standardUserDefaults()
                    if let userId = defaults.valueForKey("id") {
                        let params = "Lat=\(lat)&Long=\(long)&UserId=\(userId)"
                        AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/AllDrSearch")
                    }
                }else{
                    let params = "Lat=\(lat)&Long=\(long)"
                    AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/AllDrSearch")
                }
                
            case .AuthorizedAlways, .AuthorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    func textFieldDidChange(textField: UITextField) {
       print(textField.text)
        searchDocTableVw.tableHeaderView = nil
       let exactText =  textField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let predicate = NSPredicate(format:"Name CONTAINS[cd] %@","\(exactText)")
        let names = totalData.filteredArrayUsingPredicate(predicate)
        print(names)
        if(exactText != ""){
        if(names.count != 0){
            self.tableVwHide = true
            self.specialitydataAr.removeAllObjects()
            self.distanceDataAr.removeAllObjects()
            self.addDataAr.removeAllObjects()
            self.nameDataAr.removeAllObjects()
            self.bioDataAr.removeAllObjects()
            self.phCodeDataAr.removeAllObjects()
            self.phDataAr.removeAllObjects()
            self.qualiDataAr.removeAllObjects()
            self.languageDataAr.removeAllObjects()
            self.dr_idAr.removeAllObjects()
            self.drDeviceToken.removeAllObjects()
            self.drEmailid.removeAllObjects()
            self.profilePicAr.removeAllObjects()
            dataCount = names.count
            for i in 0...self.dataCount-1{
             let d = names[i]
             self.specialitydataAr.addObject(d.valueForKey("Speciality")!)
             self.distanceDataAr.addObject(d.valueForKey("distance")!)
             self.addDataAr.addObject(d.valueForKey("Address")!)
             self.nameDataAr.addObject(d.valueForKey("Name")!)
             self.bioDataAr.addObject(d.valueForKey("Bio")!)
             self.phDataAr.addObject(d.valueForKey("Mobile")!)
             self.phCodeDataAr.addObject(d.valueForKey("Code")!)
             self.profilePicAr.addObject(d.valueForKey("ProfilePic")!)
             self.qualiDataAr.addObject(d.valueForKey("Qualification")!)
             self.languageDataAr.addObject(d.valueForKey("Languages")!)
             self.dr_idAr.addObject(d.valueForKey("_id")!)
             self.drEmailid.addObject(d.valueForKey("Email")!)
             self.drDeviceToken.addObject(d.valueForKey("DeviceToken")!)
            }
            self.searchDocTableVw.reloadData()
        }
        else{
            self.specialitydataAr.removeAllObjects()
            self.distanceDataAr.removeAllObjects()
            self.addDataAr.removeAllObjects()
            self.nameDataAr.removeAllObjects()
            self.bioDataAr.removeAllObjects()
            self.phCodeDataAr.removeAllObjects()
            self.phDataAr.removeAllObjects()
            self.qualiDataAr.removeAllObjects()
            self.languageDataAr.removeAllObjects()
            self.dr_idAr.removeAllObjects()
            self.drDeviceToken.removeAllObjects()
            self.drEmailid.removeAllObjects()
            self.profilePicAr.removeAllObjects()
            dataCount = names.count
            let headerView: UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height*0.25))
            let searchLbl: UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width*0.085, self.view.frame.size.height*0.06, self.view.frame.size.width-(2*(self.view.frame.size.width*0.085)), self.view.frame.size.height*0.06))
            searchLbl.text = "No Search Found"
            searchLbl.textAlignment = .Center
            searchLbl.textColor = UIColor.blackColor()
            headerView.addSubview(searchLbl)
            self.searchDocTableVw.tableHeaderView = headerView
            self.searchDocTableVw.reloadData()
            
         }
        }
        else{
          dataCount = totalData.count
            self.specialitydataAr.removeAllObjects()
            self.distanceDataAr.removeAllObjects()
            self.addDataAr.removeAllObjects()
            self.nameDataAr.removeAllObjects()
            self.bioDataAr.removeAllObjects()
            self.phCodeDataAr.removeAllObjects()
            self.phDataAr.removeAllObjects()
            self.qualiDataAr.removeAllObjects()
            self.languageDataAr.removeAllObjects()
            self.dr_idAr.removeAllObjects()
            self.drDeviceToken.removeAllObjects()
            self.drEmailid.removeAllObjects()
            self.profilePicAr.removeAllObjects()
            for i in 0...self.dataCount-1{
                let d = totalData[i]
                self.specialitydataAr.addObject(d.valueForKey("Speciality")!)
                self.distanceDataAr.addObject(d.valueForKey("distance")!)
                self.addDataAr.addObject(d.valueForKey("Address")!)
                self.nameDataAr.addObject(d.valueForKey("Name")!)
                self.bioDataAr.addObject(d.valueForKey("Bio")!)
                self.phDataAr.addObject(d.valueForKey("Mobile")!)
                self.phCodeDataAr.addObject(d.valueForKey("Code")!)
                self.profilePicAr.addObject(d.valueForKey("ProfilePic")!)
                self.qualiDataAr.addObject(d.valueForKey("Qualification")!)
                self.languageDataAr.addObject(d.valueForKey("Languages")!)
                self.dr_idAr.addObject(d.valueForKey("_id")!)
                self.drEmailid.addObject(d.valueForKey("Email")!)
                self.drDeviceToken.addObject(d.valueForKey("DeviceToken")!)
            }
            self.searchDocTableVw.reloadData()
        }
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        locationManager.stopUpdatingLocation()
        
        if ((locValue.latitude == 0.0) && (locValue.longitude == 0.0)) {
            let defaults = NSUserDefaults.standardUserDefaults()
            lat = defaults.valueForKey("lat")  as! String
            long = defaults.valueForKey("long") as! String
        }
        else{
            lat = String(format: "%.8f", locValue.latitude)
            long = String(format: "%.8f", locValue.longitude)
        }
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        if(searchWho == "doctor"){
            searchTxtFld.resignFirstResponder()
            let defaults = NSUserDefaults.standardUserDefaults()
            if let userId = defaults.valueForKey("id") {
            let params = "Lat=\(lat)&Long=\(long)&UserId=\(userId)"
            AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/AllDrSearch")
            }
        }else{
        let params = "Lat=\(lat)&Long=\(long)"
        AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/AllDrSearch")
        }
    }
    func okBtnAc1(){
            let view = self.view.viewWithTag(2356)! as UIView
            view.removeFromSuperview()
    }
    override func viewWillAppear(animated: Bool) {
        searchTxtFld.text = ""
        searchTxtFld.resignFirstResponder()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableVwHide == true){
          return dataCount
        }else{
          return 0
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
         AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            let cell = tableView.dequeueReusableCellWithIdentifier("cell")
            cell!.layer.cornerRadius = 7.0
            cell!.layer.masksToBounds = true
            let drImg: UIImageView = (cell!.viewWithTag(5005) as! UIImageView)
           drImg.layer.cornerRadius = drImg.frame.size.height/2
           drImg.layer.borderWidth = self.view.frame.size.height*0.012
           drImg.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5).CGColor
          if let url = NSURL(string: "http://\(profilePicAr[indexPath.row])") {
            drImg.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
          }
            let drName = cell?.viewWithTag(5001) as! UILabel
            drName.text = "Dr. \(nameDataAr[indexPath.row])"
            let specialityLbl = cell?.viewWithTag(5002) as! UILabel
            specialityLbl.text = "\(specialitydataAr[indexPath.row])"
            let addLbl = cell?.viewWithTag(5003) as! UILabel
            addLbl.text = addDataAr[indexPath.row] as? String
        
            let mileLbl = cell?.viewWithTag(5004) as! UILabel
            mileLbl.layer.cornerRadius =  mileLbl.frame.size.height*0.09
            mileLbl.text = "\(distanceDataAr[indexPath.row]) \n miles"
            mileLbl.layer.masksToBounds = true
            let imageView: UIView = (cell?.viewWithTag(2201))!
            imageView.layer.cornerRadius =  self.view.frame.size.height*0.009
            imageView.layer.borderColor = UIColor.darkGrayColor().CGColor
            imageView.layer.shadowOffset = CGSizeMake(10, 5);
            imageView.layer.shadowRadius = 1;
            imageView.layer.shadowOpacity = 0.06;
            imageView.backgroundColor = UIColor.whiteColor()
            imageView.layer.masksToBounds = false;
            imageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
            imageView.layer.borderWidth = 0.5
            return cell!
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return self.view.frame.size.width*0.1
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        searchTxtFld.resignFirstResponder()
            let vc = storyboard!.instantiateViewControllerWithIdentifier("DocrtorsBioViewController") as! DocrtorsBioViewController
           if(searchWho == "doctor"){
            vc.docDocSearch = true
           }
           vc.adress = "\(addDataAr[indexPath.row])"
           vc.bio = "\(bioDataAr[indexPath.row])"
           vc.drname = "\(nameDataAr[indexPath.row])"
           vc.lang = "\(languageDataAr[indexPath.row])"
           var str  = "+1(\(phCodeDataAr[indexPath.row]))\(phDataAr[indexPath.row])"
           str = str.insert("-", ind: 10)
           vc.phone = str
           vc.quali = "\(qualiDataAr[indexPath.row])"
           vc.speciality = "\(specialitydataAr[indexPath.row])"
           vc.dr_Id = "\(dr_idAr[indexPath.row])"
           vc.docBio = searchDoc
           vc.sndReq = sndRequ
           vc.profilrPicUrl = profilePicAr[indexPath.row] as! NSString
           vc.drEmail = "\(drEmailid[indexPath.row])"
           vc.deviceToken =  "\(drDeviceToken[indexPath.row])"
          vc.deviceType = deviceTypeAr[indexPath.row] as! NSString
           self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func backBtnAc(sender: AnyObject) {
       self.navigationController?.popViewControllerAnimated(true)
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString)
    {
        dispatch_async(dispatch_get_main_queue()) {
           AppManager.sharedManager.hideActivityIndicatorInView(self.view)
            if((responseDict.objectForKey("data") != nil)){
            self.tableVwHide = true
            self.specialitydataAr.removeAllObjects()
            self.distanceDataAr.removeAllObjects()
            self.addDataAr.removeAllObjects()
            self.nameDataAr.removeAllObjects()
            self.bioDataAr.removeAllObjects()
            self.phCodeDataAr.removeAllObjects()
            self.phDataAr.removeAllObjects()
            self.qualiDataAr.removeAllObjects()
            self.languageDataAr.removeAllObjects()
            self.dr_idAr.removeAllObjects()
            self.drDeviceToken.removeAllObjects()
            self.drEmailid.removeAllObjects()
            self.profilePicAr.removeAllObjects()
            self.deviceTypeAr.removeAllObjects()
                
                self.totalData = []
                self.dataCount = 0
                self.dataCount = (responseDict.objectForKey("data")?.count)!
                self.totalData = responseDict.objectForKey("data") as! NSArray
                let dataDict = responseDict.objectForKey("data") as! NSArray
                for i in 0...self.dataCount-1{
                    let d = dataDict[i]
                    self.specialitydataAr.addObject(d.valueForKey("Speciality")!)
                    self.distanceDataAr.addObject(d.valueForKey("distance")!)
                    self.addDataAr.addObject(d.valueForKey("Address")!)
                    self.nameDataAr.addObject(d.valueForKey("Name")!)
                    self.bioDataAr.addObject(d.valueForKey("Bio")!)
                    self.phDataAr.addObject(d.valueForKey("Mobile")!)
                    self.phCodeDataAr.addObject(d.valueForKey("Code")!)
                    self.profilePicAr.addObject(d.valueForKey("ProfilePic")!)
                    self.qualiDataAr.addObject(d.valueForKey("Qualification")!)
                    self.languageDataAr.addObject(d.valueForKey("Languages")!)
                    self.dr_idAr.addObject(d.valueForKey("_id")!)
                    self.drEmailid.addObject(d.valueForKey("Email")!)
                    self.drDeviceToken.addObject(d.valueForKey("DeviceToken")!)
                    self.deviceTypeAr.addObject(d.valueForKey("DeviceType")!)
                }
            self.searchDocTableVw.reloadData()
            }
            else if((responseDict.objectForKey("error") != nil)){
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableVwHide = false
                    self.searchDocTableVw.reloadData()
                    let alert = UIAlertController(title: "Alert", message: "No Doctor found, please try later.", preferredStyle: UIAlertControllerStyle.Alert)
                    let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default) {
                        UIAlertAction in
                            self.navigationController!.popToRootViewControllerAnimated(true)
                        NSUserDefaults.standardUserDefaults().removeObjectForKey("usertype")
                        }
                    alert.addAction(cancelAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    
    
    //MARK: - Get Current Location Function
    func getCurrentLocationFunc() -> Void {
        
        let location = locationManager.location;
        
        locationManager.stopUpdatingLocation()
        
        if ((location?.coordinate.latitude == 0.0) && (location?.coordinate.longitude == 0.0)) {
            let defaults = NSUserDefaults.standardUserDefaults()
            lat = defaults.valueForKey("lat")  as! String
            long = defaults.valueForKey("long") as! String
        }
        else{
            lat = String(format: "%.8f", location!.coordinate.latitude)
            long = String(format: "%.8f", location!.coordinate.longitude)
        }
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                let params = "Lat=\(lat)&Long=\(long)"
                AppManager.sharedManager.postDataOnserver(params, postUrl: "patients/AllDrSearch")
        
    }
    
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension String {
    
    func contains(find: String) -> Bool{
        return self.rangeOfString(find) != nil
    }
    
    func containsIgnoringCase(find: String) -> Bool{
        return self.rangeOfString(find, options: NSStringCompareOptions.CaseInsensitiveSearch) != nil
    }
}
