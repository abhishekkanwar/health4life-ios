

import UIKit
import SlideMenuControllerSwift
class VideoCallViewController: UIViewController,WebServiceDelegate{
    
    @IBOutlet weak var shadowVw: UIView!
    @IBOutlet weak var smallBoxVw: UIView!
    @IBOutlet weak var videoCallProfileImgVw: UIImageView!
    @IBOutlet weak var cancelAppBtn: UIButton!
    @IBOutlet weak var startVideoBtn: UIButton!
    
    var name = NSString()
    var address = NSString()
    var date = NSString()
    var gender = NSString()
    var age = NSString()
    var mobile = NSString()
    var email_id = NSString()
    var time = NSString()
    var ptId = NSString()
    var deviceTokn = NSString()
    var scheduleMin = NSString()
    var book_id = NSString()
    var videoBool = Bool()
    var schedule_Id = NSString()
    var compareDate = NSString()
    var fromTime = NSString()
    var profilePicUrl = NSString()
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelAppBtn.titleLabel?.numberOfLines = 2
        cancelAppBtn.titleLabel?.textAlignment = .Center
        videoCallProfileImgVw.layer.cornerRadius = videoCallProfileImgVw.frame.size.height/2
        videoCallProfileImgVw.layer.borderWidth = videoCallProfileImgVw.frame.size.height*0.07
        videoCallProfileImgVw.layer.borderColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.9).CGColor
        if let url = NSURL(string: "http://\(profilePicUrl)") {
            videoCallProfileImgVw.sd_setImageWithURL(url, placeholderImage:UIImage(named: "placeholder.png"))
        }
        
        let userType = NSUserDefaults.standardUserDefaults().valueForKey("usertype") as! NSObject
        if(userType == 1){
            startVideoBtn.hidden = false
        }else{
            startVideoBtn.hidden = true
        }
        
        smallBoxVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        smallBoxVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        smallBoxVw.layer.shadowOffset = CGSizeMake(5, 5);
        smallBoxVw.layer.shadowRadius = 0.7;
        smallBoxVw.layer.shadowOpacity = 0.06;
        smallBoxVw.layer.masksToBounds = false;
        smallBoxVw.layer.shadowColor = UIColor.lightGrayColor().CGColor
        smallBoxVw.layer.borderWidth = 0.5
        smallBoxVw.backgroundColor = UIColor.whiteColor()
        
        shadowVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        shadowVw.layer.borderColor = UIColor.darkGrayColor().CGColor
        shadowVw.layer.shadowOffset = CGSizeMake(5, 5);
        shadowVw.layer.shadowRadius = 0.7;
        shadowVw.layer.shadowOpacity = 0.06;
        shadowVw.layer.masksToBounds = false;
        shadowVw.layer.shadowColor = UIColor.lightGrayColor().CGColor
        shadowVw.layer.borderWidth = 0.5
        shadowVw.backgroundColor = UIColor.clearColor()
        
        let nameLbl = self.view.viewWithTag(318) as! UILabel
        nameLbl.text = name as String
        
        let addLbl = self.view.viewWithTag(319) as! UILabel
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: "cityState")
        let offsetY: CGFloat = -6.0
        attachment.bounds = CGRectMake(-10, offsetY, attachment.image!.size.width, attachment.image!.size.height)
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string:"")
        myString.appendAttributedString(attachmentString)
        let myString1 = NSMutableAttributedString(string:address as String)
        myString.appendAttributedString(myString1)
        addLbl.textAlignment = .Center
        addLbl.attributedText = myString
        let dateLbl = smallBoxVw.viewWithTag(320) as! UILabel
        dateLbl.text =  "\(date) at \(time) (\(scheduleMin) Min)"
        let gnderLbl = shadowVw.viewWithTag(321) as! UILabel
        gnderLbl.text = gender as String
        let ageLbl = shadowVw.viewWithTag(322) as! UILabel
        ageLbl.text = age as String
        let mobileLbl = shadowVw.viewWithTag(323) as! UILabel
        mobileLbl.text = "\(mobile)"
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(VideoCallViewController.imageTapped(_:)))
        mobileLbl.userInteractionEnabled = true
        mobileLbl.addGestureRecognizer(tapGestureRecognizer)
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(ptId, forKey: "ptId")
    }
    func imageTapped(img: AnyObject){
        let alert = UIAlertController(title: "Please confirm your call", message: "You’re about to call the following:\n\(mobile)", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            if let url = NSURL(string: "tel://\(self.mobile)") {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    @IBAction func startVideoCallAc(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MMMM-yyyy"
        let timeZone = NSTimeZone(name: "UTC")
        dateFormatter.timeZone=timeZone
        let schDate = dateFormatter.dateFromString("\(compareDate)")!
        let dateC = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd-MMMM-yyyy"
        let defaultTimeZoneStr = formatter.stringFromDate(dateC)
        let currentD1 = dateFormatter.dateFromString("\(defaultTimeZoneStr)")!
        switch schDate.compare(currentD1) {
        case .OrderedAscending     :
            print("schDate is small than currentD1")
        case .OrderedDescending
            :   print("schDate is greater than date currentD1")
            let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            backVw.tag = 2358
            self.view.addSubview(backVw)
            let alerVw = UIView()
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.25)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.25)
            }else{
                alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.15)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.18)
            }
            alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
            alerVw.backgroundColor = UIColor.whiteColor()
            backVw.addSubview(alerVw)
            
            let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
            rightImg.image = UIImage(named: "verifyImg")
            alerVw.addSubview(rightImg)
            
            let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
            verificationLbl.text = " Alert"
            verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
            verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(verificationLbl)
            
            let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
            lineVw.backgroundColor = UIColor.grayColor()
            alerVw.addSubview(lineVw)
            
            let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
            if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                contentLbl.font          = UIFont(name: "Helvetica", size:14)
            }else{
                contentLbl.font          = UIFont(name: "Helvetica", size:19)
            }
            contentLbl.textAlignment = .Center
            contentLbl.textColor = UIColor.grayColor()
            contentLbl.text = "You are trying to start the consultation before the scheduled time,do you really want to start."
            contentLbl.numberOfLines = 4
            alerVw.addSubview(contentLbl)
            
            let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width-(alerVw.frame.size.width/2), alerVw.frame.size.height*0.22))
            okLbl.setTitle("Yes", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            okLbl.titleLabel?.textAlignment = .Center
            okLbl.addTarget(self, action: #selector(VideoCallViewController.yesBtnAc), forControlEvents: .TouchUpInside)
            okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(okLbl)
            
            let lineLbl = UILabel.init(frame: CGRectMake(okLbl.frame.size.width-2,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),1.2, alerVw.frame.size.height*0.22))
            lineLbl.backgroundColor = UIColor.lightGrayColor()
            alerVw.addSubview(lineLbl)
            
            let noLbl = UIButton.init(frame: CGRectMake(alerVw.frame.size.width/2,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width-(alerVw.frame.size.width/2), alerVw.frame.size.height*0.22))
            noLbl.setTitle("No", forState: .Normal)
            if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                noLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
            }else{
                noLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
            }
            noLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            noLbl.titleLabel?.textAlignment = .Center
            noLbl.addTarget(self, action: #selector(VideoCallViewController.noBtnAc), forControlEvents: .TouchUpInside)
            noLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
            alerVw.addSubview(noLbl)
        case .OrderedSame          :
            print("The two dates are the same")
            let timestamp1 = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .NoStyle, timeStyle: .ShortStyle)
            dateFormatter.timeStyle = .ShortStyle
            let fmt = NSDateFormatter.dateFormatFromTemplate("jm", options: 0, locale: NSLocale.currentLocale())!
            dateFormatter.dateFormat = "\(fmt)" // k = Hour in 1~24, mm = Minute
            let currentT:NSDate = dateFormatter.dateFromString("\(timestamp1)")!
            dateFormatter.dateFormat = "hh:mm a"
            let schT:NSDate = dateFormatter.dateFromString("\(time)")!
            switch schT.compare(currentT) {
            case .OrderedAscending     :
                print("schT is small than currentT")
                let defaults = NSUserDefaults.standardUserDefaults()
                let userId = defaults.valueForKey("id") as! String
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                let params = "DeviceToken=\(deviceTokn)&PatientId=\(ptId)&DrId=\(userId)&drschedulesetsId=\(schedule_Id)&BookingId=\(book_id)"
                videoBool = true
                AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrVideo")
            case .OrderedDescending
                :   print("schT is greater than date currentT")
                let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
                backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
                backVw.tag = 2358
                self.view.addSubview(backVw)
                let alerVw = UIView()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.25)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.25)
                }else{
                    alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.15)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.18)
                }
                
                alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
                alerVw.backgroundColor = UIColor.whiteColor()
                backVw.addSubview(alerVw)
                
                let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
                rightImg.image = UIImage(named: "verifyImg")
                alerVw.addSubview(rightImg)
                
                let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.45, self.view.frame.size.width*0.05))
                verificationLbl.text = " Alert"
                verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.03)
                verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(verificationLbl)
                
                let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+17,alerVw.frame.size.width,1))
                lineVw.backgroundColor = UIColor.grayColor()
                alerVw.addSubview(lineVw)
                
                let contentLbl = UILabel.init(frame: CGRectMake(alerVw.frame.size.width*0.1,verificationLbl.frame.size.height+26, alerVw.frame.size.width-(alerVw.frame.size.width*0.2),alerVw.frame.size.height*0.36))
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    contentLbl.font          = UIFont(name: "Helvetica", size:14)
                }else{
                    contentLbl.font          = UIFont(name: "Helvetica", size:19)
                }
                contentLbl.textAlignment = .Center
                contentLbl.textColor = UIColor.grayColor()
                contentLbl.text = "You are trying to start the consultation before the scheduled time,do you really want to start."
                contentLbl.numberOfLines = 4
                alerVw.addSubview(contentLbl)
                
                let okLbl = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width-(alerVw.frame.size.width/2), alerVw.frame.size.height*0.22))
                okLbl.setTitle("Yes", forState: .Normal)
                okLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                okLbl.titleLabel?.textAlignment = .Center
                okLbl.addTarget(self, action: #selector(VideoCallViewController.yesBtnAc), forControlEvents: .TouchUpInside)
                if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                }else{
                    okLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                }
                okLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(okLbl)
                
                let lineLbl = UILabel.init(frame: CGRectMake(okLbl.frame.size.width-2,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),1.2, alerVw.frame.size.height*0.22))
                lineLbl.backgroundColor = UIColor.lightGrayColor()
                alerVw.addSubview(lineLbl)
                
                let noLbl = UIButton.init(frame: CGRectMake(alerVw.frame.size.width/2,alerVw.frame.size.height-(alerVw.frame.size.height*0.2),alerVw.frame.size.width-(alerVw.frame.size.width/2), alerVw.frame.size.height*0.22))
                noLbl.setTitle("No", forState: .Normal)
                if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
                    noLbl.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
                }else{
                    noLbl.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
                }
                noLbl.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                noLbl.titleLabel?.textAlignment = .Center
                noLbl.addTarget(self, action: #selector(VideoCallViewController.noBtnAc), forControlEvents: .TouchUpInside)
                noLbl.backgroundColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
                alerVw.addSubview(noLbl)
            case .OrderedSame          :
                print("The two dates are the same")
                let defaults = NSUserDefaults.standardUserDefaults()
                let userId = defaults.valueForKey("id") as! String
                AppManager.sharedManager.delegate=self
                AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
                let params = "DeviceToken=\(deviceTokn)&PatientId=\(ptId)&DrId=\(userId)&drschedulesetsId=\(schedule_Id)&BookingId=\(book_id)"
                videoBool = true
                AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrVideo")
            }
        }
    }
    @IBAction func phAction(sender: AnyObject) {
        let alert = UIAlertController(title: "Please confirm your call", message: "You’re about to call the following:\n\(mobile)", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            if let url = NSURL(string: "tel://\(self.mobile)") {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    func noBtnAc(){
        let view = self.view.viewWithTag(2358)! as UIView
        view.removeFromSuperview()
    }
    func yesBtnAc(){
        let defaults = NSUserDefaults.standardUserDefaults()
        let userId = defaults.valueForKey("id") as! String
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        let params = "DeviceToken=\(deviceTokn)&PatientId=\(ptId)&DrId=\(userId)&drschedulesetsId=\(schedule_Id)&BookingId=\(book_id)"
        videoBool = true
        AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrVideo")
    }
    func serverReponse(responseDict: NSMutableDictionary,serviceurl:NSString){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        if(videoBool == true){
            dispatch_async(dispatch_get_main_queue()) {
                let apiKey = responseDict.valueForKey("apikey") as! NSString
                let token = responseDict.valueForKey("token") as! NSString
                let session_id = responseDict.valueForKey("session") as! NSString
                let status = responseDict.valueForKey("Status") as! NSString
                let url = responseDict.valueForKey("Url") as! NSString
                let inviteUrl = responseDict.valueForKey("inviteurl") as! NSString
                let redirectUrl = responseDict.valueForKey("Redirecturl") as! NSString
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(status, forKey: "status")
                defaults.setObject(url, forKey: "url")
                var storyboard = UIStoryboard()
                if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                }else{
                    storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                }
                let vc = storyboard.instantiateViewControllerWithIdentifier("ConferenceVideoViewController") as! ConferenceVideoViewController
                vc.redirectUrl = redirectUrl
                vc.patientId = self.ptId
                vc.kApiKey = apiKey
                vc.kSessionId = session_id
                vc.kToken = token
                vc.patName = self.name
                vc.ptAdd = self.address
                vc.schId =  self.schedule_Id
                vc.bokId = self.book_id
                vc.deviceToken = self.deviceTokn
                vc.email_id = self.email_id
                vc.inviteUrl = inviteUrl
                vc.groupId = responseDict.valueForKey("GroupId") as! String
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            dispatch_async(dispatch_get_main_queue()) {
                self.slideMenuController()!.navigationController?.popViewControllerAnimated(true)
            }
        }
    }
    func okBtnAc(){
        
    }
    @IBAction func cancelAppAction(sender: AnyObject){
        let backVw = UIView.init(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        backVw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        backVw.tag = 2356
        self.view.addSubview(backVw)
        let alerVw = UIView()
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            alerVw.frame = CGRectMake(self.view.frame.size.width*0.08,self.view.frame.size.height/2-((self.view.frame.size.height*0.37)/2), self.view.frame.size.width-(self.view.frame.size.width*0.16), self.view.frame.size.height*0.4)
        }else{
            alerVw.frame = CGRectMake(self.view.frame.size.width*0.2,self.view.frame.size.height/2-((self.view.frame.size.height*0.15)/2), self.view.frame.size.width-(self.view.frame.size.width*0.4), self.view.frame.size.height*0.25)
        }
        
        alerVw.layer.cornerRadius =  self.view.frame.size.height*0.009
        alerVw.backgroundColor = UIColor.whiteColor()
        backVw.addSubview(alerVw)
        
        let rightImg = UIImageView.init(frame: CGRectMake(12,10, self.view.frame.size.width*0.05, self.view.frame.size.width*0.05))
        rightImg.image = UIImage(named: "confirm")
        alerVw.addSubview(rightImg)
        
        let verificationLbl = UILabel.init(frame: CGRectMake(self.view.frame.size.width*0.04+20,10, self.view.frame.size.width*0.6, self.view.frame.size.width*0.05))
        verificationLbl.text = " Cancelled Appointment"
        verificationLbl.font = UIFont(name: "Helvetica", size: self.view.frame.size.height * 0.023)
        verificationLbl.textColor = UIColor.init(red: 56/255, green: 81/255, blue: 111/255, alpha: 1)
        alerVw.addSubview(verificationLbl)
        
        let lineVw = UIView.init(frame: CGRectMake(0,verificationLbl.frame.size.height+15,alerVw.frame.size.width,1))
        lineVw.backgroundColor = UIColor.grayColor()
        alerVw.addSubview(lineVw)
        let contentLbl = UILabel()
        if(UIDevice.currentDevice().userInterfaceIdiom == .Phone){
            contentLbl.font  = UIFont(name: "Helvetica", size:14)
            contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.04,verificationLbl.frame.size.height+lineVw.frame.size.height+17, alerVw.frame.size.width-(alerVw.frame.size.width*0.08),alerVw.frame.size.height*0.7)
        }else{
            contentLbl.font  = UIFont(name: "Helvetica", size:19)
            contentLbl.frame = CGRectMake(alerVw.frame.size.width*0.04,verificationLbl.frame.size.height+lineVw.frame.size.height+12, alerVw.frame.size.width-(alerVw.frame.size.width*0.08),alerVw.frame.size.height*0.6)
        }
        contentLbl.textAlignment = .Center
        contentLbl.textColor = UIColor.grayColor()
        contentLbl.text = "You have cancelled this appointment with your patient. If you meant to do this, press confirm. If not, you can go back. \n\n Your patient will be notified of the cancelled appointment and will be prompted to reschedule their appointment."
        contentLbl.numberOfLines = 10
        alerVw.addSubview(contentLbl)
        
        let goBack = UIButton.init(frame: CGRectMake(0,alerVw.frame.size.height-(alerVw.frame.size.height*0.16), alerVw.frame.size.width*0.499,alerVw.frame.size.height*0.16))
        goBack.addTarget(self, action: #selector(VideoCallViewController.goBackAc), forControlEvents: .TouchUpInside)
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            goBack.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
        }else{
            goBack.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
        }
        goBack.backgroundColor = UIColor.init(red: 51/255, green: 82/255, blue: 109/255, alpha: 1)
        goBack.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        goBack.titleLabel?.textAlignment = .Center
        goBack.setTitle("Go Back", forState: .Normal)
        alerVw.addSubview(goBack)
        
        let confirm = UIButton.init(frame: CGRectMake(goBack.frame.size.width+(alerVw.frame.size.width*0.002),alerVw.frame.size.height-(alerVw.frame.size.height*0.16), alerVw.frame.size.width*0.499,alerVw.frame.size.height*0.16))
        confirm.addTarget(self, action: #selector(VideoCallViewController.confirmAc), forControlEvents: .TouchUpInside)
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
            confirm.titleLabel!.font = UIFont.init(name: "Helvetica Bold", size: 19)
        }else{
            confirm.titleLabel!.font = UIFont.init(name: "Helvetica", size: 14)
        }
        confirm.backgroundColor = UIColor.init(red: 51/255, green: 82/255, blue: 109/255, alpha: 1)
        confirm.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        confirm.titleLabel?.textAlignment = .Center
        confirm.setTitle("Confirm", forState: .Normal)
        alerVw.addSubview(confirm)
    }
    func goBackAc(){
        let view = self.view.viewWithTag(2356)! as UIView
        view.removeFromSuperview()
    }
    func confirmAc(){
        AppManager.sharedManager.delegate=self
        AppManager.sharedManager.showActivityIndicatorInView(self.view, withLabel: "Loading...")
        let defaults = NSUserDefaults.standardUserDefaults()
        let firstname:String = defaults.valueForKey("firstName") as! String
        let lastname:String = defaults.valueForKey("lastName") as! String
        let fullname:String = "\(firstname) \(lastname)"
        let userType = defaults.valueForKey("usertype") as! NSInteger
        var createdId = NSString()
        if(userType == 1){
            let userId = defaults.valueForKey("id") as! String
            createdId = userId
        }else{
            let userId = defaults.valueForKey("staffId") as! String
            createdId = userId
        }
    let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let timeZone = NSTimeZone(name: "UTC")
        dateFormatter.timeZone=timeZone
        let dateAdd = dateFormatter.dateFromString(time as String)
        let sche = Double(scheduleMin as String)
        let toTime = dateAdd!.dateByAddingTimeInterval(sche!*(60))
        dateFormatter.dateFormat = "hh:mm a"
        let toStringTime = dateFormatter.stringFromDate(toTime)
        let drId = NSUserDefaults.standardUserDefaults().valueForKey("id") as! NSString
        let params = "DeviceToken=\(deviceTokn)&BookingId=\(book_id)&drschedulesetsId=\(schedule_Id)&Email=\(email_id)&DrName=\(fullname)&CancellationId=\(createdId)&CancellationName=\(fullname)&CancellationUserType=\(userType)&ScheduledDate=\(date)&Dr_id=\(drId)&From=\(time)&To=\(toStringTime)&PatientName=\(name)"
        print(params)
        videoBool = false
        AppManager.sharedManager.postDataOnserver(params, postUrl: "docter/DrBookingCancellation")
    }
    func failureRsponseError(failureError:NSError){
        AppManager.sharedManager.hideActivityIndicatorInView(self.view)
        AppManager.sharedManager.Showalert("", alertmessage: failureError.localizedDescription)
    }
    
    @IBAction func backBtnAc(sender: AnyObject) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
